# ---------------------------
# Setup Stage
# ---------------------------
FROM amazonlinux:2.0.20190508 as setup

# Set versions of dependencies to use.
ENV \
    WGET_VERSION="1.14" \
    TAR_VERSION="1.26" \
    XZ_VERSION="5.2.2" \
    MAN_DB_VERSION="2.6.3" \
    NODEJS_VERSION="8.14.0" \
    CHROMIUM_VERSION="73.0.3683.86" \
    NGINX_VERSION="1.12.2"
  
ENV PATH $PATH:/qma/webapp/node_modules/.bin:/opt/node-v${NODEJS_VERSION}-linux-x64/bin

# Install dependencies as ROOT.
USER root

#  install core dependencies
RUN \
    yum install -y \
        wget-${WGET_VERSION} \
        tar-${TAR_VERSION} \
        xz-${XZ_VERSION} \
        man-db-${MAN_DB_VERSION} \
        shadow-utils \
        bzip2 \
        git && \
    yum clean all && \
    amazon-linux-extras install nginx1.12=${NGINX_VERSION}

# Create the qma user, group and home directory.
RUN adduser --create-home --user-group qma

# Change ownership of qma user home directory.
WORKDIR /qma
RUN chown -R qma:qma /qma

# install nodejs
RUN \
    wget -O /tmp/nodejs.tar.xz https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.xz && \
    tar -xJf /tmp/nodejs.tar.xz -C /opt && \
    rm -rf /tmp/* && \
    chown -R qma:qma /opt/node-v${NODEJS_VERSION}-linux-x64
    
# install chromium for protractor tests
RUN \
    yum install -y \ 
        https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
        chromium-${CHROMIUM_VERSION} && \
    yum clean all
    
# install bower
RUN npm install -g bower

# Switch to the qma user.
USER qma

# Create and set application working directory
RUN mkdir /qma/webapp
WORKDIR /qma/webapp

# ---------------------------
# Develop Stage
# ---------------------------
FROM setup as develop

COPY ./.bowerrc /qma/webapp/.bowerrc
COPY ./.jshintrc /qma/webapp/.jshintrc
COPY ./bower.json /qma/webapp/bower.json
COPY ./gulpfile.js /qma/webapp/gulpfile.js
COPY ./karma.conf.js /qma/webapp/karma.conf.js
COPY ./package.json /qma/webapp/package.json
COPY ./protractor.conf.js /qma/webapp/protractor.conf.js

RUN bower install --config.interactive=false --allow-root
RUN npm install

# start app
CMD npm run serve

# ---------------------------
# Build Stage
# ---------------------------
FROM setup as build

COPY --chown=qma:qma . /qma/webapp

USER qma
WORKDIR /qma/webapp

RUN bower install --config.interactive=false --allow-root
RUN npm install

# test and build app
RUN npm run test \
    && npm run build

# ---------------------------
# Prod Stage
# ---------------------------
FROM build as prod

# Set up nginx configuration files.
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/favicon.ico /qma/webapp/dist/favicon.ico
COPY ./nginx/certs/nginx.crt /etc/nginx/nginx.crt
COPY ./nginx/certs/nginx.key /etc/nginx/nginx.key

USER root

# Give qma user necessary permissions to run nginx.
RUN \
    chown qma:qma /var/run && \
    chown -R qma:qma /var/log/nginx && \
    chown -R qma:qma /var/lib/nginx && \
    chown -R qma:qma /etc/nginx/conf.d && \
    chown -R qma:qma /etc/nginx/nginx.conf && \
    chown -R qma:qma /etc/nginx/nginx.crt && \
    chown -R qma:qma /etc/nginx/nginx.key

USER qma

# start app
CMD ["nginx"]