(function () {

  'use strict';

  angular
    .module('core')
    .factory('appVersionService', appVersionService);

  appVersionService.$inject = ['$http', 'coreApiPrefix'];

  function appVersionService($http, coreApiPrefix) {
    return {
      getAppVersion: getAppVersion
    };

    /**
     * Get a list of Environments from the server.
     *
     * @returns The result from the server containing the list of
     *          Environments.
     */
    function getAppVersion() {
      return $http.get(coreApiPrefix + 'appVersion')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }


  }

})();
