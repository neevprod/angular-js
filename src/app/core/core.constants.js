(function () {

  'use strict';

  angular
    .module('core')
    .constant('serverUrlPattern', /^\/api\//)
    .constant('coreApiPrefix', '/api/core/v1/')
    .constant('sessionCookieName', 'session')
    .constant('requestTimeout', 300000)
    .constant('downloadRequestTimeout', 600000)
    .constant('itemsPerPageOptions', [
      {name: '10', value: 10},
      {name: '25', value: 25},
      {name: '50', value: 50},
      {name: '100', value: 100},
      {name: '500', value: 500},
      {name: '1000', value: 1000},
      {name: '2500', value: 2500}
    ])
    .constant('dateTimeFormat', 'MM/DD/YYYY hh:mm:ss A')
    .constant('jobStatusCheckInterval', 3000);

})();
