(function () {

  'use strict';

  angular.module('core', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'toastr',
    'smart-table', 'angularMoment', 'angular-clipboard', 'btford.markdown', 'checklist-model'])
    .config(configure)
    .run(run);

  configure.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'stConfig',
    'toastrConfig', '$compileProvider'];

  function configure($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, stConfig, toastrConfig, $compileProvider) {
    $stateProvider
      .state('login', {
        url: '/',
        templateUrl: 'app/core/login/login.html',
        controller: 'LoginController as loginCtrl'
      })
      .state('main', {
        templateUrl: 'app/core/main/main.html',
        controller: 'MainController as mainCtrl'
      })
      .state('moduleSelection', {
        parent: 'main',
        url: '/moduleSelection',
        templateUrl: 'app/core/moduleSelection/moduleSelection.html',
        controller: 'ModuleSelectionController as moduleSelectionCtrl'
      });

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|data|blob):/);
    $httpProvider.interceptors.push('httpInterceptor');

    // Smart Table global configurations.
    stConfig.sort.skipNatural = true;
    stConfig.pagination.displayedPages = 4;
    stConfig.pagination.template = 'app/core/tables/pagination.html';

    // toastr global configurations.
    toastrConfig.preventOpenDuplicates = true;

    // Loading overlay configurations.
    jQuery.Loading.setDefaults({
      message: '<div class="spinner">\n' +
        '  <div class="rect1"></div>\n' +
        '  <div class="rect2"></div>\n' +
        '  <div class="rect3"></div>\n' +
        '  <div class="rect4"></div>\n' +
        '  <div class="rect5"></div>\n' +
        '  LOADING...' +
        '</div>'
    });
  }

  run.$inject = ['$rootScope', '$state', 'sessionService', 'permissionsService', 'toastr', '$uibModal'];

  function run($rootScope, $state, sessionService, permissionsService, toastr, $uibModal) {
    /**
     * Set the session timeout (this is in case a logged in user has refreshed the page).
     */
    sessionService.setSessionTimeout();

    /**
     * On state change, check whether the user has a valid session. If so, prevent them from
     * going to the login page. If not, keep them on the login page.
     */
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
      if (sessionService.hasActiveSession()) {
        if (toState.name === 'login') {
          event.preventDefault();
          $state.go(permissionsService.getLandingPage());
        }
      } else {
        if (toState.name !== 'login') {
          event.preventDefault();
          toastr.info('Please log in to continue.', 'No Active Session');
          sessionService.clearSession();
          $state.go('login');
        }
        if (toState.parent) {
          permissionsService.setItem('toState', toState);
          permissionsService.setItem('toParams', toParams);
        }
      }
    });

    /**
     * If there is an "unauthorized" message broadcast, switch to the login state and show an info message.
     */
    $rootScope.$on('unauthorized', function () {
      sessionService.clearSession();
      $state.go('login');
      toastr.info('Your session is no longer valid. Please log in again to continue.', 'Session Lost');
    });

    /**
     * If there is a "forbidden" message broadcast, show an error message.
     */
    $rootScope.$on('forbidden', function () {
      toastr.error('You do not have sufficient permission to perform this request.', 'Access Denied');
    });

    /**
     * If there is a "not_connected" message broadcast, show an error message.
     */
    $rootScope.$on('not_connected', function () {
      toastr.error('The connection with the server was lost, or the server is taking too long to respond.',
        'Connection Lost');
    });

    /**
     * If there is a "session_timeout" message broadcast, clear the session, switch to the
     * login state, and display a message.
     */
    $rootScope.$on('session_timeout', function () {
      sessionService.clearSession();
      $state.go('login');
      toastr.info('Your session has timed out. Please log in again to continue.', 'Session Timeout');
    });

    /**
     * If there is a "server_error" message broadcast, allow the user to fill out an issue report.
     */
    $rootScope.$on('server_error', function (event, errorId) {
      $uibModal.open({
        templateUrl: 'app/core/issueReport/issueReport.html',
        controller: 'IssueReportController as issueReportCtrl',
        resolve: {
          errorId: function () {
            return errorId;
          }
        },
        backdrop: 'static'
      });
    });
  }
})();
