'use strict';

describe('core module', function () {
  var $rootScope;
  var toastr;
  var landingPage;
  var sessionService;
  var permissionsService;
  var $state;
  var $httpBackend;

  beforeEach(module('qualityMonitoringApp'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(module(function ($provide) {
    var sessionService = jasmine.createSpyObj('sessionService', ['createSession', 'clearSession', 'hasActiveSession',
      'getUserEmail', 'getUserName', 'setSessionTimeout']);
    $provide.value('sessionService', sessionService);
    var permissionsService = jasmine.createSpyObj('permissionsService', ['getLandingPage', 'setItem', 'getItem']);
    $provide.value('permissionsService', permissionsService);
  }));

  beforeEach(inject(function (_$rootScope_, _toastr_, _sessionService_, _permissionsService_, _$state_,
                              _$httpBackend_) {
    $rootScope = _$rootScope_;
    toastr = _toastr_;
    spyOn(toastr, 'info');
    spyOn(toastr, 'error');
    sessionService = _sessionService_;
    landingPage = 'moduleSelection';
    permissionsService = _permissionsService_;
    $state = _$state_;
    $httpBackend = _$httpBackend_;
  }));

  it('should call sessionService.setSessionTimeout on startup', function () {
    expect(sessionService.setSessionTimeout).toHaveBeenCalled();
  });

  it('should ensure the user can only get to the login page if they are not logged in', function () {
    $httpBackend.expectGET('app/core/login/login.html').respond(200);
    sessionService.hasActiveSession.and.returnValue(false);

    $state.go('previewers');
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.flush();
    expect($state.current.name).toBe('login');
  });

  it('should display the correct message when a user without a session tries to go to a page other than login', function () {
    $httpBackend.expectGET('app/core/login/login.html').respond(200);
    sessionService.hasActiveSession.and.returnValue(false);

    $state.go('previewers');
    expect(toastr.info).toHaveBeenCalledWith('Please log in to continue.', 'No Active Session');
  });

  it('should clear the session when a user without a session tries to go to a page other than login', function () {
    $httpBackend.expectGET('app/core/login/login.html').respond(200);
    sessionService.hasActiveSession.and.returnValue(false);

    $state.go('previewers');
    expect(sessionService.clearSession).toHaveBeenCalled();
  });

  it('should ensure the user cannot get to the login page if they are logged in', function () {
    $httpBackend.expectGET('app/core/main/main.html').respond(200);
    $httpBackend.expectGET('app/core/moduleSelection/moduleSelection.html').respond(200);
    sessionService.hasActiveSession.and.returnValue(true);
    permissionsService.getLandingPage.and.returnValue(landingPage);

    $state.go('login');
    $rootScope.$digest();
    $httpBackend.flush();
    expect($state.current.name).toBe(landingPage);
  });

  it('should display the correct message when the "unauthorized" message is broadcast', function () {
    $rootScope.$broadcast('unauthorized');
    expect(toastr.info).toHaveBeenCalledWith('Your session is no longer valid. Please log in again to continue.',
      'Session Lost');
  });

  it('should tell sessionService to clear the session when the "unauthorized" message is broadcast', function () {
    $rootScope.$broadcast('unauthorized');
    expect(sessionService.clearSession).toHaveBeenCalled();
  });

  it('should redirect to the login page when the "unauthorized" message is broadcast', function () {
    $httpBackend.expectGET('app/core/login/login.html').respond(200);
    $rootScope.$broadcast('unauthorized');
    $rootScope.$digest();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.flush();
    expect($state.current.name).toBe('login');
  });

  it('should display the correct message when the "forbidden" message is broadcast', function () {
    $rootScope.$broadcast('forbidden');
    expect(toastr.error).toHaveBeenCalledWith('You do not have sufficient permission to perform this request.',
      'Access Denied');
  });

  it('should display the correct message when the "not_connected" message is broadcast', function () {
    $rootScope.$broadcast('not_connected');
    expect(toastr.error).toHaveBeenCalledWith('The connection with the server was lost, or the server is taking too long to respond.',
      'Connection Lost');
  });

  it('should display the correct message when the "session_timeout" message is broadcast', function () {
    $rootScope.$broadcast('session_timeout');
    expect(toastr.info).toHaveBeenCalledWith('Your session has timed out. Please log in again to continue.',
      'Session Timeout');
  });

  it('should tell sessionService to clear the session when the "session_timeout" message is broadcast', function () {
    $rootScope.$broadcast('session_timeout');
    expect(sessionService.clearSession).toHaveBeenCalled();
  });

  it('should redirect to the login page when the "session_timeout" message is broadcast', function () {
    $httpBackend.expectGET('app/core/login/login.html').respond(200);
    $rootScope.$broadcast('session_timeout');
    $rootScope.$digest();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.flush();
    expect($state.current.name).toBe('login');
  });
});
