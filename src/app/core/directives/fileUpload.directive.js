(function () {

  'use strict';

  angular.module('core').directive('qmFileUpload', fileUpload);

  /**
   * Directive for File Uploads.
   * Creates a hidden input element which can be triggered by any other element's onClick
   * function by binding it to the triggerSelect function in the directives scope.
   */
  function fileUpload() {
    return {
      scope: {
        processFile: '&',
        triggerSelect: '='
      },
      restrict: 'E',
      template:
        '<input id="file_upload" type="file" style="display:none"/>',
      link: function (scope, element) {
        var fileUploader = element.find('#file_upload');

        fileUploader.on('change',
          function () {
            if (fileUploader.prop('files') && fileUploader.prop('files')[0]) {
              var file = fileUploader.prop('files')[0];
              scope.processFile({file: file});
            }
          });

        scope.triggerSelect = function () {
          fileUploader.trigger('click');
        };
      }
    };
  }

})();
