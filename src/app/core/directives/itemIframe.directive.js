(function () {

  'use strict';

  angular.module('core')
    .directive('qmItemIframe', itemIframe);

  itemIframe.$inject = ['$window'];

  /**
   * Creates an iframe that shows a TestNav item with selected responses. A function assigned to the variable passed
   * to showItemFunction triggers the rendering of the item. The function takes in a showItem URL, an item
   * identifier, and a JSON string representing the responses to select.
   */
  function itemIframe($window) {
    return {
      scope: {
        showItemFunction: '='
      },
      restrict: 'E',
      template:
      '<form id="qmItemForm" action="" method="post" target="qmItemIframe" class="hidden">' +
      '   <input type="text" name="itemIdentifier" id="qmItemIdentifier" />' +
      '   <input type="text" name="responses" id="qmResponses" />' +
      '</form>' +
      '<iframe name="qmItemIframe" id="qmItemIframe" height="100%" width="100%"></iframe>',
      link: function (scope, element) {
        var iframe = element.find('#qmItemIframe');
        var itemIdentifierInput = element.find('#qmItemIdentifier');
        var responsesInput = element.find('#qmResponses');
        var form = element.find('#qmItemForm');

        function resizeIframe() {
          var height = angular.element('body').height() - iframe.offset().top -
            angular.element('#footer').height() - 10;
          if (height < 1000) {
            height = 1000;
          }
          iframe.height(height);
        }

        resizeIframe();

        angular.element($window).on('resize', function () {
          resizeIframe();
        });

        scope.showItemFunction = function (itemUrl, itemIdentifier, responses) {
          // Remove and re-append iframe to clear iframe history.
          iframe.remove();
          element.append(iframe);

          // Submit the hidden form to render the item.
          itemIdentifierInput.val(itemIdentifier);
          responsesInput.val(responses);
          form.attr('action', itemUrl);
          form.submit();
        };
      }
    };
  }

})();
