(function () {

  'use strict';

  angular.module('core')
    .directive('qmLoading', loading);

  loading.$inject = [];

  /**
   * When set to true, a loading overlay is shown on the element.
   */
  function loading() {
    return {
      scope: {
        qmLoading: '='
      },
      restrict: 'A',
      link: function (scope, element) {
        if (!element.attr('id')) {
          element.attr('id', 'loading_' + new Date().getTime());
        }

        scope.$watch('qmLoading', function (value) {
          if (value) {
            element.loading('resize');
            element.loading('start');
          } else {
            element.loading('stop');
          }
        });

        scope.$on('$destroy', function () {
          element.loading('stop');
          jQuery('#' + element.attr('id') + '_loading-overlay').remove();
        });
      }
    };
  }

})();
