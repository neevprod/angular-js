(function () {

  'use strict';

  angular.module('core')
    .directive('qmLoadingBreadcrumb', loadingBreadcrumb);

  loadingBreadcrumb.$inject = [];

  /**
   * When set to true, the element contents are replaced by a spinner.
   */
  function loadingBreadcrumb() {
    return {
      scope: {
        qmLoadingBreadcrumb: '='
      },
      restrict: 'A',
      transclude: true,
      template:
      '<span ng-if="!qmLoadingBreadcrumb">' +
      '   <ng-transclude></ng-transclude>' +
      '</span>' +
      '<span ng-if="qmLoadingBreadcrumb">' +
      '   <i class="fa fa-spinner fa-spin"></i>' +
      '</span>'
    };
  }

})();
