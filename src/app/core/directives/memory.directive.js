(function () {

  'use strict';

  angular.module('core')
    .directive('qmMemory', loading);

  loading.$inject = ['memoryService'];

  /**
   * When set to true, a loading overlay is shown on the element.
   */
  function loading(memoryService) {
    return {
      scope: {
        qmPageName: '@',
        qmProperty: '@',
        qmContext: '@',
        ngModel: '='
      },
      restrict: 'A',
      link: function (scope, element) {
        var initialValue = memoryService.getItem(scope.qmPageName, scope.qmProperty, scope.qmContext);
        if (initialValue) {
          scope.ngModel = initialValue;
          element[0].value = initialValue;
        }

        scope.$watch('ngModel', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            memoryService.setItem(scope.qmPageName, scope.qmProperty, scope.qmContext, newValue);
          }
        });
      }
    };
  }

})();
