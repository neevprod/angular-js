(function () {

  'use strict';

  angular.module('core')
    .directive('qmPickList', pickList);

  pickList.$inject = ['$filter'];

  /**
   * Creates a pick list for selecting multiple items.
   *
   * The customAdd and customRemove attributes are optional. These allow you to provide custom logic for the adding
   * and removing of items in the pick list.
   */
  function pickList($filter) {
    return {
      scope: {
        availableItems: '=',
        pickedItems: '=',
        displayAttribute: '@',
        collectionName: '@',
        selectHeight: '@',
        ngDisabled: '=',
        customAdd: '&',
        customRemove: '&'
      },
      restrict: 'E',
      template:
      '<div class="form-group margin-bottom clearfix">' +
      '<div class="col-xs-5 pick-list-section">' +
      '<label for="availableItems">Available {{::collectionName}}</label>' +
      '<select multiple id="availableItems" class="form-control" ' +
      'ng-model="selected.availableItems" ' +
      'ng-options="item[displayAttribute] for item in availableItems" ' +
      'ng-disabled="ngDisabled">' +
      '</select>' +
      '</div>' +
      '<div class="col-xs-2 button-controls pick-list-section">' +
      '<button type="button" class="btn btn-default btn-xs" ng-click="addAllToPicked()" ' +
      'ng-disabled="ngDisabled">' +
      '<i class="fa fa-angle-double-right"></i>' +
      '</button>' +
      '<button type="button" class="btn btn-default btn-xs" ng-click="addToPicked()" ' +
      'ng-disabled="ngDisabled">' +
      '<i class="fa fa-angle-right"></i>' +
      '</button>' +
      '<button type="button" class="btn btn-default btn-xs" ng-click="removeFromPicked()" ' +
      'ng-disabled="ngDisabled">' +
      '<i class="fa fa-angle-left"></i>' +
      '</button>' +
      '<button type="button" class="btn btn-default btn-xs" ng-click="removeAllFromPicked()" ' +
      'ng-disabled="ngDisabled">' +
      '<i class="fa fa-angle-double-left"></i>' +
      '</button>' +
      '</div>' +
      '<div class="col-xs-5 pick-list-section">' +
      '<label for="pickedItems">Selected {{::collectionName}}</label>' +
      '<select multiple id="pickedItems" class="form-control" ' +
      'ng-model="selected.pickedItems" ' +
      'ng-options="item[displayAttribute] for item in pickedItems" ' +
      'ng-disabled="ngDisabled">' +
      '</select>' +
      '</div>' +
      '</div>',
      link: function (scope, element, attrs) {
        scope.addToPicked = addToPicked;
        scope.removeFromPicked = removeFromPicked;
        scope.addAllToPicked = addAllToPicked;
        scope.removeAllFromPicked = removeAllFromPicked;

        scope.selected = {
          availableItems: [],
          pickedItems: []
        };

        scope.$watchCollection('availableItems', function () {
          scope.availableItems = $filter('orderBy')(scope.availableItems, scope.displayAttribute);
        });

        var selectElements = element.find('select');
        var buttonControls = element.find('.button-controls');
        var buttonControlsOffset = 0;
        if (scope.selectHeight) {
          if (scope.selectHeight >= 90) {
            selectElements.height(scope.selectHeight);
          } else {
            selectElements.height('90px');
          }
        }

        scope.$watch(function () {
          return selectElements.offset().top;
        }, function (value) {
          var centeringOffset = (selectElements.outerHeight() - buttonControls.outerHeight()) / 2;
          buttonControlsOffset = value - (buttonControls.offset().top - buttonControlsOffset) +
            centeringOffset + 1;
          buttonControls.css('top', buttonControlsOffset);
        });

        /**
         * Move the selected available items to the list of picked items.
         */
        function addToPicked() {
          if (!scope.pickedItems) {
            scope.pickedItems = [];
          }
          if (attrs.customAdd) {
            var promise = scope.customAdd({
              selectedItems: scope.selected.availableItems,
              availableItems: scope.availableItems, pickedItems: scope.pickedItems
            });
            promise.then(function (promiseData) {
              scope.availableItems = promiseData.availableItems;
              scope.pickedItems = promiseData.pickedItems;
            });

          } else {
            scope.pickedItems = scope.pickedItems.concat(scope.selected.availableItems);
            scope.availableItems = $filter('filter')(scope.availableItems, function (value) {
              return !isValueInList(value, scope.selected.availableItems);
            });
          }
          clearSelected();
        }

        /**
         * Move the selected picked items to the list of available items.
         */
        function removeFromPicked() {
          if (attrs.customRemove) {
            var result = scope.customRemove({
              selectedItems: scope.selected.pickedItems,
              availableItems: scope.availableItems, pickedItems: scope.pickedItems
            });
            scope.availableItems = result.availableItems;
            scope.pickedItems = result.pickedItems;
          } else {
            scope.availableItems = scope.availableItems.concat(scope.selected.pickedItems);
            scope.pickedItems = $filter('filter')(scope.pickedItems, function (value) {
              return !isValueInList(value, scope.selected.pickedItems);
            });
          }
          clearSelected();
        }

        /**
         * Move all the available items to the list of picked items.
         */
        function addAllToPicked() {
          if (!scope.pickedItems) {
            scope.pickedItems = [];
          }
          if (attrs.customAdd) {
            var promise = scope.customAdd({
              selectedItems: scope.availableItems,
              availableItems: scope.availableItems, pickedItems: scope.pickedItems
            });
            promise.then(function (promiseData) {
              scope.availableItems = promiseData.availableItems;
              scope.pickedItems = promiseData.pickedItems;
            });
          } else {
            scope.pickedItems = scope.pickedItems.concat(scope.availableItems);
            scope.availableItems = [];
          }
          clearSelected();
        }

        /**
         * Move all the picked items to the list of available items.
         */
        function removeAllFromPicked() {
          if (attrs.customRemove) {
            var result = scope.customRemove({
              selectedItems: scope.pickedItems,
              availableItems: scope.availableItems, pickedItems: scope.pickedItems
            });
            scope.availableItems = result.availableItems;
            scope.pickedItems = result.pickedItems;
          } else {
            scope.availableItems = scope.availableItems.concat(scope.pickedItems);
            scope.pickedItems = [];
          }
          clearSelected();
        }

        /**
         * Clear the selected items.
         */
        function clearSelected() {
          scope.selected.availableItems = [];
          scope.selected.pickedItems = [];
        }

        /**
         * Check whether the given value is in the given list.
         *
         * @param {object} value - The value to check.
         * @param {object[]} list - The list to check against.
         * @returns {boolean} Whether the value appears in the list.
         */
        function isValueInList(value, list) {
          for (var i = 0; i < list.length; i++) {
            if (value === list[i]) {
              return true;
            }
          }
          return false;
        }
      }
    };
  }

})();
