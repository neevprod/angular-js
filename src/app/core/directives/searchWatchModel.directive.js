(function () {

  'use strict';

  angular.module('core')
    .directive('qmCustomSearch', customSearch);

  customSearch.$inject = [];

  /**
   * This directive used for custom search for the dynamically adding search value in hidden input field.
   * @returns {{require: string, scope: {qmCustomSearchKey: string, qmCustomSearchField: string}, link: link}}
   */
  function customSearch() {
    return {
      require: '^stTable',
      scope: {
        qmCustomSearchKey: '=',
        qmCustomSearchField: '@'
      },
      link: function (scope, ele, attr, ctrl) {
        scope.$watch('qmCustomSearchKey', function (val) {
          ctrl.search(val, scope.qmCustomSearchField);
        });

      }
    };
  }
})();
