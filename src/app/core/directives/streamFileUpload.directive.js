(function () {

  'use strict';

  angular.module('core').directive('qmStreamFileUpload', qmStreamFileUpload);
  qmStreamFileUpload.$inject = [];

  function qmStreamFileUpload() {
    return {
      require: 'ngModel',
      link: function postLink(scope,elem,attrs,ngModel) {
        elem.on('change', function() {
          var files = elem[0].files;
          ngModel.$setViewValue(files);
        });
      }
    };
  }

})();
