(function () {

  'use strict';

  angular
    .module('core')
    .factory('httpInterceptor', httpInterceptor);

  httpInterceptor.$inject = ['$q', 'sessionService', '$rootScope', 'serverUrlPattern', '$cookies',
    'sessionCookieName', 'requestTimeout', 'downloadRequestTimeout'];

  function httpInterceptor($q, sessionService, $rootScope, serverUrlPattern, $cookies, sessionCookieName,
                           requestTimeout, downloadRequestTimeout) {
    return {
      request: request,
      responseError: responseError
    };

    /**
     * Adds necessary configurations to all HTTP requests.
     *
     * @param config
     * @returns the modified config
     */
    function request(config) {
      if ($cookies.get(sessionCookieName)) {
        config.headers['X-Auth-Token'] = $cookies.get(sessionCookieName);
      }
      // special timeout duration for downloads (note: URL for download must end with "Download")
      if (config.url.match(/Download$/)) {
        config.timeout = downloadRequestTimeout;
      } else {
        config.timeout = requestTimeout;
      }
      return config;
    }

    /**
     * Broadcasts messages for specific response errors.
     *
     * @param response
     * @returns the response as a rejected promise
     */
    function responseError(response) {
      if (serverUrlPattern.test(response.config.url)) {
        if (response.status === 401 && sessionService.hasActiveSession()) {
          $rootScope.$broadcast('unauthorized');
        } else if (response.status === 403) {
          $rootScope.$broadcast('forbidden');
        } else if (response.status === 500) {
          if (response.data && response.data.data && response.data.data.errorId) {
            $rootScope.$broadcast('server_error', response.data.data.errorId);
          } else {
            $rootScope.$broadcast('server_error');
          }
        } else if (response.status <= 0 || (response.status > 500 && response.status < 600)) {
          $rootScope.$broadcast('not_connected');
          response.data = {
            success: false,
            errors: [
              {
                message: ''
              }
            ]
          };
        }
      }
      return $q.reject(response);
    }
  }

})();
