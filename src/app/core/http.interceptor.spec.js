'use strict';

describe('http interceptor', function () {
  var httpInterceptor;
  var $rootScope;
  var sessionService;
  var $cookies;
  var sessionCookieName;
  var requestTimeout;
  var downloadRequestTimeout;

  beforeEach(module('core'));

  beforeEach(module(function ($provide) {
    var sessionService = jasmine.createSpyObj('sessionService', ['createSession', 'clearSession', 'hasActiveSession',
      'getUserEmail', 'getUserName', 'setSessionTimeout']);
    $provide.value('sessionService', sessionService);
  }));

  beforeEach(inject(function (_httpInterceptor_, _$rootScope_, _sessionService_, _$cookies_, _sessionCookieName_,
                              _requestTimeout_, _downloadRequestTimeout_) {
    httpInterceptor = _httpInterceptor_;
    $rootScope = _$rootScope_;
    sessionService = _sessionService_;
    $cookies = _$cookies_;
    sessionCookieName = _sessionCookieName_;
    requestTimeout = _requestTimeout_;
    downloadRequestTimeout = _downloadRequestTimeout_;
  }));

  afterEach(function () {
    $cookies.remove(sessionCookieName);
  });

  describe('request', function () {
    it('should set the timeout of outgoing requests to be the value of requestTimeout', function () {
      var config = {
        method: 'POST',
        url: '/api/token',
        headers: {}
      };
      httpInterceptor.request(config);
      expect(config.timeout).toBe(requestTimeout);
    });

    it('should set the timeout of outgoing download requests to be the value of downloadRequestTimeout', function () {
      var config = {
        method: 'POST',
        url: '/api/qtiValidation/v1/previewers/1/tenants/3/testCasesDownload',
        headers: {}
      };
      httpInterceptor.request(config);
      expect(config.timeout).toBe(downloadRequestTimeout);
    });

    it('should set the X-Auth-Token header with the session token if a session exists', function () {
      var token = 'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwLCJ1c2VySWQiOiIxIiwidXNlckVtYWlsIjoidGVzdGVyQHRlc3QuY29tIiwidXNlck5hbWUiOiJUZXN0ZXIgVGVzdGluZ3RvbiJ9.vvNFPyokiTiq6iFYvllNgo9Pr9Soel36mlNs70k5f3k';
      $cookies.put(sessionCookieName, token);
      var config = {
        method: 'POST',
        url: '/api/token',
        headers: {}
      };
      httpInterceptor.request(config);
      expect(config.headers['X-Auth-Token']).toBe(token);
    });

    it('should not set the X-Auth-Token header if a session does not exist', function () {
      var config = {
        method: 'POST',
        url: '/api/token',
        headers: {}
      };
      httpInterceptor.request(config);
      expect(config.headers['X-Auth-Token']).toBe(undefined);
    });
  });

  describe('responseError', function () {
    beforeEach(function () {
      spyOn($rootScope, '$broadcast');
    });

    it('should broadcast an "unauthorized" message when receiving a 401 response from the server when logged in', function () {
      sessionService.hasActiveSession.and.returnValue(true);
      var response = {
        status: 401,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('unauthorized');
    });

    it('should not broadcast any message when receiving a 401 response from the server when not logged in', function () {
      sessionService.hasActiveSession.and.returnValue(false);
      var response = {
        status: 401,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });

    it('should not broadcast an "unauthorized" message when the url of the response does not match that of the server', function () {
      sessionService.hasActiveSession.and.returnValue(true);
      var response = {
        status: 401,
        config: {
          url: '/login'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });

    it('should broadcast a "forbidden" message when receiving a 403 response from the server', function () {
      sessionService.hasActiveSession.and.returnValue(true);
      var response = {
        status: 403,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('forbidden');
    });

    it('should not broadcast a "forbidden" message when the url of the response does not match that of the server', function () {
      sessionService.hasActiveSession.and.returnValue(true);
      var response = {
        status: 403,
        config: {
          url: '/login'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });

    it('should broadcast a "server_error" message when receiving a 500 response from the server', function () {
      var response = {
        status: 500,
        config: {
          url: '/api/qtiValidation/previewers'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('server_error');
    });

    it('should broadcast a "server_error" message with the errorId when receiving a 500 response with an errorId from the server', function () {
      var errorId = 'ABC123';
      var response = {
        status: 500,
        config: {
          url: '/api/token'
        },
        data: {
          data: {
            errorId: errorId
          }
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('server_error', errorId);
    });

    it('should not broadcast a "server_error" message when the url of the response does not match that of the server', function () {
      var response = {
        status: 500,
        config: {
          url: '/login'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });

    it('should broadcast a "not_connected" message when a server response has a status of 0', function () {
      var response = {
        status: 0,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('not_connected');
    });

    it('should broadcast a "not_connected" message when a server response has a status > 500 and < 600', function () {
      var response = {
        status: 501,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('not_connected');
    });

    it('should broadcast a "not_connected" message when a server response has a status > 500 and < 600', function () {
      var response = {
        status: 599,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).toHaveBeenCalledWith('not_connected');
    });

    it('should not broadcast a "not_connected" message when a server response has a status < 500', function () {
      var response = {
        status: 499,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });

    it('should not broadcast a "not_connected" message when a server response has a status > 599', function () {
      var response = {
        status: 600,
        config: {
          url: '/api/token'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });

    it('should not broadcast a "not_connected" message when the url of the response does not match that of the server', function () {
      var response = {
        status: 0,
        config: {
          url: '/login'
        }
      };
      httpInterceptor.responseError(response);
      expect($rootScope.$broadcast).not.toHaveBeenCalled();
    });
  });
});
