(function () {

  'use strict';

  angular.module('core')
    .controller('IssueReportController', IssueReportController);

  IssueReportController.$inject = ['$uibModalInstance', 'issueReportService', 'sessionService', 'errorId'];

  function IssueReportController($uibModalInstance, issueReportService, sessionService, errorId) {
    var viewModel = this;

    viewModel.email = sessionService.getUserEmail();
    viewModel.description = '';
    viewModel.errorId = errorId;
    viewModel.reportSubmitted = false;
    viewModel.submitReport = submitReport;
    viewModel.close = close;

    /**
     * Submit the issue report using the issueReportService if the form is valid.
     *
     * @param isValid A boolean indicating whether the form is valid.
     */
    function submitReport(isValid) {
      if (isValid) {
        viewModel.reportSubmitted = true;
        issueReportService.reportIssue(viewModel.email, viewModel.description, viewModel.errorId);
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }
  }

})();
