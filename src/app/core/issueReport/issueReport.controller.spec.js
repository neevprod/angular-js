'use strict';

describe('issue report controller', function () {
  var controller;
  var $uibModalInstance;
  var errorId;
  var issueReportService;

  beforeEach(module('core'));

  beforeEach(inject(function ($controller) {
    errorId = 'ABC123';
    $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
    issueReportService = jasmine.createSpyObj('issueReportService', ['reportIssue']);
    controller = $controller('IssueReportController', {
      $uibModalInstance: $uibModalInstance,
      errorId: errorId, issueReportService: issueReportService
    });
  }));

  it('should use issueReportService to report the issue when the form is valid', function () {
    var email = 'tester@test.com';
    var description = 'The app broke.';
    controller.email = email;
    controller.description = description;

    controller.submitReport(true);

    expect(issueReportService.reportIssue).toHaveBeenCalledWith(email, description, errorId);
  });

  it('should not report the issue when the form is invalid', function () {
    var email = 'tester@test.com';
    var description = 'The app broke.';
    controller.email = email;
    controller.description = description;

    controller.submitReport(false);

    expect(issueReportService.reportIssue).not.toHaveBeenCalled();
  });

  it('should change submitted value to true after submitting report', function () {
    var email = 'tester@test.com';
    var description = 'The app broke.';
    controller.email = email;
    controller.description = description;

    expect(controller.reportSubmitted).toBe(false);

    controller.submitReport(true);

    expect(controller.reportSubmitted).toBe(true);
  });

  it('should not change submitted value to true when the report is not submitted', function () {
    var email = 'tester@test.com';
    var description = 'The app broke.';
    controller.email = email;
    controller.description = description;

    expect(controller.reportSubmitted).toBe(false);

    controller.submitReport(false);

    expect(controller.reportSubmitted).toBe(false);
  });
});
