(function () {

  'use strict';

  angular
    .module('core')
    .factory('issueReportService', issueReportService);

  issueReportService.$inject = ['$http', 'coreApiPrefix'];

  function issueReportService($http, coreApiPrefix) {
    return {
      reportIssue: reportIssue
    };

    /**
     * Submits an issue report to the server.
     *
     * @param email The email address of the person reporting the issue.
     * @param description The description of the issue.
     * @param errorId The ID of the exception in the server log.
     * @returns The result of the call to the server.
     */
    function reportIssue(email, description, errorId) {
      var data = {
        email: email,
        description: description,
        errorId: errorId
      };
      return $http.post(coreApiPrefix + 'issueReport', data)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
