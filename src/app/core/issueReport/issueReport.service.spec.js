'use strict';

describe('issue report service', function () {
  var issueReportService;
  var coreApiPrefix;
  var $httpBackend;

  beforeEach(module('core'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_issueReportService_, _coreApiPrefix_, _$httpBackend_) {
    issueReportService = _issueReportService_;
    coreApiPrefix = _coreApiPrefix_;
    $httpBackend = _$httpBackend_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should submit the issue report properly', function () {
    var email = 'tester@test.com';
    var description = 'The application broke.';
    var errorId = 'ABC123';

    $httpBackend.expectPOST(coreApiPrefix + 'issueReport', {email: email, description: description, errorId: errorId})
      .respond(200);

    issueReportService.reportIssue(email, description, errorId);

    $httpBackend.flush();
  });

  it('should return the response data when the call succeeds', function () {
    var email = 'tester@test.com';
    var description = 'The application broke.';
    var errorId = 'ABC123';

    $httpBackend.whenPOST(coreApiPrefix + 'issueReport', {email: email, description: description, errorId: errorId})
      .respond(200, {success: true});
    var handler = jasmine.createSpy('handler').and.callFake(function (result) {
      expect(result.success).toBe(true);
    });

    issueReportService.reportIssue(email, description, errorId).then(handler);

    $httpBackend.flush();
    expect(handler).toHaveBeenCalled();
  });

  it('should return the response data when the call fails', function () {
    var email = 'tester@test.com';
    var description = 'The application broke.';
    var errorId = 'ABC123';

    $httpBackend.whenPOST(coreApiPrefix + 'issueReport', {email: email, description: description, errorId: errorId})
      .respond(400, {success: false, errors: [{message: 'Error message.'}]});
    var handler = jasmine.createSpy('handler').and.callFake(function (result) {
      expect(result.success).toBe(false);
      expect(result.errors[0].message).toBe('Error message.');
    });

    issueReportService.reportIssue(email, description, errorId).then(handler);

    $httpBackend.flush();
    expect(handler).toHaveBeenCalled();
  });
});
