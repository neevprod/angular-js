(function () {

  'use strict';

  angular.module('core')
    .controller('JsonDetailsController', JsonDetailsController);

  JsonDetailsController.$inject = ['$uibModalInstance', 'title', 'json'];

  function JsonDetailsController($uibModalInstance, title, json) {
    var viewModel = this;

    viewModel.title = title;
    if (title === 'Item XML') {
      viewModel.json = json;
    }
    else {
      viewModel.json = JSON.stringify(JSON.parse(json), null, 2);
    }
    viewModel.confirmationTooltipEnabled = false;
    viewModel.close = close;
    viewModel.copySuccess = copySuccess;
    viewModel.copyError = copyError;
    viewModel.disableConfirmationTooltip = disableConfirmationTooltip;

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    /**
     * Show the tooltip to indicate copy success.
     */
    function copySuccess() {
      viewModel.confirmationTooltipText = 'Copy succeeded.';
      viewModel.confirmationTooltipEnabled = true;
    }

    /**
     * Show the tooltip to indicate copy error.
     */
    function copyError() {
      viewModel.confirmationTooltipText = 'Copy failed.';
      viewModel.confirmationTooltipEnabled = true;
    }

    /**
     * Disable the copy confirmation tooltip.
     */
    function disableConfirmationTooltip() {
      viewModel.confirmationTooltipEnabled = false;
    }

  }

})();
