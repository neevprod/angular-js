(function () {
  'use strict';

  angular.module('core').controller('LoginController', LoginController);

  LoginController.$inject = ['loginService', 'permissionsService', '$state'];

  function LoginController(loginService, permissionsService, $state) {
    var viewModel = this;
    viewModel.networkId = '';
    viewModel.password = '';
    viewModel.message = '';
    viewModel.login = login;
    viewModel.pendingLogin = false;

    /**
     * Log in using loginService and go to the previewers page if
     * successful.
     *
     * @param networkId
     *            The networkId entered by the user.
     * @param password
     *            The password entered by the user.
     * @param isValid
     *            A boolean indicating whether the form is valid.
     */
    function login(networkId, password, isValid) {
      viewModel.message = '';
      if (isValid) {
        var toState = permissionsService.getItem('toState');
        var toParams = permissionsService.getItem('toParams');
        viewModel.pendingLogin = true;
        loginService.login(networkId, password).then(function (result) {
          viewModel.pendingLogin = false;
          if (result.success) {
            if (toState) {
              $state.go(toState.name, toParams);
            } else {
              $state.go(permissionsService.getLandingPage());
            }
          } else {
            viewModel.message = result.errors[0].message;
          }
        });
      }
    }

  }
})();
