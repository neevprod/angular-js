'use strict';

describe('login controller', function () {
  var controller;
  var loginService;
  var landingPage;
  var permissionsService;
  var $state;
  var deferred;
  var $rootScope;

  var email;
  var password;

  beforeEach(module('core'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, $q, _$rootScope_) {
    loginService = jasmine.createSpyObj('loginService', ['login']);
    deferred = $q.defer();
    loginService.login.and.returnValue(deferred.promise);

    landingPage = 'landingPageStateName';
    permissionsService = jasmine.createSpyObj('permissionsService', ['getLandingPage', 'setItem', 'getItem']);
    permissionsService.getLandingPage.and.returnValue(landingPage);

    $state = jasmine.createSpyObj('$state', ['go']);
    $rootScope = _$rootScope_;
    controller = $controller('LoginController', {
      loginService: loginService,
      permissionsService: permissionsService, $state: $state
    });
  }));

  beforeEach(function () {
    email = 'tester@test.com';
    password = 'password';
  });

  it('should use loginService to log in when the form is valid', function () {
    controller.login(email, password, true);
    expect(loginService.login).toHaveBeenCalledWith(email, password);
  });

  it('should not call loginService when the form is not valid', function () {
    controller.login(email, password, false);
    expect(loginService.login).not.toHaveBeenCalledWith(email, password);
  });

  it('should set pendingLogin to false on initialization', function () {
    expect(controller.pendingLogin).toBe(false);
  });

  it('should update pendingLogin to true while waiting for the result from loginService', function () {
    controller.login(email, password, true);
    expect(controller.pendingLogin).toBe(true);

    deferred.resolve({success: false, errors: [{message: 'error'}]});
    $rootScope.$digest();
    expect(controller.pendingLogin).toBe(false);
  });

  it('should redirect to the correct landing page if the login is successful', function () {
    controller.login(email, password, true);

    deferred.resolve({success: true});
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith(landingPage);
  });

  it('should not redirect from the login page if the login is unsuccessful', function () {
    controller.login(email, password, true);

    deferred.resolve({success: false, errors: [{message: 'error'}]});
    $rootScope.$digest();
    expect($state.go).not.toHaveBeenCalled();
  });

  it('should set the error message if the login is unsuccessful', function () {
    controller.login(email, password, true);

    deferred.resolve({success: false, errors: [{message: 'Error message.'}]});
    $rootScope.$digest();
    expect(controller.message).toBe('Error message.');
  });
});
