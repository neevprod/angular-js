(function () {
  'use strict';

  angular
    .module('core')
    .factory('loginService', loginService);

  loginService.$inject = ['$http', 'sessionService', 'permissionsService', 'coreApiPrefix'];

  function loginService($http, sessionService, permissionsService, coreApiPrefix) {
    return {
      login: login
    };

    /**
     * Logs in to the server. If successful, tells sessionService to create
     * a new client side session.
     *
     * @param networkId
     *            The user's networkId.
     * @param password
     *            The user's password.
     * @returns The result from the server call.
     */
    function login(networkId, password) {
      var data = {
        networkId: networkId,
        password: password
      };
      return $http.post(coreApiPrefix + 'token', data)
        .then(function (result) {
          sessionService.createSession(result.data.data.token);
          permissionsService.setPermissions(result.data.data.userPermissions);
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
