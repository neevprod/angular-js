'use strict';

describe('login service', function () {
  var loginService;
  var $httpBackend;
  var sessionService;
  var permissionsService;
  var coreApiPrefix;

  var networkId;
  var password;
  var data;

  beforeEach(module('core'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(module(function ($provide) {
    var sessionService = jasmine.createSpyObj('sessionService', ['createSession', 'clearSession',
      'hasActiveSession', 'getUserEmail', 'getUserName', 'setSessionTimeout']);
    $provide.value('sessionService', sessionService);
    var permissionsService = jasmine.createSpyObj('permissionsService', ['setPermissions']);
    $provide.value('permissionsService', permissionsService);
  }));

  beforeEach(inject(function (_loginService_, _permissionsService_, _$httpBackend_, _sessionService_,
                              _coreApiPrefix_) {
    loginService = _loginService_;
    permissionsService = _permissionsService_;
    $httpBackend = _$httpBackend_;
    sessionService = _sessionService_;
    coreApiPrefix = _coreApiPrefix_;
  }));

  beforeEach(function () {
    networkId = 'tester';
    password = 'password';
    data = {
      token: 'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwLCJ1c2VySWQiOiIxIiwidXNlckVtYWlsIjoidGVzdGVyQHRlc3QuY29tIiwidXNlck5hbWUiOiJUZXN0ZXIgVGVzdGluZ3RvbiJ9.vvNFPyokiTiq6iFYvllNgo9Pr9Soel36mlNs70k5f3k',
      userPermissions: ['permission1', 'permission2', 'permission3']
    };
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should submit the login request properly', function () {
    $httpBackend.expectPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(200, {success: true, data: data});

    loginService.login(networkId, password);

    $httpBackend.flush();
  });

  it('should return the response data when the call succeeds', function () {
    $httpBackend.whenPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(200, {success: true, data: data});
    var handler = jasmine.createSpy('handler').and.callFake(function (result) {
      expect(result.success).toBe(true);
      expect(result.data).toEqual(data);
    });

    loginService.login(networkId, password).then(handler);

    $httpBackend.flush();
    expect(handler).toHaveBeenCalled();
  });

  it('should return the response data when the call fails', function () {
    $httpBackend.whenPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(400, {success: false, errors: [{message: 'Error message.'}]});

    var handler = jasmine.createSpy('handler').and.callFake(function (result) {
      expect(result.success).toBe(false);
      expect(result.errors[0].message).toBe('Error message.');
    });

    loginService.login(networkId, password).then(handler);

    $httpBackend.flush();
    expect(handler).toHaveBeenCalled();
  });

  it('should tell sessionService to create a session when the call succeeds', function () {
    $httpBackend.whenPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(200, {success: true, data: data});

    loginService.login(networkId, password);

    $httpBackend.flush();
    expect(sessionService.createSession).toHaveBeenCalledWith(data.token);
  });

  it('should not tell sessionService to create a session when the call fails', function () {
    $httpBackend.whenPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(400, {success: false, errors: [{message: 'Error message.'}]});

    loginService.login(networkId, password);

    $httpBackend.flush();
    expect(sessionService.createSession).not.toHaveBeenCalled();
  });

  it('should tell permissionsService to initialize when the call succeeds', function () {
    $httpBackend.whenPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(200, {success: true, data: data});

    loginService.login(networkId, password);

    $httpBackend.flush();
    expect(permissionsService.setPermissions).toHaveBeenCalledWith(data.userPermissions);
  });

  it('should not tell permissionsService to initialize when the call fails', function () {
    $httpBackend.whenPOST(coreApiPrefix + 'token', {networkId: networkId, password: password})
      .respond(400, {success: false, errors: [{message: 'Error message.'}]});

    loginService.login(networkId, password);

    $httpBackend.flush();
    expect(permissionsService.setPermissions).not.toHaveBeenCalled();
  });
});
