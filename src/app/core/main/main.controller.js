(function () {

  'use strict';

  angular.module('core')
    .controller('MainController', MainController);

  MainController.$inject = ['sessionService', 'permissionsService', 'qtiScvTenantExecutionService',
    'notificationsService', 'testCaseService', '$state', '$rootScope', '$timeout', '$interval', '$scope',
    '$uibModal', 'jobStatusCheckInterval', 'appVersionService'];

  function MainController(sessionService, permissionsService, qtiScvTenantExecutionService, notificationsService,
                          testCaseService, $state, $rootScope, $timeout, $interval, $scope, $uibModal,
                          jobStatusCheckInterval, appVersionService) {
    var viewModel = this;
    var notificationPopupTimeout;

    viewModel.user = sessionService.getUserName();
    viewModel.year = new Date().getFullYear();
    viewModel.landingPage = permissionsService.getLandingPage();
    viewModel.currentJobs = [];
    viewModel.currentDownloads = [];
    viewModel.notificationCount = 0;
    viewModel.pendingNotificationCount = 0;
    viewModel.completedNotificationCount = 0;
    viewModel.notificationPopupText = '';
    viewModel.notificationPopupIsOpen = false;
    viewModel.logout = logout;
    viewModel.removeJob = removeJob;
    viewModel.removeDownload = removeDownload;
    viewModel.updateNotificationCount = updateNotificationCount;
    getAppVersion();
    viewModel.showReleaseModal = showReleaseModal;

    /**
     * Logs the user out of the application.
     */
    function logout() {
      sessionService.clearSession();
      $state.go('login');
    }

    /**
     * Checks status of all current jobs in the notification dropdown.
     */
    function checkCurrentJobs() {
      for (var i = 0; i < viewModel.currentJobs.length; i++) {
        checkJob(viewModel.currentJobs[i]);
      }
    }

    /**
     * Checks the status of one of the jobs in the notification dropdown.
     *
     * @param {object} job - The job to check.
     */
    function checkJob(job) {
      if (job.isRunning) {
        if (job.type === 'qti_scv') {
          qtiScvTenantExecutionService.getQtiScvJobStatusForTenant(job.details.data.previewerId,
            job.details.data.tenantId)
            .then(function (result) {
              if (!result.data.isRunning) {
                job.isRunning = false;
                job.successful = result.data.jobSuccessful;

                var qtiScvJobData = result.data.jobData;
                if (qtiScvJobData) {
                  job.data = ['SCV Execution ID: ' + qtiScvJobData.histExecId];
                }

                var qtiScvJobErrors = result.data.jobErrors;
                if (qtiScvJobErrors) {
                  job.errors = [];
                  for (var i = 0; i < qtiScvJobErrors.length; i++) {
                    job.errors.push(qtiScvJobErrors[i].message);
                  }
                }

                notificationsService.notifyQtiScvJobComplete(job.details.data.previewerId,
                  job.details.data.tenantId);
                updateNotificationCount();
                showNotificationPopup(job.name, false, true);
              }
            });
        } else if (job.type === 'test_case_upload') {
          testCaseService.getTestCaseUploadStatus(job.details.data.previewerId, job.details.data.tenantId,
            job.id)
            .then(function (result) {
              if (!result.data.isRunning) {
                job.isRunning = false;
                job.successful = result.data.jobSuccessful;

                var uploadJobData = result.data.jobData;
                if (uploadJobData) {
                  job.data = [];
                  job.data.push('Test Case Uploads ID: ' + uploadJobData.testCaseUploadsId);
                  job.data.push('Items Count: ' + uploadJobData.itemsCount);
                  job.data.push('Test Cases Count: ' + uploadJobData.testCasesCount);
                  job.data.push('Failed Test Cases Count: ' + uploadJobData.failedTestCasesCount);
                  job.data.push('Mismatched Test Cases Count: ' +
                    uploadJobData.mismatchedTestCasesCount);
                }

                var uploadJobErrors = result.data.jobErrors;
                if (uploadJobErrors) {
                  job.errors = [];
                  for (var i = 0; i < uploadJobErrors.length; i++) {
                    job.errors.push(uploadJobErrors[i].message);
                  }
                }

                notificationsService.notifyTestCaseUploadJobComplete(job.details.data.previewerId,
                  job.details.data.tenantId);
                updateNotificationCount();
                showNotificationPopup(job.name, false, true);
              }
            });
        } else if (job.type === 'Bryce stuff') {


        }
      }
    }

    /**
     * Removes a job from the notifications list.
     *
     * @param {object} job - The job to remove.
     */
    function removeJob(job) {
      var index = viewModel.currentJobs.indexOf(job);
      if (index >= 0) {
        viewModel.currentJobs.splice(index, 1);
        updateNotificationCount();
      }
    }

    /**
     * Removes a download from the notifications list.
     *
     * @param {object} download - The download to remove.
     */
    function removeDownload(download) {
      if (download.csvUrl) {
        URL.revokeObjectURL(download.csvUrl);
      }
      var index = viewModel.currentDownloads.indexOf(download);
      if (index >= 0) {
        viewModel.currentDownloads.splice(index, 1);
        updateNotificationCount();
      }
    }

    /**
     * Updates the notification counter
     */
    function updateNotificationCount() {
      viewModel.notificationCount = viewModel.currentJobs.length + viewModel.currentDownloads.length;
      if (!viewModel.notificationCount) {
        viewModel.showingNotifications = false;
        viewModel.pendingNotificationCount = 0;
        viewModel.completedNotificationCount = 0;
      } else {
        var pending = 0;
        var completed = 0;

        for (var i = 0; i < viewModel.currentJobs.length; i++) {
          if (viewModel.currentJobs[i].isRunning) {
            pending++;
          } else {
            completed++;
          }
        }
        for (var j = 0; j < viewModel.currentDownloads.length; j++) {
          if (viewModel.currentDownloads[j].isRunning) {
            pending++;
          } else {
            completed++;
          }
        }

        viewModel.pendingNotificationCount = pending;
        viewModel.completedNotificationCount = completed;
      }
    }

    $rootScope.$on('new_job', function (event, jobType, jobId, jobName, jobDetails) {
      var newJob = true;
      for (var i = 0; i < viewModel.currentJobs.length; i++) {
        if (viewModel.currentJobs[i].id === jobId && viewModel.currentJobs[i].isRunning) {
          newJob = false;
        }
      }
      if (newJob) {
        var job = {
          type: jobType,
          id: jobId,
          name: jobName,
          isRunning: true,
          details: jobDetails,
          showData: false
        };
        viewModel.currentJobs.push(job);
        updateNotificationCount();
        showNotificationPopup(job.name, false, false);
      }
    });

    /**
     * Listens for new tenant-level downloads and updates notifications accordingly.
     */
    $rootScope.$on('new_download', function (event, downloadType, downloadId, downloadName, downloadDetails,
                                             downloadFileName) {
      var download = {
        type: downloadType,
        name: downloadName,
        id: downloadId,
        isRunning: true,
        details: downloadDetails,
        fileName: downloadFileName
      };
      viewModel.currentDownloads.push(download);
      updateNotificationCount();
      showNotificationPopup(download.name, true, false);
    });

    /**
     * Listens for new export downloads and updates notifications accordingly.
     */
    $rootScope.$on('new_export', function (event, downloadId, downloadName, downloadFileName) {
      var download = {
        name: downloadName,
        id: downloadId,
        isRunning: true,
        fileName: downloadFileName
      };
      viewModel.currentDownloads.push(download);
      updateNotificationCount();
      showNotificationPopup(download.name, true, false);
    });
    /**
     * Listens for completed tenant-level downloads and updates notifications accordingly.
     */
    $rootScope.$on('download_complete', function (event, downloadId, successful, data) {
      for (var i = 0; i < viewModel.currentDownloads.length; i++) {
        if (viewModel.currentDownloads[i].id === downloadId) {
          viewModel.currentDownloads[i].isRunning = false;
          viewModel.currentDownloads[i].successful = successful;
          if (successful) {
            viewModel.currentDownloads[i].error = data.errorItemIdentifiers;
            viewModel.currentDownloads[i].errorType = data.errorType;
            var csvData = new Blob([data.download], {type: 'text/csv;charset=utf-8'});
            viewModel.currentDownloads[i].csvUrl = URL.createObjectURL(csvData);
          }
          updateNotificationCount();
          showNotificationPopup(viewModel.currentDownloads[i].name, true, true);
        }
      }
    });

    /**
     * Checks current jobs for completion at a regular interval.
     */
    $interval(checkCurrentJobs, jobStatusCheckInterval);

    /**
     * Show a tooltip updating the user of the notification status.
     *
     * @param {string} itemName - The name of the item this tooltip is about.
     * @param {boolean} isDownload - Whether this item is a download or a job.
     * @param {boolean} isFinished - Whether this item is finished or starting.
     */
    function showNotificationPopup(itemName, isDownload, isFinished) {
      if (!viewModel.showingNotifications) {
        $timeout.cancel(notificationPopupTimeout);

        if (!isFinished) {
          if (isDownload) {
            viewModel.notificationPopupText = itemName + ' is being prepared.';
          } else {
            viewModel.notificationPopupText = itemName + ' is starting.';
          }
        } else {
          if (isDownload) {
            viewModel.notificationPopupText = itemName + ' is ready to download.';
          } else {
            viewModel.notificationPopupText = itemName + ' is completed.';
          }
        }

        viewModel.notificationPopupIsOpen = true;
        notificationPopupTimeout = $timeout(function () {
          viewModel.notificationPopupIsOpen = false;
        }, 3000);
      }
    }

    /**
     * If the user closes the notifications menu, hide the extra job data the next time it's opened.
     */
    $scope.$watch(function () {
      return viewModel.showingNotifications;
    }, function (value) {
      if (!value) {
        for (var i = 0; i < viewModel.currentJobs.length; i++) {
          viewModel.currentJobs[i].showData = false;
        }
      }
    });

    function getAppVersion() {
      return appVersionService.getAppVersion().then(function (result) {
        if (result.success) {
          viewModel.appVersion = result.data.appVersion;
        }
      });
    }

    function showReleaseModal() {
      $uibModal.open({
        templateUrl: 'app/core/releaseVersion/releaseVersion.html',
        controller: 'ReleaseVersionController as releaseVersionCtrl'
      });

    }

  }
})();
