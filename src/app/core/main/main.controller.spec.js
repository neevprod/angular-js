'use strict';

describe('main controller', function () {
  var controller;
  var sessionService;
  var permissionsService;
  var testCaseService;
  var qtiScvTenantExecutionService;
  var landingPage;
  var $state;

  beforeEach(module('core'));

  beforeEach(inject(function ($controller, _$rootScope_) {
    sessionService = jasmine.createSpyObj('sessionService', ['clearSession', 'getUserName']);
    sessionService.getUserName.and.returnValue('Tester Testington');
    landingPage = 'landingPageStateName';
    permissionsService = jasmine.createSpyObj('permissionsService', ['getLandingPage']);
    permissionsService.getLandingPage.and.callFake(function () {
      return landingPage;
    });
    testCaseService = jasmine.createSpyObj('testCaseService', ['getTestCaseUploadStatus']);
    qtiScvTenantExecutionService = jasmine.createSpyObj('qtiScvTenantExecutionService', ['getQtiScvJobStatusForTenant']);
    $state = jasmine.createSpyObj('$state', ['go']);
    controller = $controller('MainController', {
      sessionService: sessionService, testCaseService: testCaseService,
      permissionsService: permissionsService, $state: $state,
      qtiScvTenantExecutionService: qtiScvTenantExecutionService, $scope: _$rootScope_.$new()
    });
  }));

  it('should set user to the name of the logged in user on initialization', function () {
    // Then
    expect(controller.user).toBe('Tester Testington');
  });

  it('should set year to the current year on initialization', function () {
    // Given
    var year = new Date().getFullYear();

    // Then
    expect(controller.year).toBe(year);
  });

  it('should set landingPage to the correct landing page on initialization', function () {
    // Then
    expect(controller.landingPage).toBe(landingPage);
  });

  describe('logout', function () {
    it('should tell sessionService to clear the session', function () {
      // When
      controller.logout();

      // Then
      expect(sessionService.clearSession).toHaveBeenCalled();
    });

    it('should redirect to the login page', function () {
      // When
      controller.logout();

      // Then
      expect($state.go).toHaveBeenCalledWith('login');
    });
  });
});
