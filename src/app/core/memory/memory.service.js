(function () {

  'use strict';

  angular
    .module('core')
    .factory('memoryService', memoryService);

  memoryService.$inject = ['$window'];

  function memoryService($window) {
    return {
      setItem: setItem,
      getItem: getItem
    };

    /**
     * Set an item to remember.
     *
     * @param {string} pageName - The name of the page the item is on.
     * @param {string} property - The name of the item to remember.
     * @param {string} context - The context within which to remember the item.
     * @param {string} value - The value to remember.
     */
    function setItem(pageName, property, context, value) {
      var data = {
        value: value
      };

      if (context) {
        $window.localStorage.setItem('memory_' + pageName + '_' + property + '_' + context,
          JSON.stringify(data));
      } else {
        $window.localStorage.setItem('memory_' + pageName + '_' + property, JSON.stringify(data));
      }
    }

    /**
     * Get a remembered item.
     *
     * @param {string} pageName - The name of the page the item is on.
     * @param {string} property - The name of the remembered item.
     * @param {string} context - The context within which the item is remembered.
     * @returns {string} The value that is remembered.
     */
    function getItem(pageName, property, context) {
      var data;
      if (context) {
        data = $window.localStorage.getItem('memory_' + pageName + '_' + property + '_' + context);
      } else {
        data = $window.localStorage.getItem('memory_' + pageName + '_' + property);
      }

      if (data) {
        return JSON.parse(data).value;
      } else {
        return null;
      }
    }
  }

})();
