(function () {

  'use strict';

  angular.module('core')
    .controller('ModuleSelectionController', ModuleSelectionController);

  ModuleSelectionController.$inject = ['permissionsService'];

  function ModuleSelectionController(permissionsService) {
    var viewModel = this;

    viewModel.modules = [];

    activate();

    function activate() {
      var modules = permissionsService.getAllowedModules();

      if (modules.indexOf('qtiValidation') >= 0) {
        viewModel.modules.push({
          name: 'QTI Validation',
          icon: 'fa-file-code-o',
          state: 'previewers'
        });
      }
      if (modules.indexOf('systemValidation') >= 0) {
        viewModel.modules.push({
          name: 'System Validation',
          icon: 'fa-sitemap',
          state: 'environments'
        });
      }
      if (modules.indexOf('userManagement') >= 0) {
        viewModel.modules.push({
          name: 'User Management',
          icon: 'fa-users',
          state: 'users'
        });
      }
      if (modules.indexOf('gridAutomation') >= 0) {
        viewModel.modules.push({
          name: 'Grid Automation',
          icon: 'fa-barcode',
          state: 'gridJobHistory'
        });
      }
    }
  }

})();
