'use strict';

describe('module selection controller', function () {
  var controller;
  var $controller;
  var permissionsService;

  beforeEach(module('core'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_$controller_) {
    permissionsService = jasmine.createSpyObj('permissionsService', ['getAllowedModules']);
    $controller = _$controller_;
  }));

  it('should set modules to contain all modules if all modules are allowed', function () {
    // Given
    permissionsService.getAllowedModules.and.returnValue(['qtiValidation', 'systemValidation']);

    // When
    controller = $controller('ModuleSelectionController', {permissionsService: permissionsService});

    // Then
    expect(controller.modules).toEqual([{
      name: 'QTI Validation',
      icon: 'fa-file-code-o',
      state: 'previewers'
    }, {
      name: 'System Validation',
      icon: 'fa-sitemap',
      state: 'environments'
    }]);
  });

  it('should set modules to contain only qtiValidation if only that module is allowed', function () {
    // Given
    permissionsService.getAllowedModules.and.returnValue(['qtiValidation']);

    // When
    controller = $controller('ModuleSelectionController', {permissionsService: permissionsService});

    // Then
    expect(controller.modules).toEqual([{
      name: 'QTI Validation',
      icon: 'fa-file-code-o',
      state: 'previewers'
    }]);
  });

  it('should set modules to contain only systemValidation if only that module is allowed', function () {
    // Given
    permissionsService.getAllowedModules.and.returnValue(['systemValidation']);

    // When
    controller = $controller('ModuleSelectionController', {permissionsService: permissionsService});

    // Then
    expect(controller.modules).toEqual([{
      name: 'System Validation',
      icon: 'fa-sitemap',
      state: 'environments'
    }]);
  });

  it('should set modules to contain only gridAutomation if only that module is allowed', function () {
    // Given
    permissionsService.getAllowedModules.and.returnValue(['gridAutomation']);

    // When
    controller = $controller('ModuleSelectionController', {permissionsService: permissionsService});

    // Then
    expect(controller.modules).toEqual([{
      name: 'Grid Automation',
      icon: 'fa-barcode',
      state: 'gridJobHistory'
    }]);
  });

  it('should set modules to be empty if no modules are allowed', function () {
    // Given
    permissionsService.getAllowedModules.and.returnValue([]);

    // When
    controller = $controller('ModuleSelectionController', {permissionsService: permissionsService});

    // Then
    expect(controller.modules).toEqual([]);
  });
});
