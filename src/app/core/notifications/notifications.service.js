(function () {

  'use strict';

  angular
    .module('core')
    .factory('notificationsService', notificationsService);

  notificationsService.$inject = ['$rootScope'];

  function notificationsService($rootScope) {
    return {
      notifyNewQtiScvJob: notifyNewQtiScvJob,
      notifyQtiScvJobComplete: notifyQtiScvJobComplete,
      notifyNewDownload: notifyNewDownload,
      notifyDownloadComplete: notifyDownloadComplete,
      notifyNewTestCaseUploadJob: notifyNewTestCaseUploadJob,
      notifyTestCaseUploadJobComplete: notifyTestCaseUploadJobComplete,
      notifyNewExport: notifyNewExport,
      notifyNewGridJob: notifyNewGridJob,
      notifyGridJobComplete: notifyGridJobComplete
    };

    /**
     * Broadcast a message that a new QTI-SCV job has started.
     *
     * @param {string} jobId - The ID of the job.
     * @param {number} previewerId - The ID of the previewer.
     * @param {string} previewerName - The name of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     * @param {string} tenantName - The name of the tenant.
     */
    function notifyNewQtiScvJob(jobId, previewerId, previewerName, tenantId, tenantName) {
      var jobDetails = {
        data: {
          previewerId: previewerId,
          tenantId: tenantId
        },
        displayData: ['Previewer: ' + previewerName, 'Tenant: ' + tenantName]
      };
      notifyNewJob('qti_scv', jobId, 'Score Consistency Validation', jobDetails);
    }

    /**
     * Broadcast a message that a new test case upload job has started.
     *
     * @param {string} jobId - The ID of the job.
     * @param {number} previewerId - The ID of the previewer.
     * @param {string} previewerName - The name of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     * @param {string} tenantName - The name of the tenant.
     */
    function notifyNewTestCaseUploadJob(jobId, previewerId, previewerName, tenantId, tenantName) {
      var jobDetails = {
        data: {
          previewerId: previewerId,
          tenantId: tenantId
        },
        displayData: ['Previewer: ' + previewerName, 'Tenant: ' + tenantName]
      };
      notifyNewJob('test_case_upload', jobId, 'Test Case Upload', jobDetails);
    }

    /**
     * Broadcast a message that a QTI-SCV job has completed.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     */
    function notifyQtiScvJobComplete(previewerId, tenantId) {
      $rootScope.$broadcast('qti_scv_job_complete', previewerId, tenantId);
    }

    /**
     * Broadcast a message that a test case upload job has completed.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     */
    function notifyTestCaseUploadJobComplete(previewerId, tenantId) {
      $rootScope.$broadcast('test_case_upload_job_complete', previewerId, tenantId);
    }

    /**
     * Broadcast a message that a new download has started.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @param {string} previewerName - The name of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     * @param {string} tenantName The name of the tenant.
     * @param {number} downloadId - The ID of the download.
     * @param {string} level - The level of the download.
     * @param {string} downloadType - The type of the download.
     */
    function notifyNewDownload(previewerId, previewerName, tenantId, tenantName, downloadId, level, downloadType) {
      var downloadName;
      var downloadFileName;
      if (downloadType === 'itemResponsePool') {
        downloadName = 'Item Response Pool';
        downloadFileName = 'item_response_pool.csv';
      } else if (downloadType === 'testCaseCoverage') {
        downloadName = 'Test Case Coverage';
        downloadFileName = 'test_case_coverage.csv';
      } else if (downloadType === 'identifier') {
        downloadName = 'Item identifiers';
        downloadFileName = 'item_identifiers_' + previewerName + '_' + tenantName + '.csv';
      }
      downloadName += ' (' + level + ')';

      var downloadDetails = {
        data: {
          previewerId: previewerId,
          tenantId: tenantId
        },
        displayData: ['Previewer: ' + previewerName, 'Tenant: ' + tenantName]
      };

      $rootScope.$broadcast('new_download', downloadType, downloadId, downloadName, downloadDetails,
        downloadFileName);
    }

    /**
     * Broadcast a message that a new Export has started.
     *
     * @param {number} downloadId - The ID of the download.
     */
    function notifyNewExport(downloadId) {
      var downloadName = 'Export Failed Scenarios';
      var downloadFileName = 'failed_scenarios.csv';
      $rootScope.$broadcast('new_export', downloadId, downloadName, downloadFileName);
    }


    /**
     * Broadcast a message that a new grid automation job has started.
     * WHAT DO i CHANGE HERE?! !@#$%^&*()
     * do I need to add 'extra' back-end code?
     * @param {number} previewerId - The ID of the previewer.
     * @param {string} previewerName - The name of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     * @param {string} tenantName The name of the tenant.
     * @param {number} downloadId - The ID of the download.
     * @param {string} level - The level of the download.
     * @param {string} downloadType - The type of the download.
     */
    function notifyNewGridJob(previewerId, previewerName, tenantId, tenantName, downloadId, level, downloadType) {
      var downloadName;
      var downloadFileName;
      if (downloadType === 'itemResponsePool') {
        downloadName = 'Item Response Pool';
        downloadFileName = 'item_response_pool.csv';
      } else if (downloadType === 'testCaseCoverage') {
        downloadName = 'Test Case Coverage';
        downloadFileName = 'test_case_coverage.csv';
      } else if (downloadType === 'identifier') {
        downloadName = 'Item identifiers';
        downloadFileName = 'item_identifiers_' + previewerName + '_' + tenantName + '.csv';
      }
      downloadName += ' (' + level + ')';

      var downloadDetails = {
        data: {
          previewerId: previewerId,
          tenantId: tenantId
        },
        displayData: ['Previewer: ' + previewerName, 'Tenant: ' + tenantName]
      };

      $rootScope.$broadcast('new_download', downloadType, downloadId, downloadName, downloadDetails,
        downloadFileName);
    }

    /**
     * Broadcast a message that a grid automation job has completed.
     * WHAT DO i CHANGE HERE?! !@#$%^&*()
     * do I need to add 'extra' back-end code?
     * @param {number} previewerId - The ID of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     */
    function notifyGridJobComplete(previewerId, tenantId) {
      $rootScope.$broadcast('qti_scv_job_complete', previewerId, tenantId);
    }

    /**
     * Broadcast a message that a download has completed.
     *
     * @param {number} downloadId - The ID of the download.
     * @param {boolean} success - Whether the download was successful.
     * @param {string} data - The download data.
     */
    function notifyDownloadComplete(downloadId, success, data) {
      $rootScope.$broadcast('download_complete', downloadId, success, data);
    }

    /**
     * Broadcast a message that a new job has started.
     *
     * @param {string} jobType - The type of job.
     * @param {string} jobId - The ID of the job.
     * @param {string} jobName - The name of the job.
     * @param {string[]} jobDetails - A list of details about the job.
     */
    function notifyNewJob(jobType, jobId, jobName, jobDetails) {
      $rootScope.$broadcast('new_job', jobType, jobId, jobName, jobDetails);
    }

  }

})();
