(function () {

  'use strict';

  angular
    .module('core')
    .factory('permissionsService', permissionsService);

  permissionsService.$inject = ['$window'];

  function permissionsService($window) {
    var userPermissions;
    var isInitialized;

    return {
      setPermissions: setPermissions,
      getAllModules: getAllModules,
      getAllowedModules: getAllowedModules,
      getLandingPage: getLandingPage,
      isAdmin: isAdmin,
      hasModuleLevelPermission: hasModuleLevelPermission,
      hasTenantLevelPermission: hasTenantLevelPermission,
      hasAnyTenantLevelPermission: hasAnyTenantLevelPermission,
      hasEnvironmentLevelPermission: hasEnvironmentLevelPermission,
      hasAnyEnvironmentLevelPermission: hasAnyEnvironmentLevelPermission,
      setItem: setItem,
      getItem: getItem

    };

    /**
     * Initialize the permissions service by telling it what permissions the user has.
     *
     * @param permissions Which permissions the user has, if they are not an admin user.
     */
    function setPermissions(permissions) {
      $window.localStorage.setItem('permissions', JSON.stringify(permissions));
      userPermissions = permissions;
      isInitialized = true;
    }

    /**
     * If the userPermissions variable is not initialized, initialize it from local storage.
     */
    function reinitializeIfNecessary() {
      if (!isInitialized) {
        userPermissions = JSON.parse($window.localStorage.getItem('permissions'));
        isInitialized = true;
      }
    }

    /**
     * Get the list of modules in the application.
     *
     * @returns {Array} The list of modules in the application.
     */
    function getAllModules() {
      return ['qtiValidation', 'systemValidation', 'userManagement', 'gridAutomation'];
    }

    /**
     * Get the list of modules the user has access to.
     *
     * @returns {Array} The list of modules the user has access to.
     */
    function getAllowedModules() {
      reinitializeIfNecessary();

      var allModules = getAllModules();
      var allowedModules = [];

      if (!userPermissions.admin) {
        for (var i = 0; i < userPermissions.modulePermissions.length; i++) {
          var module = userPermissions.modulePermissions[i].moduleName;
          if (allModules.indexOf(module) >= 0) {
            allowedModules.push(module);
          }
        }
      } else {
        allowedModules = allModules;
      }

      return allowedModules;
    }

    /**
     * Get the landing page the user should see, based on which modules they have access to.
     */
    function getLandingPage() {
      var modules = getAllowedModules();
      if (modules.length !== 1) {
        return 'moduleSelection';
      } else {
        if (modules[0] === 'qtiValidation') {
          return 'previewers';
        } else if (modules[0] === 'systemValidation'){
        	return 'environments';
        } else if (modules[0] === 'gridAutomation'){
            return 'gridJobHistory';
        } 
      }
    }

    /**
     * Find out whether the user is an admin user.
     *
     * @returns {boolean} Whether the user is an admin user.
     */
    function isAdmin() {
      reinitializeIfNecessary();
      return !!userPermissions.admin;
    }

    /**
     * Find out whether the user has the given module level permission.
     *
     * @param {string} permissionName - The feature name.
     * @param {string} accessType - The access type.
     * @param {string} moduleName - The module name.
     * @returns {boolean} Whether the user has permission.
     */
    function hasModuleLevelPermission(permissionName, accessType, moduleName) {
      reinitializeIfNecessary();

      if (userPermissions.admin) {
        return true;
      } else {
        for (var i = 0; i < userPermissions.modulePermissions.length; i++) {
          if (userPermissions.modulePermissions[i].moduleName === moduleName &&
            userPermissions.modulePermissions[i].permissionType === 'ModuleLevelPermission') {
            var modulePermissions = userPermissions.modulePermissions[i].permissions;
            for (var j = 0; j < modulePermissions.length; j++) {
              if (modulePermissions[j].name === permissionName) {
                if (modulePermissions[j][accessType]) {
                  return true;
                } else {
                  break;
                }
              }
            }
          }
        }

        return false;
      }
    }

    /**
     * Find out whether the user has the given tenant level permission.
     *
     * @param {string} permissionName - The feature name.
     * @param {string} accessType - The access type.
     * @param {string} moduleName - The module name.
     * @param {number} previewerId - The previewer ID.
     * @param {number} tenantId - The tenant ID.
     * @returns {boolean} Whether the user has permission.
     */
    function hasTenantLevelPermission(permissionName, accessType, moduleName, previewerId, tenantId) {
      reinitializeIfNecessary();

      if (userPermissions.admin) {
        return true;
      } else {
        for (var i = 0; i < userPermissions.modulePermissions.length; i++) {
          if (userPermissions.modulePermissions[i].moduleName === moduleName &&
            userPermissions.modulePermissions[i].permissionType === 'TenantLevelPermission') {
            var previewersList = userPermissions.modulePermissions[i].previewerPermissions;

            for (var j = 0; j < previewersList.length; j++) {
              if (previewersList[j].previewerId === previewerId) {
                var tenantsList = previewersList[j].tenantPermissions;

                for (var k = 0; k < tenantsList.length; k++) {
                  if (tenantsList[k].tenantId === tenantId || tenantsList[k].tenantId === '*') {
                    var tenantPermissions = tenantsList[k].permissions;

                    for (var l = 0; l < tenantPermissions.length; l++) {
                      if (tenantPermissions[l].name === permissionName) {
                        if (tenantPermissions[l][accessType]) {
                          return true;
                        } else {
                          break;
                        }
                      }
                    }
                  }
                }

                return false;
              }
            }
          }
        }

        return false;
      }
    }

    /**
     * Find out whether the user has the given tenant level permission for any tenant.
     *
     * @param {string} permissionName - The feature name.
     * @param {string} accessType - The access type.
     * @param {string} moduleName - The module name.
     * @param {number} previewerId - The previewer ID.
     * @returns {boolean} Whether the user has permission.
     */
    function hasAnyTenantLevelPermission(permissionName, accessType, moduleName, previewerId) {
      reinitializeIfNecessary();

      if (userPermissions.admin) {
        return true;
      } else {
        for (var i = 0; i < userPermissions.modulePermissions.length; i++) {
          if (userPermissions.modulePermissions[i].moduleName === moduleName &&
            userPermissions.modulePermissions[i].permissionType === 'TenantLevelPermission') {
            var previewersList = userPermissions.modulePermissions[i].previewerPermissions;

            for (var j = 0; j < previewersList.length; j++) {
              if (previewersList[j].previewerId === previewerId) {
                var tenantsList = previewersList[j].tenantPermissions;

                for (var k = 0; k < tenantsList.length; k++) {
                  var tenantPermissions = tenantsList[k].permissions;

                  for (var l = 0; l < tenantPermissions.length; l++) {
                    if (tenantPermissions[l].name === permissionName) {
                      if (tenantPermissions[l][accessType]) {
                        return true;
                      } else {
                        break;
                      }
                    }
                  }
                }

                return false;
              }
            }
          }
        }

        return false;
      }
    }

    /**
     * Find out whether the user has the given environment level permission.
     *
     * @param {string} permissionName - The feature name.
     * @param {string} accessType - The access type.
     * @param {string} moduleName - The module name.
     * @param {number} environmentId - The environment ID.
     * @returns {boolean} Whether the user has permission.
     */
    function hasEnvironmentLevelPermission(permissionName, accessType, moduleName, environmentId) {
      reinitializeIfNecessary();

      if (userPermissions.admin) {
        return true;
      } else {
        for (var i = 0; i < userPermissions.modulePermissions.length; i++) {
          if (userPermissions.modulePermissions[i].moduleName === moduleName &&
            userPermissions.modulePermissions[i].permissionType === 'EnvironmentLevelPermission') {
            var environmentsList = userPermissions.modulePermissions[i].environmentPermissions;

            for (var j = 0; j < environmentsList.length; j++) {
              if (environmentsList[j].environmentId === environmentId ||
                environmentsList[j].environmentId === '*') {
                var environmentPermissions = environmentsList[j].permissions;

                for (var k = 0; k < environmentPermissions.length; k++) {
                  if (environmentPermissions[k].name === permissionName) {
                    if (environmentPermissions[k][accessType]) {
                      return true;
                    } else {
                      break;
                    }
                  }
                }
              }
            }

            return false;
          }
        }

        return false;
      }
    }

    /**
     * Find out whether the user has the given environment level permission for any environment.
     *
     * @param {string} permissionName - The feature name.
     * @param {string} accessType - The access type.
     * @param {string} moduleName - The module name.
     * @returns {boolean} Whether the user has permission.
     */
    function hasAnyEnvironmentLevelPermission(permissionName, accessType, moduleName) {
      reinitializeIfNecessary();

      if (userPermissions.admin) {
        return true;
      } else {
        for (var i = 0; i < userPermissions.modulePermissions.length; i++) {
          if (userPermissions.modulePermissions[i].moduleName === moduleName &&
            userPermissions.modulePermissions[i].permissionType === 'EnvironmentLevelPermission') {
            var environmentsList = userPermissions.modulePermissions[i].environmentPermissions;

            for (var j = 0; j < environmentsList.length; j++) {
              var environmentPermissions = environmentsList[j].permissions;

              for (var k = 0; k < environmentPermissions.length; k++) {
                if (environmentPermissions[k].name === permissionName) {
                  if (environmentPermissions[k][accessType]) {
                    return true;
                  } else {
                    break;
                  }
                }
              }
            }

            return false;
          }
        }

        return false;
      }
    }


    function setItem(key, value) {
      $window.localStorage.setItem(key, JSON.stringify(value));
    }

    function getItem(key) {
      return JSON.parse($window.localStorage.getItem(key));
    }

  }

})();
