'use strict';

describe('permissions service', function () {
  var permissionsService;
  var $window;

  var permissions;

  beforeEach(module('core'));

  beforeEach(inject(function (_permissionsService_, _$window_) {
    permissionsService = _permissionsService_;
    $window = _$window_;
  }));

  beforeEach(function () {
    $window.localStorage.clear();
  });

  describe('setPermissions', function () {
    it('should store the permissions in localStorage', function () {
      // Given
      spyOn($window.localStorage, 'setItem');
      permissions = {
        'admin': true
      };

      // When
      permissionsService.setPermissions(permissions);

      // Then
      expect($window.localStorage.setItem).toHaveBeenCalledWith('permissions', JSON.stringify(permissions));
    });
  });

  describe('isAdmin', function () {
    it('should return true if the user is an admin user', function () {
      // Given
      permissions = {
        'admin': true
      };
      permissionsService.setPermissions(permissions);

      // When
      var returnValue = permissionsService.isAdmin();

      // Then
      expect(returnValue).toBe(true);
    });

    it('should return false if the user is not an admin user', function () {
      // Given
      permissions = {
        'admin': false
      };
      permissionsService.setPermissions(permissions);

      // When
      var returnValue = permissionsService.isAdmin();

      // Then
      expect(returnValue).toBe(false);
    });

    it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
      // Given
      permissions = {
        'admin': true
      };
      spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
        if (name === 'permissions') {
          return JSON.stringify(permissions);
        }
      });

      // When
      var returnValue = permissionsService.isAdmin();

      // Then
      expect(returnValue).toBe(true);
    });
  });

  describe('hasModuleLevelPermission', function () {
    beforeEach(function () {
      permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'ModuleLevelPermission',
          'moduleName': 'moduleLevelModule',
          'permissions': [{
            'name': 'feature1',
            'createAccess': true,
            'readAccess': false,
            'updateAccess': false,
            'deleteAccess': false
          }, {
            'name': 'feature2',
            'createAccess': false,
            'readAccess': true,
            'updateAccess': false,
            'deleteAccess': false
          }, {
            'name': 'feature3',
            'createAccess': false,
            'readAccess': false,
            'updateAccess': true,
            'deleteAccess': false
          }, {
            'name': 'feature4',
            'createAccess': false,
            'readAccess': false,
            'updateAccess': false,
            'deleteAccess': true
          }]
        }]
      };
    });

    it('should return true when checking create access if the user has create access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature1', 'createAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true when checking read access if the user has read access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature2', 'readAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true when checking update access if the user has update access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature3', 'updateAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true when checking delete access if the user has delete access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature4', 'deleteAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if the user is an admin user', function () {
      // Given
      permissions = {
        'admin': true
      };
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature1', 'deleteAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return false if the user does not have access to the requested feature', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature1', 'deleteAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if the feature is not present in the permissions', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature10', 'createAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if the wrong module is given', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature1', 'createAccess', 'wrongModule');

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
      // Given
      spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
        if (name === 'permissions') {
          return JSON.stringify(permissions);
        }
      });

      // When
      var hasPermission =
        permissionsService.hasModuleLevelPermission('feature1', 'createAccess', 'moduleLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });
  });

  describe('hasTenantLevelPermission', function () {
    beforeEach(function () {
      permissions = {
        'admin': false,
        'modulePermissions': [
          {
            'permissionType': 'TenantLevelPermission',
            'moduleName': 'qtiValidation',
            'previewerPermissions': [
              {
                'previewerId': 1,
                'tenantPermissions': [
                  {
                    'tenantId': 3,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 6,
                    'permissions': [
                      {
                        'name': 'feature0',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      },
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': true,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 2,
                'tenantPermissions': [
                  {
                    'tenantId': 4,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': true,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 5,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': true
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 3,
                'tenantPermissions': [
                  {
                    'tenantId': '*',
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': true,
                        'updateAccess': true,
                        'deleteAccess': true
                      }
                    ]
                  },
                  {
                    'tenantId': 7,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 4,
                'tenantPermissions': [
                  {
                    'tenantId': '*',
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 7,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': true,
                        'updateAccess': true,
                        'deleteAccess': true
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 5,
                'tenantPermissions': [
                  {
                    'tenantId': '*',
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 7,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      };
    });

    describe('- create access -', function () {
      it('should return true if the user has create access', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the create access is set to false', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1, 6);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'wrongModule', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature2', 'createAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 9, 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1, 9);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return true if create access is set to true in a wildcard tenant but not the actual tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 3, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if create access is set to true in a wildcard tenant but the actual tenant is not present', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 3, 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if create access is set to true in a the actual tenant but not in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 4, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if create access is set to false both in a the actual tenant and in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 5, 7);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(true);
      });
    });

    describe('- read access -', function () {
      it('should return true if the user has read access', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1, 6);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the read access is set to false', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'wrongModule', 1, 6);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature2', 'readAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 9, 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1, 9);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return true if read access is set to true in a wildcard tenant but not the actual tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 3, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if read access is set to true in a wildcard tenant but the actual tenant is not present', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 3, 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if read access is set to true in a the actual tenant but not in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 4, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if read access is set to false both in a the actual tenant and in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 5, 7);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1, 6);

        // Then
        expect(hasPermission).toBe(true);
      });
    });

    describe('- update access -', function () {
      it('should return true if the user has update access', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 2, 4);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the update access is set to false', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'wrongModule', 2, 4);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature2', 'updateAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 9, 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 1, 9);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return true if update access is set to true in a wildcard tenant but not the actual tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 3, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if update access is set to true in a wildcard tenant but the actual tenant is not present', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 3, 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if update access is set to true in a the actual tenant but not in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 4, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if update access is set to false both in a the actual tenant and in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 5, 7);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 2, 4);

        // Then
        expect(hasPermission).toBe(true);
      });
    });

    describe('- delete access -', function () {
      it('should return true if the user has delete access', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 2, 5);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the delete access is set to false', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'wrongModule', 2, 5);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature2', 'deleteAccess', 'qtiValidation', 1, 3);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 9, 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 1, 9);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return true if delete access is set to true in a wildcard tenant but not the actual tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 3, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if delete access is set to true in a wildcard tenant but the actual tenant is not present', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 3, 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if delete access is set to true in a the actual tenant but not in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 4, 7);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if delete access is set to false both in a the actual tenant and in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 5, 7);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 2, 5);

        // Then
        expect(hasPermission).toBe(true);
      });
    });
  });

  describe('hasAnyTenantLevelPermission', function () {
    beforeEach(function () {
      permissions = {
        'admin': false,
        'modulePermissions': [
          {
            'permissionType': 'TenantLevelPermission',
            'moduleName': 'qtiValidation',
            'previewerPermissions': [
              {
                'previewerId': 1,
                'tenantPermissions': [
                  {
                    'tenantId': 3,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': true,
                        'updateAccess': true,
                        'deleteAccess': true
                      }
                    ]
                  },
                  {
                    'tenantId': 6,
                    'permissions': [
                      {
                        'name': 'feature0',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      },
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 2,
                'tenantPermissions': [
                  {
                    'tenantId': 2,
                    'permissions': [
                      {
                        'name': 'feature0',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      },
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': true,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 4,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': true,
                        'updateAccess': true,
                        'deleteAccess': true
                      }
                    ]
                  },
                  {
                    'tenantId': 5,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 3,
                'tenantPermissions': [
                  {
                    'tenantId': 2,
                    'permissions': [
                      {
                        'name': 'feature0',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      },
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': true,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 4,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 5,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': true,
                        'updateAccess': true,
                        'deleteAccess': true
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 4,
                'tenantPermissions': [
                  {
                    'tenantId': 3,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  },
                  {
                    'tenantId': 6,
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': false,
                        'readAccess': false,
                        'updateAccess': false,
                        'deleteAccess': false
                      }
                    ]
                  }
                ]
              },
              {
                'previewerId': 5,
                'tenantPermissions': [
                  {
                    'tenantId': '*',
                    'permissions': [
                      {
                        'name': 'feature1',
                        'createAccess': true,
                        'readAccess': true,
                        'updateAccess': true,
                        'deleteAccess': true
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      };
    });

    describe('- create access -', function () {
      it('should return true if the user has create access in the first tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has create access in a middle tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 2);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has create access in the last tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has create access in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 5);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the create access is set to false in all of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'wrongModule', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of any of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature2', 'createAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 10);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'createAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });
    });

    describe('- read access -', function () {
      it('should return true if the user has read access in the first tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has read access in a middle tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 2);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has read access in the last tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has read access in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 5);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the read access is set to false in all of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'wrongModule', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of any of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature2', 'readAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 10);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'readAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });
    });

    describe('- update access -', function () {
      it('should return true if the user has update access in the first tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has update access in a middle tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 2);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has update access in the last tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has update access in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 5);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the update access is set to false in all of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'wrongModule', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of any of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature2', 'updateAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 10);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'updateAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });
    });

    describe('- delete access -', function () {
      it('should return true if the user has delete access in the first tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has delete access in a middle tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 2);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has delete access in the last tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 3);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user has delete access in a wildcard tenant', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 5);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return true if the user is an admin', function () {
        // Given
        permissions = {
          'admin': true
        };
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(true);
      });

      it('should return false if the delete access is set to false in all of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 4);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the wrong module is given', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'wrongModule', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if the feature is not present in the permissions of any of the tenants', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature2', 'deleteAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should return false if permissions are not present for the previewer', function () {
        // Given
        permissionsService.setPermissions(permissions);

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 10);

        // Then
        expect(hasPermission).toBe(false);
      });

      it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
        // Given
        spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
          if (name === 'permissions') {
            return JSON.stringify(permissions);
          }
        });

        // When
        var hasPermission =
          permissionsService.hasAnyTenantLevelPermission('feature1', 'deleteAccess', 'qtiValidation', 1);

        // Then
        expect(hasPermission).toBe(true);
      });
    });
  });

  describe('hasEnvironmentLevelPermission', function () {
    beforeEach(function () {
      permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'EnvironmentLevelPermission',
          'moduleName': 'environmentLevelModule',
          'environmentPermissions': [{
            'environmentId': 1,
            'permissions': [{
              'name': 'feature1',
              'createAccess': true,
              'readAccess': false,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'feature2',
              'createAccess': false,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'feature3',
              'createAccess': false,
              'readAccess': false,
              'updateAccess': true,
              'deleteAccess': false
            }, {
              'name': 'feature4',
              'createAccess': false,
              'readAccess': false,
              'updateAccess': false,
              'deleteAccess': true
            }]
          }, {
            'environmentId': 2,
            'permissions': [{
              'name': 'feature21',
              'createAccess': false,
              'readAccess': false,
              'updateAccess': false,
              'deleteAccess': true
            }, {
              'name': 'feature22',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': true,
              'deleteAccess': false
            }]
          }, {
            'environmentId': '*',
            'permissions': [{
              'name': 'feature20',
              'createAccess': false,
              'readAccess': false,
              'updateAccess': false,
              'deleteAccess': true
            }, {
              'name': 'feature22',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': true,
              'deleteAccess': false
            }]
          }]
        }]
      };
    });

    it('should return true when checking create access if the user has create access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature1', 'createAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true when checking read access if the user has read access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasEnvironmentLevelPermission('feature2', 'readAccess', 'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true when checking update access if the user has update access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature3', 'updateAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true when checking delete access if the user has delete access', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature4', 'deleteAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if the user is an admin user', function () {
      // Given
      permissions = {
        'admin': true
      };
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature1', 'deleteAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return false if the user does not have access to the requested feature', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature1', 'deleteAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if the feature is not present in the permissions', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature10', 'createAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if the wrong module is given', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasEnvironmentLevelPermission('feature1', 'createAccess', 'wrongModule', 1);

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if permissions are not present for the environment', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature1', 'deleteAccess',
        'environmentLevelModule', 10);

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return true if access is set to true in a wildcard environment but not the actual environment', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature20', 'deleteAccess',
        'environmentLevelModule', 2);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if access is set to true in a wildcard environment but the actual environment is not present', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature20', 'deleteAccess',
        'environmentLevelModule', 10);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if access is set to true in the actual environment but not in a wildcard environment', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature21', 'deleteAccess',
        'environmentLevelModule', 2);

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return false if access is set to false both in a the actual environment and in a wildcard environment', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature22', 'deleteAccess',
        'environmentLevelModule', 2);

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
      // Given
      spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
        if (name === 'permissions') {
          return JSON.stringify(permissions);
        }
      });

      // When
      var hasPermission = permissionsService.hasEnvironmentLevelPermission('feature1', 'createAccess',
        'environmentLevelModule', 1);

      // Then
      expect(hasPermission).toBe(true);
    });
  });

  describe('hasAnyEnvironmentLevelPermission', function () {
    beforeEach(function () {
      permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'EnvironmentLevelPermission',
          'moduleName': 'environmentLevelModule',
          'environmentPermissions': [{
            'environmentId': 1,
            'permissions': [{
              'name': 'feature1',
              'createAccess': true,
              'readAccess': false,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'feature5',
              'createAccess': false,
              'readAccess': true,
              'updateAccess': true,
              'deleteAccess': true
            }]
          }, {
            'environmentId': 2,
            'permissions': [{
              'name': 'feature2',
              'createAccess': false,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'feature5',
              'createAccess': false,
              'readAccess': true,
              'updateAccess': true,
              'deleteAccess': true
            }]
          }, {
            'environmentId': 3,
            'permissions': [{
              'name': 'feature3',
              'createAccess': false,
              'readAccess': false,
              'updateAccess': true,
              'deleteAccess': false
            }, {
              'name': 'feature5',
              'createAccess': false,
              'readAccess': true,
              'updateAccess': true,
              'deleteAccess': true
            }]
          }, {
            'environmentId': '*',
            'permissions': [{
              'name': 'feature4',
              'createAccess': false,
              'readAccess': false,
              'updateAccess': false,
              'deleteAccess': true
            }, {
              'name': 'feature5',
              'createAccess': false,
              'readAccess': true,
              'updateAccess': true,
              'deleteAccess': true
            }]
          }]
        }]
      };
    });

    it('should return true if the user has access in the first tenant', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature1', 'createAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if the user has access in a middle tenant', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasAnyEnvironmentLevelPermission('feature2', 'readAccess', 'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if the user has access in the last tenant', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature3', 'updateAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if the user has access in a wildcard tenant', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature4', 'deleteAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return true if the user is an admin', function () {
      // Given
      permissions = {
        'admin': true
      };
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature1', 'deleteAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });

    it('should return false if access is set to false in all of the tenants', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature5', 'createAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if the wrong module is given', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission =
        permissionsService.hasAnyEnvironmentLevelPermission('feature1', 'createAccess', 'wrongModule');

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should return false if the feature is not present in the permissions of any of the environments', function () {
      // Given
      permissionsService.setPermissions(permissions);

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature10', 'createAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(false);
    });

    it('should still function correctly if it is not initialized but the permissions are in localStorage', function () {
      // Given
      spyOn($window.localStorage, 'getItem').and.callFake(function (name) {
        if (name === 'permissions') {
          return JSON.stringify(permissions);
        }
      });

      // When
      var hasPermission = permissionsService.hasAnyEnvironmentLevelPermission('feature1', 'createAccess',
        'environmentLevelModule');

      // Then
      expect(hasPermission).toBe(true);
    });
  });

  describe('getAllModules', function () {
    it('should return the list of all modules that have a user interface', function () {
      // Given
      var modules = ['qtiValidation', 'systemValidation', 'userManagement', 'gridAutomation'];

      // When
      var returnedModules = permissionsService.getAllModules();

      // Then
      expect(returnedModules).toEqual(modules);
    });
  });

  describe('getAllowedModules', function () {
    it('should return the list of modules defined in the user permissions', function () {
      // Given
      var permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'EnvironmentLevelPermission',
          'moduleName': 'systemValidation',
          'environmentPermissions': [{
            'environmentId': 1,
            'permissions': [{
              'name': 'divJobExecutions',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'divJob',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }]
          }]
        }]
      };
      permissionsService.setPermissions(permissions);

      // When
      var returnedModules = permissionsService.getAllowedModules();

      // Then
      expect(returnedModules).toEqual(['systemValidation']);
    });

    it('should return the list of all modules if the user has admin permissions', function () {
      // Given
      var permissions = {
        'admin': true
      };
      permissionsService.setPermissions(permissions);

      // When
      var returnedModules = permissionsService.getAllowedModules();

      // Then
      expect(returnedModules).toEqual(permissionsService.getAllModules());
    });

    it('should not return modules that are not defined in the client side app', function () {
      // Given
      var permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'TenantLevelPermission',
          'moduleName': 'qtiValidation',
          'previewerPermissions': [{
            'previewerId': 1,
            'tenantPermissions': [{
              'tenantId': 1,
              'permissions': [{
                'name': 'items',
                'createAccess': false,
                'readAccess': false,
                'updateAccess': false,
                'deleteAccess': false
              }]
            }]
          }]
        }, {
          'permissionType': 'ModuleLevelPermission',
          'moduleName': 'qtiRespGen',
          'permissions': [{
            'name': 'responses',
            'createAccess': false,
            'readAccess': true,
            'updateAccess': false,
            'deleteAccess': false
          }]
        }, {
          'permissionType': 'EnvironmentLevelPermission',
          'moduleName': 'systemValidation',
          'environmentPermissions': [{
            'environmentId': 1,
            'permissions': [{
              'name': 'divJob',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'divJobExecutions',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }]
          }]
        }]
      };
      permissionsService.setPermissions(permissions);


      // When
      var returnedModules = permissionsService.getAllowedModules();

      // Then
      expect(returnedModules).toEqual(['qtiValidation', 'systemValidation']);
    });
  });

  describe('getLandingPage', function () {
    it('should return moduleSelection if the user has access to more than one module', function () {
      // Given
      var permissions = {
        'admin': true
      };
      permissionsService.setPermissions(permissions);

      // When
      var landingPage = permissionsService.getLandingPage();

      // Then
      expect(landingPage).toBe('moduleSelection');
    });

    it('should return moduleSelection if the user does not have access to any modules', function () {
      // Given
      var permissions = {
        'admin': false,
        'modulePermissions': []
      };
      permissionsService.setPermissions(permissions);

      // When
      var landingPage = permissionsService.getLandingPage();

      // Then
      expect(landingPage).toBe('moduleSelection');
    });

    it('should return previewers if the user only has access to qtiValidation', function () {
      // Given
      var permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'TenantLevelPermission',
          'moduleName': 'qtiValidation',
          'previewerPermissions': [{
            'previewerId': 3,
            'tenantPermissions': [{
              'tenantId': 71,
              'permissions': [{
                'name': 'items',
                'createAccess': false,
                'readAccess': true,
                'updateAccess': false,
                'deleteAccess': false
              }, {
                'name': 'qtiScvExecutions',
                'createAccess': false,
                'readAccess': true,
                'updateAccess': false,
                'deleteAccess': false
              }, {
                'name': 'qtiScvTenantJobSubscriptions',
                'createAccess': true,
                'readAccess': false,
                'updateAccess': false,
                'deleteAccess': false
              }]
            }]
          }]
        }]
      };
      permissionsService.setPermissions(permissions);

      // When
      var landingPage = permissionsService.getLandingPage();

      // Then
      expect(landingPage).toBe('previewers');
    });

    it('should return environments if the user only has access to systemValidation', function () {
      // Given
      var permissions = {
        'admin': false,
        'modulePermissions': [{
          'permissionType': 'EnvironmentLevelPermission',
          'moduleName': 'systemValidation',
          'environmentPermissions': [{
            'environmentId': 1,
            'permissions': [{
              'name': 'divJobExecutions',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }, {
              'name': 'divJob',
              'createAccess': true,
              'readAccess': true,
              'updateAccess': false,
              'deleteAccess': false
            }]
          }]
        }]
      };
      permissionsService.setPermissions(permissions);

      // When
      var landingPage = permissionsService.getLandingPage();

      // Then
      expect(landingPage).toBe('environments');
    });
  });
});
