(function () {

  'use strict';

  angular.module('core').controller('ReleaseVersionController', ReleaseVersionController);

  ReleaseVersionController.$inject = ['$uibModalInstance'];

  function ReleaseVersionController($uibModalInstance) {
    var viewModel = this;
    viewModel.close = close;

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }


  }

})();
