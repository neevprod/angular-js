(function () {

  'use strict';

  angular
    .module('core')
    .factory('sessionService', sessionService);

  sessionService.$inject = ['$cookies', '$timeout', '$rootScope', 'sessionCookieName', '$window'];

  function sessionService($cookies, $timeout, $rootScope, sessionCookieName, $window) {
    var sessionTimeoutPromise;

    return {
      createSession: createSession,
      clearSession: clearSession,
      hasActiveSession: hasActiveSession,
      getUserEmail: getUserEmail,
      getUserName: getUserName,
      setSessionTimeout: setSessionTimeout
    };

    /**
     * Initialize a session. The purpose of this is to monitor the status of the server-side session
     * so that we can respond accordingly on the client side.
     *
     * @param token The Json Web Token that contains the session information.
     */
    function createSession(token) {
      clearSession();

      var session = jwt_decode(token); // jshint ignore:line
      $cookies.put(sessionCookieName, token);
      $cookies.put('sessionExpirationTime', new Date().getTime() / 1000 + (session.exp - session.iat));
      $cookies.put('lastActiveServerTime', session.iat);
      setSessionTimeout();
    }

    /**
     * Clear the session.
     */
    function clearSession() {
      if (sessionTimeoutPromise) {
        $timeout.cancel(sessionTimeoutPromise);
      }
      $cookies.remove(sessionCookieName);
      $cookies.remove('sessionExpirationTime');
      $cookies.remove('lastActiveServerTime');
      $window.localStorage.clear();
    }

    /**
     * Use this to find out whether there is an active session.
     *
     * @returns {boolean}
     */
    function hasActiveSession() {
      if ($cookies.get(sessionCookieName)) {
        return true;
      } else {
        return false;
      }
    }

    /**
     * Get the email address of the logged in user.
     *
     * @returns {string} The email address of the user.
     */
    function getUserEmail() {
      if ($cookies.get(sessionCookieName)) {
        var userEmail = jwt_decode($cookies.get(sessionCookieName)).userEmail; // jshint ignore:line
        if (userEmail) {
          return userEmail;
        } else {
          return '';
        }
      } else {
        return '';
      }
    }

    /**
     * Get the name of the logged in user.
     *
     * @returns {string} The name of the user.
     */
    function getUserName() {
      if ($cookies.get(sessionCookieName)) {
        var userName = jwt_decode($cookies.get(sessionCookieName)).userName; // jshint ignore:line
        if (userName) {
          return userName;
        } else {
          return '';
        }
      } else {
        return '';
      }
    }

    /**
     * Start a timeout that broadcasts a message when the session expires. This has no effect if
     * there is no active session.
     */
    function setSessionTimeout() {
      if (hasActiveSession()) {
        if (sessionTimeoutPromise) {
          $timeout.cancel(sessionTimeoutPromise);
        }
        var duration = Number($cookies.get('sessionExpirationTime')) * 1000 - new Date().getTime();
        if (duration < 0) {
          duration = 0;
        }
        sessionTimeoutPromise = $timeout(handleSessionTimeout, duration);
      }
    }

    /**
     * If the last active server time is the same as the stored time, this function broadcasts a message
     * indicating that the session has timed out. If it is not the same, it resets the session expiration
     * accordingly.
     */
    function handleSessionTimeout() {
      var session = jwt_decode($cookies.get(sessionCookieName)); // jshint ignore:line
      var lastActive = session.iat;
      if (lastActive === Number($cookies.get('lastActiveServerTime'))) {
        $rootScope.$broadcast('session_timeout');
      } else {
        var difference = lastActive - Number($cookies.get('lastActiveServerTime'));
        $cookies.put('sessionExpirationTime', Number($cookies.get('sessionExpirationTime')) + difference);
        $cookies.put('lastActiveServerTime', lastActive);
        setSessionTimeout();
      }
    }
  }

})();
