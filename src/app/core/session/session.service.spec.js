'use strict';

describe('session service', function () {
  var sessionService;
  var $cookies;
  var $timeout;
  var $rootScope;
  var sessionCookieName;
  var $window;

  var token;
  var userEmail;
  var userName;
  var sessionTimeout;
  var loginTime;

  beforeEach(module('core'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_sessionService_, _$cookies_, _$timeout_, _$rootScope_,
                              _sessionCookieName_, _$window_) {
    sessionService = _sessionService_;
    $cookies = _$cookies_;
    $timeout = _$timeout_;
    $rootScope = _$rootScope_;
    sessionCookieName = _sessionCookieName_;
    $window = _$window_;
    $window.localStorage.clear();
  }));

  beforeEach(function () {
    token = 'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwLCJ1c2VySWQiOiIxIiwidXNlckVtYWlsIjoidGVzdGVyQHRlc3QuY29tIiwidXNlck5hbWUiOiJUZXN0ZXIgVGVzdGluZ3RvbiJ9.vvNFPyokiTiq6iFYvllNgo9Pr9Soel36mlNs70k5f3k';
    userEmail = 'tester@test.com';
    userName = 'Tester Testington';
    sessionTimeout = 10;
    loginTime = 12340;

    jasmine.clock().install();
  });

  afterEach(function () {
    jasmine.clock().uninstall();
    $cookies.remove(sessionCookieName);
    $cookies.remove('sessionExpirationTime');
    $cookies.remove('lastActiveServerTime');
  });

  describe('createSession', function () {
    it('should clear local storage before creating the new session', function () {
      // Given
      $window.localStorage.setItem('testKey', 'testValue');

      // When
      sessionService.createSession(token);

      // Then
      expect($window.localStorage.getItem('testKey')).toBe(null);
    });

    it('should store the necessary session information', function () {
      jasmine.clock().mockDate();
      sessionService.createSession(token);

      expect($cookies.get(sessionCookieName)).toBe(token);
      expect(Number($cookies.get('sessionExpirationTime'))).toBe(new Date().getTime() / 1000 + sessionTimeout);
      expect(Number($cookies.get('lastActiveServerTime'))).toBe(loginTime);
    });

    it('should set the session timeout', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      jasmine.clock().mockDate(new Date(12345000));
      spyOn($rootScope, '$broadcast').and.returnValue({preventDefault: true});

      sessionService.createSession(token);

      $timeout.flush();
      expect($rootScope.$broadcast).toHaveBeenCalledWith('session_timeout');
    });
  });

  describe('clearSession', function () {
    it('should remove all session information', function () {
      spyOn($cookies, 'remove');
      spyOn($window.localStorage, 'clear');

      sessionService.clearSession();

      expect($cookies.remove).toHaveBeenCalledWith(sessionCookieName);
      expect($cookies.remove).toHaveBeenCalledWith('sessionExpirationTime');
      expect($cookies.remove).toHaveBeenCalledWith('lastActiveServerTime');
      expect($window.localStorage.clear).toHaveBeenCalled();
    });

    it('should cancel the session timeout if one exists', function () {
      $cookies.put('sessionExpirationTime', '123456');
      $cookies.put(sessionCookieName, token);

      sessionService.setSessionTimeout();

      spyOn($timeout, 'cancel');
      sessionService.clearSession();
      expect($timeout.cancel).toHaveBeenCalled();
    });

    it('should not cancel the session timeout if one does not exist', function () {
      spyOn($timeout, 'cancel');
      sessionService.clearSession();
      expect($timeout.cancel).not.toHaveBeenCalled();
    });
  });

  describe('hasActiveSession', function () {
    it('should return true if the session cookie exists', function () {
      $cookies.put(sessionCookieName, token);
      expect(sessionService.hasActiveSession()).toBe(true);
    });

    it('should return false if the session cookie does not exist', function () {
      expect(sessionService.hasActiveSession()).toBe(false);
    });
  });

  describe('getUserEmail', function () {
    it('should return the user email if the userEmail value is set', function () {
      $cookies.put(sessionCookieName, token);
      expect(sessionService.getUserEmail()).toBe(userEmail);
    });

    it('should return an empty string if the userEmail value is not set', function () {
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwfQ.L4GH8Pqs40nsTCYYruYJJ5VUntf_7at5CRqjiTZqdOc');
      expect(sessionService.getUserEmail()).toBe('');
    });

    it('should return an empty string if the session cookie does not exist', function () {
      expect(sessionService.getUserEmail()).toBe('');
    });
  });

  describe('getUserName', function () {
    it('should return the name of the user if the userName value is set', function () {
      $cookies.put(sessionCookieName, token);
      expect(sessionService.getUserName()).toBe(userName);
    });

    it('should return an empty string if the userName value is not set', function () {
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwfQ.L4GH8Pqs40nsTCYYruYJJ5VUntf_7at5CRqjiTZqdOc');
      expect(sessionService.getUserName()).toBe('');
    });

    it('should return an empty string if the session cookie does not exist', function () {
      expect(sessionService.getUserName()).toBe('');
    });
  });

  describe('setSessionTimeout', function () {
    it('should cancel the existing timeout if there is one', function () {
      $cookies.put('sessionExpirationTime', '123456');
      $cookies.put(sessionCookieName, token);

      sessionService.setSessionTimeout();

      spyOn($timeout, 'cancel');
      sessionService.setSessionTimeout();
      expect($timeout.cancel).toHaveBeenCalled();
    });

    it('should broadcast the session timeout message if the last active time of the Play session is still the same', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwfQ.L4GH8Pqs40nsTCYYruYJJ5VUntf_7at5CRqjiTZqdOc');

      jasmine.clock().mockDate(new Date(12345000));
      spyOn($rootScope, '$broadcast').and.returnValue({preventDefault: true});
      sessionService.setSessionTimeout();
      $timeout.flush();
      expect($rootScope.$broadcast).toHaveBeenCalledWith('session_timeout');
    });

    it('should not broadcast the session timeout message before the expiration time', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwfQ.L4GH8Pqs40nsTCYYruYJJ5VUntf_7at5CRqjiTZqdOc');

      jasmine.clock().mockDate(new Date(12340000));
      spyOn($rootScope, '$broadcast');
      sessionService.setSessionTimeout();
      $timeout.flush(9999);
      expect($rootScope.$broadcast).not.toHaveBeenCalledWith('session_timeout');
    });

    it('should broadcast the session timeout message even if the current time is past the session expiration time', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQwLCJleHAiOjEyMzUwfQ.L4GH8Pqs40nsTCYYruYJJ5VUntf_7at5CRqjiTZqdOc');

      jasmine.clock().mockDate(new Date(12355000));
      spyOn($rootScope, '$broadcast').and.returnValue({preventDefault: true});
      sessionService.setSessionTimeout();
      $timeout.flush();
      expect($rootScope.$broadcast).toHaveBeenCalledWith('session_timeout');
    });

    it('should not broadcast the session timeout message if the last active time of the Play session has changed', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQ1LCJleHAiOjEyMzU1fQ.ERLbLuI7bMY3UZ5FjkuIlt1QDZPwnXWi9POwUD7xEEA');

      jasmine.clock().mockDate(new Date(12345000));
      spyOn($rootScope, '$broadcast').and.returnValue({preventDefault: true});
      sessionService.setSessionTimeout();
      $timeout.flush();
      expect($rootScope.$broadcast).not.toHaveBeenCalledWith('session_timeout');
    });

    it('should update the last active time if the last active time of the Play session has changed', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQ1LCJleHAiOjEyMzU1fQ.ERLbLuI7bMY3UZ5FjkuIlt1QDZPwnXWi9POwUD7xEEA');

      jasmine.clock().mockDate(new Date(12346000));
      sessionService.setSessionTimeout();
      $timeout.flush();
      expect(Number($cookies.get('lastActiveServerTime'))).toBe(12345);
    });

    it('should update the session expiration time if the last active time of the Play session has changed', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');
      $cookies.put(sessionCookieName,
        'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjEyMzQ1LCJleHAiOjEyMzU1fQ.ERLbLuI7bMY3UZ5FjkuIlt1QDZPwnXWi9POwUD7xEEA');

      jasmine.clock().mockDate(new Date(12346000));
      sessionService.setSessionTimeout();
      $timeout.flush();
      expect(Number($cookies.get('sessionExpirationTime'))).toBe(12350 + (12345 - 12340));
    });

    it('should not broadcast the session timeout message if there is no active session', function () {
      $cookies.put('sessionExpirationTime', '12350');
      $cookies.put('lastActiveServerTime', '12340');

      jasmine.clock().mockDate(new Date(12345000));
      spyOn($rootScope, '$broadcast');
      sessionService.setSessionTimeout();
      $timeout.verifyNoPendingTasks();
      expect($rootScope.$broadcast).not.toHaveBeenCalledWith('session_timeout');
    });
  });
});
