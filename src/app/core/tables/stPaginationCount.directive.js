(function () {

  'use strict';

  angular.module('core')
    .directive('stPaginationCount', paginationCount);

  /**
   * Adds the pagination count to the page.
   */
  function paginationCount() {
    return {
      scope: true,
      restrict: 'E',
      require: '^stTable',
      template: '<div>{{first}} to {{last}} of {{total}}</div>',
      link: function (scope, element, attr, ctrl) {
        scope.$watchGroup(['currentPage', 'stItemsByPage', 'total'], function (watches) {
          var currentPage = watches[0];
          var itemsPerPage = watches[1];
          var total = watches[2];

          scope.first = (currentPage - 1) * itemsPerPage + Math.min(1, total);
          scope.last = (currentPage - 1) * itemsPerPage +
            Math.min(itemsPerPage, total - (currentPage - 1) * itemsPerPage);
        });

        scope.$watchGroup([function () {
          return ctrl.tableState().pagination.total;
        }, ctrl.getFilteredCollection], function (watches) {
          var total = watches[0];
          var filtered = watches[1];

          if (total) {
            scope.total = total;
          } else {
            scope.total = (filtered || []).length;
          }
        });
      }
    };
  }

})();
