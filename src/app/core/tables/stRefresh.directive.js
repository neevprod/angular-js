(function () {

  'use strict';

  angular.module('core')
    .directive('stRefresh', addTableRefreshFunction);

  /**
   * Adds a function that can be called to refresh the table.
   */
  function addTableRefreshFunction() {
    return {
      scope: {
        stRefresh: '='
      },
      restrict: 'A',
      require: '^stTable',
      link: function (scope, element, attr, ctrl) {
        scope.stRefresh = function () {
          ctrl.tableState().pagination.start = 0;
          ctrl.pipe();
        };
      }
    };
  }

})();
