(function () {

  'use strict';

  angular.module('core')
    .directive('stSelectAll', selectAll);

  /**
   * Creates a checkbox that can select all or deselect all of the items in the given collection.
   */
  function selectAll() {
    return {
      scope: {
        collection: '=',
        anySelected: '='
      },
      restrict: 'E',
      template: '<input type="checkbox" ng-click="checkBoxClicked()" ng-model="allSelected" />',
      link: function (scope) {
        // Select or deselect all items in collection when the select all checkbox value is clicked.
        scope.checkBoxClicked = function () {
          scope.collection.forEach(function (value) {
            if (value.isSelectable !== false) {
              value.isSelected = scope.allSelected;
            }
          });
        };

        // Update the value of the select all checkbox based on whether all the selectable items are selected,
        // and update the anySelected variable based on whether any items are selected.
        scope.$watch(function () {
          var numberSelectable = 0;
          var numberSelected = 0;
          scope.collection.forEach(function (value) {
            if (value.isSelectable !== false) {
              numberSelectable++;
            }
            if (value.isSelected) {
              numberSelected++;
            }
          });

          if (numberSelected === 0) {
            return 'none';
          } else if (numberSelected === numberSelectable) {
            return 'all';
          } else {
            return 'some';
          }
        }, function (howManySelected) {
          if (howManySelected === 'all') {
            scope.allSelected = true;
            scope.anySelected = true;
          } else if (howManySelected === 'some') {
            scope.allSelected = false;
            scope.anySelected = true;
          } else {
            scope.allSelected = false;
            scope.anySelected = false;
          }
        });

        // Deselect when switching pages.
        scope.$watch('collection', function (newValue, oldValue) {
          if (oldValue) {
            oldValue.forEach(function (value) {
              value.isSelected = false;
            });
            scope.allSelected = false;
          }
        });

        // Initialize the select all check box to not be checked.
        scope.allSelected = false;
      }
    };
  }

})();
