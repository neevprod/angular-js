(function () {

  'use strict';

  angular.module('gridAutomation')
    .constant('gridAutomationApiPrefix',
      '/api/gridAutomation/v1/');
})();
