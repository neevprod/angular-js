(function () {

  'use strict';

  angular.module('gridAutomation', ['core', 'angularFileUpload']).config(configure);
  configure.$inject = ['$stateProvider'];

  function configure($stateProvider) {
    $stateProvider.state('scheduleGridJob', {
      parent: 'main',
      url: '/gridAutomation/scheduleGridJob',
      templateUrl: 'app/gridAutomation/scheduleGridJob/scheduleGridJob.html',
      controller: 'ScheduleGridJobController as scheduleGridJobCtrl'

    })
      .state('gridJobHistory', {
        parent: 'main',
        url: '/gridAutomation/gridJobHistory',
        templateUrl: 'app/gridAutomation/gridJobHistory/gridJobHistory.html',
        controller: 'GridJobHistoryController as gridJobHistoryCtrl'

      });
  }
})();
