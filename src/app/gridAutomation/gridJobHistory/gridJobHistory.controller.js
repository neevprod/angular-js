(function () {

  'use strict';

  angular.module('gridAutomation').controller('GridJobHistoryController', gridJobHistoryController);

  gridJobHistoryController.$inject = ['gridJobHistoryService', 'memoryService', 'itemsPerPageOptions', 'dateTimeFormat', '$stateParams', '$state'];

  function gridJobHistoryController(gridJobHistoryService, memoryService, itemsPerPageOptions, dateTimeFormat, $stateParams, $state) {

    var viewModel = this;
    viewModel.loadingGridJobs = true;
    viewModel.gridJobsInitialized = false;
    viewModel.gridJobs = [];

    viewModel.goToScheduleNewJob = goToScheduleNewGridJob;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('gridJobs', 'perPage', '') || viewModel.itemsPerPageOptions[0].value;
    viewModel.dateTimeFormat = dateTimeFormat;
    activate();

    viewModel.runTypes = [
      {'key': 'DV', 'value': 'Data Validation'},
      {'key': 'PL', 'value': 'Preliminary Lineup'},
      {'key': 'FL', 'value': 'Final Lineup'},
      {'key': 'FP', 'value': 'Final Print'}
    ];
//    viewModel.getRunTypeName = getRunTypeName();

    viewModel.getRunTypeName = function (runTypeKey) {
//    	alert(runTypeKey);
      for (var i = 0; i < viewModel.runTypes.length; i++) {
        var runType = viewModel.runTypes[i];
        if (runType.key === runTypeKey) {
          return runType.value;
        }
      }
    };


    function activate() {

      var jobsJustStarted = [];
      if ($stateParams.gridJobJustCreated) {
        viewModel.alertSuccess = true;
        viewModel.alertMessage = 'A new Grid job was successfully created.';
        jobsJustStarted.push($stateParams.gridJobJustCreated);
      }
      getGridJobs(jobsJustStarted).then(function (gridJobs) {
        viewModel.loadingGridJobs = false;
        viewModel.gridJobsInitialized = true;
        viewModel.gridJobs = gridJobs;
        viewModel.displayedGridJobs = gridJobs;
      });
    }


    function goToScheduleNewGridJob() {
      $state.go('scheduleGridJob');
    }


    /**
     * Retrieve the list of grid jobs.
     *
     * @returns The list of grid jobs.
     */
    function getGridJobs() {
      return gridJobHistoryService
        .getGridJobs()
        .then(
          function (result) {
            if (result.success) {
              var gridJobs = result.data.gridJobExecs;

              return gridJobs;
            }
          });
    }

  }

})();
