(function () {
  'use strict';
  angular
    .module('gridAutomation')
    .factory('gridJobHistoryService', gridJobHistoryService);
  gridJobHistoryService.$inject = ['$http', 'gridAutomationApiPrefix'];

  function gridJobHistoryService($http, gridAutomationApiPrefix) {

    return {
      getActiveContracts: getActiveContracts,
      getGridJobs: getGridJobs
    };

    function getActiveContracts() {
      return $http.get(gridAutomationApiPrefix + 'activeContracts')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of Grid jobs from the server.
     *
     * @returns The result from the server containing the list of Grid jobs.
     */
    function getGridJobs() {
      return $http.get(gridAutomationApiPrefix + 'gridJobExecutions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

  }
})();
