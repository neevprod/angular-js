(function () {

  'use strict';

  angular.module('gridAutomation').controller('ScheduleGridJobController', scheduleGridJobController);

  scheduleGridJobController.$inject = ['scheduleGridJobService', 'FileUploader', '$stateParams', '$state'];

  function scheduleGridJobController(scheduleGridJobService, FileUploader, $stateParams, $state) {

    var viewModel = this;
    viewModel.alertSuccess = true;
    viewModel.alertMessage = '';
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.files = [];
    viewModel.submit = submit;
    viewModel.today = today;
    viewModel.minDate = new Date();
    viewModel.format = 'MM/dd/yyyy';
    viewModel.loading = false;

    viewModel.popupDeliveryCalendar = {};
    viewModel.popupDeliveryCalendar.opened = false;
    viewModel.openDeliveryCalendar = openDeliveryCalendar;
    viewModel.clearAlertMessage = clearAlertMessage;
    viewModel.cancel = cancel;
    viewModel.runTypes = [
      {'key': 'DV', 'value': 'Data Validation'},
      {'key': 'PL', 'value': 'Preliminary Lineup'},
      {'key': 'FL', 'value': 'Final Lineup'},
      {'key': 'FP', 'value': 'Final Print'}
    ];

    activate();

    function getActiveContracts() {
      return scheduleGridJobService.getActiveContracts().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.contracts);
        }
      });
    }

    function getActivePrograms() {
      return scheduleGridJobService.getActivePrograms().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.programs);
        }
      });
    }

    function getActiveAdmins() {
      return scheduleGridJobService.getActiveAdmins().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.admins);
        }
      });
    }

    function getActiveSeasons() {
      return scheduleGridJobService.getActiveSeasons().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.seasons);
        }
      });
    }


    /**
     * Loads the CPAS list and prepares the calendar
     */
    function activate() {
      getActiveContracts().then(function (contracts) {
        viewModel.contracts = contracts;
      });
      getActivePrograms().then(function (programs) {
        viewModel.programs = programs;
      });
      getActiveAdmins().then(function (admins) {
        viewModel.admins = admins;
      });
      getActiveSeasons().then(function (seasons) {
        viewModel.seasons = seasons;
      });
    }


    function today() {
      viewModel.deliveryDate = new Date();
    }

    function openDeliveryCalendar() {
      viewModel.popupDeliveryCalendar.opened = true;
    }

    function clearAlertMessage() {
      viewModel.alertMessage = '';
      viewModel.overAllStatistics = '';
      viewModel.failedTestAttemptsPerState = '';
      viewModel.testAttemptsPerForm = '';
    }

    function submit(isValid) {
      if (isValid) {
        viewModel.loading = true;
        clearAlertMessage();
        var temp = angular.copy(viewModel.gridFile);
        delete temp.jobContract;
        delete temp.jobProgram;
        delete temp.jobAdmin;
        delete temp.jobSeason;
        temp.gridContractId = viewModel.gridFile.jobContract.gridContractId;
        temp.programId = viewModel.gridFile.jobProgram.programId;
        temp.adminId = viewModel.gridFile.jobAdmin.adminId;
        temp.seasonId = viewModel.gridFile.jobSeason.seasonId;
        var data = angular.toJson(temp);
        return scheduleGridJobService.postGridFile(data, viewModel.files)
          .then(function (result) {
            viewModel.loading = false;
            if (result.success) {
              $state.go('gridJobHistory');
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });
      }
    }

    function cancel() {
      $state.go('gridJobHistory');
    }
  }

})();
