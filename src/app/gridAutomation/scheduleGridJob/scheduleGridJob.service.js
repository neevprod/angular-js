(function () {
  'use strict';
  angular
    .module('gridAutomation')
    .factory('scheduleGridJobService', scheduleGridJobService);
  scheduleGridJobService.$inject = ['$http', 'gridAutomationApiPrefix'];

  function scheduleGridJobService($http, gridAutomationApiPrefix) {
    return {
      getActiveContracts: getActiveContracts,
      getActivePrograms: getActivePrograms,
      getActiveAdmins: getActiveAdmins,
      getActiveSeasons: getActiveSeasons,
      postGridFile: postGridFile
    };

    /**
     * Get a list of contracts
     *
     * @returns The result from the server containing the list of contracts
     */
    function getActiveContracts() {
      return $http.get(gridAutomationApiPrefix + 'activeContracts')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getActivePrograms() {
      return $http.get(gridAutomationApiPrefix + 'activePrograms')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getActiveAdmins() {
      return $http.get(gridAutomationApiPrefix + 'activeAdmins')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getActiveSeasons() {
      return $http.get(gridAutomationApiPrefix + 'activeSeasons')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function postGridFile(json, filesArray) {
      var fd = new FormData();
      fd.append('data', json);
      for (var i = 0; i < filesArray.length; i++) {
        fd.append('file_' + i, filesArray[i]);
      }

      var config = {
        headers: {'Content-Type': undefined},
        transformRequest: angular.identity
      };

      return $http.post(gridAutomationApiPrefix + 'submitJob', fd, config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
