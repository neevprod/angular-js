(function () {

  'use strict';

  angular.module('qtiValidation').controller('ConfirmationTestCaseDeletionController', ConfirmationTestCaseDeletionController);

  ConfirmationTestCaseDeletionController.$inject = ['$uibModalInstance', 'previewerId', 'tenantId', 'file', 'testCaseService'];

  function ConfirmationTestCaseDeletionController($uibModalInstance, previewerId, tenantId, file, testCaseService) {
    var viewModel = this;
    viewModel.close = close;
    viewModel.processFile = processFile;
    viewModel.alertMessage = '';
    viewModel.load = false;

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }

    function processFile() {
      viewModel.load = true;
      var reader = new FileReader();
      reader.onload = function () {
        var parsedCsv = Papa.parse(reader.result, {
          delimiter: ',',
          skipEmptyLines: true
        });

        if (parsedCsv.errors.length) {
          throw 'Error parsing CSV.';
        }
        testCaseService.deleteTestCases(previewerId, tenantId, parsedCsv.data).then(
          function (result) {
            if (result.success) {
              viewModel.load = false;
              viewModel.alertMessage = 'success|All test cases for the uploaded items were deleted successfully. ';
              $uibModalInstance.close(viewModel.alertMessage);
            } else {
              viewModel.alertMessage = result.errors[0].message;
              viewModel.load = false;
              $uibModalInstance.close(viewModel.alertMessage);
            }
          });

      };
      reader.readAsText(file);
    }
  }
})();
