(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$stateParams', '$rootScope', 'previewerService', 'tenantService',
    'qtiScvTenantExecutionService', 'testCaseService', 'permissionsService', 'memoryService', '$uibModal',
    'itemsPerPageOptions', 'dateTimeFormat', 'notificationsService', '$window'];

  function DashboardController($stateParams, $rootScope, previewerService, tenantService,
                               qtiScvTenantExecutionService, testCaseService, permissionsService, memoryService,
                               $uibModal, itemsPerPageOptions, dateTimeFormat, notificationsService, $window) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.tenantId = $stateParams.tenantId;
    viewModel.loadingData = true;
    viewModel.dataInitialized = false;
    viewModel.executions = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('dashboard', 'perPage',
      viewModel.previewerId + '_' + viewModel.tenantId) || viewModel.itemsPerPageOptions[0].value;
    viewModel.wasCompleted = wasCompleted;
    viewModel.tenantLevel = tenantLevel;
    viewModel.showItemExceptions = showItemExceptions;
    viewModel.startQtiScvJob = startQtiScvJob;
    viewModel.startTestCaseDownload = startTestCaseDownload;
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.hasTestCaseReadPermission = permissionsService.hasTenantLevelPermission('testCases', 'readAccess',
      'qtiValidation', $stateParams.previewerId, $stateParams.tenantId);

    activate();

    /**
     * Loads the previewer name, tenant name and the data for the list.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }
      if ($stateParams.tenantName) {
        viewModel.tenantName = $stateParams.tenantName;
      } else {
        getTenantName(viewModel.previewerId, viewModel.tenantId).then(function (tenantName) {
          if (tenantName) {
            viewModel.tenantName = tenantName;
          }
        });
      }

      getData(viewModel.previewerId, viewModel.tenantId).then(function (executions) {
        viewModel.executions = executions;
        viewModel.displayedExecutions = executions;
        viewModel.loadingData = false;
        viewModel.dataInitialized = true;
      });
    }

    /**
     * Get the list of QTI-SCV executions for the current tenant.
     *
     * @returns the list of QTI-SCV executions
     */
    function getData(previewerId, tenantId) {
      return qtiScvTenantExecutionService.getQtiScvTenantExecutionsByTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.qtiScvTenantExecutions;
        }
      });
    }

    /**
     * Sends a notification to start a new Scoring Validation Job.
     */
    function startQtiScvJob() {
      return qtiScvTenantExecutionService.startQtiScvJobForTenant(viewModel.previewerId, viewModel.previewerName, viewModel.tenantId, viewModel.tenantName);
    }

    /**
     * Starts a tenant-level test case download for either an Item Response Pool or Test Case Coverage CSV
     *
     *  @param isItemResponsePool Flag to determine the download type (true = Item Response Pool, false = Test Case Coverage)
     * @returns Data for CSV download (if successful)
     */
    function startTestCaseDownload(isItemResponsePool) {
      var downloadId = 0;
      if ($window.localStorage.getItem('downloadId') !== null) {
        downloadId = parseInt($window.localStorage.getItem('downloadId')) + 1;
      }
      $window.localStorage.setItem('downloadId', downloadId);
      if (isItemResponsePool) {
        notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName, viewModel.tenantId, viewModel.tenantName, downloadId, 'Tenant', 'itemResponsePool');
        return testCaseService.downloadTestCasesForTenant(viewModel.previewerId, viewModel.tenantId).then(function (result) {
          if (result.success) {
            notificationsService.notifyDownloadComplete(downloadId, true, result.data);
          } else {
            notificationsService.notifyDownloadComplete(downloadId, false, null);
          }
        });
      } else {
        notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName, viewModel.tenantId, viewModel.tenantName, downloadId, 'Tenant', 'testCaseCoverage');
        return testCaseService.downloadTestCaseCoverageForTenant(viewModel.previewerId, viewModel.tenantId).then(function (result) {
          if (result.success) {
            notificationsService.notifyDownloadComplete(downloadId, true, result.data);
          } else {
            notificationsService.notifyDownloadComplete(downloadId, false, null);
          }
        });
      }
    }

    /**
     * Get the name of the tenant with the given ID.
     *
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId the ID of the tenant
     * @returns {string} the name of the tenant
     */
    function getTenantName(previewerId, tenantId) {
      return tenantService.getTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.tenant.name;
        }
      });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @returns {string} the name of the previewer
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Returns a string "Yes" or "No" depending on whether the QTI-SCV execution was completed.
     *
     * @param qtiScvExecution The QTI-SCV execution to check.
     * @returns {string} Whether the QTI-SCV execution was completed.
     */
    function wasCompleted(execution) {
      if (execution.execCompleted) {
        return 'Yes';
      } else {
        return 'No';
      }
    }

    /**
     * Returns a string "Yes" or "No" depending on whether the QTI-SCV execution was at the tenant level or not.
     *
     * @param qtiScvExecution The QTI-SCV execution to check.
     * @returns {string} Whether the QTI-SCV execution at the tenant level.
     */
    function tenantLevel(histExec) {
      if (histExec.tenantLevelExec) {
        return 'Yes';
      } else {
        return 'No';
      }
    }

    /**
     * Show a modal with the list of item exceptions for the given QTI-SCV tenant execution and tenant.
     *
     * @param qtiScvTenantExecutionId {number} The ID of the QTI-SCV tenant execution.
     * @param tenantId {number} The ID of the tenant.
     */
    function showItemExceptions(qtiScvTenantExecutionId) {
      $uibModal.open({
        templateUrl: 'app/qtiValidation/qtiScvExecutions/itemExceptions/itemExceptions.html',
        controller: 'ItemExceptionsController as itemExceptionsCtrl',
        resolve: {
          previewerId: function () {
            return viewModel.previewerId;
          },
          tenantId: function () {
            return viewModel.tenantId;
          },
          qtiScvTenantExecutionId: function () {
            return qtiScvTenantExecutionId;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Listens for completed QTI SCV Job messages and updates the Scoring Validation Jobs history list accordingly.
     */
    $rootScope.$on('qti_scv_job_complete', function (event, previewerId, tenantId) {
      if (previewerId === viewModel.previewerId && tenantId === viewModel.tenantId) {
        viewModel.loadingData = true;
        getData(viewModel.previewerId, viewModel.tenantId).then(function (executions) {
          viewModel.executions = executions;
          viewModel.displayedExecutions = executions;
          viewModel.loadingData = false;
          viewModel.dataInitialized = true;
        });
      }
    });
  }

})();
