(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('itemService', itemService);

  itemService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function itemService($http, qtiValidationApiPrefix) {
    return {
      getItems: getItems,
      getItem: getItem,
      getTestMapItems: getTestMapItems,
      getItemsForDownLoad: getItemsForDownLoad,
      getItemXMLByIdentifier: getItemXMLByIdentifier
    };

    /**
     * Get a list of items that belong to the given tenant.
     *
     * @param {number} previewerId - The ID of the previewer that contains the tenant.
     * @param {number} tenantId - The ID of the tenant.
     * @param {Object} [filterParams] - An object containing some or all of the following properties:
     * @param {number} [filterParams.startIndex] - The index of the item to start at.
     * @param {number} [filterParams.numberToShow] - The number of items to show.
     * @param {string} [filterParams.sortColumn] - The name of the column to sort by.
     * @param {boolean} [filterParams.sortReversed] - Whether to reverse the sort order.
     * @param {string} [filterParams.search] - The search term.
     * @param {number} [filterParams.testCaseFilter] - Filter by whether items have test cases (can be -1, 0, or 1).
     * @param {number} [filterParams.fingerprintFilter] - Filter by whether items have fingerprints (can be -1, 0, or 1).
     * @returns The result from the server containing the list of items.
     */
    function getItems(previewerId, tenantId, filterParams) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/items'
      };
      if (filterParams) {
        config.params = {
          startIndex: filterParams.startIndex,
          numberToShow: filterParams.numberToShow,
          sortColumn: filterParams.sortColumn,
          sortReversed: filterParams.sortReversed,
          search: filterParams.search,
          testCaseFilter: filterParams.testCaseFilter,
          fingerprintFilter: filterParams.fingerprintFilter,
          itemIds: filterParams.itemIds
        };
      }

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of items that belong to the given tenant.
     *
     * @param {number} previewerId - The ID of the previewer that contains the tenant.
     * @param {number} tenantId - The ID of the tenant.
     * @param {number} formId - The ID of the form.
     * @param {number} identifier - The ID of the testmap.
     * @param {Object} [filterParams] - An object containing some or all of the following properties:
     * @param {number} [filterParams.startIndex] - The index of the item to start at.
     * @param {number} [filterParams.numberToShow] - The number of items to show.
     * @param {string} [filterParams.sortColumn] - The name of the column to sort by.
     * @param {boolean} [filterParams.sortReversed] - Whether to reverse the sort order.
     * @param {string} [filterParams.search] - The search term.
     * @param {number} [filterParams.testCaseFilter] - Filter by whether items have test cases (can be -1, 0, or 1).
     * @param {number} [filterParams.fingerprintFilter] - Filter by whether items have fingerprints (can be -1, 0, or 1).
     * @returns The result from the server containing the list of items.
     */
    function getTestMapItems(previewerId, tenantId, formId, identifier, filterParams) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/form/' + formId + '/identifier/' + identifier + '/testMapItems'
      };
      if (filterParams) {
        config.params = {
          startIndex: filterParams.startIndex,
          numberToShow: filterParams.numberToShow,
          sortColumn: filterParams.sortColumn,
          sortReversed: filterParams.sortReversed,
          search: filterParams.search,
          testCaseFilter: filterParams.testCaseFilter,
          fingerprintFilter: filterParams.fingerprintFilter
        };
      }

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }


    /**
     * Get a specific item that belongs to the given tenant.
     *
     * @param {number} previewerId - The ID of the previewer than contains the tenant.
     * @param {number} tenantId - The ID of the tenant.
     * @param {number} itemId - The ID of the item.
     * @returns The result from the server containing the item.
     */
    function getItem(previewerId, tenantId, itemId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/items/' + itemId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of items that belong to the given tenant.
     *
     * @param {number} previewerId - The ID of the previewer that contains the tenant.
     * @param {number} tenantId - The ID of the tenant.
     * @returns The result from the server containing the list of items to download.
     */
    function getItemsForDownLoad(previewerId, tenantId) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/itemsToDownload'
      };
      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
    
    /**
     * Get xml for the given item identifier.
     *
     * @param {number} previewerId - The ID of the previewer that contains the tenant.
     * @param {number} tenantId - The ID of the tenant.
     * @param {number} itemIdentifier - The identifier of the item.
     * @returns The result from the server containing the xml of item .
     */
    function getItemXMLByIdentifier(previewerId, tenantId, itemIdentifier) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId+ '/tenants/' + tenantId + '/itemIdentifier/' + itemIdentifier
      };
      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
    

  }

})();
