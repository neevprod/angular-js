'use strict';

describe('item service', function () {
  var itemService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_itemService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    itemService = _itemService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getItems', function () {
    it('should submit the items GET request properly when no filters are given', function () {
      // Given
      var data = [{id: 1, identifier: 'item1'}, {id: 2, identifier: 'item2'},
        {id: 3, identifier: 'item3'}];

      // Then
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/1/tenants/2/items')
        .respond(200, {success: true, data: data});

      // When
      itemService.getItems(1, 2);
      $httpBackend.flush();
    });

    it('should submit the items GET request properly when filters are given', function () {
      // Given
      var data = [{id: 1, identifier: 'item1'}, {id: 2, identifier: 'item2'},
        {id: 3, identifier: 'item3'}];
      var params = {
        startIndex: 0,
        numberToShow: 10,
        sortColumn: 'id',
        sortReversed: true,
        search: 'query',
        testCaseFilter: 1,
        fingerprintFilter: -1
      };

      // Then
      $httpBackend.expect('GET', qtiValidationApiPrefix + 'previewers/1/tenants/2/items' +
        '?fingerprintFilter=' + params.fingerprintFilter +
        '&numberToShow=' + params.numberToShow +
        '&search=' + params.search +
        '&sortColumn=' + params.sortColumn +
        '&sortReversed=' + params.sortReversed +
        '&startIndex=' + params.startIndex +
        '&testCaseFilter=' + params.testCaseFilter)
        .respond(200, {success: true, data: data});

      // When
      itemService.getItems(1, 2, params);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{id: 1, identifier: 'item1'}, {id: 2, identifier: 'item2'},
        {id: 3, identifier: 'item3'}];
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants/2/items')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      itemService.getItems(1, 2).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants/2/items')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      itemService.getItems(1, 2).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

  describe('getItem', function () {
    it('should submit the items GET request properly', function () {
      // Given
      var data = {id: 1, identifier: 'item1'};

      // Then
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/1/tenants/2/items/3')
        .respond(200, {success: true, data: data});

      // When
      itemService.getItem(1, 2, 3);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = {id: 1, identifier: 'item1'};
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/1/tenants/2/items/3')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      itemService.getItem(1, 2, 3).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants/2/items/3')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      itemService.getItem(1, 2, 3).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
