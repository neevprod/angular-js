(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('ItemsController', ItemsController);

  ItemsController.$inject = ['$stateParams', 'previewerService', 'tenantService', 'itemService', 'permissionsService',
    'itemsPerPageOptions', 'testCaseService', 'notificationsService', 'memoryService', '$window', '$rootScope',
    '$uibModal', 'testMapsService', '$filter'];

  function ItemsController($stateParams, previewerService, tenantService, itemService, permissionsService,
                           itemsPerPageOptions, testCaseService, notificationsService, memoryService, $window,
                           $rootScope, $uibModal, testMapsService, $filter) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.tenantId = $stateParams.tenantId;
    viewModel.loadingItems = true;
    viewModel.itemsInitialized = false;
    viewModel.items = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('items', 'perPage',
      viewModel.previewerId + '_' + viewModel.tenantId) || viewModel.itemsPerPageOptions[0].value;
    viewModel.formIdentifiers = [];
    viewModel.testCaseFilter = Number(memoryService.getItem('items', 'testCaseFilter',
      viewModel.previewerId + '_' + viewModel.tenantId));
    viewModel.fingerprintFilter = Number(memoryService.getItem('items', 'fingerprintFilter',
      viewModel.previewerId + '_' + viewModel.tenantId));
    viewModel.hasTestCaseReadPermission = permissionsService.hasTenantLevelPermission('testCases', 'readAccess',
      'qtiValidation', $stateParams.previewerId, $stateParams.tenantId);
    viewModel.hasTestCaseCreatePermission = permissionsService.hasTenantLevelPermission('testCases',
      'createAccess', 'qtiValidation', $stateParams.previewerId, $stateParams.tenantId);
    viewModel.searchItems = searchItems;
    viewModel.getItems = getItems;
    viewModel.setTestCaseFilter = setTestCaseFilter;
    viewModel.setFingerprintFilter = setFingerprintFilter;
    viewModel.isFingerprinted = isFingerprinted;
    viewModel.startTestCaseDownload = startTestCaseDownload;
    viewModel.processTestCaseUpload = processTestCaseUpload;
    viewModel.showCreateTestCasesDialog = showCreateTestCasesDialog;
    viewModel.clearAlertMessage = clearAlertMessage;
    viewModel.startTestIdentifiersDownload = startTestIdentifiersDownload;
    viewModel.startTestIdentifiersUpload = startTestIdentifiersUpload;
    viewModel.showItemXml = showItemXml;
    activate();

    function searchItems(tableState) {
      if (viewModel.testMapId) {
        getFormIdentifiers(tableState);
      } else {
        getItems(tableState);
      }
    }

    /**
     * Loads the previewer name, the tenant name, and the list of items.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      }
      previewerService.getPreviewer(viewModel.previewerId).then(function (result) {
        if (result.success) {
          viewModel.previewerName = result.data.previewer.url;
          viewModel.previewerProtocol = result.data.previewer.protocol;
        }
      });

      if ($stateParams.tenantName) {
        viewModel.tenantName = $stateParams.tenantName;
      } else {
        getTenantName(viewModel.previewerId, viewModel.tenantId).then(function (tenantName) {
          if (tenantName) {
            viewModel.tenantName = tenantName;
          }
        });
      }
      if ($stateParams.identifier) {
        viewModel.testMapId = $stateParams.identifier;
      }
    }

    /**
     * Get the list of items from the itemService.
     *
     * @param tableState the state of the table
     */
    function getItems(tableState) {
      viewModel.loadingItems = true;
      clearAlertMessage();
      var filterParams = {
        startIndex: tableState.pagination.start,
        numberToShow: viewModel.itemsPerPage,
        sortColumn: tableState.sort.predicate,
        sortReversed: tableState.sort.reverse,
        testCaseFilter: viewModel.testCaseFilter,
        fingerprintFilter: viewModel.fingerprintFilter
      };
      if (tableState.search.predicateObject && tableState.search.predicateObject.identifier) {
        filterParams.search = tableState.search.predicateObject.identifier;
      }
      itemService.getItems(viewModel.previewerId, viewModel.tenantId, filterParams).then(function (result) {
        if (result.success) {
          viewModel.items = result.data.items;
          tableState.pagination.total = result.data.totalResults;
          tableState.pagination.numberOfPages =
            Math.ceil(result.data.totalResults / tableState.pagination.number);
          viewModel.alertSuccess = true;
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
          viewModel.items = [];
        }
        viewModel.loadingItems = false;
        viewModel.itemsInitialized = true;
      });
    }

    /**
     * Get the name of the tenant with the given ID.
     *
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId the ID of the tenant
     * @returns {string} the name of the tenant
     */
    function getTenantName(previewerId, tenantId) {
      return tenantService.getTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.tenant.name;
        }
      });
    }

    /**
     * Set the test case filter to the given value and refresh the table if the value is valid.
     *
     * @param {number} value - the value to set (must be -1, 0, or 1)
     */
    function setTestCaseFilter(value) {
      if (value >= -1 && value <= 1) {
        viewModel.testCaseFilter = value;
        memoryService.setItem('items', 'testCaseFilter', viewModel.previewerId + '_' + viewModel.tenantId,
          value);
        viewModel.refreshTable();
      }
    }

    /**
     * Set the fingerprint filter to the given value and refresh the table if the value is valid.
     *
     * @param {number} value - the value to set (must be -1, 0, or 1)
     */
    function setFingerprintFilter(value) {
      if (value >= -1 && value <= 1) {
        viewModel.fingerprintFilter = value;
        memoryService.setItem('items', 'fingerprintFilter', viewModel.previewerId + '_' + viewModel.tenantId,
          value);
        viewModel.refreshTable();
      }
    }

    /**
     * Returns a string "Yes" or "No" depending on whether the item is fingerprinted.
     *
     * @param item the item to check
     * @returns {string} whether the item is fingerprinted
     */
    function isFingerprinted(item) {
      if (item.fingerprinted) {
        return 'Yes';
      } else {
        return 'No';
      }
    }

    /**
     * Converts the data from a Test Case CSV Upload into JSON, which is used to start a test case upload job.
     *
     * @param file The CSV file to process.
     */
    function processTestCaseUpload(file) {
      var reader = new FileReader();
      reader.onload = function () {
        try {
          var parsedCsv = Papa.parse(reader.result, {
            delimiter: ',',
            skipEmptyLines: true
          });

          if (parsedCsv.errors.length) {
            throw 'Error parsing CSV.';
          }

          var result = [];
          var item = {};
          var responses = [];
          var prevItemId = null;

          for (var i = 1; i < parsedCsv.data.length; i++) {
            var currentRow = parsedCsv.data[i];

            var itemId = currentRow[0];
            if (itemId.startsWith('`')) {
              itemId = itemId.substr(1);
            }

            if (itemId !== prevItemId) {
              if (i !== 1) {
                item.responses = responses;
                result.push(item);
              }

              item = {};
              responses = [];
              item.itemId = itemId;
              var identifiersToInteractionsStrings = currentRow[3].split(';');
              var identifiersToInteractions = [];
              for (var j = 0; j < identifiersToInteractionsStrings.length; j++) {
                var identifierToInteraction = {};
                var identifierToInteractionParts = identifiersToInteractionsStrings[j].split(':');
                identifierToInteraction.identifier = identifierToInteractionParts[0];
                identifierToInteraction.interaction = identifierToInteractionParts[1];
                identifiersToInteractions.push(identifierToInteraction);
              }
              item.identifiersToInteractions = identifiersToInteractions;
            }

            var response = {};

            response.attempted = currentRow[2].toLowerCase() === 'true';

            response.mods = JSON.parse(currentRow[4]);

            var outcome = {};
            outcome.identifier = 'SCORE';
            outcome.value = parseFloat(currentRow[1]);
            response.outcomes = [outcome];

            responses.push(response);
            prevItemId = itemId;
          }

          item.responses = responses;
          result.push(item);

          testCaseService.startTestCaseUpload(viewModel.previewerId, viewModel.previewerName,
            viewModel.tenantId, viewModel.tenantName, result);
        } catch (error) {
          viewModel.alertMessage = 'An error occurred while reading the CSV file.';
          viewModel.alertSuccess = false;
        }
      };

      reader.readAsText(file);
    }

    /**
     * Shows the dialog that triggers creation of test cases for selected items.
     */
    function showCreateTestCasesDialog() {
      var selectedItemIds = [];
      viewModel.items.forEach(function (item) {
        if (item.isSelected) {
          selectedItemIds.push(item.id);
        }
      });

      $uibModal.open({
        templateUrl: 'app/qtiValidation/testCases/createTestCases/createTestCases.html',
        controller: 'CreateTestCasesController as createTestCasesCtrl',
        resolve: {
          previewerId: function () {
            return viewModel.previewerId;
          },
          previewerName: function () {
            return viewModel.previewerName;
          },
          tenantId: function () {
            return viewModel.tenantId;
          },
          tenantName: function () {
            return viewModel.tenantName;
          },
          ids: function () {
            return selectedItemIds;
          },
          isAtTestMapLevel: function () {
            return false;
          }
        },
        backdrop: 'static',
        size: 'sm'
      });
    }

    /**
     * Starts a item-level test case download for either an Item Response Pool or Test Case Coverage CSV
     *
     *  @param isItemResponsePool Flag to determine the download type (true = Item Response Pool, false = Test Case Coverage)
     * @returns Data for CSV download (if successful)
     */
    function startTestCaseDownload(isItemResponsePool) {
      var selectedItemIds = [];

      viewModel.items.forEach(function (item) {
        if (item.isSelected) {
          selectedItemIds.push(item.identifier);
        }
      });

      var downloadId = 0;
      if ($window.localStorage.getItem('downloadId') !== null) {
        downloadId = parseInt($window.localStorage.getItem('downloadId')) + 1;
      }
      $window.localStorage.setItem('downloadId', downloadId);
      if (isItemResponsePool) {
        notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName, viewModel.tenantId, viewModel.tenantName, downloadId, 'Item', 'itemResponsePool');
        return testCaseService.downloadTestCasesForItems(viewModel.previewerId, viewModel.tenantId, selectedItemIds).then(function (result) {
          if (result.success) {
            notificationsService.notifyDownloadComplete(downloadId, true, result.data);
          } else {
            notificationsService.notifyDownloadComplete(downloadId, false, null);
          }
        });
      } else {
        notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName, viewModel.tenantId, viewModel.tenantName, downloadId, 'Item', 'testCaseCoverage');
        return testCaseService.downloadTestCaseCoverageForItems(viewModel.previewerId, viewModel.tenantId, selectedItemIds).then(function (result) {
          if (result.success) {
            notificationsService.notifyDownloadComplete(downloadId, true, result.data);
          } else {
            notificationsService.notifyDownloadComplete(downloadId, false, null);
          }
        });
      }
    }

    /**
     * Downloads item Id and item identifiers information to a CSV for a particular previewerId and tenantId
     *
     * @returns Data for CSV download (if successful)
     */
    function startTestIdentifiersDownload() {

      var downloadId = 0;
      if ($window.localStorage.getItem('downloadId') !== null) {
        downloadId = parseInt($window.localStorage.getItem('downloadId')) + 1;
      }
      $window.localStorage.setItem('downloadId', downloadId);

      notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName, viewModel.tenantId,
        viewModel.tenantName, downloadId, 'Item', 'identifier');
      return itemService.getItemsForDownLoad(viewModel.previewerId, viewModel.tenantId).then(function (result) {
        if (result.success) {
          var csvContent = '';
          for (var i = 0; i < result.data.length; i++) {
            for (var j = 0; j < result.data[i].items.length; j++) {
              var item = result.data[i].items[j];
              var line = item.id + ',' + item.identifier + '\n';
              csvContent = csvContent + line;
            }
          }
          var csv = {};
          csv.download = csvContent;
          notificationsService.notifyDownloadComplete(downloadId, true, csv);
        } else {
          notificationsService.notifyDownloadComplete(downloadId, false, null);
        }
      });
    }

    function startTestIdentifiersUpload(file) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/qtiValidation/confirmationTestCaseDeletion/confirmationModal.html',
        controller: 'ConfirmationTestCaseDeletionController as confirmCtrl',
        backdrop: 'static',
        resolve: {
          previewerId: function () {
            return viewModel.previewerId;
          },
          tenantId: function () {
            return viewModel.tenantId;
          },
          file: function () {
            return file;
          }
        }

      });
      modalInstance.result.then(function (alertMessage) {
        if (alertMessage) {
          var index = alertMessage.indexOf('success');
          if (index > -1) {
            viewModel.alertSuccess = true;
            viewModel.alertMessage = alertMessage.split('success|')[1];

          }
          else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = alertMessage;
          }
          viewModel.refreshTable();
        }


      });

    }

    /**
     * Clears the alert message.
     */
    function clearAlertMessage() {
      viewModel.alertMessage = '';
    }


    /**
     * Get the list of Form Identifiers in test maps
     *
     * @returns the list of formIdentifiers in TestMaps
     */
    function getFormIdentifiers(tableState) {
      viewModel.loadingItems = true;
      clearAlertMessage();
      var filterParams = {
        startIndex: tableState.pagination.start,
        numberToShow: viewModel.itemsPerPage,
        sortColumn: tableState.sort.predicate,
        sortReversed: tableState.sort.reverse,
        testCaseFilter: viewModel.testCaseFilter,
        fingerprintFilter: viewModel.fingerprintFilter
      };
      testMapsService.getTestMaps(viewModel.previewerId, viewModel.tenantId).then(function (result) {
        if (result.success) {
          viewModel.testMaps = result.data.testMaps;
          angular.forEach(result.data.testMaps, function (value) {
            viewModel.formIdentifiers.push(value.id);
          });
          var originalTestMap = $filter('filter')(viewModel.testMaps, {id: viewModel.testMapId});
          if (originalTestMap.length > 0) {
            itemService.getTestMapItems(viewModel.previewerId, viewModel.tenantId, originalTestMap[0].identifier, originalTestMap[0].formId, filterParams).then(function (result) {
              if (result.success) {
                viewModel.ids = [];
                angular.forEach(result.data.items, function (value) {
                  viewModel.ids.push(value.id);
                });
                filterParams.itemIds = viewModel.ids;
                itemService.getItems(viewModel.previewerId, viewModel.tenantId, filterParams).then(function (result) {
                  if (result.success) {
                    viewModel.items = result.data.items;
                    tableState.pagination.total = result.data.totalResults;
                    tableState.pagination.numberOfPages =
                      Math.ceil(result.data.totalResults / tableState.pagination.number);
                    viewModel.alertSuccess = true;
                  } else {
                    viewModel.alertSuccess = false;
                    viewModel.alertMessage = result.errors[0].message;
                    viewModel.items = [];
                  }
                  viewModel.loadingItems = false;
                  viewModel.itemsInitialized = true;
                });
              } else {
                viewModel.alertSuccess = false;
                viewModel.alertMessage = result.errors[0].message;
                viewModel.items = [];
                viewModel.loadingItems = false;
              }
            });
          }
          else {
            viewModel.loadingItems = false;
            viewModel.items = [];
            viewModel.itemsInitialized = true;
            tableState.pagination.total = 0;
            tableState.pagination.numberOfPages = 0;
          }
        }
      });

    }

    /**
     * Listens for completed Test Case Upload Job messages and updates the Items list accordingly.
     */
    $rootScope.$on('test_case_upload_job_complete', function (event, previewerId, tenantId) {
      if (previewerId === viewModel.previewerId && tenantId === viewModel.tenantId) {
        viewModel.refreshTable();
      }
    });

    /**
     * Show the given Item Xml in a modal.
     *
     * @param {string} itemIdentifier - The itemIdentifier Xml to show.
     */
    function showItemXml(item) {
      viewModel.loadingItems = true;
      itemService.getItemXMLByIdentifier(viewModel.previewerId, viewModel.tenantId, item.identifier).then(function (result) {
        if (result.success) {
          viewModel.itemXml = result.data.itemXml;
          $uibModal.open({
            templateUrl: 'app/core/jsonDetails/jsonDetails.html',
            controller: 'JsonDetailsController as jsonDetailsCtrl',
            resolve: {
              title: function () {
                return 'Item XML';
              },
              json: function () {
                return viewModel.itemXml;
              }
            },
            size: 'lg'
          });
          viewModel.loadingItems = false;
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
          viewModel.loadingItems = false;
        }
      });
    }
  }
})();
