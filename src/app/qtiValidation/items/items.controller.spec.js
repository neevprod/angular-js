'use strict';

describe('items controller', function () {
  var controller;
  var $stateParams;
  var previewerService;
  var tenantService;
  var itemService;
  var permissionsService;
  var deferredPreviewer;
  var deferredTenant;
  var deferredItem;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_, $window) {
    $stateParams = {previewerId: 1, tenantId: 2};
    $window.localStorage.clear();

    deferredPreviewer = $q.defer();
    previewerService = jasmine.createSpyObj('previewerService', ['getPreviewer']);
    previewerService.getPreviewer.and.callFake(function (previewerId) {
      if (previewerId === $stateParams.previewerId) {
        return deferredPreviewer.promise;
      }
    });

    deferredTenant = $q.defer();
    tenantService = jasmine.createSpyObj('tenantService', ['getTenant']);
    tenantService.getTenant.and.callFake(function (previewerId, tenantId) {
      if (previewerId === $stateParams.previewerId && tenantId === $stateParams.tenantId) {
        return deferredTenant.promise;
      }
    });

    deferredItem = $q.defer();
    itemService = jasmine.createSpyObj('itemService', ['getItems']);
    itemService.getItems.and.callFake(function (previewerId, tenantId) {
      if (previewerId === $stateParams.previewerId && tenantId === $stateParams.tenantId) {
        return deferredItem.promise;
      }
    });

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasTenantLevelPermission']);
    permissionsService.hasTenantLevelPermission.and.returnValue(true);

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;

    controller = $controller('ItemsController', {
      $stateParams: $stateParams, previewerService: previewerService,
      tenantService: tenantService, itemService: itemService, permissionsService: permissionsService,
      itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set previewerId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.previewerId).toBe($stateParams.previewerId);
  });

  it('should set tenantId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.tenantId).toBe($stateParams.tenantId);
  });

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should call previewerService to get the previewer information during initialization', function () {
    // Then
    expect(previewerService.getPreviewer).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set the previewer name after previewerService returns', function () {
    // Given
    var previewer = {id: 1, previewerUrl: 'previewer1.com', url: 'previewer1'};

    // When
    deferredPreviewer.resolve({success: true, data: {previewer: previewer}});
    $rootScope.$digest();

    // Then
    expect(controller.previewerName).toEqual(previewer.url);
  });

  it('should set the previewer protocol after previewerService returns', function () {
    // Given
    var previewer = {id: 1, previewerUrl: 'previewer1.com', url: 'previewer1', protocol: 'https://'};

    // When
    deferredPreviewer.resolve({success: true, data: {previewer: previewer}});
    $rootScope.$digest();

    // Then
    expect(controller.previewerProtocol).toEqual(previewer.protocol);
  });

  it('should call tenantService to get the tenant name during initialization', function () {
    // Then
    expect(tenantService.getTenant).toHaveBeenCalledWith($stateParams.previewerId, $stateParams.tenantId);
  });

  it('should set the tenant name after tenantService returns', function () {
    // Given
    var tenant = {previewerTenantId: 1, name: 'tenant1'};

    // When
    deferredTenant.resolve({success: true, data: {tenant: tenant}});
    $rootScope.$digest();

    // Then
    expect(controller.tenantName).toEqual(tenant.name);
  });

  it('should set loadingItems to true on initialization', function () {
    // Then
    expect(controller.loadingItems).toBe(true);
  });

  it('should set the list of items to an empty array on initialization', function () {
    // Then
    expect(controller.items).toEqual([]);
  });

  it('should set testCaseFilter to 0 on initialization if no value was previously set', function () {
    // Then
    expect(controller.testCaseFilter).toEqual(0);
  });

  it('should set fingerprintFilter to 0 on initialization if no value was previously set', function () {
    // Then
    expect(controller.fingerprintFilter).toEqual(0);
  });

  it('should set hasTestCaseReadPermission to true if the user has that permission', function () {
    // Then
    expect(controller.hasTestCaseReadPermission).toBe(true);
  });

  it('should set hasTestCaseReadPermission to false if the user does not have that permission', function () {
    //Given
    permissionsService.hasTenantLevelPermission.and.callFake(function (permissionName, accessType) {
      if (permissionName === 'testCases' && accessType === 'readAccess') {
        return false;
      } else {
        return true;
      }
    });

    // Then
    expect(controller.hasTestCaseReadPermission).toBe(true);
  });

  describe('getItems', function () {
    var tableState;
    var expectedFilterParams;
    var items;

    beforeEach(function () {
      tableState = {
        pagination: {
          start: 0,
          number: 10
        },
        sort: {
          predicate: 'id',
          reverse: true
        },
        search: {
          predicateObject: {
            identifier: 'query'
          }
        }
      };
      controller.itemsPerPage = 10;
      controller.testCaseFilter = 1;
      controller.fingerprintFilter = -1;

      expectedFilterParams = {
        startIndex: 0,
        numberToShow: 10,
        sortColumn: 'id',
        sortReversed: true,
        search: 'query',
        testCaseFilter: 1,
        fingerprintFilter: -1
      };
      items = [{id: 1, identifier: 'item1'}, {id: 2, identifier: 'item2'},
        {id: 3, identifier: 'item3'}];
    });

    it('should call itemService with the correct arguments', function () {
      // When
      controller.getItems(tableState);

      // Then
      expect(itemService.getItems).toHaveBeenCalledWith($stateParams.previewerId, $stateParams.tenantId,
        expectedFilterParams);
    });

    it('should update the items list and the table state with the results from itemService', function () {
      // When
      controller.getItems(tableState);
      deferredItem.resolve({
        success: true,
        data: {
          totalResults: 500,
          items: items
        }
      });
      $rootScope.$digest();

      // Then
      expect(controller.items).toEqual(items);
      expect(tableState.pagination.total).toBe(500);
      expect(tableState.pagination.numberOfPages).toBe(Math.ceil(500 / tableState.pagination.number));
    });

    it('should set loadingItems to true before calling itemService', function () {
      // When
      controller.getItems(tableState);

      // Then
      expect(controller.loadingItems).toBe(true);
    });

    it('should set loadingItems to false after calling itemService', function () {
      // When
      controller.getItems(tableState);
      deferredItem.resolve({
        success: true,
        data: {
          totalResults: 500,
          items: items
        }
      });
      $rootScope.$digest();

      // Then
      expect(controller.loadingItems).toBe(false);
    });
  });

  describe('setTestCaseFilter', function () {
    beforeEach(function () {
      controller.refreshTable = function () {
      };
      spyOn(controller, 'refreshTable');
    });

    it('should update testCaseFilter if the value given is valid', function () {
      // When
      controller.setTestCaseFilter(1);

      // Then
      expect(controller.testCaseFilter).toBe(1);
    });

    it('should refresh the table if the value given is valid', function () {
      // When
      controller.setTestCaseFilter(-1);

      // Then
      expect(controller.refreshTable).toHaveBeenCalled();
    });

    it('should not update testCaseFilter if the value given is not valid', function () {
      // When
      controller.setTestCaseFilter(2);

      // Then
      expect(controller.testCaseFilter).toBe(0);
    });

    it('should not refresh the table if the value given is not valid', function () {
      // When
      controller.setTestCaseFilter(-2);

      // Then
      expect(controller.refreshTable).not.toHaveBeenCalled();
    });
  });

  describe('setFingerprintFilter', function () {
    beforeEach(function () {
      controller.refreshTable = function () {
      };
      spyOn(controller, 'refreshTable');
    });

    it('should update fingerprintFilter if the value given is valid', function () {
      // When
      controller.setFingerprintFilter(1);

      // Then
      expect(controller.fingerprintFilter).toBe(1);
    });

    it('should refresh the table if the value given is valid', function () {
      // When
      controller.setFingerprintFilter(-1);

      // Then
      expect(controller.refreshTable).toHaveBeenCalled();
    });

    it('should not update fingerprintFilter if the value given is not valid', function () {
      // When
      controller.setFingerprintFilter(2);

      // Then
      expect(controller.fingerprintFilter).toBe(0);
    });

    it('should not refresh the table if the value given is not valid', function () {
      // When
      controller.setFingerprintFilter(-2);

      // Then
      expect(controller.refreshTable).not.toHaveBeenCalled();
    });
  });

  describe('isFingerprinted', function () {
    it('should return "Yes" if the item is fingerprinted', function () {
      // Given
      var item = {
        id: 1,
        identifier: 'item1',
        numTestCases: 3,
        fingerprinted: true
      };

      // When
      var result = controller.isFingerprinted(item);

      // Then
      expect(result).toBe('Yes');
    });

    it('should return "No" if the item is not fingerprinted', function () {
      // Given
      var item = {
        id: 1,
        identifier: 'item1',
        numTestCases: 3,
        fingerprinted: false
      };

      // When
      var result = controller.isFingerprinted(item);

      // Then
      expect(result).toBe('No');
    });
  });
});
