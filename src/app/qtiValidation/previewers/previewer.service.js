(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('previewerService', previewerService);

  previewerService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function previewerService($http, qtiValidationApiPrefix) {
    return {
      getPreviewers: getPreviewers,
      getPreviewer: getPreviewer,
      getResponseGenVersions: getResponseGenVersions,
      getSelectedRespGenVersion: getSelectedRespGenVersion
    };

    /**
     * Get the list of previewers from the server.
     *
     * @returns the result from the server containing the list of previewers
     */
    function getPreviewers() {
      return $http.get(qtiValidationApiPrefix + 'previewers')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a previewer from the server by the given ID.
     *
     * @param previewerId the ID of the previewer
     * @returns the result from the server containing the previewer
     */
    function getPreviewer(previewerId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the list of Response Generator versions from the Pearson Maven repository.
     *
     * @returns the result from the server containing the list of response generator versions
     */
    function getResponseGenVersions() {
      return $http.get(qtiValidationApiPrefix + 'respGenVersions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a version from the server by the given ID.
     *
     * @param respGenVersionId the ID of the response generator version to download
     * @returns the result from the server containing the requested version
     */
    function getSelectedRespGenVersion(respGenVersionId) {
      return $http.get(qtiValidationApiPrefix + 'respGenVersionId/' + respGenVersionId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
