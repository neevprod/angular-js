'use strict';

describe('previewer service', function () {
  var previewerService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_previewerService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    previewerService = _previewerService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getPreviewers', function () {
    it('should submit the previewers GET request properly', function () {
      var data = [{id: 1, previewerUrl: 'previewer1.com'}, {id: 2, previewerUrl: 'previewer2.com'},
        {id: 3, previewerUrl: 'previewer3.com'}];

      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers')
        .respond(200, {success: true, data: data});

      previewerService.getPreviewers();

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      var data = [{id: 1, previewerUrl: 'previewer1.com'}, {id: 2, previewerUrl: 'previewer2.com'},
        {id: 3, previewerUrl: 'previewer3.com'}];

      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers')
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      previewerService.getPreviewers().then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      previewerService.getPreviewers().then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });

  describe('getPreviewer', function () {
    it('should submit the previewers GET request properly', function () {
      var data = {id: 1, previewerUrl: 'previewer1.com'};

      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/1')
        .respond(200, {success: true, data: data});

      previewerService.getPreviewer(1);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      var data = {id: 1, previewerUrl: 'previewer1.com'};

      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1')
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      previewerService.getPreviewer(1).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      previewerService.getPreviewer(1).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });
});
