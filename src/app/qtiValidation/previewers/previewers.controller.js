(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('PreviewersController', PreviewersController);

  PreviewersController.$inject = ['previewerService', 'permissionsService'];

  function PreviewersController(previewerService, permissionsService) {
    var viewModel = this;
    viewModel.loadingPreviewers = true;
    viewModel.downloading = false;
    viewModel.previewers = [];
    viewModel.respGenVersions = [];
    viewModel.selectedRespGenVersion = '';
    viewModel.alertMessage = '';
    viewModel.alertSuccess = false;
    viewModel.isAdmin = permissionsService.isAdmin();
    activate();
    getRespGenVersions();
    viewModel.downloadSelectedRespGenVersion = downloadSelectedRespGenVersion;

    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    /**
     * Loads the list of previewers into the viewmodel.
     */
    function activate() {
      viewModel.loadingPreviewers = true;
      getPreviewers().then(function (previewers) {
        viewModel.loadingPreviewers = false;
        viewModel.previewers = previewers;
      });
    }

    /**
     * Get the list of previewers from the previewerService.
     *
     * @returns the list of previewers
     */
    function getPreviewers() {
      return previewerService.getPreviewers().then(function (result) {
        if (result.success) {
          return result.data.previewers;
        }
      });
    }

    /**
     * Get the list of response generator versions from previewerService.
     *
     * @returns the list of response generator versions
     */
    function getRespGenVersions() {
      return previewerService.getResponseGenVersions().then(function (result) {
        if (result.success) {
          viewModel.respGenVersions = result.data.respGenVersions;
          viewModel.selectedRespGenVersion = result.data.respGenVersions[0];
          return result.data.respGenVersions;
        }
      });
    }

    function downloadSelectedRespGenVersion() {
      viewModel.clearAlertMessage();
      viewModel.downloading = true;
      previewerService.getSelectedRespGenVersion(viewModel.selectedRespGenVersion).then(function (result) {
        if (result.success) {
          viewModel.alertMessage = 'Response generator was downloaded/installed successfully';
          viewModel.alertSuccess = true;
          viewModel.downloading = false;
        } else {
          viewModel.alertMessage = 'Error occurred while downloading file from Maven Repository';
          viewModel.alertSuccess = false;
          viewModel.downloading = false;
        }
      });
    }
  }
})();
