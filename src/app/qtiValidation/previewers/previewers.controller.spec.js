'use strict';

describe('previewers controller', function () {
  var controller;
  var previewerService;
  var permissionsService;
  var deferred;
  var deferredResponseGenVersions;
  var deferredPermissionsService;
  var $rootScope;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, $q, _$rootScope_) {
    previewerService = jasmine.createSpyObj('previewerService', ['getPreviewers', 'getResponseGenVersions']);
    permissionsService = jasmine.createSpyObj('permissionsService', ['isAdmin']);

    deferred = $q.defer();
    deferredResponseGenVersions = $q.defer();
    deferredPermissionsService = $q.defer();

    previewerService.getPreviewers.and.returnValue(deferred.promise);

    previewerService.getResponseGenVersions.and.callFake(function () {
      return deferredResponseGenVersions.promise;
    });

    permissionsService.isAdmin.and.callFake(function () {
      return deferredPermissionsService.promise;
    });

    deferredResponseGenVersions.resolve({success: true, data: {respGenVersions: [3, 2, 1]}});
    deferredPermissionsService.resolve({success: true, data: {admin: true}});

    $rootScope = _$rootScope_;
    controller = $controller('PreviewersController', {
      previewerService: previewerService,
      permissionsService: permissionsService
    });
  }));

  it('should set loadingPreviewers to true on initialization', function () {
    expect(controller.loadingPreviewers).toBe(true);
  });

  it('should set loadingPreviewers to false after initialization', function () {
    deferred.resolve({success: true, data: {previewers: []}});
    $rootScope.$digest();
    expect(controller.loadingPreviewers).toBe(false);
  });

  it('should set the list of previewers to an empty array on initialization', function () {
    expect(controller.previewers).toEqual([]);
  });

  it('should call previewerService to get the list of previewers during initialization', function () {
    expect(previewerService.getPreviewers).toHaveBeenCalled();
  });

  it('should set the list of previewers after previewerService returns', function () {
    var previewerArray = [{id: 1, previewerUrl: 'previewer1.com'}, {id: 2, previewerUrl: 'previewer2.com'},
      {id: 3, previewerUrl: 'previewer3.com'}];
    deferred.resolve({success: true, data: {previewers: previewerArray}});
    $rootScope.$digest();
    expect(controller.previewers).toEqual(previewerArray);
  });
});
