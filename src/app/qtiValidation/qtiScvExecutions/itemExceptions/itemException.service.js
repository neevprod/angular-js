(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('itemExceptionService', itemExceptionService);

  itemExceptionService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function itemExceptionService($http, qtiValidationApiPrefix) {
    return {
      getItemExceptions: getItemExceptions
    };

    /**
     * Get a list of item exceptions from the server by previewer ID, tenant ID, and validated item ID.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @param qtiScvTenantExecutionId {number} The the ID of the QTI-SCV tenant execution.
     * @returns The result from the server containing the list of item exceptions.
     */
    function getItemExceptions(previewerId, tenantId, qtiScvTenantExecutionId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId + '/itemExceptions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
