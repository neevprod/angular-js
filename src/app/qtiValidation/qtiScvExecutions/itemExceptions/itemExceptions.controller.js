(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('ItemExceptionsController', ItemExceptionsController);

  ItemExceptionsController.$inject = ['$uibModalInstance', 'itemExceptionService', 'previewerId', 'tenantId',
    'qtiScvTenantExecutionId'];

  function ItemExceptionsController($uibModalInstance, itemExceptionService, previewerId, tenantId,
                                    qtiScvTenantExecutionId) {
    var viewModel = this;

    viewModel.loadingItemExceptions = true;
    viewModel.itemExceptions = [];
    viewModel.previewerId = previewerId;
    viewModel.tenantId = tenantId;
    viewModel.qtiScvTenantExecutionId = qtiScvTenantExecutionId;
    viewModel.close = close;

    activate();

    /**
     * Loads the list of item exceptions.
     */
    function activate() {
      getItemExceptions(viewModel.previewerId, viewModel.tenantId, viewModel.qtiScvTenantExecutionId)
        .then(function (itemExceptions) {
          viewModel.loadingItemExceptions = false;
          viewModel.itemExceptions = itemExceptions;
        });
    }

    /**
     * Get the list of item exceptions from the itemExceptionService.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @param qtiScvTenantExecutionId {number} The the ID of the QTI-SCV tenant execution.
     * @returns The list of item exceptions.
     */
    function getItemExceptions(previewerId, tenantId, qtiScvTenantExecutionId) {
      return itemExceptionService.getItemExceptions(previewerId, tenantId, qtiScvTenantExecutionId)
        .then(function (result) {
          if (result.success) {
            return result.data.itemExceptions;
          }
        });
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }
  }

})();
