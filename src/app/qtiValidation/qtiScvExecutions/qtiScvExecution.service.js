(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('qtiScvExecutionService', qtiScvExecutionService);

  qtiScvExecutionService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function qtiScvExecutionService($http, qtiValidationApiPrefix) {
    return {
      getQtiScvExecutions: getQtiScvExecutions
    };

    /**
     * Get a list of QTI-SCV executions by previewer ID from the server.
     *
     * @returns The result from the server containing the list of QTI-SCV executions.
     */
    function getQtiScvExecutions(previewerId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
