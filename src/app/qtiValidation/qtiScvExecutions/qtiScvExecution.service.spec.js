'use strict';

describe('QTI-SCV execution service', function () {
  var qtiScvExecutionService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  var previewerId;
  var data;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_qtiScvExecutionService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    qtiScvExecutionService = _qtiScvExecutionService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  beforeEach(function () {
    previewerId = 1;
    data = [{histExecId: 3}, {histExecId: 2}, {histExecId: 1}];
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getQtiScvExecutions', function () {
    it('should submit the qtiScvExecutions GET request properly', function () {
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions')
        .respond(200, {success: true, data: data});

      qtiScvExecutionService.getQtiScvExecutions(previewerId);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions')
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      qtiScvExecutionService.getQtiScvExecutions(previewerId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      qtiScvExecutionService.getQtiScvExecutions(previewerId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });

});
