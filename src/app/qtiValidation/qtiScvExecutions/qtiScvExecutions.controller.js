(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('QtiScvExecutionsController', QtiScvExecutionsController);

  QtiScvExecutionsController.$inject = ['$stateParams', 'previewerService', 'qtiScvExecutionService', 'memoryService',
    'itemsPerPageOptions', 'dateTimeFormat'];

  function QtiScvExecutionsController($stateParams, previewerService, qtiScvExecutionService, memoryService,
                                      itemsPerPageOptions, dateTimeFormat) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.loadingQtiScvExecutions = true;
    viewModel.qtiScvExecutionsInitialized = false;
    viewModel.qtiScvExecutions = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('qtiScvExecutions', 'perPage', viewModel.previewerId) ||
      viewModel.itemsPerPageOptions[0].value;
    viewModel.wasCompleted = wasCompleted;
    viewModel.dateTimeFormat = dateTimeFormat;

    activate();

    /**
     * Loads the previewer name and the list of QTI-SCV executions.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      getQtiScvExecutions(viewModel.previewerId).then(function (qtiScvExecutions) {
        viewModel.loadingQtiScvExecutions = false;
        viewModel.qtiScvExecutionsInitialized = true;
        viewModel.qtiScvExecutions = qtiScvExecutions;
        viewModel.displayedQtiScvExecutions = qtiScvExecutions;
      });
    }

    /**
     * Get the list of QTI-SCV executions from the qtiScvExecutionService.
     *
     * @param previewerId {number} The previewer ID.
     * @returns The list of QTI-SCV executions.
     */
    function getQtiScvExecutions(previewerId) {
      return qtiScvExecutionService.getQtiScvExecutions(previewerId).then(function (result) {
        if (result.success) {
          return result.data.qtiScvExecutions;
        }
      });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @param previewerId {number} The previewer ID.
     * @returns {string} the name of the previewer
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Returns a string "Yes" or "No" depending on whether the QTI-SCV execution was completed.
     *
     * @param qtiScvExecution The QTI-SCV execution to check.
     * @returns {string} Whether the QTI-SCV execution was completed.
     */
    function wasCompleted(qtiScvExecution) {
      if (qtiScvExecution.execCompleted) {
        return 'Yes';
      } else {
        return 'No';
      }
    }
  }

})();
