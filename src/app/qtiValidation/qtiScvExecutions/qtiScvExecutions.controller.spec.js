'use strict';

describe('QTI-SCV executions controller', function () {
  var controller;
  var $stateParams;
  var previewerService;
  var qtiScvExecutionService;
  var deferredPreviewer;
  var deferredQtiScvExecution;
  var itemsPerPageOptions;
  var dateTimeFormat;
  var $rootScope;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, _dateTimeFormat_, $q, _$rootScope_) {
    $stateParams = {previewerId: 1};

    deferredPreviewer = $q.defer();
    previewerService = jasmine.createSpyObj('previewerService', ['getPreviewer']);
    previewerService.getPreviewer.and.callFake(function (previewerId) {
      if (previewerId === 1) {
        return deferredPreviewer.promise;
      }
    });

    deferredQtiScvExecution = $q.defer();
    qtiScvExecutionService = jasmine.createSpyObj('qtiScvExecutionService', ['getQtiScvExecutions']);
    qtiScvExecutionService.getQtiScvExecutions.and.callFake(function (previewerId) {
      if (previewerId === $stateParams.previewerId) {
        return deferredQtiScvExecution.promise;
      }
    });

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    dateTimeFormat = _dateTimeFormat_;

    controller = $controller('QtiScvExecutionsController', {
      $stateParams: $stateParams, previewerService:
      previewerService, qtiScvExecutionService: qtiScvExecutionService, itemsPerPageOptions: itemsPerPageOptions,
      dateTimeFormat: dateTimeFormat
    });
  }));

  it('should set previewerId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.previewerId).toBe($stateParams.previewerId);
  });

  it('should call previewerService to get the previewer name during initialization', function () {
    // Then
    expect(previewerService.getPreviewer).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set dateTimeFormat to the dependency injected format during initialization', function () {
    // Then
    expect(controller.dateTimeFormat).toEqual(dateTimeFormat);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should set the previewer name after previewerService returns', function () {
    // Given
    var previewer = {id: 1, previewerUrl: 'previewer1.com', url: 'previewer1'};

    // When
    deferredPreviewer.resolve({success: true, data: {previewer: previewer}});
    $rootScope.$digest();

    // Then
    expect(controller.previewerName).toEqual(previewer.url);
  });

  it('should set the list of qtiScvExecutions to an empty array during initialization', function () {
    // Then
    expect(controller.qtiScvExecutions).toEqual([]);
  });

  it('should set loadingQtiScvExecutions to true during initialization', function () {
    // Then
    expect(controller.loadingQtiScvExecutions).toEqual(true);
  });

  it('should call qtiScvExecutionService to get the list of qtiScvExecutions during initialization', function () {
    // Then
    expect(qtiScvExecutionService.getQtiScvExecutions).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set the list of qtiScvExecutions after qtiScvExecutionService returns', function () {
    // Given
    var qtiScvExecutions = [{histExecId: 3}, {histExecId: 2}, {histExecId: 1}];

    // When
    deferredQtiScvExecution.resolve({success: true, data: {qtiScvExecutions: qtiScvExecutions}});
    $rootScope.$digest();

    // Then
    expect(controller.qtiScvExecutions).toEqual(qtiScvExecutions);
    expect(controller.displayedQtiScvExecutions).toEqual(qtiScvExecutions);
  });

  it('should set loadingQtiScvExecutions to false after qtiScvExecutionService returns', function () {
    // Given
    var qtiScvExecutions = [{histExecId: 3}, {histExecId: 2}, {histExecId: 1}];

    // When
    deferredQtiScvExecution.resolve({success: true, data: {qtiScvExecutions: qtiScvExecutions}});
    $rootScope.$digest();

    // Then
    expect(controller.loadingQtiScvExecutions).toEqual(false);
  });

  describe('wasCompleted', function () {
    it('should return "Yes" if the qtiScvExecution was completed', function () {
      // Given
      var qtiScvExecution = {
        histExecId: 3,
        execStart: 1441730410000,
        execStop: 1441730412000,
        execCompleted: true,
        tenantLevelExec: false
      };

      // When
      var result = controller.wasCompleted(qtiScvExecution);

      // Then
      expect(result).toBe('Yes');
    });

    it('should return "No" if the item qtiScvExecution was not completed', function () {
      // Given
      var qtiScvExecution = {
        histExecId: 3,
        execStart: 1441730410000,
        execStop: 1441730412000,
        execCompleted: false,
        tenantLevelExec: false
      };

      // When
      var result = controller.wasCompleted(qtiScvExecution);

      // Then
      expect(result).toBe('No');
    });
  });

});
