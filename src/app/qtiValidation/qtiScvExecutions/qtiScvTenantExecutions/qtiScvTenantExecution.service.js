(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('qtiScvTenantExecutionService', qtiScvTenantExecutionService);

  qtiScvTenantExecutionService.$inject = ['$http', 'qtiValidationApiPrefix', 'notificationsService'];

  function qtiScvTenantExecutionService($http, qtiValidationApiPrefix, notificationsService) {

    return {
      getQtiScvTenantExecutions: getQtiScvTenantExecutions,
      getQtiScvTenantExecution: getQtiScvTenantExecution,
      getQtiScvTenantExecutionsByTenant: getQtiScvTenantExecutionsByTenant,
      startQtiScvJobForTenant: startQtiScvJobForTenant,
      getQtiScvJobStatusForTenant: getQtiScvJobStatusForTenant
    };

    /**
     * Get a list of QTI-SCV tenant executions from the server by previewer ID and QTI-SCV execution ID.
     *
     * @param previewerId {number} The previewer ID.
     * @param qtiScvExecutionId {number} The QTI-SCV execution ID.
     * @returns The result from the server containing the list of QTI-SCV executions.
     */
    function getQtiScvTenantExecutions(previewerId, qtiScvExecutionId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of QTI-SCV tenant executions from the server by previewer ID and tenant ID.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @returns The result from the server containing the list of QTI-SCV executions.
     */
    function getQtiScvTenantExecutionsByTenant(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' +
        tenantId + '/qtiScvTenantExecutions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function startQtiScvJobForTenant(previewerId, previewerName, tenantId, tenantName) {
      return $http.post(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' +
        tenantId + '/qtiScvJob')
        .then(function (result) {
          if (result.data.success) {
            notificationsService.notifyNewQtiScvJob(result.data.data.jobId, previewerId, previewerName,
              tenantId, tenantName);
          }
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getQtiScvJobStatusForTenant(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' +
        tenantId + '/qtiScvJob')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a QTI-SCV tenant execution from the server by its ID for the given tenant.
     *
     * @param previewerId {number} The previewer ID.
     * @param qtiScvExecutionId {number} The QTI-SCV execution ID.
     * @param qtiScvTenantExecutionId {number} The QTI-SCV tenant execution ID.
     * @returns The result from the server containing the QTI-SCV execution.
     */
    function getQtiScvTenantExecution(previewerId, qtiScvExecutionId, qtiScvTenantExecutionId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
