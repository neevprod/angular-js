'use strict';

describe('QTI-SCV tenant execution service', function () {
  var qtiScvTenantExecutionService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  var previewerId;
  var qtiScvExecutionId;
  var qtiScvTenantExecutionId;
  var data;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_qtiScvTenantExecutionService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    qtiScvTenantExecutionService = _qtiScvTenantExecutionService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  beforeEach(function () {
    previewerId = 1;
    qtiScvExecutionId = 13;
    qtiScvTenantExecutionId = 5;
    data = [{histTenantId: 1}, {histTenantId: 2}, {histTenantId: 3}];
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getQtiScvTenantExecutions', function () {
    it('should submit the qtiScvExecutions GET request properly', function () {
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions')
        .respond(200, {success: true, data: data});

      qtiScvTenantExecutionService.getQtiScvTenantExecutions(previewerId, qtiScvExecutionId);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions')
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      qtiScvTenantExecutionService.getQtiScvTenantExecutions(previewerId, qtiScvExecutionId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      qtiScvTenantExecutionService.getQtiScvTenantExecutions(previewerId, qtiScvExecutionId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });

  describe('getQtiScvTenantExecution', function () {
    it('should submit the qtiScvExecution GET request properly', function () {
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId)
        .respond(200, {success: true, data: data});

      qtiScvTenantExecutionService.getQtiScvTenantExecution(previewerId, qtiScvExecutionId,
        qtiScvTenantExecutionId);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId)
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      qtiScvTenantExecutionService.getQtiScvTenantExecution(previewerId, qtiScvExecutionId,
        qtiScvTenantExecutionId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/qtiScvExecutions/' +
        qtiScvExecutionId + '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId)
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      qtiScvTenantExecutionService.getQtiScvTenantExecution(previewerId, qtiScvExecutionId,
        qtiScvTenantExecutionId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });

});
