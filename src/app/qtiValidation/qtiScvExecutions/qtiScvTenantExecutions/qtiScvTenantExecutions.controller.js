(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('QtiScvTenantExecutionsController', QtiScvTenantExecutionsController);

  QtiScvTenantExecutionsController.$inject = ['$stateParams', 'previewerService', 'qtiScvTenantExecutionService',
    'memoryService', 'itemsPerPageOptions', '$uibModal'];

  function QtiScvTenantExecutionsController($stateParams, previewerService, qtiScvTenantExecutionService,
                                            memoryService, itemsPerPageOptions, $uibModal) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.qtiScvExecutionId = $stateParams.qtiScvExecutionId;
    viewModel.loadingQtiScvTenantExecutions = true;
    viewModel.qtiScvTenantExecutionsInitialized = false;
    viewModel.qtiScvTenantExecutions = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('qtiScvTenantExecutions', 'perPage',
      viewModel.qtiScvExecutionId) || viewModel.itemsPerPageOptions[0].value;

    viewModel.getTotalIssues = getTotalIssues;
    viewModel.showItemExceptions = showItemExceptions;

    activate();

    /**
     * Loads the previewer name and the list of QTI-SCV executions.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      getQtiScvTenantExecutions(viewModel.previewerId, viewModel.qtiScvExecutionId)
        .then(function (qtiScvTenantExecutions) {
          viewModel.loadingQtiScvTenantExecutions = false;
          viewModel.qtiScvTenantExecutionsInitialized = true;
          viewModel.qtiScvTenantExecutions = qtiScvTenantExecutions;
          viewModel.displayedQtiScvTenantExecutions = qtiScvTenantExecutions;
        });
    }

    /**
     * Get the list of QTI-SCV tenant executions from the qtiScvTenantExecutionService.
     *
     * @param previewerId {number} The previewer ID.
     * @param qtiScvExecutionId {number} The QTI-SCV execution ID.
     * @returns The list of QTI-SCV executions.
     */
    function getQtiScvTenantExecutions(previewerId, qtiScvExecutionId) {
      return qtiScvTenantExecutionService.getQtiScvTenantExecutions(previewerId, qtiScvExecutionId)
        .then(function (result) {
          if (result.success) {
            return result.data.qtiScvTenantExecutions;
          }
        });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @param previewerId {number} The previewer ID.
     * @returns {string} the name of the previewer
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Get the total number of issues that were found in the QTI-SCV tenant execution. This is equal to the number
     * of failed test cases plus the number of item exceptions.
     *
     * @param {object} qtiScvTenantExecution - The QTI-SCV tenant execution.
     * @returns {number} The total number of issues.
     */
    function getTotalIssues(qtiScvTenantExecution) {
      return qtiScvTenantExecution.itemsWithFailedTcs + qtiScvTenantExecution.itemExceptions;
    }

    /**
     * Show a modal with the list of item exceptions for the given QTI-SCV tenant execution and tenant.
     *
     * @param qtiScvTenantExecutionId {number} The ID of the QTI-SCV tenant execution.
     * @param tenantId {number} The ID of the tenant.
     */
    function showItemExceptions(qtiScvTenantExecutionId, tenantId) {
      $uibModal.open({
        templateUrl: 'app/qtiValidation/qtiScvExecutions/itemExceptions/itemExceptions.html',
        controller: 'ItemExceptionsController as itemExceptionsCtrl',
        resolve: {
          previewerId: function () {
            return viewModel.previewerId;
          },
          tenantId: function () {
            return tenantId;
          },
          qtiScvTenantExecutionId: function () {
            return qtiScvTenantExecutionId;
          }
        },
        size: 'lg'
      });
    }
  }

})();
