'use strict';

describe('QTI-SCV tenant executions controller', function () {
  var controller;
  var $stateParams;
  var previewerService;
  var qtiScvTenantExecutionService;
  var deferredPreviewer;
  var deferredQtiScvTenantExecution;
  var itemsPerPageOptions;
  var dateTimeFormat;
  var $rootScope;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, _dateTimeFormat_, $q, _$rootScope_) {
    $stateParams = {previewerId: 1, qtiScvExecutionId: 13};

    deferredPreviewer = $q.defer();
    previewerService = jasmine.createSpyObj('previewerService', ['getPreviewer']);
    previewerService.getPreviewer.and.callFake(function (previewerId) {
      if (previewerId === 1) {
        return deferredPreviewer.promise;
      }
    });

    deferredQtiScvTenantExecution = $q.defer();
    qtiScvTenantExecutionService = jasmine.createSpyObj('qtiScvTenantExecutionService',
      ['getQtiScvTenantExecutions']);
    qtiScvTenantExecutionService.getQtiScvTenantExecutions.and.callFake(function (previewerId, qtiScvExecutionId) {
      if (previewerId === $stateParams.previewerId && qtiScvExecutionId === $stateParams.qtiScvExecutionId) {
        return deferredQtiScvTenantExecution.promise;
      }
    });

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    dateTimeFormat = _dateTimeFormat_;

    controller = $controller('QtiScvTenantExecutionsController', {
      $stateParams: $stateParams, previewerService:
      previewerService, qtiScvTenantExecutionService: qtiScvTenantExecutionService,
      itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set previewerId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.previewerId).toBe($stateParams.previewerId);
  });

  it('should set qtiScvExecutionId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.qtiScvExecutionId).toBe($stateParams.qtiScvExecutionId);
  });

  it('should call previewerService to get the previewer name during initialization', function () {
    // Then
    expect(previewerService.getPreviewer).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should set the previewer name after previewerService returns', function () {
    // Given
    var previewer = {id: 1, previewerUrl: 'previewer1.com', url: 'previewer1'};

    // When
    deferredPreviewer.resolve({success: true, data: {previewer: previewer}});
    $rootScope.$digest();

    // Then
    expect(controller.previewerName).toEqual(previewer.url);
  });

  it('should set the list of qtiScvTenantExecutions to an empty array during initialization', function () {
    // Then
    expect(controller.qtiScvTenantExecutions).toEqual([]);
  });

  it('should set loadingQtiScvTenantExecutions to true during initialization', function () {
    // Then
    expect(controller.loadingQtiScvTenantExecutions).toEqual(true);
  });

  it('should call qtiScvTenantExecutionService to get the list of qtiScvTenantExecutions during initialization', function () {
    // Then
    expect(qtiScvTenantExecutionService.getQtiScvTenantExecutions)
      .toHaveBeenCalledWith($stateParams.previewerId, $stateParams.qtiScvExecutionId);
  });

  it('should set the list of qtiScvTenantExecutions after qtiScvTenantExecutionService returns', function () {
    // Given
    var qtiScvTenantExecutions = [{histTenantId: 3}, {histTenantId: 2}, {histTenantId: 1}];

    // When
    deferredQtiScvTenantExecution.resolve({
      success: true,
      data: {qtiScvTenantExecutions: qtiScvTenantExecutions}
    });
    $rootScope.$digest();

    // Then
    expect(controller.qtiScvTenantExecutions).toEqual(qtiScvTenantExecutions);
    expect(controller.displayedQtiScvTenantExecutions).toEqual(qtiScvTenantExecutions);
  });

  it('should set loadingQtiScvTenantExecutions to false after qtiScvTenantExecutionService returns', function () {
    // Given
    var qtiScvTenantExecutions = [{histTenantId: 3}, {histTenantId: 2}, {histTenantId: 1}];

    // When
    deferredQtiScvTenantExecution.resolve({
      success: true,
      data: {qtiScvTenantExecutions: qtiScvTenantExecutions}
    });
    $rootScope.$digest();

    // Then
    expect(controller.loadingQtiScvTenantExecutions).toEqual(false);
  });

  describe('getTotalIssues', function () {
    it('should return the number of failed test cases plus the number of item exceptions', function () {
      // Given
      var qtiScvTenantExecution = {
        itemsWithFailedTcs: 4,
        itemExceptions: 3
      };

      // When
      var totalIssues = controller.getTotalIssues(qtiScvTenantExecution);

      // Then
      expect(totalIssues).toBe(7);
    });
  });

});
