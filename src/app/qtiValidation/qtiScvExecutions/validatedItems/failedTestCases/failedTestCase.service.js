(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('failedTestCaseService', failedTestCaseService);

  failedTestCaseService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function failedTestCaseService($http, qtiValidationApiPrefix) {
    return {
      getFailedTestCases: getFailedTestCases
    };

    /**
     * Get a list of failed test cases from the server by previewer ID, tenant ID, and validated item ID.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @param validatedItemId {number} The the ID of the validated item.
     * @returns The result from the server containing the list of failed test cases.
     */
    function getFailedTestCases(previewerId, tenantId, validatedItemId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/validatedItems/' + validatedItemId + '/failedTestCases')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
