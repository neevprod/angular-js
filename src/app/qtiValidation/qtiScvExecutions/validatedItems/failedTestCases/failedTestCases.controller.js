(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('FailedTestCasesController', FailedTestCasesController);

  FailedTestCasesController.$inject = ['$uibModalInstance', 'failedTestCaseService', 'permissionsService',
    'previewerId', 'previewerName', 'tenantId', 'tenantName', 'itemId', 'itemName', 'validatedItemId'];

  function FailedTestCasesController($uibModalInstance, failedTestCaseService, permissionsService, previewerId,
                                     previewerName, tenantId, tenantName, itemId, itemName, validatedItemId) {
    var viewModel = this;

    viewModel.loadingFailedTestCases = true;
    viewModel.failedTestCases = [];
    viewModel.previewerId = previewerId;
    viewModel.previewerName = previewerName;
    viewModel.tenantId = tenantId;
    viewModel.tenantName = tenantName;
    viewModel.itemId = itemId;
    viewModel.itemName = itemName;
    viewModel.validatedItemId = validatedItemId;
    viewModel.hasTestCasesReadAccess = permissionsService.hasTenantLevelPermission('testCases', 'readAccess',
      'qtiValidation', previewerId, tenantId);

    viewModel.close = close;
    viewModel.showMods = showMods;
    viewModel.showOutcomes = showOutcomes;

    activate();

    /**
     * Loads the list of failed test cases.
     */
    function activate() {
      getFailedTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.validatedItemId)
        .then(function (failedTestCases) {
          viewModel.loadingFailedTestCases = false;
          viewModel.failedTestCases = failedTestCases;
        });
    }

    /**
     * Get the list of failed test cases from the failedTestCaseService.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @param validatedItemId {number} The validated item ID.
     * @returns The list of failed test cases.
     */
    function getFailedTestCases(previewerId, tenantId, validatedItemId) {
      return failedTestCaseService.getFailedTestCases(previewerId, tenantId, validatedItemId)
        .then(function (result) {
          if (result.success) {
            return result.data.failedTestCases;
          }
        });
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    /**
     * Returns an array of more human-readable versions of the given mods.
     *
     * @param modsString The mods to display, in string form.
     * @returns {Array} An array of strings representing the mods.
     */
    function showMods(modsString) {
      if (modsString) {
        var mods = JSON.parse(modsString);
        var result = [];
        for (var i = 0; i < mods.length; i++) {
          result.push(mods[i].did + ': ' + JSON.stringify(mods[i].r));
        }
        return result;
      } else {
        return [];
      }
    }

    /**
     * Returns an array of more human-readable versions of the given outcomes.
     *
     * @param outcomesString The outcomes to display, in string form.
     * @returns {Array} An array of strings representing the outcomes.
     */
    function showOutcomes(outcomesString) {
      if (outcomesString) {
        var outcomes = JSON.parse(outcomesString);
        var result = [];
        for (var i = 0; i < outcomes.length; i++) {
          if (outcomes[i].identifier) {
            result.push(outcomes[i].identifier + ': ' + JSON.stringify(outcomes[i].value));
          } else {
            result.push(outcomes[i].key + ': ' + JSON.stringify(outcomes[i].value));
          }
        }
        return result;
      } else {
        return [];
      }
    }
  }

})();
