(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('validatedItemService', validatedItemService);

  validatedItemService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function validatedItemService($http, qtiValidationApiPrefix) {
    return {
      getValidatedItems: getValidatedItems
    };

    /**
     * Get a list of validated items from the server by previewer ID, tenant ID, and QTI-SCV tenant execution ID.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @param qtiScvTenantExecutionId {number} The QTI-SCV tenant execution ID.
     * @returns The result from the server containing the list of validated items.
     */
    function getValidatedItems(previewerId, tenantId, qtiScvTenantExecutionId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId + '/validatedItems')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
