'use strict';

describe('QTI-SCV validated item service', function () {
  var validatedItemService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  var previewerId;
  var tenantId;
  var qtiScvTenantExecutionId;
  var data;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_validatedItemService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    validatedItemService = _validatedItemService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  beforeEach(function () {
    previewerId = 1;
    tenantId = 3;
    qtiScvTenantExecutionId = 5;
    data = [{histItemId: 1}, {histItemId: 2}, {histItemId: 3}];
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getValidatedItems', function () {
    it('should submit the validated items GET request properly', function () {
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId + '/validatedItems')
        .respond(200, {success: true, data: data});

      validatedItemService.getValidatedItems(previewerId, tenantId, qtiScvTenantExecutionId);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId + '/validatedItems')
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      validatedItemService.getValidatedItems(previewerId, tenantId, qtiScvTenantExecutionId)
        .then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/qtiScvTenantExecutions/' + qtiScvTenantExecutionId + '/validatedItems')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      validatedItemService.getValidatedItems(previewerId, tenantId, qtiScvTenantExecutionId)
        .then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });

});
