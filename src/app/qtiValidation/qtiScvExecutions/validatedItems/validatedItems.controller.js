(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('ValidatedItemsController', ValidatedItemsController);

  ValidatedItemsController.$inject = ['$stateParams', '$window', 'previewerService', 'qtiScvTenantExecutionService',
    'validatedItemService', 'permissionsService', 'memoryService', '$uibModal', 'itemsPerPageOptions'];

  function ValidatedItemsController($stateParams, $window, previewerService, qtiScvTenantExecutionService,
                                    validatedItemService, permissionsService, memoryService, $uibModal,
                                    itemsPerPageOptions) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.qtiScvExecutionId = $stateParams.qtiScvExecutionId;
    viewModel.qtiScvTenantExecutionId = $stateParams.qtiScvTenantExecutionId;
    viewModel.loadingValidatedItems = true;
    viewModel.validatedItemsInitialized = false;
    viewModel.validatedItems = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('validatedItems', 'perPage',
      viewModel.qtiScvTenantExecutionId) || viewModel.itemsPerPageOptions[0].value;
    viewModel.showFailedTestCases = showFailedTestCases;

    activate();

    /**
     * Loads the previewer name and the list of QTI-SCV executions.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      }
      previewerService.getPreviewer(viewModel.previewerId).then(function (result) {
        if (result.success) {
          viewModel.previewerName = result.data.previewer.url;
          viewModel.previewerProtocol = result.data.previewer.protocol;
        }
      });

      if ($stateParams.fromDashboard === true) {
        viewModel.fromDashboard = true;
        $window.localStorage.setItem('validatedItemsFromDashboard', 'true');
      } else if ($stateParams.fromDashboard === false) {
        viewModel.fromDashboard = false;
        $window.localStorage.setItem('validatedItemsFromDashboard', 'false');
      } else if ($window.localStorage.getItem('validatedItemsFromDashboard') === 'true') {
        viewModel.fromDashboard = true;
      } else {
        viewModel.fromDashboard = false;
      }

      if ($stateParams.tenantId && $stateParams.tenantName) {
        viewModel.tenantId = $stateParams.tenantId;
        viewModel.tenantName = $stateParams.tenantName;
        viewModel.hasTestCasesReadPermission = permissionsService.hasTenantLevelPermission('testCases',
          'readAccess', 'qtiValidation', viewModel.previewerId, viewModel.tenantId);

        getValidatedItems(viewModel.previewerId, viewModel.tenantId, viewModel.qtiScvTenantExecutionId)
          .then(function (validatedItems) {
            viewModel.loadingValidatedItems = false;
            viewModel.validatedItemsInitialized = true;
            viewModel.validatedItems = validatedItems;
            viewModel.displayedValidatedItems = validatedItems;
          });
      } else {
        getQtiScvTenantExecution(viewModel.previewerId, viewModel.qtiScvExecutionId,
          viewModel.qtiScvTenantExecutionId)
          .then(function (qtiScvTenantExecution) {
            if (qtiScvTenantExecution) {
              viewModel.tenantId = qtiScvTenantExecution.previewerTenant.tenantId;
              viewModel.tenantName = qtiScvTenantExecution.previewerTenant.name;
              viewModel.hasTestCasesReadPermission = permissionsService.hasTenantLevelPermission('testCases',
                'readAccess', 'qtiValidation', viewModel.previewerId, viewModel.tenantId);
            }

            getValidatedItems(viewModel.previewerId, viewModel.tenantId, viewModel.qtiScvTenantExecutionId)
              .then(function (validatedItems) {
                viewModel.loadingValidatedItems = false;
                viewModel.validatedItemsInitialized = true;
                viewModel.validatedItems = validatedItems;
                viewModel.displayedValidatedItems = validatedItems;
              });
          });
      }
    }

    /**
     * Get the list of validated items from the validatedItemsService.
     *
     * @param previewerId {number} The previewer ID.
     * @param tenantId {number} The tenant ID.
     * @param qtiScvTenantExecutionId {number} The QTI-SCV tenant execution ID.
     * @returns The list of QTI-SCV executions.
     */
    function getValidatedItems(previewerId, tenantId, qtiScvTenantExecutionId) {
      return validatedItemService.getValidatedItems(previewerId, tenantId, qtiScvTenantExecutionId)
        .then(function (result) {
          if (result.success) {
            return result.data.validatedItems;
          }
        });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @param previewerId {number} The previewer ID.
     * @param qtiScvExecutionId {number} The ID of the QTI-SCV execution.
     * @param qtiScvTenantExecutionId {number} The ID of the QTI-SCV tenant execution.
     * @returns {string} the name of the previewer
     */
    function getQtiScvTenantExecution(previewerId, qtiScvExecutionId, qtiScvTenantExecutionId) {
      return qtiScvTenantExecutionService.getQtiScvTenantExecution(previewerId, qtiScvExecutionId,
        qtiScvTenantExecutionId)
        .then(function (result) {
          if (result.success) {
            return result.data.qtiScvTenantExecution;
          }
        });
    }

    /**
     * Show a modal with the list of failed test cases for the given validated item.
     *
     * @param itemId {number} The ID of the item.
     * @param itemName {string} The name of the item.
     * @param validatedItemId {number} The ID of the validated item.
     */
    function showFailedTestCases(itemId, itemName, validatedItemId) {
      $uibModal.open({
        templateUrl: 'app/qtiValidation/qtiScvExecutions/validatedItems/failedTestCases/failedTestCases.html',
        controller: 'FailedTestCasesController as failedTestCasesCtrl',
        resolve: {
          previewerId: function () {
            return viewModel.previewerId;
          },
          previewerName: function () {
            return viewModel.previewerName;
          },
          tenantId: function () {
            return viewModel.tenantId;
          },
          tenantName: function () {
            return viewModel.tenantName;
          },
          itemId: function () {
            return itemId;
          },
          itemName: function () {
            return itemName;
          },
          validatedItemId: function () {
            return validatedItemId;
          }
        },
        size: 'lg'
      });
    }
  }

})();
