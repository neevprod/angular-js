(function () {

  'use strict';

  angular.module('systemValidation').controller('QtiScvTenantJobSubscriptionController', QtiScvTenantJobSubscriptionController);

  QtiScvTenantJobSubscriptionController.$inject = ['$uibModalInstance', 'tenant', 'qtiScvTenantJobSubscriptionService'];

  function QtiScvTenantJobSubscriptionController($uibModalInstance, tenant, qtiScvTenantJobSubscriptionService) {
    var viewModel = this;
    viewModel.alertSuccess = false;
    viewModel.loading = false;
    viewModel.tenant = tenant;
    viewModel.subscriptionType = (tenant.subscribed === 'yes' ? 'unsubscribe from' : 'subscribe to');
    viewModel.manageSubscription = manageSubscription;
    viewModel.close = close;


    /**
     *  Sends subscription request if the tenant is not subscribed otherwise sends unsubscription request.
     * @param tenant The information of a tenant which includes tenantId, previewerId and tenant is subscribed or
     * not.
     */
    function manageSubscription(tenant) {
      viewModel.loading = true;
      if (tenant.subscribed === 'yes') {
        qtiScvTenantJobSubscriptionService.removeQtiScvTenantJobSubscription(tenant.previewerId, tenant.tenantId).then(
          function (result) {
            if (result.success) {
              viewModel.alertSuccess = true;
              viewModel.loading = false;
              close();
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });
      } else {
        qtiScvTenantJobSubscriptionService.createQtiScvTenantJobSubscription(tenant.previewerId, tenant.tenantId).then(
          function (result) {
            if (result.success) {
              viewModel.alertSuccess = true;
              viewModel.loading = false;
              close();
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }

})();
