(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('qtiScvTenantJobSubscriptionService', qtiScvTenantJobSubscriptionService);

  qtiScvTenantJobSubscriptionService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function qtiScvTenantJobSubscriptionService($http, qtiValidationApiPrefix) {
    return {
      createQtiScvTenantJobSubscription: createQtiScvTenantJobSubscription,
      removeQtiScvTenantJobSubscription: removeQtiScvTenantJobSubscription
    };

    /**
     * Create QtiScvTenantJobSubscription for the given tenant under the given previewer.
     *
     * @returns A boolean value indicating whether the subscription was created successfully.
     */
    function createQtiScvTenantJobSubscription(previewerId, tenantId) {
      return $http.post(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/qtiScvTenantJobSubscription')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Removes the QtiScvTenantJobSubscription for the given tenant under the given previewer.
     *
     * @returns A boolean value indicating whether the subscription was removed successfully.
     */
    function removeQtiScvTenantJobSubscription(previewerId, tenantId) {
      return $http.delete(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/qtiScvTenantJobSubscription')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
