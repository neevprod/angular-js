'use strict';

describe('qtiScvTenantJobSubscription service', function () {
  var qtiScvTenantJobSubscriptionService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_qtiScvTenantJobSubscriptionService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    qtiScvTenantJobSubscriptionService = _qtiScvTenantJobSubscriptionService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('createQtiScvTenantJobSubscription', function () {
    var previewerId = 1;
    var tenantId = 1;
    it('should submit the qtiScvTenantJobSubscription POST request properly', function () {

      $httpBackend.expectPOST(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/qtiScvTenantJobSubscription')
        .respond(200);

      qtiScvTenantJobSubscriptionService.createQtiScvTenantJobSubscription(previewerId, tenantId);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      $httpBackend.whenPOST(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/qtiScvTenantJobSubscription')
        .respond(200, {success: true});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
      });
      qtiScvTenantJobSubscriptionService.createQtiScvTenantJobSubscription(previewerId, tenantId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.whenPOST(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/qtiScvTenantJobSubscription')
        .respond(400, {success: false, errors: [{message: 'Error message.'}]});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });
      qtiScvTenantJobSubscriptionService.createQtiScvTenantJobSubscription(previewerId, tenantId).then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });
});
