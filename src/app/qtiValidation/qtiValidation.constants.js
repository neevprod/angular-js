(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .constant('qtiValidationApiPrefix', '/api/qtiValidation/v1/');

})();
