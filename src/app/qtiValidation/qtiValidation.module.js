(function () {

  'use strict';

  angular.module('qtiValidation', ['core'])
    .config(configure);

  configure.$inject = ['$stateProvider'];

  function configure($stateProvider) {
    $stateProvider
      .state('previewers', {
        parent: 'main',
        url: '/qtiValidation/previewers',
        templateUrl: 'app/qtiValidation/previewers/previewers.html',
        controller: 'PreviewersController as previewersCtrl'
      })
      .state('tenants', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants',
        params: {
          previewerName: ''
        },
        templateUrl: 'app/qtiValidation/tenants/tenants.html',
        controller: 'TenantsController as tenantsCtrl'
      })
      .state('testMaps', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/testMaps',
        params: {
          previewerName: '',
          tenantName: ''
        },
        templateUrl: 'app/qtiValidation/testMaps/testmaps.html',
        controller: 'TestMapsController as testMapsCtrl'
      })
      .state('items', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/items',
        params: {
          previewerName: '',
          tenantName: ''
        },
        templateUrl: 'app/qtiValidation/items/items.html',
        controller: 'ItemsController as itemsCtrl'
      })
      .state('testMapItems', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/testMapItems',
        params: {
          previewerName: '',
          tenantName: '',
          identifier: ''
        },
        templateUrl: 'app/qtiValidation/items/items.html',
        controller: 'ItemsController as itemsCtrl'
      })
      .state('testCases', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/items/{itemId:int}/testCases',
        params: {
          previewerName: '',
          tenantName: '',
          itemName: ''
        },
        templateUrl: 'app/qtiValidation/testCases/testCases.html',
        controller: 'TestCasesController as testCasesCtrl'
      })
      .state('playTestCases', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/items/{itemId:int}/playTestCases',
        params: {
          previewerName: '',
          tenantName: '',
          itemName: '',
          testCaseId: null
        },
        templateUrl: 'app/qtiValidation/testCases/playTestCases/playTestCases.html',
        controller: 'PlayTestCasesController as playTestCasesCtrl'
      })
      .state('testCaseUploads', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/testCaseUploads',
        params: {
          previewerName: '',
          tenantName: '',
          fromTestMapsPage: null
        },
        templateUrl: 'app/qtiValidation/testCases/testCaseUploads/testCaseUploads.html',
        controller: 'TestCaseUploadsController as testCaseUploadsCtrl'
      })
      .state('qtiScvExecutions', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/qtiScvExecutions',
        params: {
          previewerName: ''
        },
        templateUrl: 'app/qtiValidation/qtiScvExecutions/qtiScvExecutions.html',
        controller: 'QtiScvExecutionsController as qtiScvExecutionsCtrl'
      })
      .state('qtiScvTenantExecutions', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/qtiScvExecutions/{qtiScvExecutionId:int}/qtiScvTenantExecutions',
        params: {
          previewerName: ''
        },
        templateUrl: 'app/qtiValidation/qtiScvExecutions/qtiScvTenantExecutions/qtiScvTenantExecutions.html',
        controller: 'QtiScvTenantExecutionsController as qtiScvTenantExecutionsCtrl'
      })
      .state('validatedItems', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/qtiScvExecutions/{qtiScvExecutionId:int}/qtiScvTenantExecutions/' +
        '{qtiScvTenantExecutionId:int}/validatedItems',
        params: {
          previewerName: '',
          tenantId: null,
          tenantName: '',
          fromDashboard: ''
        },
        templateUrl: 'app/qtiValidation/qtiScvExecutions/validatedItems/validatedItems.html',
        controller: 'ValidatedItemsController as validatedItemsCtrl'
      })
      .state('dashboard', {
        parent: 'main',
        url: '/qtiValidation/previewers/{previewerId:int}/tenants/{tenantId:int}/dashboard',
        params: {
          previewerName: '',
          tenantName: ''
        },
        templateUrl: 'app/qtiValidation/dashboard/dashboard.html',
        controller: 'DashboardController as dashboardCtrl'
      });
  }

})();
