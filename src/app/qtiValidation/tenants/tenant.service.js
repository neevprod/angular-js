(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('tenantService', tenantService);

  tenantService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function tenantService($http, qtiValidationApiPrefix) {
    return {
      getTenants: getTenants,
      getTenant: getTenant
    };

    /**
     * Get a list of tenants by previewer ID from the server.
     *
     * @returns the result from the server containing the list of tenants
     */
    function getTenants(previewerId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a tenant by its ID.
     *
     * @param previewerId the previewer the tenant resides in
     * @param tenantId the ID of the tenant
     * @returns the result from the server containing the tenant
     */
    function getTenant(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

  }

})();
