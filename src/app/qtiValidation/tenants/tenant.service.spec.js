'use strict';

describe('tenant service', function () {
  var tenantService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_tenantService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    tenantService = _tenantService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getTenants', function () {
    it('should submit the tenants GET request properly', function () {
      // Given
      var data = [{tenantId: 1, name: 'tenant1'}, {tenantId: 2, name: 'tenant2'},
        {tenantId: 3, name: 'tenant3'}];

      // Then
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/1/tenants')
        .respond(200, {success: true, data: data});

      // When
      tenantService.getTenants(1);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{tenantId: 1, name: 'tenant1'}, {tenantId: 2, name: 'tenant2'},
        {tenantId: 3, name: 'tenant3'}];
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      tenantService.getTenants(1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      tenantService.getTenants(1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

  describe('getTenant', function () {
    it('should submit the tenants GET request properly', function () {
      // Given
      var data = {tenantId: 1, name: 'tenant1'};

      // Then
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/1/tenants/1')
        .respond(200, {success: true, data: data});

      // When
      tenantService.getTenant(1, 1);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = {tenantId: 1, name: 'tenant1'};
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants/1')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      tenantService.getTenant(1, 1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/1/tenants/1')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      tenantService.getTenant(1, 1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
