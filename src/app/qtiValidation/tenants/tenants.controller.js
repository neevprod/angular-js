(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('TenantsController', TenantsController);

  TenantsController.$inject = ['$stateParams', 'previewerService', 'tenantService',
    'qtiScvTenantJobSubscriptionService', 'permissionsService', 'memoryService', 'itemsPerPageOptions', '$uibModal'];

  function TenantsController($stateParams, previewerService, tenantService, qtiScvTenantJobSubscriptionService,
                             permissionsService, memoryService, itemsPerPageOptions, $uibModal) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.loadingTenants = true;
    viewModel.tenantsInitialized = false;
    viewModel.tenants = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('tenants', 'perPage', viewModel.previewerId) ||
      viewModel.itemsPerPageOptions[0].value;
    viewModel.showCheckboxes = permissionsService.hasAnyTenantLevelPermission('qtiScvTenantJobSubscriptions',
      'createAccess', 'qtiValidation', viewModel.previewerId);
    viewModel.showScv = permissionsService
      .hasAnyTenantLevelPermission('qtiScvExecutions', 'readAccess', 'qtiValidation', viewModel.previewerId);
    viewModel.createQtiScvTenantJobSubscriptions = createQtiScvTenantJobSubscriptions;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    viewModel.hasItemReadAccess = hasItemReadAccess;
    viewModel.hasTestMapReadAccess = hasTestMapReadAccess;
    viewModel.hasQtiScvReadAccess = hasQtiScvReadAccess;
    viewModel.disPlayConfirmationDialog = disPlayConfirmationDialog;
    viewModel.tenantSubscriptionFilter = Number(memoryService.getItem('tenants', 'tenantSubscriptionFilter', viewModel.previewerId));
    viewModel.setTenantSubscriptionFilter = setTenantSubscriptionFilter;

    activate();

    /**
     * Loads the previewer name and the list of tenants.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      getTenants(viewModel.previewerId).then(function (tenants) {
        viewModel.loadingTenants = false;
        viewModel.tenantsInitialized = true;
        viewModel.tenants = tenants;
        viewModel.displayedTenants = tenants;
      });

      setTenantSubscriptionFilter(viewModel.tenantSubscriptionFilter);
    }

    /**
     * Get the list of tenants from the tenantService and specify whether each should be selectable.
     *
     * @returns the list of tenants
     */
    function getTenants(previewerId) {
      return tenantService.getTenants(previewerId).then(function (result) {
        if (result.success) {
          var tenants = result.data.tenants;
          tenants.forEach(function (tenant) {
            tenant.isSelectable =
              permissionsService.hasTenantLevelPermission('qtiScvTenantJobSubscriptions',
                'createAccess', 'qtiValidation', previewerId, tenant.tenantId);
          });
          return tenants;
        }
      });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @returns {string} the name of the previewer
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Create new QtiScv job subscriptions for each of the selected tenants.
     */
    function createQtiScvTenantJobSubscriptions() {
      viewModel.creatingSubscriptions = true;

      var selectedTenantIds = [];
      viewModel.tenants.forEach(function (tenant) {
        if (tenant.isSelected) {
          selectedTenantIds.push(tenant.tenantId);
        }
      });

      qtiScvTenantJobSubscriptionService
        .createQtiScvTenantJobSubscriptions(viewModel.previewerId, selectedTenantIds).then(function (result) {
        viewModel.creatingSubscriptions = false;
        viewModel.tenants.forEach(function (tenant) {
          tenant.isSelected = false;
        });

        if (result.success) {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'Subscription(s) created successfully.';
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'An error occurred while creating subscription(s).';
        }
      });
    }

    /**
     * Return whether the user has read access to items for the given tenant.
     *
     * @param {number} tenantId - The tenant ID.
     * @returns {boolean} Whether the user has access.
     */
    function hasItemReadAccess(tenantId) {
      return permissionsService
        .hasTenantLevelPermission('items', 'readAccess', 'qtiValidation', viewModel.previewerId, tenantId);
    }

    /**
     * Return whether the user has read access to test maps for the given tenant.
     *
     * @param {number} tenantId - The tenant ID.
     * @returns {boolean} Whether the user has access.
     */
    function hasTestMapReadAccess(tenantId) {
      return permissionsService
        .hasTenantLevelPermission('testMaps', 'readAccess', 'qtiValidation', viewModel.previewerId, tenantId);
    }

    /**
     * Return whether the user has read access to QTI-SCV executions for the given tenant.
     *
     * @param {number} tenantId - The tenant ID.
     * @returns {boolean} Whether the user has access.
     */
    function hasQtiScvReadAccess(tenantId) {
      return permissionsService.hasTenantLevelPermission('qtiScvExecutions', 'readAccess', 'qtiValidation',
        viewModel.previewerId, tenantId);
    }

    function disPlayConfirmationDialog(tenant) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/qtiValidation/qtiScvTenantJobSubscriptions/confirmationModal.html',
        controller: 'QtiScvTenantJobSubscriptionController as tenantSubscriptionCtrl',
        backdrop: 'static',
        resolve: {
          tenant: function () {
            return tenant;
          }
        }
      });
      modalInstance.result.then(function () {
        getTenants(viewModel.previewerId).then(function (tenants) {
          viewModel.loadingTenants = false;
          viewModel.tenantsInitialized = true;
          viewModel.tenants = tenants;
          viewModel.displayedTenants = tenants;
        });
      });
    }

    /**
     * Set the tenant subscription filter to the given value and sets the search value based on the filter value.
     *
     * @param {number} value - the value to set (must be -1, 0, or 1)
     */
    function setTenantSubscriptionFilter(value) {

      if (value >= -1 && value <= 1) {
        if (value === -1) {
          viewModel.searchKey = 'no';

        } else if (value === 1) {
          viewModel.searchKey = 'yes';
        } else {
          viewModel.searchKey = '';
        }

        viewModel.tenantSubscriptionFilter = value;
        memoryService.setItem('tenants', 'tenantSubscriptionFilter', viewModel.previewerId, value);
      }

    }
  }

})();
