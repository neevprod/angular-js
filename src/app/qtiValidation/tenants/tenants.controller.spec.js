'use strict';

describe('tenants controller', function () {
  var controller;
  var $stateParams;
  var previewerService;
  var tenantService;
  var permissionsService;
  var deferredPreviewer;
  var deferredTenant;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {previewerId: 1};

    deferredPreviewer = $q.defer();
    previewerService = jasmine.createSpyObj('previewerService', ['getPreviewer']);
    previewerService.getPreviewer.and.callFake(function (previewerId) {
      if (previewerId === 1) {
        return deferredPreviewer.promise;
      }
    });

    deferredTenant = $q.defer();
    tenantService = jasmine.createSpyObj('tenantService', ['getTenants']);
    tenantService.getTenants.and.callFake(function (previewerId) {
      if (previewerId === 1) {
        return deferredTenant.promise;
      }
    });

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasAnyTenantLevelPermission',
      'hasTenantLevelPermission']);

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('TenantsController', {
      $stateParams: $stateParams, previewerService: previewerService,
      tenantService: tenantService, permissionsService: permissionsService,
      itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set previewerId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.previewerId).toBe($stateParams.previewerId);
  });

  it('should call previewerService to get the previewer name during initialization', function () {
    // Then
    expect(previewerService.getPreviewer).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should set the previewer name after previewerService returns', function () {
    // Given
    var previewer = {id: 1, previewerUrl: 'previewer1.com', url: 'previewer1'};

    // When
    deferredPreviewer.resolve({success: true, data: {previewer: previewer}});
    $rootScope.$digest();

    // Then
    expect(controller.previewerName).toEqual(previewer.url);
  });

  it('should set the list of tenants to an empty array during initialization', function () {
    // Then
    expect(controller.tenants).toEqual([]);
  });

  it('should call tenantService to get the list of tenants during initialization', function () {
    // Then
    expect(tenantService.getTenants).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set the list of tenants after tenantService returns', function () {
    // Given
    var tenantArray = [{tenantId: 1, name: 'tenant1'}, {tenantId: 2, name: 'tenant2'},
      {tenantId: 3, name: 'tenant3'}];

    // When
    deferredTenant.resolve({success: true, data: {tenants: tenantArray}});
    $rootScope.$digest();

    // Then
    expect(controller.tenants).toEqual(tenantArray);
    expect(controller.displayedTenants).toEqual(tenantArray);
  });
});
