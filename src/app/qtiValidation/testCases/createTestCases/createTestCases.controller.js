(function () {

  'use strict';

  angular.module('core')
    .controller('CreateTestCasesController', CreateTestCasesController);

  CreateTestCasesController.$inject = ['$uibModalInstance', 'testCaseService', 'previewerId', 'previewerName',
    'tenantId', 'tenantName', 'ids', 'isAtTestMapLevel'];

  function CreateTestCasesController($uibModalInstance, testCaseService, previewerId, previewerName, tenantId,
                                     tenantName, ids, isAtTestMapLevel) {
    var viewModel = this;

    viewModel.maxCorrect = 3;
    viewModel.maxIncorrect = 3;
    viewModel.jobStarted = false;
    viewModel.jobStarting = false;
    viewModel.alertMessage = '';
    viewModel.alertSuccess = false;
    viewModel.submit = submit;
    viewModel.close = close;

    /**
     * Start test case creation if the form is valid.
     *
     * @param isValid A boolean indicating whether the form is valid.
     */
    function submit(isValid) {
      if (isValid) {
        viewModel.jobStarting = true;

        if (!isAtTestMapLevel) {
          testCaseService.createTestCasesForItems(previewerId, previewerName, tenantId, tenantName, ids,
            viewModel.maxCorrect, viewModel.maxIncorrect)
            .then(function (result) {
              updateAlertMessage(result.success);
            });
        } else {
          testCaseService.createTestCasesForTestMaps(previewerId, previewerName, tenantId, tenantName, ids,
            viewModel.maxCorrect, viewModel.maxIncorrect)
            .then(function (result) {
              updateAlertMessage(result.success);
            });
        }
      }
    }

    /**
     * Update the alert message based on whether the operation was successful.
     *
     * @param {boolean} success - A boolean indicating whether the operation was successful.
     */
    function updateAlertMessage(success) {
      viewModel.jobStarting = false;
      if (success) {
        viewModel.jobStarted = true;
        viewModel.alertSuccess = true;
        viewModel.alertMessage = 'Test case creation has been successfully initialized.';
      } else {
        viewModel.alertSuccess = false;
        viewModel.alertMessage =
          'An error occurred while initializing the test case creation process.';
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }
  }

})();
