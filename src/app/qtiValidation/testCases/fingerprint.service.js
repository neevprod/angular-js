(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('fingerprintService', fingerprintService);

  fingerprintService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function fingerprintService($http, qtiValidationApiPrefix) {
    return {
      updateFingerprint: updateFingerprint
    };

    /**
     * Update the fingerprint for the given test cases.
     *
     * @returns The result from the server indicating whether the operation was successful.
     */
    function updateFingerprint(previewerId, tenantId, itemId, testCaseIds) {
      var data = {
        testCaseIds: testCaseIds
      };
      return $http.put(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/items/' + itemId + '/fingerprint', data)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
