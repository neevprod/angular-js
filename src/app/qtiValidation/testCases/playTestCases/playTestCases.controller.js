(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('PlayTestCasesController', PlayTestCasesController);

  PlayTestCasesController.$inject = ['$stateParams', 'previewerService', 'tenantService', 'itemService',
    'testCaseService', 'fingerprintService', 'permissionsService'];

  function PlayTestCasesController($stateParams, previewerService, tenantService, itemService, testCaseService,
                                   fingerprintService, permissionsService) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.tenantId = $stateParams.tenantId;
    viewModel.loadingTestCases = true;
    viewModel.testCasesInitialized = false;
    viewModel.testCases = [];
    viewModel.itemId = $stateParams.itemId;

    viewModel.hasFingerPrintUpdateAccess = permissionsService.hasTenantLevelPermission('fingerprint',
      'updateAccess', 'qtiValidation', viewModel.previewerId, viewModel.tenantId);
    viewModel.hasTestCaseUpdateAccess = permissionsService.hasTenantLevelPermission('testCases',
      'updateAccess', 'qtiValidation', viewModel.previewerId, viewModel.tenantId);
    viewModel.hasTestCaseDeleteAccess = permissionsService.hasTenantLevelPermission('testCases',
      'deleteAccess', 'qtiValidation', viewModel.previewerId, viewModel.tenantId);

    viewModel.setTestCase = setTestCase;
    viewModel.showMods = showMods;
    viewModel.showOutcomes = showOutcomes;
    viewModel.getPassFail = getPassFail;
    viewModel.updateFingerprint = updateFingerprint;
    viewModel.deleteTestCase = deleteTestCase;
    viewModel.updateTestCase = updateTestCase;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    activate();

    /**
     * Loads the data necessary to initialize the page.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      if ($stateParams.tenantName) {
        viewModel.tenantName = $stateParams.tenantName;
      } else {
        getTenantName(viewModel.previewerId, viewModel.tenantId).then(function (tenantName) {
          if (tenantName) {
            viewModel.tenantName = tenantName;
          }
        });
      }

      if ($stateParams.itemName) {
        viewModel.itemName = $stateParams.itemName;
      }

      getTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (testCases) {
        viewModel.loadingTestCases = false;
        viewModel.testCasesInitialized = true;
        viewModel.testCases = testCases;

        if (testCases.length) {
          var index = 0;
          var testCaseFound = false;
          if ($stateParams.testCaseId) {
            for (var i = 0; i < testCases.length; i++) {
              if (testCases[i].id === $stateParams.testCaseId) {
                index = i;
                testCaseFound = true;
                break;
              }
            }
            if (!testCaseFound) {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = 'The specified test case (ID ' + $stateParams.testCaseId +
                ') was not found.';
            }
          }
          showTestCaseOnInitialize(index);
        } else {
          getItemName(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (itemName) {
            if (itemName) {
              viewModel.itemName = itemName;
            }
          });
        }
      });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @returns {string} The name of the previewer.
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Get the name of the tenant with the given ID.
     *
     * @param previewerId - The ID of the previewer containing the tenant.
     * @param tenantId - The ID of the tenant.
     * @returns {string} - The name of the tenant.
     */
    function getTenantName(previewerId, tenantId) {
      return tenantService.getTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.tenant.name;
        }
      });
    }

    /**
     * Get the name of the given item under the given tenant.
     *
     * @param previewerId - The ID of the previewer containing the tenant.
     * @param tenantId - The ID of the tenant.
     * @param itemId - The ID of the item.
     * @returns {string} - The name of the tenant.
     */
    function getItemName(previewerId, tenantId, itemId) {
      return itemService.getItem(previewerId, tenantId, itemId).then(function (result) {
        if (result.success) {
          return result.data.item.identifier;
        }
      });
    }

    /**
     * Show the test case given by its index on initialization.
     *
     * @param {number} index - The index of the test case to play.
     */
    function showTestCaseOnInitialize(index) {
      viewModel.testCaseIndex = index;
      viewModel.testCase = viewModel.testCases[index];
      resetPaginationOptions();

      if ($stateParams.itemName) {
        viewModel.itemName = $stateParams.itemName;
        showTestCase();
      } else {
        getItemName(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (itemName) {
          if (itemName) {
            viewModel.itemName = itemName;
            showTestCase();
          }
        });
      }

    }

    /**
     * Set the current test case to be the one at the given index.
     *
     * @param {number} index - The index of the test case to play.
     */
    function setTestCase(index) {
      if (index >= 0 && index < viewModel.testCases.length) {
        viewModel.testCaseIndex = index;
        viewModel.testCase = viewModel.testCases[index];
        resetPaginationOptions();

        showTestCase();
      }
    }

    /**
     * Set the pagination options according to the currently selected test case.
     */
    function resetPaginationOptions() {
      var paginationOptions = [];
      if (viewModel.testCaseIndex === viewModel.testCases.length - 1 && viewModel.testCases.length > 3) {
        for (var i = viewModel.testCaseIndex - 3; i < viewModel.testCaseIndex + 1; i++) {
          paginationOptions.push(i);
        }
      } else if (viewModel.testCaseIndex - 2 >= 0) {
        for (var j = viewModel.testCaseIndex - 2; j < viewModel.testCaseIndex + 2; j++) {
          if (j < viewModel.testCases.length) {
            paginationOptions.push(j);
          }
        }
      } else {
        for (var k = 0; k < 4; k++) {
          if (k < viewModel.testCases.length) {
            paginationOptions.push(k);
          }
        }
      }
      viewModel.paginationOptions = paginationOptions;
    }

    /**
     * Show the current test case.
     */
    function showTestCase() {
      testCaseService.getShowTestCaseUrl(viewModel.previewerId, viewModel.tenantId)
        .then(function (result) {
          if (result.success) {
            var url = result.data.showTestCaseUrl;
            var responses = viewModel.testCases[viewModel.testCaseIndex].mods;
            for (var i = 0; i < responses.length; i++) {
              if (responses[i].did) {
                responses[i].identifier = responses[i].did;
                delete responses[i].did;
              }
              if (responses[i].r) {
                responses[i].value = responses[i].r;
                delete responses[i].r;
              }
            }
            var strResponses =
              '"' + JSON.stringify(responses).replace(/\\/g, '\\\\').replace(/"/g, '\\\"') + '"';
            viewModel.showItem(url, viewModel.itemName, strResponses);
          }
        });
    }

    /**
     * Get the list of test cases.
     *
     * @param {number} previewerId - The ID of the previewer the tenant belongs to.
     * @param {number} tenantId - The ID of the tenant the item belongs to.
     * @param {string} itemId - The ID of the item.
     * @returns A promise that returns the list of tenants.
     */
    function getTestCases(previewerId, tenantId, itemId) {
      return testCaseService.getTestCases(previewerId, tenantId, itemId, true).then(function (result) {
        if (result.success) {
          return result.data.testCases;
        }
      });
    }

    /**
     * Returns an array of more human-readable versions of the given mods.
     *
     * @param mods The mods to show.
     * @returns {Array} An array of strings representing the mods.
     */
    function showMods(mods) {
      var result = [];
      for (var i = 0; i < mods.length; i++) {
        var identifier;
        var value;

        if (mods[i].did) {
          identifier = mods[i].did;
        } else {
          identifier = mods[i].identifier;
        }

        if (mods[i].r) {
          value = mods[i].r;
        } else {
          value = mods[i].value;
        }

        result.push(identifier + ': ' + JSON.stringify(value));
      }
      return result;
    }

    /**
     * Returns an array of more human-readable versions of the given outcomes.
     *
     * @param outcomes The outcomes.
     * @returns {Array} An array of strings representing the outcomes.
     */
    function showOutcomes(outcomes) {
      var result = [];
      if (outcomes) {
        for (var i = 0; i < outcomes.length; i++) {
          result.push(outcomes[i].identifier + ': ' + JSON.stringify(outcomes[i].value));
        }
      }
      return result;
    }

    /**
     * Returns a string indicating whether the test case passed or failed.
     *
     * @param testCase The test case.
     * @returns {string} A string indicating whether the test case passed.
     */
    function getPassFail(testCase) {
      if (testCase.passed) {
        return 'Pass';
      } else {
        return 'Fail';
      }
    }

    /**
     * Updates the fingerprint for the current test case.
     */
    function updateFingerprint() {
      viewModel.loadingTestCases = true;
      fingerprintService.updateFingerprint(viewModel.previewerId, viewModel.tenantId, viewModel.itemId,
        [viewModel.testCase.id]).then(function (result) {
        refreshDataWithMessage(result.success, 'Test case fingerprinted successfully.',
          'An error occurred while fingerprinting the test case.');
      });
    }

    /**
     * Deletes the current test case.
     */
    function deleteTestCase() {
      viewModel.loadingTestCases = true;
      testCaseService.deleteItemTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.itemId,
        [viewModel.testCase.id]).then(function (result) {
        refreshDataWithMessage(result.success, 'Test case deleted successfully.',
          'An error occurred while deleting the test case.');
      });
    }

    /**
     * Updates the current test case to match the actual outcomes.
     */
    function updateTestCase() {
      viewModel.loadingTestCases = true;
      testCaseService.updateTestCase(viewModel.previewerId, viewModel.tenantId, viewModel.itemId,
        viewModel.testCase.id).then(function (result) {
        refreshDataWithMessage(result.success, 'New outcomes accepted successfully.',
          'An error occurred while accepting new outcomes.');
      });
    }

    /**
     * Refreshes the test case data and sets an alert message depending on whether an operation was successful.
     *
     * @param {boolean} success - A boolean indicating whether the operation was successful.
     * @param {string} successMessage - The success message.
     * @param {string} failMessage - The failure message.
     */
    function refreshDataWithMessage(success, successMessage, failMessage) {
      if (success) {
        getTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (testCases) {
          viewModel.loadingTestCases = false;
          viewModel.alertSuccess = true;
          viewModel.alertMessage = successMessage;

          viewModel.testCases = testCases;
          if (testCases.length) {
            if (viewModel.testCaseIndex >= testCases.length) {
              viewModel.testCaseIndex = testCases.length - 1;
            }
            showTestCaseOnInitialize(viewModel.testCaseIndex);
          }
        });
      } else {
        viewModel.loadingTestCases = false;
        viewModel.alertSuccess = false;
        viewModel.alertMessage = failMessage;

        showTestCase();
      }
    }
  }

})();
