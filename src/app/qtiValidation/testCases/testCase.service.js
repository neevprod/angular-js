(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('testCaseService', testCaseService);

  testCaseService.$inject = ['$http', 'qtiValidationApiPrefix', 'notificationsService'];

  function testCaseService($http, qtiValidationApiPrefix, notificationsService) {
    return {
      getTestCases: getTestCases,
      deleteTestCases: deleteTestCases,
      updateTestCase: updateTestCase,
      getShowTestCaseUrl: getShowTestCaseUrl,
      downloadTestCasesForTenant: downloadTestCasesForTenant,
      downloadTestCaseCoverageForTenant: downloadTestCaseCoverageForTenant,
      downloadTestCasesForItems: downloadTestCasesForItems,
      downloadTestCaseCoverageForItems: downloadTestCaseCoverageForItems,
      downloadTestCasesForTestMaps: downloadTestCasesForTestMaps,
      downloadTestCaseCoverageForTestMaps: downloadTestCaseCoverageForTestMaps,
      startTestCaseUpload: startTestCaseUpload,
      createTestCasesForItems: createTestCasesForItems,
      createTestCasesForTestMaps: createTestCasesForTestMaps,
      getTestCaseUploadStatus: getTestCaseUploadStatus,
      getTestCaseUploads: getTestCaseUploads,
      deleteItemTestCases: deleteItemTestCases
    };

    /**
     * Get a list of test cases for the given item.
     *
     * @param {number} previewerId - The ID of the previewer that the tenant belongs to.
     * @param {number} tenantId - The ID of the tenant that the item belongs to.
     * @param {number} itemId - The ID of the item.
     * @param {boolean} run - Whether to run the test cases when retrieving them.
     * @returns The result from the server containing the list of test cases.
     */
    function getTestCases(previewerId, tenantId, itemId, run) {
      var url = qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/items/' +
        itemId + '/testCases';
      if (run) {
        url = url + '?run=true';
      }
      return $http.get(url)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Delete the given test cases.
     *
     * @param {number} previewerId - The ID of the previewer that the tenant belongs to.
     * @param {number} tenantId - The ID of the tenant that the item belongs to.
     * @param {number} itemId - The ID of the item.
     * @param {number[]} testCaseIds - The list of test case IDs to delete.
     * @returns The result from the server indicating success or failure.
     */
    function deleteTestCases(previewerId, tenantId, parsedCsv) {
      var config = {
        method: 'DELETE',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId,
        params: {
          parsedCsv: parsedCsv
        }
      };

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Delete the given test cases.
     *
     * @param {number} previewerId - The ID of the previewer that the tenant belongs to.
     * @param {number} tenantId - The ID of the tenant that the item belongs to.
     * @param {number} itemId - The ID of the item.
     * @param {number[]} testCaseIds - The list of test case IDs to delete.
     * @returns The result from the server indicating success or failure.
     */
    function deleteItemTestCases(previewerId, tenantId, itemId, testCaseIds) {
      var config = {
        method: 'DELETE',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/items/' +
        itemId + '/testCases',
        params: {
          testCaseIds: testCaseIds
        }
      };

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Update the given test case to match the actual outcomes.
     *
     * @param {number} previewerId - The ID of the previewer that the tenant belongs to.
     * @param {number} tenantId - The ID of the tenant that the item belongs to.
     * @param {number} itemId - The ID of the item.
     * @param {number} testCaseId - The ID of the test case.
     * @returns The result from the server indicating success or failure.
     */
    function updateTestCase(previewerId, tenantId, itemId, testCaseId) {
      return $http.put(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/items/' +
        itemId + '/testCases/' + testCaseId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the show test case URL for the given tenant.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The result from the server containing the URL.
     */
    function getShowTestCaseUrl(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/showTestCaseUrl')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on a Tenants Dashboard.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The CSV data for download.
     */
    function downloadTestCasesForTenant(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/testCasesDownload')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the CSV data for a Test Case Coverage test case download performed on a Tenants Dashboard.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The CSV data for download.
     */
    function downloadTestCaseCoverageForTenant(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/testCaseCoverageDownload')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on a Tenants Dashboard.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The CSV data for download.
     */
    function downloadTestCasesForItems(previewerId, tenantId, itemIds) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/itemsTestCasesDownload',
        params: {
          itemIds: itemIds
        }
      };
      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the CSV data for a Test Case Coverage test case download performed on a Tenants Dashboard.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The CSV data for download.
     */
    function downloadTestCaseCoverageForItems(previewerId, tenantId, itemIds) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/itemsTestCaseCoverageDownload',
        params: {
          itemIds: itemIds
        }
      };

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on a Tenants Dashboard.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The CSV data for download.
     */
    function downloadTestCasesForTestMaps(previewerId, tenantId, testMapIds) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/testMapsTestCasesDownload',
        params: {
          testMapIds: testMapIds
        }
      };
      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the CSV data for a Test Case Coverage test case download performed on a Tenants Dashboard.
     *
     * @param previewerId {number} The ID of the previewer that the tenant belongs to.
     * @param tenantId {number} The ID of the tenant.
     * @returns The CSV data for download.
     */
    function downloadTestCaseCoverageForTestMaps(previewerId, tenantId, testMapIds) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/testMapsTestCaseCoverageDownload',
        params: {
          testMapIds: testMapIds
        }
      };

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Starts a test case upload job for the test case data loaded in a CSV via the UI.
     *
     * @param previewerId {number} The ID of the previewer.
     * @param previewerName {string} The name of the previewer.
     * @param tenantId {number} The ID of the tenant.
     * @param tenantName {string} The name of the tenant.
     * @param json {object} The JSON data for the test cases being loaded.
     * @returns The Job ID (upload ID) for the job.
     */
    function startTestCaseUpload(previewerId, previewerName, tenantId, tenantName, json) {
      return $http.post(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' +
        tenantId + '/testCases', json)
        .then(function (result) {
          if (result.data.success) {
            notificationsService.notifyNewTestCaseUploadJob(result.data.data.jobId, previewerId,
              previewerName, tenantId, tenantName);
          }
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Starts a test case upload job that generates and uploads test cases for the given item IDs.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @param {string} previewerName - The name of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     * @param {string} tenantName - The name of the tenant.
     * @param {number[]} itemIds - The list of item IDs to create test cases for.
     * @param {number} maxCorrect - The maximum number of correct responses to generate for each item.
     * @param {number} maxIncorrect - The maximum number of incorrect responses to generate for each item.
     */
    function createTestCasesForItems(previewerId, previewerName, tenantId, tenantName, itemIds, maxCorrect,
                                     maxIncorrect) {
      var body = {
        itemIds: itemIds,
        maxCorrect: maxCorrect,
        maxIncorrect: maxIncorrect
      };

      return $http.post(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' +
        tenantId + '/testCases', body)
        .then(function (result) {
          if (result.data.success) {
            notificationsService.notifyNewTestCaseUploadJob(result.data.data.jobId, previewerId,
              previewerName, tenantId, tenantName);
          }
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Starts a test case upload job that generates and uploads test cases for the given test map IDs.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @param {string} previewerName - The name of the previewer.
     * @param {number} tenantId - The ID of the tenant.
     * @param {string} tenantName - The name of the tenant.
     * @param {number[]} testMapIds - The list of test map IDs to create test cases for.
     * @param {number} maxCorrect - The maximum number of correct responses to generate for each item.
     * @param {number} maxIncorrect - The maximum number of incorrect responses to generate for each item.
     */
    function createTestCasesForTestMaps(previewerId, previewerName, tenantId, tenantName, testMapIds, maxCorrect,
                                        maxIncorrect) {
      var body = {
        testMapIds: testMapIds,
        maxCorrect: maxCorrect,
        maxIncorrect: maxIncorrect
      };

      return $http.post(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' +
        tenantId + '/testCases', body)
        .then(function (result) {
          if (result.data.success) {
            notificationsService.notifyNewTestCaseUploadJob(result.data.data.jobId, previewerId,
              previewerName, tenantId, tenantName);
          }
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Checks whether a Test Case Upload job is still running.
     *
     * @param previewerId {number} The ID of the previewer.
     * @param tenantId {number} The ID of the tenant.
     * @param jobId {string} The job ID for the test case upload job being checked.
     * @returns Boolean value for the isRunning attribute of the job.
     */
    function getTestCaseUploadStatus(previewerId, tenantId, jobId) {
      return $http.get(qtiValidationApiPrefix + 'testCaseUploadJobs/' + jobId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Gets the test case upload history for the given tenant.
     *
     * @param previewerId {number} The ID of the previewer containing the tenant.
     * @param tenantId {number} The ID of the tenant.
     * @returns The result from the server containing the list of test case uploads.
     */
    function getTestCaseUploads(previewerId, tenantId) {
      return $http.get(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/testCaseUploads')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

  }

})();
