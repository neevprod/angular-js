'use strict';

describe('test case service', function () {
  var testCaseService;
  var $httpBackend;
  var qtiValidationApiPrefix;

  var previewerId;
  var tenantId;
  var itemId;
  var data;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_testCaseService_, _$httpBackend_, _qtiValidationApiPrefix_) {
    testCaseService = _testCaseService_;
    $httpBackend = _$httpBackend_;
    qtiValidationApiPrefix = _qtiValidationApiPrefix_;
  }));

  beforeEach(function () {
    previewerId = 1;
    tenantId = 2;
    itemId = 3;
    data = [{id: 1, mods: 'mods1', outcomes: 'outcomes1'}, {id: 2, mods: 'mods2', outcomes: 'outcomes2'},
      {id: 3, mods: 'mods3', outcomes: 'outcomes3'}];
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getTestCases', function () {
    it('should submit the test cases GET request properly', function () {
      // Then
      $httpBackend.expectGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/items/' + itemId + '/testCases')
        .respond(200, {success: true, data: data});

      // When
      testCaseService.getTestCases(previewerId, tenantId, itemId);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/items/' + itemId + '/testCases')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      testCaseService.getTestCases(previewerId, tenantId, itemId).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId +
        '/items/' + itemId + '/testCases')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      testCaseService.getTestCases(previewerId, tenantId, itemId).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
