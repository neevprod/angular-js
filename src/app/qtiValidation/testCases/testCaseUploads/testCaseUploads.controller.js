(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('TestCaseUploadsController', TestCaseUploadsController);

  TestCaseUploadsController.$inject = ['$stateParams', 'itemsPerPageOptions', 'testCaseService', 'previewerService',
    'tenantService', 'memoryService', 'dateTimeFormat', '$uibModal', '$window'];

  function TestCaseUploadsController($stateParams, itemsPerPageOptions, testCaseService, previewerService,
                                     tenantService, memoryService, dateTimeFormat, $uibModal, $window) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.tenantId = $stateParams.tenantId;
    viewModel.loadingTestCaseUploads = true;
    viewModel.testCaseUploadsInitialized = false;
    viewModel.testCaseUploads = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('testCaseUploads', 'perPage', viewModel.previewerId + '_' +
      viewModel.tenantId) + viewModel.itemsPerPageOptions[0].value;
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.showJsonResult = showJsonResult;

    activate();

    /**
     * Loads the test case uploads.
     */
    function activate() {
      if ($stateParams.fromTestMapsPage !== null) {
        viewModel.fromTestMapsPage = $stateParams.fromTestMapsPage;
        $window.localStorage.setItem('testCaseUploadsFromTestMapsPage', $stateParams.fromTestMapsPage);
      } else if ($window.localStorage.getItem('testCaseUploadsFromTestMapsPage') === 'true') {
        viewModel.fromTestMapsPage = true;
      } else if ($window.localStorage.getItem('testCaseUploadsFromTestMapsPage') === 'false') {
        viewModel.fromTestMapsPage = false;
      }

      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      if ($stateParams.tenantName) {
        viewModel.tenantName = $stateParams.tenantName;
      } else {
        getTenantName(viewModel.previewerId, viewModel.tenantId).then(function (tenantName) {
          if (tenantName) {
            viewModel.tenantName = tenantName;
          }
        });
      }

      testCaseService.getTestCaseUploads(viewModel.previewerId, viewModel.tenantId).then(function (result) {
        viewModel.loadingTestCaseUploads = false;
        viewModel.testCaseUploadsInitialized = true;

        if (result.success) {
          viewModel.testCaseUploads = result.data.testCaseUploads;
          viewModel.displayedTestCaseUploads = result.data.testCaseUploads;
        }
      });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @param previewerId the ID of the previewer
     * @returns {string} the name of the previewer
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Get the name of the tenant with the given ID.
     *
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId the ID of the tenant
     * @returns {string} the name of the tenant
     */
    function getTenantName(previewerId, tenantId) {
      return tenantService.getTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.tenant.name;
        }
      });
    }

    /**
     * Show the given JSON result in a modal.
     *
     * @param {string} jsonResult - The JSON result to show.
     */
    function showJsonResult(jsonResult) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'JsonDetailsController as jsonDetailsCtrl',
        resolve: {
          title: function () {
            return 'Upload JSON Result';
          },
          json: function () {
            return jsonResult;
          }
        },
        size: 'lg'
      });
    }
  }

})();
