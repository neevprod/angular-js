(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('TestCasesController', TestCasesController);

  TestCasesController.$inject = ['$stateParams', 'previewerService', 'tenantService', 'itemService',
    'testCaseService', 'fingerprintService', 'permissionsService', 'memoryService', 'itemsPerPageOptions'];

  function TestCasesController($stateParams, previewerService, tenantService, itemService, testCaseService,
                               fingerprintService, permissionsService, memoryService, itemsPerPageOptions) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.tenantId = $stateParams.tenantId;
    viewModel.itemId = $stateParams.itemId;
    viewModel.loadingTestCases = true;
    viewModel.testCasesInitialized = false;
    viewModel.testCases = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('testCases', 'perPage', viewModel.previewerId + '_' +
      viewModel.tenantId + '_' + viewModel.itemId) || viewModel.itemsPerPageOptions[0].value;
    viewModel.fingerprintAllowed = permissionsService.hasTenantLevelPermission('fingerprint', 'updateAccess',
      'qtiValidation', viewModel.previewerId, viewModel.tenantId);
    viewModel.deleteAllowed = permissionsService.hasTenantLevelPermission('testCases', 'deleteAccess',
      'qtiValidation', viewModel.previewerId, viewModel.tenantId);

    viewModel.isFingerprinted = isFingerprinted;
    viewModel.showMods = showMods;
    viewModel.showScore = showScore;
    viewModel.showOutcomes = showOutcomes;
    viewModel.updateFingerprint = updateFingerprint;
    viewModel.deleteItemTestCases = deleteItemTestCases;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.getPassFail = getPassFail;

    activate();

    /**
     * Loads the previewer name, the tenant name, the item name, and the list of test cases.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      if ($stateParams.tenantName) {
        viewModel.tenantName = $stateParams.tenantName;
      } else {
        getTenantName(viewModel.previewerId, viewModel.tenantId).then(function (tenantName) {
          if (tenantName) {
            viewModel.tenantName = tenantName;
          }
        });
      }

      if ($stateParams.itemName) {
        viewModel.itemName = $stateParams.itemName;
      } else {
        getItemName(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (itemName) {
          if (itemName) {
            viewModel.itemName = itemName;
          }
        });
      }

      getTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (testCases) {
        viewModel.loadingTestCases = false;
        viewModel.testCasesInitialized = true;
        viewModel.testCases = testCases;
        viewModel.displayedTestCases = testCases;
      });
    }

    /**
     * Get the name of the previewer with the given ID.
     *
     * @param {number} previewerId - The ID of the previewer.
     * @returns {string} The name of the previewer.
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Get the name of the tenant with the given ID.
     *
     * @param previewerId - The ID of the previewer containing the tenant.
     * @param tenantId - The ID of the tenant.
     * @returns {string} - The name of the tenant.
     */
    function getTenantName(previewerId, tenantId) {
      return tenantService.getTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.tenant.name;
        }
      });
    }

    /**
     * Get the name of the given item under the given tenant.
     *
     * @param previewerId - The ID of the previewer containing the tenant.
     * @param tenantId - The ID of the tenant.
     * @param itemId - The ID of the item.
     * @returns {string} - The name of the tenant.
     */
    function getItemName(previewerId, tenantId, itemId) {
      return itemService.getItem(previewerId, tenantId, itemId).then(function (result) {
        if (result.success) {
          return result.data.item.identifier;
        }
      });
    }

    /**
     * Get the list of test cases.
     *
     * @param {number} previewerId - The ID of the previewer the tenant belongs to.
     * @param {number} tenantId - The ID of the tenant the item belongs to.
     * @param {string} itemId - The ID of the item.
     * @returns A promise that returns the list of tenants.
     */
    function getTestCases(previewerId, tenantId, itemId) {
      return testCaseService.getTestCases(previewerId, tenantId, itemId, true).then(function (result) {
        if (result.success) {
          return result.data.testCases;
        } else {
           viewModel.alertMessage = result.errors[0].message;
           viewModel.alertSuccess = false;
           viewModel.loadingTestCases = false;
        }
      });
    }

    /**
     * Returns a string "Yes" or "No" depending on whether the test case is fingerprinted.
     *
     * @param testCase The test case to check.
     * @returns {string} Whether the test case is fingerprinted.
     */
    function isFingerprinted(testCase) {
      if (testCase.fingerprinted) {
        return 'Yes';
      } else {
        return 'No';
      }
    }

    /**
     * Returns an array of more human-readable versions of the mods in the given test case.
     *
     * @param testCase The test case to get the mods from.
     * @returns {Array} An array of strings representing the mods.
     */
    function showMods(testCase) {
      var mods = testCase.mods;
      var result = [];
      for (var i = 0; i < mods.length; i++) {
        result.push(mods[i].did + ': ' + JSON.stringify(mods[i].r));
      }
      return result;
    }

    /**
     * Returns a string that shows the score out of the max score in the given test case.
     *
     * @param testCase The test case to get the score from.
     * @returns {string} The score out of the max score.
     */
    function showScore(testCase) {
      var score = '?';
      var maxScore = '?';
      var outcomes = testCase.outcomes;
      for (var i = 0; i < outcomes.length; i++) {
        if (outcomes[i].identifier.toLowerCase() === 'score') {
          score = outcomes[i].value;
        } else if (outcomes[i].identifier.toLowerCase() === 'maxscore') {
          maxScore = outcomes[i].value;
        }
      }
      return score + ' / ' + maxScore;
    }

    /**
     * Returns an array of more human-readable versions of the outcomes in the given test case.
     *
     * @param testCase The test case to get the outcomes from.
     * @returns {Array} An array of strings representing the outcomes.
     */
    function showOutcomes(testCase) {
      var outcomes = testCase.outcomes;
      var result = [];
      for (var i = 0; i < outcomes.length; i++) {
        result.push(outcomes[i].identifier + ': ' + JSON.stringify(outcomes[i].value));
      }
      return result;
    }

    /**
     * Updates the fingerprint for the selected test cases.
     */
    function updateFingerprint() {
      viewModel.loadingTestCases = true;

      var selectedTestCases = [];
      viewModel.testCases.forEach(function (testCase) {
        if (testCase.isSelected) {
          selectedTestCases.push(testCase.id);
        }
      });

      fingerprintService.updateFingerprint(viewModel.previewerId, viewModel.tenantId, viewModel.itemId,
        selectedTestCases).then(function (result) {
        refreshDataWithMessage(result.success, 'Test case(s) fingerprinted successfully.',
          'An error occurred while fingerprinting test case(s).');
      });
    }

    /**
     * Deletes the selected test cases.
     */
    function deleteItemTestCases() {
      viewModel.loadingTestCases = true;

      var selectedTestCases = [];
      viewModel.testCases.forEach(function (testCase) {
        if (testCase.isSelected) {
          selectedTestCases.push(testCase.id);
        }
      });

      testCaseService.deleteItemTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.itemId,
        selectedTestCases).then(function (result) {
        refreshDataWithMessage(result.success, 'Test case(s) deleted successfully.',
          'An error occurred while deleting test case(s).');
      });
    }

    /**
     * Refreshes the test case data and sets an alert message depending on whether an operation was successful.
     *
     * @param {boolean} success - A boolean indicating whether the operation was successful.
     * @param {string} successMessage - The success message.
     * @param {string} failMessage - The failure message.
     */
    function refreshDataWithMessage(success, successMessage, failMessage) {
      if (success) {
        getTestCases(viewModel.previewerId, viewModel.tenantId, viewModel.itemId).then(function (testCases) {
          viewModel.loadingTestCases = false;
          viewModel.alertSuccess = true;
          viewModel.alertMessage = successMessage;

          viewModel.testCases = testCases;
          viewModel.displayedTestCases = testCases;
        });
      } else {
        viewModel.loadingTestCases = false;
        viewModel.alertSuccess = false;
        viewModel.alertMessage = failMessage;
      }
    }

    /**
     * Returns a string indicating whether the test case passed or failed.
     *
     * @param testCase The test case.
     * @returns {string} A string indicating whether the test case passed.
     */
    function getPassFail(testCase) {
      if (testCase.passed) {
        return 'Pass';
      } else {
        return 'Fail';
      }
    }
  }

})();
