'use strict';

describe('test cases controller', function () {
  var controller;
  var $stateParams;
  var previewerService;
  var tenantService;
  var itemService;
  var testCaseService;
  var permissionsService;
  var deferredPreviewer;
  var deferredTenant;
  var deferredItem;
  var deferredTestCase;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('qtiValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {previewerId: 1, tenantId: 2, itemId: 3};

    deferredPreviewer = $q.defer();
    previewerService = jasmine.createSpyObj('previewerService', ['getPreviewer']);
    previewerService.getPreviewer.and.callFake(function (previewerId) {
      if (previewerId === $stateParams.previewerId) {
        return deferredPreviewer.promise;
      }
    });

    deferredTenant = $q.defer();
    tenantService = jasmine.createSpyObj('tenantService', ['getTenant']);
    tenantService.getTenant.and.callFake(function (previewerId, tenantId) {
      if (previewerId === $stateParams.previewerId && tenantId === $stateParams.tenantId) {
        return deferredTenant.promise;
      }
    });

    deferredItem = $q.defer();
    itemService = jasmine.createSpyObj('itemService', ['getItem']);
    itemService.getItem.and.callFake(function (previewerId, tenantId, itemId) {
      if (previewerId === $stateParams.previewerId && tenantId === $stateParams.tenantId &&
        itemId === $stateParams.itemId) {
        return deferredItem.promise;
      }
    });

    deferredTestCase = $q.defer();
    testCaseService = jasmine.createSpyObj('testCaseService', ['getTestCases']);
    testCaseService.getTestCases.and.callFake(function (previewerId, tenantId, itemId) {
      if (previewerId === $stateParams.previewerId && tenantId === $stateParams.tenantId &&
        itemId === $stateParams.itemId) {
        return deferredTestCase.promise;
      }
    });

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasTenantLevelPermission']);

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;

    controller = $controller('TestCasesController', {
      $stateParams: $stateParams, previewerService: previewerService,
      tenantService: tenantService, itemService: itemService, testCaseService: testCaseService,
      permissionsService: permissionsService, itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set previewerId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.previewerId).toBe($stateParams.previewerId);
  });

  it('should set tenantId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.tenantId).toBe($stateParams.tenantId);
  });

  it('should set itemId to the value in $stateParams during initialization', function () {
    // Then
    expect(controller.itemId).toBe($stateParams.itemId);
  });

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should call previewerService to get the previewer name during initialization', function () {
    // Then
    expect(previewerService.getPreviewer).toHaveBeenCalledWith($stateParams.previewerId);
  });

  it('should set the previewer name after previewerService returns', function () {
    // Given
    var previewer = {id: 1, previewerUrl: 'previewer1.com', url: 'previewer1'};

    // When
    deferredPreviewer.resolve({success: true, data: {previewer: previewer}});
    $rootScope.$digest();

    // Then
    expect(controller.previewerName).toEqual(previewer.url);
  });

  it('should call tenantService to get the tenant name during initialization', function () {
    // Then
    expect(tenantService.getTenant).toHaveBeenCalledWith($stateParams.previewerId, $stateParams.tenantId);
  });

  it('should set the tenant name after tenantService returns', function () {
    // Given
    var tenant = {previewerTenantId: 2, name: 'tenant2'};

    // When
    deferredTenant.resolve({success: true, data: {tenant: tenant}});
    $rootScope.$digest();

    // Then
    expect(controller.tenantName).toEqual(tenant.name);
  });

  it('should call itemService to get the item name during initialization', function () {
    // Then
    expect(itemService.getItem).toHaveBeenCalledWith($stateParams.previewerId, $stateParams.tenantId,
      $stateParams.itemId);
  });

  it('should set the item name after itemService returns', function () {
    // Given
    var item = {id: 3, identifier: 'item3'};

    // When
    deferredItem.resolve({success: true, data: {item: item}});
    $rootScope.$digest();

    // Then
    expect(controller.itemName).toEqual(item.identifier);
  });

  it('should set loadingTestCases to true on initialization', function () {
    // Then
    expect(controller.loadingTestCases).toBe(true);
  });

  it('should set the list of test cases to an empty array on initialization', function () {
    // Then
    expect(controller.testCases).toEqual([]);
  });

  it('should call testCaseService to get the list of test cases during initialization', function () {
    // Then
    expect(testCaseService.getTestCases).toHaveBeenCalledWith($stateParams.previewerId, $stateParams.tenantId,
      $stateParams.itemId, true);
  });

  it('should set the list of test cases after testCaseService returns', function () {
    // Given
    var testCases = [{id: 1, mods: 'mods1', outcomes: 'outcomes1'}, {id: 2, mods: 'mods2', outcomes: 'outcomes2'},
      {id: 3, mods: 'mods3', outcomes: 'outcomes3'}];

    // When
    deferredTestCase.resolve({success: true, data: {testCases: testCases}});
    $rootScope.$digest();

    // Then
    expect(controller.testCases).toEqual(testCases);
    expect(controller.displayedTestCases).toEqual(testCases);
  });

  it('should set loadingTestCases to false after testCaseService returns', function () {
    // Given
    var testCases = [{id: 1, mods: 'mods1', outcomes: 'outcomes1'}, {id: 2, mods: 'mods2', outcomes: 'outcomes2'},
      {id: 3, mods: 'mods3', outcomes: 'outcomes3'}];

    // When
    deferredTestCase.resolve({success: true, data: testCases});
    $rootScope.$digest();

    // Then
    expect(controller.loadingTestCases).toBe(false);
  });

  describe('isFingerprinted', function () {
    it('should return "Yes" if the test case is fingerprinted', function () {
      // Given
      var testCase = {
        id: 1,
        fingerprinted: true
      };

      // When
      var result = controller.isFingerprinted(testCase);

      // Then
      expect(result).toBe('Yes');
    });

    it('should return "No" if the test case is not fingerprinted', function () {
      // Given
      var testCase = {
        id: 1,
        fingerprinted: false
      };

      // When
      var result = controller.isFingerprinted(testCase);

      // Then
      expect(result).toBe('No');
    });
  });

  describe('showMods', function () {
    it('should return an array of the mods in a simplified format (did: r)', function () {
      // Given
      var testCase = {
        mods: [
          {
            did: 'RESPONSE1',
            r: ['A', 'B']
          },
          {
            did: 'RESPONSE2',
            r: ['C']
          }
        ]
      };

      // When
      var result = controller.showMods(testCase);

      // Then
      expect(result).toEqual(['RESPONSE1: ["A","B"]', 'RESPONSE2: ["C"]']);
    });

    it('should return an empty array if the list of mods is empty', function () {
      // Given
      var testCase = {
        mods: []
      };

      // When
      var result = controller.showMods(testCase);

      // Then
      expect(result).toEqual([]);
    });
  });

  describe('showScore', function () {
    it('should return a string representing the score out of the max score', function () {
      // Given
      var testCase = {
        outcomes: [
          {
            identifier: 'SCORE',
            value: '1'
          },
          {
            identifier: 'MAXSCORE',
            value: '3'
          }
        ]
      };

      // When
      var result = controller.showScore(testCase);

      // Then
      expect(result).toBe('1 / 3');
    });

    it('should show a question mark for the score if it is not found', function () {
      // Given
      var testCase = {
        outcomes: [
          {
            identifier: 'MAXSCORE',
            value: '3'
          }
        ]
      };

      // When
      var result = controller.showScore(testCase);

      // Then
      expect(result).toBe('? / 3');
    });

    it('should show a question mark for the max score if it is not found', function () {
      // Given
      var testCase = {
        outcomes: [
          {
            identifier: 'SCORE',
            value: '1'
          }
        ]
      };

      // When
      var result = controller.showScore(testCase);

      // Then
      expect(result).toBe('1 / ?');
    });
  });

  describe('showOutcomes', function () {
    it('should return an array of the outcomes in a simplified format (identifier: value)', function () {
      // Given
      var testCase = {
        outcomes: [
          {
            identifier: 'SCORE',
            value: '1'
          },
          {
            identifier: 'MAXSCORE',
            value: '3'
          },
          {
            identifier: 'OTHER_OUTCOME',
            value: '34'
          }
        ]
      };

      // When
      var result = controller.showOutcomes(testCase);

      // Then
      expect(result).toEqual(['SCORE: "1"', 'MAXSCORE: "3"', 'OTHER_OUTCOME: "34"']);
    });

    it('should return an empty array if the list of outcomes is empty', function () {
      // Given
      var testCase = {
        outcomes: []
      };

      // When
      var result = controller.showOutcomes(testCase);

      // Then
      expect(result).toEqual([]);
    });
  });
});
