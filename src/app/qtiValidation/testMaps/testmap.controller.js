(function () {

  'use strict';

  angular.module('qtiValidation')
    .controller('TestMapsController', TestMapsController);

  TestMapsController.$inject = ['$stateParams', 'previewerService', 'tenantService', 'testMapsService', 'testCaseService',
    'permissionsService', 'itemsPerPageOptions', 'notificationsService', 'memoryService', '$window', '$uibModal'];

  function TestMapsController($stateParams, previewerService, tenantService, testMapsService, testCaseService,
                              permissionsService, itemsPerPageOptions, notificationsService, memoryService, $window,
                              $uibModal) {
    var viewModel = this;

    viewModel.previewerId = $stateParams.previewerId;
    viewModel.tenantId = $stateParams.tenantId;
    viewModel.loadingTestMaps = true;
    viewModel.testMapsInitialized = false;
    viewModel.testMaps = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('testMaps', 'perPage', viewModel.previewerId + '_' +
      viewModel.tenantId) || viewModel.itemsPerPageOptions[0].value;
    viewModel.showCheckboxes = true;
    viewModel.hasTestCaseReadPermission = permissionsService.hasTenantLevelPermission('testCases', 'readAccess',
      'qtiValidation', $stateParams.previewerId, $stateParams.tenantId);
    viewModel.hasTestCaseCreatePermission = permissionsService.hasTenantLevelPermission('testCases',
      'createAccess', 'qtiValidation', $stateParams.previewerId, $stateParams.tenantId);
    viewModel.startTestCaseDownload = startTestCaseDownload;
    viewModel.showCreateTestCasesDialog = showCreateTestCasesDialog;

    activate();

    /**
     * Loads the previewer name and the list of tenants.
     */
    function activate() {
      if ($stateParams.previewerName) {
        viewModel.previewerName = $stateParams.previewerName;
      } else {
        getPreviewerName(viewModel.previewerId).then(function (previewerName) {
          if (previewerName) {
            viewModel.previewerName = previewerName;
          }
        });
      }

      if ($stateParams.tenantName) {
        viewModel.tenantName = $stateParams.tenantName;
      } else {
        getTenantName(viewModel.previewerId, viewModel.tenantId).then(function (tenantName) {
          if (tenantName) {
            viewModel.tenantName = tenantName;
          }
        });
      }

      getTestMaps(viewModel.previewerId, viewModel.tenantId).then(function (testMaps) {
        viewModel.loadingTestMaps = false;
        viewModel.testMapsInitialized = true;
        viewModel.testMaps = testMaps;
        viewModel.displayedTestMaps = testMaps;
      });
    }

    /**
     * Get the list of test maps
     *
     * @returns the list of test maps
     */
    function getTestMaps(previewerId, tenantId) {
      return testMapsService.getTestMaps(previewerId, tenantId).then(function (result) {
        if (result.success) {
          var testMaps = result.data.testMaps;
          testMaps.forEach(function (testMap) {
            testMap.isSelectable = true;
          });
          return testMaps;
        }
      });
    }


    /**
     * Get the name of the previewer with the given ID.
     *
     * @returns {string} the name of the previewer
     */
    function getPreviewerName(previewerId) {
      return previewerService.getPreviewer(previewerId).then(function (result) {
        if (result.success) {
          return result.data.previewer.url;
        }
      });
    }

    /**
     * Get the name of the tenant with the given ID.
     *
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId the ID of the tenant
     * @returns {string} the name of the tenant
     */
    function getTenantName(previewerId, tenantId) {
      return tenantService.getTenant(previewerId, tenantId).then(function (result) {
        if (result.success) {
          return result.data.tenant.name;
        }
      });
    }

    /**
     * Shows the dialog that triggers creation of test cases for selected test maps.
     */
    function showCreateTestCasesDialog() {
      var selectedTestMapIds = [];
      viewModel.testMaps.forEach(function (testMap) {
        if (testMap.isSelected) {
          selectedTestMapIds.push(testMap.id);
        }
      });

      $uibModal.open({
        templateUrl: 'app/qtiValidation/testCases/createTestCases/createTestCases.html',
        controller: 'CreateTestCasesController as createTestCasesCtrl',
        resolve: {
          previewerId: function () {
            return viewModel.previewerId;
          },
          previewerName: function () {
            return viewModel.previewerName;
          },
          tenantId: function () {
            return viewModel.tenantId;
          },
          tenantName: function () {
            return viewModel.tenantName;
          },
          ids: function () {
            return selectedTestMapIds;
          },
          isAtTestMapLevel: function () {
            return true;
          }
        },
        backdrop: 'static',
        size: 'sm'
      });
    }

    /**
     * Starts a testmap-level test case download for either an Item Response Pool or Test Case Coverage CSV
     *
     *  @param isItemResponsePool Flag to determine the download type (true = Item Response Pool, false = Test Case Coverage)
     * @returns Data for CSV download (if successful)
     */
    function startTestCaseDownload(isItemResponsePool) {
      var selectedTestMapIds = [];

      viewModel.testMaps.forEach(function (testMap) {
        if (testMap.isSelected) {
          selectedTestMapIds.push(testMap.id);
        }
      });

      var downloadId = 0;
      if ($window.localStorage.getItem('downloadId') !== null) {
        downloadId = parseInt($window.localStorage.getItem('downloadId')) + 1;
      }
      $window.localStorage.setItem('downloadId', downloadId);
      if (isItemResponsePool) {
        notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName,
          viewModel.tenantId, viewModel.tenantName, downloadId, 'Test Map', 'itemResponsePool');
        return testCaseService.downloadTestCasesForTestMaps(viewModel.previewerId, viewModel.tenantId,
          selectedTestMapIds).then(function (result) {
          if (result.success) {
            notificationsService.notifyDownloadComplete(downloadId, true,
              result.data);
          } else {
            notificationsService.notifyDownloadComplete(downloadId, false, null);
          }
        });
      } else {
        notificationsService.notifyNewDownload(viewModel.previewerId, viewModel.previewerName,
          viewModel.tenantId, viewModel.tenantName, downloadId, 'Test Map', 'testCaseCoverage');
        return testCaseService.downloadTestCaseCoverageForTestMaps(viewModel.previewerId, viewModel.tenantId,
          selectedTestMapIds).then(function (result) {
          if (result.success) {
            notificationsService.notifyDownloadComplete(downloadId, true,
              result.data);
          } else {
            notificationsService.notifyDownloadComplete(downloadId, false, null);
          }
        });
      }
    }
  }

})();
