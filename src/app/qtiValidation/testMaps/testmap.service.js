(function () {

  'use strict';

  angular
    .module('qtiValidation')
    .factory('testMapsService', testMapsService);

  testMapsService.$inject = ['$http', 'qtiValidationApiPrefix'];

  function testMapsService($http, qtiValidationApiPrefix) {
    return {
      getTestMaps: getTestMaps
    };

    /**
     * Get a list of test maps that belong to the given tenant.
     *
     * @param {number} previewerId - The ID of the previewer that contains the tenant.
     * @param {number} tenantId - The ID of the tenant.
     * @returns The result from the server containing the list of test maps.
     */
    function getTestMaps(previewerId, tenantId) {
      var config = {
        method: 'GET',
        url: qtiValidationApiPrefix + 'previewers/' + previewerId + '/tenants/' + tenantId + '/testMaps'
      };
      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
