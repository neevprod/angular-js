(function() {

    'use strict';

    angular.module('qualityMonitoringApp', ['core', 'qtiValidation','systemValidation','userManagement','gridAutomation']);

})();
