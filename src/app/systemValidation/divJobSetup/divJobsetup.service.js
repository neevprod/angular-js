(function () {

  'use strict';

  angular.module('systemValidation').factory('divJobSetupService', divJobSetupService);

  divJobSetupService.$inject = ['$window'];

  function divJobSetupService($window) {
    var selectedEnvironment;
    var testType;
    var selectedScopeTreePath;
    var testSessions;
    var testForms;
    var availableTestCodes;
    //Path is not selected from GUI (Read from GUI)
    var divJobPathId;
    var scenarios;
    var selectedTestCodes;
    var studentScenarios;
    var errorMessage;
    var selectedPathStates;
    var testnavScenarioClasses = [];
    var epenScenarioClasses = [];

    var setEnvironment = function (environment) {
      $window.localStorage.setItem('divJob_Environment', JSON.stringify(environment));
      selectedEnvironment = environment;
    };
    var getEnvironment = function () {
      if (selectedEnvironment === null || selectedEnvironment === undefined) {
        selectedEnvironment = JSON.parse($window.localStorage.getItem('divJob_Environment'));
      }
      return selectedEnvironment;
    };

    var setTestType = function (selectedTestType) {
      $window.localStorage.setItem('divJob_testType', JSON.stringify(selectedTestType));
      testType = selectedTestType;
    };

    var getTestType = function () {
      if (testType === null || testType === undefined) {
        testType = JSON.parse($window.localStorage.getItem('divJob_testType'));
      }
      return testType;
    };

    var setScopeTreePath = function (scopeTreePath) {
      $window.localStorage.setItem('divJob_ScopeTreePath', JSON.stringify(scopeTreePath));
      selectedScopeTreePath = scopeTreePath;
    };
    var getScopeTreePath = function () {
      if (selectedScopeTreePath === null || selectedScopeTreePath === undefined) {
        selectedScopeTreePath = JSON.parse($window.localStorage.getItem('divJob_ScopeTreePath'));
      }
      return selectedScopeTreePath;
    };
    var setDivJobPathId = function (pathId) {
      $window.localStorage.setItem('divJob_PathId', JSON.stringify(pathId));
      divJobPathId = pathId;
    };

    var getDivJobPathId = function () {
      return JSON.parse($window.localStorage.getItem('divJob_PathId'));
    };
    var getTestSessions = function () {
      if (testSessions === null || testSessions === undefined) {
        testSessions = JSON.parse($window.localStorage.getItem('divJob_TestSessions'));
      }
      return angular.copy(testSessions);
    };
    var setTestSessions = function (tss) {
      $window.localStorage.setItem('divJob_TestSessions', JSON.stringify(tss));
      testSessions = angular.copy(tss);
    };

    var setTestnavScenarioClasses = function (scenarioClasses) {
      $window.localStorage.setItem('divJob_TestnavScenarioClasses', JSON.stringify(scenarioClasses));
      testnavScenarioClasses = scenarioClasses;
    };
    var getTestnavScenarioClasses = function () {
      if (testnavScenarioClasses.length === 0) {
        testnavScenarioClasses = JSON.parse($window.localStorage.getItem('divJob_TestnavScenarioClasses'));
      }
      return testnavScenarioClasses;
    };

    var setEpenScenarioClasses = function (scenarioClasses) {
      $window.localStorage.setItem('divJob_EpenScenarioClasses', JSON.stringify(scenarioClasses));
      epenScenarioClasses = scenarioClasses;
    };
    var getEpenScenarioClasses = function () {
      if (epenScenarioClasses.length === 0) {
        epenScenarioClasses = JSON.parse($window.localStorage.getItem('divJob_EpenScenarioClasses'));
      }
      return epenScenarioClasses;
    };

    var setTestCodes = function (testCodes) {
      $window.localStorage.setItem('divJob_TestCodes', JSON.stringify(testCodes));
      selectedTestCodes = angular.copy(testCodes);
    };
    var getTestCodes = function () {
      if (selectedTestCodes === null || selectedTestCodes === undefined) {
        selectedTestCodes = JSON.parse($window.localStorage.getItem('divJob_TestCodes'));
      }
      return selectedTestCodes;
    };

    var setTestForms = function (tForms) {
      $window.localStorage.setItem('divJob_TestForms', JSON.stringify(tForms));
      testForms = angular.copy(tForms);
    };
    var getTestForms = function () {
      if (testForms === null || testForms === undefined) {
        testForms = JSON.parse($window.localStorage.getItem('divJob_TestForms'));
      }
      return angular.copy(testForms);
    };

    var getAvailableTestCodes = function () {
      if (availableTestCodes === null || availableTestCodes === undefined) {
        availableTestCodes = JSON.parse($window.localStorage.getItem('divJob_AvailableTestCodes'));
      }
      return availableTestCodes;
    };
    var setAvailableTestCodes = function (testCodes) {
      $window.localStorage.setItem('divJob_AvailableTestCodes', JSON.stringify(testCodes));
      availableTestCodes = angular.copy(testCodes);
    };

    var getScenarios = function () {
      if (scenarios === null || scenarios === undefined) {
        scenarios = JSON.parse($window.localStorage.getItem('divJob_Scenarios'));
      }
      return scenarios;
    };
    var setScenarios = function (sc) {
      $window.localStorage.setItem('divJob_Scenarios', JSON.stringify(sc));
      scenarios = angular.copy(sc);
    };

    var getStudentScenarios = function () {
      if (studentScenarios === null || studentScenarios === undefined) {
        studentScenarios = JSON.parse($window.localStorage.getItem('divJob_StudentScenarios'));
      }
      return studentScenarios;
    };
    var setStudentScenarios = function (ss) {
      $window.localStorage.setItem('divJob_StudentScenarios', JSON.stringify(ss));
      studentScenarios = ss;
    };

    var getErrorMessage = function () {
      return errorMessage;
    };

    var setErrorMessage = function (errorMsg) {
      errorMessage = errorMsg;
    };

    var setPathStates = function (selectedPathStates) {
      $window.localStorage.setItem('divJob_PathStates', JSON.stringify(selectedPathStates));
      selectedPathStates = angular.copy(selectedPathStates);
    };
    var getPathStates = function () {
      if (selectedPathStates === null || selectedPathStates === undefined) {
        selectedPathStates = JSON.parse($window.localStorage.getItem('divJob_PathStates'));
      }
      return selectedPathStates;
    };

    var clear = function () {
      //Removing divJob records from localStorage
      $window.localStorage.removeItem('divJob_testType');
      $window.localStorage.removeItem('divJob_Environment');
      $window.localStorage.removeItem('divJob_PathId');
      $window.localStorage.removeItem('divJob_ScopeTreePath');
      $window.localStorage.removeItem('divJob_TestSessions');
      $window.localStorage.removeItem('divJob_TestnavScenarioClasses');
      $window.localStorage.removeItem('divJob_EpenScenarioClasses');
      $window.localStorage.removeItem('divJob_TestCodes');
      $window.localStorage.removeItem('divJob_TestForms');
      $window.localStorage.removeItem('divJob_AvailableTestCodes');
      $window.localStorage.removeItem('divJob_Scenarios');
      $window.localStorage.removeItem('divJob_StudentScenarios');
      $window.localStorage.removeItem('divJob_PathStates');

      //clearing divJob records
      testnavScenarioClasses = undefined;
      selectedEnvironment = undefined;
      testType = undefined;
      selectedScopeTreePath = undefined;
      testSessions = undefined;
      testnavScenarioClasses = [];
      epenScenarioClasses = [];
      testForms = undefined;
      availableTestCodes = undefined;
      divJobPathId = undefined;
      scenarios = undefined;
      selectedTestCodes = undefined;
      studentScenarios = undefined;
      selectedPathStates = undefined;

    };
    return {
      setEnvironment: setEnvironment,
      getEnvironment: getEnvironment,
      setTestType: setTestType,
      getTestType: getTestType,
      setScopeTreePath: setScopeTreePath,
      getScopeTreePath: getScopeTreePath,
      setDivJobPathId: setDivJobPathId,
      getDivJobPathId: getDivJobPathId,
      getTestSessions: getTestSessions,
      setTestSessions: setTestSessions,
      setTestnavScenarioClasses: setTestnavScenarioClasses,
      getTestnavScenarioClasses: getTestnavScenarioClasses,
      setEpenScenarioClasses: setEpenScenarioClasses,
      getEpenScenarioClasses: getEpenScenarioClasses,
      setTestCodes: setTestCodes,
      getTestCodes: getTestCodes,
      setTestForms: setTestForms,
      getTestForms: getTestForms,
      getAvailableTestCodes: getAvailableTestCodes,
      setAvailableTestCodes: setAvailableTestCodes,
      getScenarios: getScenarios,
      setScenarios: setScenarios,
      getStudentScenarios: getStudentScenarios,
      setStudentScenarios: setStudentScenarios,
      getErrorMessage: getErrorMessage,
      setErrorMessage: setErrorMessage,
      getPathStates: getPathStates,
      setPathStates: setPathStates,
      clear: clear
    };
  }
})();
