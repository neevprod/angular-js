(function () {

  'use strict';

  angular.module('systemValidation').controller('ApplyAllFormsController',
    ApplyAllFormsController);

  ApplyAllFormsController.$inject = ['$uibModalInstance'];

  function ApplyAllFormsController($uibModalInstance) {
    var viewModel = this;

    viewModel.message = 'This will replace the scenario(s) selected for all other forms with the scenario(s) selected for the current form. Are you sure you want to proceed?';
    viewModel.applyToAll = applyToAll;
    viewModel.close = close;


    function applyToAll() {
      $uibModalInstance.close('Yes');
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }

})();
