(function () {

  'use strict';

  angular.module('systemValidation')
    .controller('PercentEntryController', PercentEntryController);

  PercentEntryController.$inject = ['$uibModalInstance'];

  function PercentEntryController($uibModalInstance) {
    var viewModel = this;

    viewModel.submit = submit;
    viewModel.close = close;

    /**
     * Submit the password change if the form is valid.
     *
     * @param isValid A boolean indicating whether the form is valid.
     */
    function submit(isValid) {
      if (isValid) {
        $uibModalInstance.close(viewModel.percent);
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }

  }

})();
