(function () {

  'use strict';

  angular.module('systemValidation').controller('ScenarioSelectionController', ScenarioSelectionController);

  ScenarioSelectionController.$inject = ['$stateParams', 'itemsPerPageOptions', 'divJobSetupService',
    'scenarioSelectionService', 'studentSelectionService', '$state', '$timeout', '$filter', '$uibModal', '$interval',
    'jobStatusCheckInterval', '$scope'];

  function ScenarioSelectionController($stateParams, itemsPerPageOptions, divJobSetupService,
                                       scenarioSelectionService, studentSelectionService, $state, $timeout, $filter, $uibModal, $interval, jobStatusCheckInterval, $scope) {
    var viewModel = this;
    viewModel.loadingScenarios = true;
    viewModel.scenariosInitialized = false;
    viewModel.alertMessage = [];
    viewModel.warningMessage = '';
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = viewModel.itemsPerPageOptions[0];
    viewModel.testForms = [];
    viewModel.displayedTestForms = [];
    viewModel.testnavScenarioClasses = [];
    viewModel.epenScenarioClasses = [];
    viewModel.environment = divJobSetupService.getEnvironment();
    viewModel.epenItemExist = false;
    viewModel.selectedPathStates = divJobSetupService.getPathStates();
    viewModel.submit = submit;
    viewModel.addSelectedScenarios = addSelectedScenarios;
    viewModel.addSelectedScenariosForBatteryTest = addSelectedScenariosForBatteryTest;
    viewModel.deleteSelectedScenario = deleteSelectedScenario;
    viewModel.deleteSelectedBatteryScenario = deleteSelectedBatteryScenario;
    viewModel.applyToAll = applyToAll;
    viewModel.applyToAllBatteryForms = applyToAllBatteryForms;
    viewModel.displayPercentEntryDialog = displayPercentEntryDialog;

    if (viewModel.selectedPathStates && viewModel.selectedPathStates.indexOf(4) === -1) {
      viewModel.disableEpen2 = true;
    } else {
      viewModel.disableEpen2 = false;
    }
    if (viewModel.environment === null) {
      divJobSetupService.setErrorMessage('Please select environment details.');
      $state.go('environments');
      return;
    }
    viewModel.messages = [];
    var testType = divJobSetupService.getTestType();

    if (!testType) {
      $state.go('environments');
      return;
    }

    var scopeTreePath = divJobSetupService.getScopeTreePath();

    if (!scopeTreePath) {
      $state.go('environments');
      return;
    }

    var inputChanged = isInputChanged();
    var testSessions;
    if (!inputChanged) {
      viewModel.testnavScenarioClasses = divJobSetupService.getTestnavScenarioClasses();
      viewModel.epenScenarioClasses = divJobSetupService.getEpenScenarioClasses();
      testSessions = divJobSetupService.getTestSessions();
    } else {
      divJobSetupService.setTestSessions($stateParams.testSessions);
      testSessions = $stateParams.testSessions;
    }
    var sessionIds = [];
    var testCodes = [];
    if (testType === 'Battery') {
      sessionIds = readBatterySessionIds(testSessions);
    } else {
      sessionIds = readSessionIds(testSessions);
    }

    if (!sessionIds) {
      $state.go('environments');
      return;
    }

    if (angular.isObject(divJobSetupService.getTestForms()) && !inputChanged) {

      $timeout(function () {
        viewModel.loadingScenarios = false;
      }, 0);
      viewModel.scenariosInitialized = true;
      viewModel.testForms = divJobSetupService.getTestForms();
      viewModel.displayedTestForms = divJobSetupService.getTestForms();
    } else {
      if (inputChanged) {
        divJobSetupService.setTestForms($stateParams.testSessions);
      }
      getTestForms();
    }

    var intervalPromise;

    function getTestForms() {
      displayLoading();
      viewModel.alertMessage = [];
      scenarioSelectionService.startGetTestFormsJob(viewModel.environment, testType, scopeTreePath, testCodes, sessionIds).then(
        function (result) {
          if (result.success) {
            var jobId = result.data.jobId;

            intervalPromise = $interval(function () {
              checkGetTestFormsJob(jobId);
            }, jobStatusCheckInterval);
          } else {
            viewModel.loadingScenarios = false;
            viewModel.scenariosInitialized = true;
            viewModel.alertSuccess = false;
            viewModel.alertMessage.push(result.errors[0].message);
          }
        });
    }

    /**
     * Check the status of the get test forms job. If it's complete, set the test forms to the DIV job service.
     *
     * @param {string} jobId - The ID of the job to check.
     */
    function checkGetTestFormsJob(jobId) {
      scenarioSelectionService.checkGetTestFormsJob(jobId).then(function (result) {
        if (!result.success) {
          viewModel.loadingScenarios = false;
          viewModel.scenariosInitialized = true;
          viewModel.alertSuccess = false;
          viewModel.alertMessage.push(result.errors[0].message);
        } else if (!result.data.isRunning) {
          viewModel.loadingScenarios = false;
          viewModel.scenariosInitialized = true;
          if (result.data.jobSuccessful) {
            viewModel.testForms = result.data.jobData.forms.testDetails;
            viewModel.displayedTestForms = result.data.jobData.forms.testDetails;
            viewModel.testnavScenarioClasses = result.data.jobData.forms.testnavScenarioClasses;
            divJobSetupService.setTestnavScenarioClasses(viewModel.testnavScenarioClasses);
            viewModel.epenScenarioClasses = result.data.jobData.forms.epenScenarioClasses;
            divJobSetupService.setEpenScenarioClasses(viewModel.epenScenarioClasses);
            divJobSetupService.setTestForms(viewModel.testForms);
            var errorForms = '';
            viewModel.testForms.forEach(function (testForm) {
              if (testForm.testCaseCoverage && testForm.testCaseCoverage.length > 0) {
                var csvData = new Blob([testForm.testCaseCoverage], {type: 'text/csv;charset=utf-8'});
                testForm.testCaseCoverage = URL.createObjectURL(csvData);
                if (errorForms === '') {
                  if (testForm.errorFormCodes) {
                    errorForms += testForm.errorFormCodes.toString();
                  } else {
                    errorForms += testForm.formCode;
                  }
                } else {
                  if (testForm.errorFormCodes) {
                    errorForms += ', ' + testForm.errorFormCodes.toString();
                  } else {
                    errorForms += ', ' + testForm.formCode;
                  }
                }
              }
            });
            if (errorForms.length > 0) {
              viewModel.alertMessage.push('Insufficient Item Test Case Coverage found for following forms: ' + errorForms);
            }

            if (result.data.jobErrors) {
              viewModel.alertSuccess = undefined;
              result.data.jobErrors.forEach(function (error) {
                viewModel.alertMessage.push(error.message);
              });
            }
          } else {
            viewModel.alertSuccess = false;
            if (result.data.jobErrors[0].message !== undefined && result.data.jobErrors[0].message.indexOf('TestNav') !== -1) {
              viewModel.messages = result.data.jobErrors[0].message.split(';;');
              viewModel.alertMessage.push(viewModel.messages[1]);
            } else {
              viewModel.alertMessage.push(result.data.jobErrors[0].message);
            }
          }
        }

        if (!viewModel.loadingScenarios) {
          $interval.cancel(intervalPromise);
        }
      });
    }

    function readSessionIds(testSessions) {
      var sessionIds = [];
      angular.forEach(testSessions, function (testDetail) {
        testCodes.push(testDetail.testCode);
        angular.forEach(testDetail.selectedTestSessions, function (session) {
          sessionIds.push(session.sessionId);
        });

      });
      return sessionIds;
    }

    function readBatterySessionIds(testDetails) {
      var sessionIds = [];
      angular.forEach(testDetails, function (testDetail) {
        testCodes.push(testDetail.testCode);
        angular.forEach(testDetail.selectedTestSessions, function (testSession) {
          sessionIds.push(testSession.sessionId);
        });

      });
      return sessionIds;
    }


    function submit() {
      viewModel.submitted = true;
      viewModel.alertMessage = [];
      var invalidScenarioSelection = [];
      var invalidNoAttemptedScenarioForms = [];
      var notSelectedScenarioCounter = 0;
      for (var j = 0; j < viewModel.testForms.length; j++) {
        var selectedScenarios = viewModel.testForms[j].selectedScenarios;
        if (!selectedScenarios || selectedScenarios.length === 0) {
          notSelectedScenarioCounter++;

        }
      }
      var validScenarioSelection = true;

      if (notSelectedScenarioCounter === viewModel.testForms.length) {
        validScenarioSelection = false;
        viewModel.alertSuccess = false;
        viewModel.alertMessage.push('Please select a scenario for at least one form.');
      }
      if (invalidScenarioSelection.length > 0) {
        validScenarioSelection = false;
        viewModel.alertSuccess = false;
        viewModel.alertMessage
          .push('TestNav/ePEN2 scenario is not selected for the following form codes: ' + invalidScenarioSelection
            .join(', '));
      }

      if (invalidNoAttemptedScenarioForms.length > 0) {
        validScenarioSelection = false;
        viewModel.alertSuccess = false;
        viewModel.alertMessage
          .push('Please select nonAttempted ePEN2 scenario for the following form codes: ' + invalidNoAttemptedScenarioForms
            .join(', '));
      }

      if (validScenarioSelection) {
        $state.go('studentSelection', {
          environmentId: viewModel.environment.environmentId,
          'scenarios': viewModel.testForms
        });
      } else {
        viewModel.submitted = false;
      }
    }


    function isInputChanged() {
      var curTestSessions = $stateParams.testSessions;
      var prevTestSessions = divJobSetupService.getTestSessions();
      if (curTestSessions === null) {
        curTestSessions = prevTestSessions;
      }
      if (angular.equals(curTestSessions, prevTestSessions)) {
        return false;
      }
      return true;
    }

    viewModel.clearAlertMessages = function () {
      viewModel.alertMessage = [];
    };
    viewModel.clearAlertMessage = function (testDetail) {
      testDetail.alertMessage = '';
    };
    $scope.$on('$destroy', function () {
      $interval.cancel(intervalPromise);
    });


    function addSelectedScenarios(testDetail) {
      testDetail.alertMessage = '';
      if (!testDetail.selectedTestNavScenario) {
        testDetail.showErrorMessage = true;
        return;
      }
      viewModel.loadingScenarios = true;
      displayLoading();
      viewModel.alertMessage = [];
      //Appending percentage value to the testNav scenario class name
      if (testDetail.selectedTestNavScenario === 'percentCorrect' || (testDetail.selectedTestNavScenario && testDetail.selectedTestNavScenario.scenarioClass === 'percentCorrect')) {
        testDetail.selectedTestNavScenario = 'percentCorrect(' + testDetail.testNavPercentValue + '%)';
      }
      //Appending percentage value to the epen scenario class name
      if (testDetail.selectedEpenScenario === 'percentCorrect' || (testDetail.selectedEpenScenario && testDetail.selectedEpenScenario.scenarioClass === 'percentCorrect')) {
        testDetail.selectedEpenScenario = 'percentCorrect(' + testDetail.epenPercentValue + '%)';
      }
      //Initializing selectedScenario array
      if (!testDetail.selectedScenarios) {
        testDetail.selectedScenarios = [];
      }
      for (var i = 0; i < testDetail.selectedScenarios.length; i++) {
        var scenario = testDetail.selectedScenarios[i];
        scenario.epen = (scenario.epen === null) ? undefined : scenario.epen;
        if (angular.equals(scenario.epen, testDetail.selectedEpenScenario) && angular.equals(scenario.testNav, testDetail.selectedTestNavScenario)) {
          viewModel.loadingScenarios = false;
          testDetail.alertSuccess = false;
          testDetail.alertMessage = 'The selection is already made for this scenario class combination.';
          return;
        }
      }

      //constructing request body from testDetail
      var selectedForm = {
        'scopeTreePath': scopeTreePath,
        'testCode': testDetail.testCode,
        'formCode': testDetail.formCode,
        'previewerUrl': testDetail.previewerUrl,
        'tenant': testDetail.tenant,
        'tn8CustomerCode': testDetail.testNavCustomerCode,
        'includesAdaptiveIndicator': testDetail.includesAdaptiveIndicator,
        'availableStudents': testDetail.numberOfStudents,
        'testnavScenarioClass': testDetail.selectedTestNavScenario,
        'epenScenarioClass': testDetail.selectedEpenScenario,
        'selectedScenarios': testDetail.selectedScenarios,
        'epenScenarioDisable': testDetail.epenScenarioDisable,
        'testSessions': testDetail.testSessions
      };

      //constructing the selected scenario to display.
      var selectedScenario = {
        testNav: testDetail.selectedTestNavScenario,
        epen: testDetail.selectedEpenScenario,
        maxCount: ''
      };
      scenarioSelectionService.getMaxScenarioCount(viewModel.environment.environmentId, selectedForm).then(
        function (result) {
          viewModel.loadingScenarios = false;
          if (result.success) {
            selectedScenario.maxCount = result.data.totalStudentsUsed;
            selectedScenario.testNavScenarioNames = result.data.testNavScenarioNames;
            selectedScenario.epenScenarioNames = result.data.epenScenarioNames;
            selectedScenario.uuids = result.data.uuids;
            selectedScenario.warning = result.data.warning;
            selectedScenario.warningMessage = result.data.warningMessage;
            testDetail.selectedScenarios.push(selectedScenario);
            testDetail.remainingStudents = result.data.remainingStudents;
            testDetail.selectedTestNavScenario = undefined;
            testDetail.selectedEpenScenario = undefined;
            testDetail.testNavPercentValue = undefined;
            testDetail.epenPercentValue = undefined;
            for (var i = 0; i < viewModel.testForms.length; i++) {
              if (viewModel.testForms[i].testCode === testDetail.testCode && viewModel.testForms[i].formCode === testDetail.formCode) {
                viewModel.testForms[i] = testDetail;
              }
            }
            viewModel.displayedTestForms = viewModel.testForms;
            divJobSetupService.setTestForms(viewModel.testForms);
          } else {
            testDetail.alertSuccess = false;
            testDetail.alertMessage = result.errors[0].message;
          }
        });
      testDetail.showErrorMessage = undefined;
    }

    function addSelectedScenariosForBatteryTest(selectedForm, index) {
      selectedForm.alertSuccess = true;
      selectedForm.alertMessage = '';
      if (!selectedForm.selectedTestNavScenario) {
        selectedForm.showErrorMessage = true;
        return;
      }
      displayLoading();
      viewModel.loadingScenarios = true;
      viewModel.alertMessage = [];
      selectedForm.scopeTreePath = scopeTreePath;
      //Initializing selectedScenario array
      if (!selectedForm.selectedScenarios) {
        selectedForm.selectedScenarios = [];
      }

      //Appending percentage value to the testNav scenario class name
      if (selectedForm.selectedTestNavScenario === 'percentCorrect' || selectedForm.selectedTestNavScenario.scenarioClass === 'percentCorrect') {
        selectedForm.selectedTestNavScenario = 'percentCorrect(' + selectedForm.testNavPercentValue + '%)';
      }
      //Appending percentage value to the epen scenario class name
      if (selectedForm.selectedEpenScenario === 'percentCorrect' ||
        (selectedForm.selectedEpenScenario !== undefined && selectedForm.selectedEpenScenario !== null && selectedForm.selectedEpenScenario.scenarioClass === 'percentCorrect')) {
        selectedForm.selectedEpenScenario = 'percentCorrect(' + selectedForm.epenPercentValue + '%)';
      }

      //constructing request body from selected form
      var selectedScenario = {
        testNav: selectedForm.selectedTestNavScenario,
        epen: selectedForm.selectedEpenScenario
      };

      for (var i = 0; i < selectedForm.selectedScenarios.length; i++) {
        var scenario = selectedForm.selectedScenarios[i];
        if (angular.equals(scenario.epen, selectedScenario.epen) && angular.equals(scenario.testNav, selectedScenario.testNav)) {
          viewModel.loadingScenarios = false;
          selectedForm.alertSuccess = false;
          selectedForm.alertMessage = 'The selection is already made for this scenario class combination.';
          return;
        }
      }
      selectedForm.index = index;
      scenarioSelectionService.getMaxBatteryScenarioCount(viewModel.environment.environmentId, selectedForm).then(
        function (result) {
          viewModel.loadingScenarios = false;
          if (result.success) {
            var form = angular.copy(viewModel.testForms[result.formIndex]);
            delete form.selectedTestNavScenario;
            delete form.selectedEpenScenario;
            selectedScenario.units = result.data.units;
            selectedScenario.maxCount = result.data.totalStudentsUsed;
            selectedScenario.warning = result.data.warning;
            selectedScenario.warningMessage = result.data.warningMessage;
            form.selectedScenarios.push(selectedScenario);
            form.remainingStudents = result.data.remainingStudents;

            viewModel.testForms[result.formIndex] = form;
            viewModel.displayedTestForms = viewModel.testForms;
            divJobSetupService.setTestForms(viewModel.testForms);
          } else {
            selectedForm.alertSuccess = false;
            selectedForm.alertMessage = result.errors[0].message;
          }
        });
      selectedForm.showErrorMessage = undefined;
    }

    function deleteSelectedScenario(testDetail, selectedScenario) {
      viewModel.loadingScenarios = true;
      testDetail.alertSuccess = true;
      testDetail.alertMessage = '';
      var index = testDetail.selectedScenarios.indexOf(selectedScenario);
      if (index > -1) {
        var uuids = [];
        for (var uuid in selectedScenario.uuids) {
          uuids.push(uuid);
        }

        studentSelectionService
          .releaseReservedStudent(uuids.join('_'))
          .then(function (result) {
            if (result.success) {

              scenarioSelectionService.getAvailableStudentsCount(viewModel.environment.environmentId, scopeTreePath, testDetail.testCode, testDetail.formCode)
                .then(function (result) {
                  viewModel.loadingScenarios = false;
                  if (result.success) {
                    testDetail.remainingStudents = result.data.availableStudentsCount;
                    testDetail.selectedScenarios.splice(index, 1);
                    for (var i = 0; i < viewModel.testForms.length; i++) {
                      if (viewModel.testForms[i].testCode === testDetail.testCode && viewModel.testForms[i].formCode === testDetail.formCode) {
                        viewModel.testForms[i] = testDetail;
                      }
                    }
                    viewModel.displayedTestForms = viewModel.testForms;
                    divJobSetupService.setTestForms(viewModel.testForms);
                  } else {
                    viewModel.alertSuccess = false;
                    viewModel.alertMessage.push(result.errors[0].message);
                  }
                });
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage.push(result.errors[0].message);
            }
          });
      }
    }

    function deleteSelectedBatteryScenario(testDetail, selectedScenario) {
      displayLoading();
      selectedScenario.alertSuccess = true;
      viewModel.clearAlertMessages();
      viewModel.loadingScenarios = true;
      var index = testDetail.selectedScenarios.indexOf(selectedScenario);
      testDetail.alertSuccess = true;
      testDetail.alertMessage = '';
      var uuids = [];
      var unitTestCodes = [];
      var unitFormCodes = [];
      angular.forEach(selectedScenario.units, function (unit) {
        angular.forEach(unit.uuids, function (u) {
          uuids.push(u.uuid);
        });
        unitTestCodes.push(unit.testCode);
        unitFormCodes.push(unit.formCode);
      });

      var key = testDetail.batteryTestCode + unitTestCodes.join('_') + unitFormCodes.join('_');
      studentSelectionService
        .releaseReservedStudent(uuids.join('_'))
        .then(function (result) {
          viewModel.loadingScenarios = false;
          if (result.success) {
            scenarioSelectionService.getAvailableBatteryStudentCount(viewModel.environment.environmentId, scopeTreePath, sessionIds, testDetail.batteryTestCode, key)
              .then(function (result) {
                if (result.success) {
                  var selectedUnitTestCodes = [];
                  var selectedUnitFormCodes = [];
                  angular.forEach(testDetail.units, function (unit) {
                    selectedUnitTestCodes.push(unit.testCode);
                    selectedUnitFormCodes.push(unit.formCode);
                  });
                  var selectedBatteryTestFormCodes = testDetail.batteryTestCode + selectedUnitTestCodes.join('_') + selectedUnitFormCodes.join('_');
                  testDetail.remainingStudents = result.data.remainingStudents;
                  testDetail.selectedScenarios.splice(index, 1);
                  for (var i = 0; i < viewModel.testForms.length; i++) {
                    var testForm = viewModel.testForms[i];
                    var unitTestCodes = [];
                    var unitFormCodes = [];
                    for (var unitIndex = 0; unitIndex < testForm.units.length; unitIndex++) {
                      unitTestCodes.push(testForm.units[unitIndex].testCode);
                      unitFormCodes.push(testForm.units[unitIndex].formCode);
                    }
                    var batteryTestFormCodes = testForm.batteryTestCode + unitTestCodes.join('_') + unitFormCodes.join('_');

                    if (batteryTestFormCodes === selectedBatteryTestFormCodes) {
                      viewModel.testForms[i] = testDetail;
                    }
                  }
                  viewModel.displayedTestForms = viewModel.testForms;
                  divJobSetupService.setTestForms(viewModel.testForms);
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage.push(result.errors[0].message);
                }
              });
          } else {
            testDetail.alertSuccess = false;
            testDetail.alertMessage = result.errors[0].message;
          }
        });
    }

    function applyToAll(scenarios, formIndex) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/systemValidation/divJobSetup/scenarioSelection/confirmationModal.html',
        controller: 'ApplyAllFormsController as applyAllFormsCtrl',
        backdrop: 'static'
      });
      modalInstance.result.then(function (action) {
          if (action === 'Yes') {
            viewModel.loadingScenarios = true;
            displayLoading();
            var warningForms = [];
            var counter = {'count': 0, 'doNotProcessFormCounts': 0};
            viewModel.loadingScenarios = true;
            viewModel.alertMessage = [];
            var copyOfSelectedScenarios = angular.copy(scenarios);
            angular.forEach(copyOfSelectedScenarios, function (selectedScenario) {
              delete selectedScenario.maxCount;
              delete selectedScenario.testNavScenarioNames;
              delete selectedScenario.epenScenarioNames;
              delete selectedScenario.uuids;
            });

            for (var index = 0; index < viewModel.testForms.length; index++) {
              var selectedScenarios = angular.copy(copyOfSelectedScenarios);
              var form = viewModel.testForms[index];
              form.alertSuccess = true;
              form.alertMessage = '';
              if (index === formIndex) {
                counter.doNotProcessFormCounts++;
                continue;
              }
              if (form.remainingStudents === 0 && !(form.selectedScenarios && form.selectedScenarios.length > 0)) {
                warningForms.push(form.formCode);
                counter.doNotProcessFormCounts++;
                form.alertSuccess = false;
                form.alertMessage = 'No available student test sessions found for this form.';
                viewModel.testForms[index] = form;
                viewModel.displayedTestForms = viewModel.testForms;
                divJobSetupService.setTestForms(viewModel.testForms);
                if (counter.doNotProcessFormCounts === viewModel.testForms.length - 1) {
                  viewModel.warningMessage = 'Due to the lack of students all selected scenarios are not applied to the following forms: ' + warningForms.join(', ');
                  viewModel.loadingScenarios = false;
                }
                continue;
              }

              //if a form does not contain epen items then remove selected EPEN scenario for this form
              if (form.epenScenarioDisable === true) {
                angular.forEach(selectedScenarios, function (selectedScenario) {
                  if (selectedScenario.epen) {
                    selectedScenario.epen = undefined;
                  }
                });
              }

              angular.forEach(form.selectedScenarios, function (selectedScenario) {
                delete selectedScenario.testNav;
                delete selectedScenario.maxCount;
                delete selectedScenario.testNavScenarioNames;
                delete selectedScenario.epenScenarioNames;
              });
              applySelectedScenarios(selectedScenarios, form, index, warningForms, counter);
            }
            if (viewModel.testForms.length === counter.doNotProcessFormCounts) {
              viewModel.loadingScenarios = false;
              if (warningForms.length > 0) {
                viewModel.warningMessage = 'Due to the lack of students all selected scenarios are not applied to the following forms: ' + warningForms.join(', ');
              }
            }
          }
        }
      );
    }

    function applySelectedScenarios(selectedScenarios, form, i, warningForms, counter) {
      //constructing request body from testDetail
      var selectedForm = {
        'scopeTreePath': scopeTreePath,
        'testCode': form.testCode,
        'formCode': form.formCode,
        'previewerUrl': form.previewerUrl,
        'tenant': form.tenant,
        'tn8CustomerCode': form.testNavCustomerCode,
        'includesAdaptiveIndicator': form.includesAdaptiveIndicator,
        'selectedScenarios': selectedScenarios,
        'existingScenarios': form.selectedScenarios
      };
      scenarioSelectionService.applySelectedScenarios(viewModel.environment.environmentId, selectedForm).then(
        function (result) {
          counter.count++;
          if (result.success) {
            var testForm = viewModel.testForms[i];
            if (testForm.alertMessage.length === 0) {
              testForm.alertSuccess = true;
              testForm.alertMessage = '';
            }
            testForm.selectedScenarios = result.data.selectedScenariosResult;
            testForm.remainingStudents = result.data.remainingStudents;
            if (result.data.alertMessage) {
              testForm.alertMessage = result.data.alertMessage;
              warningForms.push(testForm.formCode);
            }
            delete testForm.selectedTestNavScenario;
            delete testForm.selectedEpenScenario;
            viewModel.testForms[i] = testForm;
            viewModel.displayedTestForms = viewModel.testForms;
            divJobSetupService.setTestForms(viewModel.testForms);
          } else {
            for (var j = 0; j < viewModel.testForms.length; j++) {
              var form = viewModel.testForms[j];
              if (result.data.testCode === form.testCode && result.data.formCode === form.formCode) {
                form.alertSuccess = false;
                form.alertMessage = result.errors[0].message;
              }
            }
          }
          if (counter.count === (viewModel.testForms.length - 1 - counter.doNotProcessFormCounts)) {
            viewModel.loadingScenarios = false;
            if (warningForms.length > 0) {
              viewModel.warningMessage = 'Due to the lack of students all selected scenarios are not applied to the following forms: ' + warningForms.join(', ');
            }
          }
        });
    }

    function applyToAllBatteryForms(scenarios, formIndex) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/systemValidation/divJobSetup/scenarioSelection/confirmationModal.html',
        controller: 'ApplyAllFormsController as applyAllFormsCtrl',
        backdrop: 'static'
      });
      modalInstance.result.then(function (action) {
        if (action === 'Yes') {
          displayLoading();
          var warningForms = [];
          var counter = {'count': 0};
          var doNotProcessFormCounts = 0;
          viewModel.loadingScenarios = true;
          viewModel.alertMessage = [];
          //deep copy of selected scenario
          var selectedScenarios = angular.copy(scenarios);

          //Removing maxCount and Units attributes from selected scenario to reduce the size of request body.
          angular.forEach(selectedScenarios, function (selectedScenario) {
            delete selectedScenario.maxCount;
            delete selectedScenario.units;
          });
          for (var index = 0; index < viewModel.testForms.length; index++) {
            var form = angular.copy(viewModel.testForms[index]);
            form.alertSuccess = true;
            form.alertMessage = '';
            if (index === formIndex) {
              continue;
            }
            if (form.remainingStudents === 0 && !(form.selectedScenarios && form.selectedScenarios.length > 0)) {
              warningForms.push(form.formCode);
              doNotProcessFormCounts++;
              form.alertSuccess = false;
              form.alertMessage = 'No available student test sessions found for this form.';
              viewModel.testForms[index] = form;
              divJobSetupService.setTestForms(viewModel.testForms);
              if (doNotProcessFormCounts === viewModel.testForms.length) {
                viewModel.warningMessage = 'Due to the lack of students all selected scenarios are not applied to the following forms: ' + warningForms.join(', ');
                viewModel.loadingScenarios = false;
              }
              continue;
            }
            form.scopeTreePath = scopeTreePath;
            //if a form does not contain epen items then remove selected EPEN scenario for this form
            if (form.epenScenarioDisable === true) {
              angular.forEach(selectedScenarios, function (selectedScenario) {
                if (selectedScenario.epen) {
                  delete selectedScenario.epen;
                }
              });
            }
            //Initializing selectedScenario array
            form.selectedScenarios = angular.copy(selectedScenarios);
            form.formIndex = index;
            applySelectedBatteryScenarios(form, counter);
          }

        }
      });
    }

    function applySelectedBatteryScenarios(form, counter) {
      scenarioSelectionService.applySelectedBatteryScenarios(viewModel.environment.environmentId, form).then(
        function (result) {
          var i = result.formIndex;
          counter.count++;
          if (result.success) {
            var testForm = viewModel.testForms[i];
            testForm.alertSuccess = true;
            testForm.alertMessage = '';
            testForm.selectedScenarios = result.data.selectedScenariosResult;
            testForm.remainingStudents = result.data.remainingStudents;
            delete testForm.selectedTestNavScenario;
            delete testForm.selectedEpenScenario;
            viewModel.testForms[i] = testForm;
            viewModel.displayedTestForms = viewModel.testForms;
            divJobSetupService.setTestForms(viewModel.testForms);
          } else {
            var form = viewModel.testForms[i];
            form.alertSuccess = false;
            form.alertMessage = result.errors[0].message;
            viewModel.testForms[i] = form;
            viewModel.displayedTestForms = viewModel.testForms;
          }
          if (counter.count === viewModel.testForms.length - 1) {
            viewModel.loadingScenarios = false;
          }
        });
    }

    function displayPercentEntryDialog(testDetail, scenarioType) {
      if ((scenarioType === 'testNav' && testDetail.selectedTestNavScenario !== 'percentCorrect')) {
        testDetail.testNavPercentValue = undefined;
        return;
      }
      if ((scenarioType === 'epen' && testDetail.selectedEpenScenario !== 'percentCorrect')) {
        testDetail.epenPercentValue = undefined;
        return;
      }
      var modal = $uibModal
        .open({
          templateUrl: 'app/systemValidation/divJobSetup/scenarioSelection/percentEntry/percentEntry.html',
          controller: 'PercentEntryController as percentEntryCtrl',
          backdrop: 'static',
          keyboard: false
        });
      modal.result.then(function (percentValue) {
        if (scenarioType === 'testNav') {
          testDetail.testNavPercentValue = percentValue;
          var disableEpenScenario = testDetail.epenScenarioDisable;
          if (testType === 'Battery') {
            disableEpenScenario = isEpenScenarioDisable(testDetail);
          }

          if (!testDetail.epenPercentValue && (disableEpenScenario === false && viewModel.disableEpen2 === false)) {
            angular.forEach(viewModel.epenScenarioClasses, function (epenScenarioClass) {
              if (epenScenarioClass.scenarioClass === 'percentCorrect') {
                testDetail.selectedEpenScenario = epenScenarioClass;
                testDetail.epenPercentValue = percentValue;
              }
            });
          }
        } else {
          testDetail.epenPercentValue = percentValue;
          if (!testDetail.testNavPercentValue) {
            angular.forEach(viewModel.testnavScenarioClasses, function (testNavScenarioClass) {
              if (testNavScenarioClass.scenarioClass === 'percentCorrect') {
                testDetail.selectedTestNavScenario = testNavScenarioClass;
                testDetail.testNavPercentValue = percentValue;
              }
            });
          }
        }
      });
    }

    function isEpenScenarioDisable(testDetail) {
      for (var i = 0; i < testDetail.units.length; i++) {
        if (testDetail.units[i].epenScenarioDisable === false) {
          return false;
        }
      }
      return true;
    }

    function displayLoading() {
      angular.element('#loadingOverlay').css('display', 'block');
    }
  }
})();
