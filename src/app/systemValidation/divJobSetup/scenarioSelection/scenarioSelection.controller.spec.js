'use strict';

describe('Scenario selection controller', function () {
  var controller;
  var $stateParams;
  var scenarioSelectionService;
  var deferredTestForms;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredTestForms = $q.defer();
    scenarioSelectionService = jasmine.createSpyObj('scenarioSelectionService', ['startGetTestFormsJob']);
    scenarioSelectionService.startGetTestFormsJob.and.callFake(function () {
      return deferredTestForms.promise;
    });

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;

    controller = $controller('ScenarioSelectionController', {
      $stateParams: $stateParams,
      scenarioSelectionService: scenarioSelectionService,
      itemsPerPageOptions: itemsPerPageOptions,
      $scope: $rootScope.$new()
    });

  }));

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization',
    function () {
      // Then
      expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0]);
    });

  it('should set the list of test forms to an empty array during initialization', function () {
    // Then
    expect(controller.testForms).toEqual([]);
  });

});
