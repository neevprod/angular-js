(function () {

  'use strict';

  angular.module('systemValidation').factory('scenarioSelectionService', scenarioSelectionService);

  scenarioSelectionService.$inject = ['$http', 'systemValidationApiPrefix'];

  function scenarioSelectionService($http, systemValidationApiPrefix) {
    return {
      startGetTestFormsJob: startGetTestFormsJob,
      checkGetTestFormsJob: checkGetTestFormsJob,
      getMaxScenarioCount: getMaxScenarioCount,
      getMaxBatteryScenarioCount: getMaxBatteryScenarioCount,
      getAvailableStudentsCount: getAvailableStudentsCount,
      getAvailableBatteryStudentCount: getAvailableBatteryStudentCount,
      applySelectedScenarios: applySelectedScenarios,
      applySelectedBatteryScenarios: applySelectedBatteryScenarios
    };

    function startGetTestFormsJob(environment, testType, scopeTreePath, testCodes, sessionIds) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environment.environmentId +
        '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) + '/sessionIds/' +
        sessionIds + '/testCodes/' + testCodes + '/getTestFormsJobs')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function checkGetTestFormsJob(jobId) {
      return $http.get(systemValidationApiPrefix + 'getTestFormsJobs/' + jobId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getMaxScenarioCount(environmentId, json) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/scenarios', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getMaxBatteryScenarioCount(environmentId, json) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/batteryScenarios', json)
        .then(function (result) {
          result.data.formIndex = json.index;
          return result.data;
        })
        .catch(function (result) {
          result.data.formIndex = json.index;
          return result.data;
        });
    }

    function getAvailableStudentsCount(environmentId, scopeTreePath, testCode, formCode) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/scopeTreePath/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) + '/testCodes/' + testCode + '/formCodes/' + formCode + '/availableStudentsCount')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getAvailableBatteryStudentCount(environmentId, scopeTreePath, sessionIds, batteryTestCode, key) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/scopeTreePath/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|'))
        + '/sessionIds/' + sessionIds + '/batteryTestCode/' + batteryTestCode + '/' + key + '/batteryStudentCounts')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function applySelectedScenarios(environmentId, json) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/applySelectedScenarios', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function applySelectedBatteryScenarios(environmentId, json) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/applySelectedScenariosToBattery', json)
        .then(function (result) {
          result.data.formIndex = json.formIndex;
          return result.data;
        })
        .catch(function (result) {
          result.data.formIndex = json.formIndex;
          return result.data;
        });
    }
  }

})();
