(function () {

  'use strict';

  angular.module('systemValidation').controller('StudentSelectionController', StudentSelectionController);

  StudentSelectionController.$inject = ['$stateParams', 'divJobSetupService', 'studentSelectionService',
    'divJobService', 'itemsPerPageOptions', '$state', '$interval', 'jobStatusCheckInterval', '$scope', '$filter'];

  function StudentSelectionController($stateParams, divJobSetupService, studentSelectionService, divJobService,
                                      itemsPerPageOptions, $state, $interval, jobStatusCheckInterval, $scope, $filter) {

    var viewModel = this;
    viewModel.alertSuccess = true;
    viewModel.alertMessage = '';
    viewModel.loadingStudentScenarios = true;
    viewModel.studentScenariosInitialized = false;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = viewModel.itemsPerPageOptions[0];
    viewModel.removeRow = removeRow;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.environment = divJobSetupService.getEnvironment();
    var scopeTreePath = divJobSetupService.getScopeTreePath();
    var testType = divJobSetupService.getTestType();
    if (!viewModel.environment || !scopeTreePath) {
      divJobSetupService.setErrorMessage('Please select environment details.');
      $state.go('environments');
      return;
    }

    var divJobPathId = divJobSetupService.getDivJobPathId();
    if ($stateParams.scenarios) {
      divJobSetupService.setTestForms($stateParams.scenarios);
      viewModel.testFormsJson = angular.copy($stateParams.scenarios);
    } else {
      viewModel.testFormsJson = divJobSetupService.getTestForms();
    }
    getStudents();
    viewModel.submit = submitStudents;

    var intervalPromise;

    function getStudents() {

      var selectedForm = [];

      angular.forEach(viewModel.testFormsJson, function (testForm) {
        if (testForm.selectedScenarios && testForm.selectedScenarios.length !== 0) {
          delete testForm.units;
          delete testForm.testSessions;
          delete testForm.remainingStudents;
          selectedForm.push(testForm);
        }
      });

      studentSelectionService.startGetStudentScenariosJob(viewModel.environment, testType, scopeTreePath, selectedForm).then(
        function (result) {
          if (result.success) {
            var jobId = result.data.jobId;

            intervalPromise = $interval(function () {
              checkGetStudentScenariosJob(jobId);
            }, jobStatusCheckInterval);
          } else {
            viewModel.loadingStudentScenarios = false;
            viewModel.studentScenariosInitialized = true;
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
    }

    function checkGetStudentScenariosJob(jobId) {
      studentSelectionService.checkGetStudentScenariosJob(jobId).then(function (result) {
        if (!result.success) {
          viewModel.loadingStudentScenarios = false;
          viewModel.studentScenariosInitialized = true;
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
        } else if (!result.data.isRunning) {
          viewModel.loadingStudentScenarios = false;
          viewModel.studentScenariosInitialized = true;
          if (result.data.jobSuccessful) {
            viewModel.studentScenarios = result.data.jobData.studentScenarios;
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.data.jobErrors[0].message;
          }
        }

        if (!viewModel.loadingStudentScenarios) {
          $interval.cancel(intervalPromise);
        }
      });
    }

    function submitStudents() {
      viewModel.submitted = true;
      var testType = divJobSetupService.getTestType();
      for (var i = 0; i < viewModel.studentScenarios.length; i++) {
        var studentScenario = viewModel.studentScenarios[i];
        if ((testType === 'Battery' && !studentScenario.batteryUuid) || (testType !== 'Battery' && !studentScenario.uuid)) {
          viewModel.submitted = false;
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'Some scenarios do not have a student assigned. Please either use a different session or remove some scenarios.';
          return;
        }
      }
      if (viewModel.submitted) {
        studentSelectionService.submitStudents(viewModel.environment, divJobPathId, scopeTreePath,
          viewModel.studentScenarios, testType).then(function (result) {
          if (result.success) {
            divJobSetupService.clear();
            startDivJob(result.data.divJob.divJobExecId);
          } else {
            viewModel.submitted = false;
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
      }
    }

    function startDivJob(jobId) {
      divJobService.startDivJob(viewModel.environment.environmentId, jobId).then(function (result) {
        if (result.success) {
          $state.go('divJobs', {
            environmentId: viewModel.environment.environmentId,
            divJobJustCreated: jobId
          });
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
        }
      });
    }

    /**
     * Removes the row from the table and release the student if all
     * scenarios has students otherwise assigns testSession to the next
     * available scenario.
     */
    function removeRow(student) {
      viewModel.alertSuccess = true;
      var index = viewModel.studentScenarios.indexOf(student);
      if (student.numberOfStudents < student.numberOfScenarios) {
        var studentWithoutTestSession;
        var unitTestCodesToDelete = [];
        var unitFormCodesToDelete = [];
        if (divJobSetupService.getTestType() === 'Battery') {
          for (var j = 0; j < student.studentScenarios.length; j++) {
            unitTestCodesToDelete.push(student.studentScenarios[j].testnavScenario.testCode);
            unitFormCodesToDelete.push(student.studentScenarios[j].testnavScenario.formCode);
          }
        }
        for (var i = 0; i < viewModel.studentScenarios.length; i++) {
          var studentScenario = viewModel.studentScenarios[i];
          if (divJobSetupService.getTestType() === 'Battery' && !studentScenario.batteryUuid) {
            var unitTestCodesToReplace = [];
            var unitFormCodesToReplace = [];
            for (var unitFormIndex = 0; unitFormIndex < studentScenario.studentScenarios.length; unitFormIndex++) {
              unitTestCodesToReplace.push(studentScenario.studentScenarios[unitFormIndex].testnavScenario.testCode);
              unitFormCodesToReplace.push(studentScenario.studentScenarios[unitFormIndex].testnavScenario.formCode);
            }
            if (studentScenario.batteryTestCode === student.batteryTestCode && unitTestCodesToDelete.join('_') === unitTestCodesToReplace.join('_') && unitFormCodesToDelete.join('_') === unitFormCodesToReplace.join('_')) {
              studentScenario.batteryUuid = student.batteryUuid;
              for (var selectedScenarioIndex = 0; selectedScenarioIndex < studentScenario.studentScenarios.length; selectedScenarioIndex++) {
                studentScenario.studentScenarios[selectedScenarioIndex].testSession = student.studentScenarios[selectedScenarioIndex].testSession;
                studentScenario.studentScenarios[selectedScenarioIndex].uuid = student.studentScenarios[selectedScenarioIndex].uuid;
              }
              studentWithoutTestSession = true;
              viewModel.alertMessage = '';
              break;
            }
          } else if (divJobSetupService.getTestType() !== 'Battery' && studentScenario.testnavScenario.testCode === student.testnavScenario.testCode
            && studentScenario.testnavScenario.formCode === student.testnavScenario.formCode
            && !studentScenario.uuid) {
            var testForms = divJobSetupService.getTestForms();
            for (var l = 0; l < testForms.length; l++) {
              var testForm = testForms[l];
              if (testForm.testCode === student.testnavScenario.testCode && testForm.formCode === student.testnavScenario.formCode) {
                for (var m = 0; m < testForm.selectedScenarios.length; m++) {
                  var selectedScenario = testForm.selectedScenarios[m];
                  if (selectedScenario.testNav === student.testnavScenario.scenarioClass.scenarioClass) {
                    selectedScenario.testNavScenarioNames.splice(selectedScenario.testNavScenarioNames.indexOf(student.testnavScenario.scenarioName), 1);
                    if (student.epenScenario) {
                      selectedScenario.epenScenarioNames.splice(selectedScenario.epenScenarioNames.indexOf(student.epenScenario.scenarioName), 1);
                    }
                  }
                }
              }
            }
            divJobSetupService.setTestForms(testForms);
            studentScenario.testSession = student.testSession;
            studentScenario.uuid = student.uuid;
            studentWithoutTestSession = true;
            viewModel.alertMessage = '';
            break;
          }
        }
        if (!studentWithoutTestSession) {
          releaseReservedStudent(student);
        }
      } else {
        releaseReservedStudent(student);
      }
      viewModel.studentScenarios.splice(index, 1);
    }

    function releaseReservedStudent(student) {
      viewModel.alertMessage = '';
      if (divJobSetupService.getTestType() === 'Battery') {
        angular.forEach(student.studentScenarios, function (studentScenario) {
          studentSelectionService
            .releaseReservedStudent(studentScenario.uuid)
            .then(
              function (result) {
                if (result.success) {
                  if (viewModel.alertMessage.length === 0) {
                    var formCode = student.studentScenarios.map(function (unit) {
                      return unit.testnavScenario.formCode;
                    }).join('_');
                    var testCode = student.studentScenarios.map(function (unit) {
                      return unit.testnavScenario.testCode;
                    }).join('_');
                    var batteryTestCode = student.batteryTestCode;
                    var testForms = divJobSetupService.getTestForms();
                    angular.forEach(testForms, function (form) {
                      var curFormCode = form.units.map(function (unit) {
                        return unit.formCode;
                      }).join('_');
                      var curTestCode = form.units.map(function (unit) {
                        return unit.testCode;
                      }).join('_');
                      var curBatteryTestCode = form.batteryTestCode;
                      if (curBatteryTestCode === batteryTestCode && curTestCode === testCode && curFormCode === formCode) {
                        angular.forEach(form.selectedScenarios, function (selectedScenario) {


                          var i = 0;
                          angular.forEach(selectedScenario.units, function (unit) {
                            var testnavScenarioIndex = unit.testNavScenarioNames.indexOf(student.studentScenarios[i].testnavScenario.scenarioName);
                            var epenScenarioIndex = unit.epenScenarioNames.indexOf(student.studentScenarios[i].epenScenario.scenarioName);
                            if (testnavScenarioIndex > -1 && testnavScenarioIndex < unit.testNavScenarioNames.length - 1) {
                              unit.testNavScenarioNames.splice(testnavScenarioIndex, 1);
                            }
                            if (epenScenarioIndex < unit.epenScenarioNames.length - 1) {
                              unit.epenScenarioNames.splice(epenScenarioIndex, 1);
                            }
                            if (unit.testNavScenarioNames.length === 1 && unit.epenScenarioNames.length === 1) {
                              unit.testNavScenarioNames.splice(testnavScenarioIndex, 1);
                              unit.epenScenarioNames.splice(epenScenarioIndex, 1);
                            }
                            delete unit.uuids[$filter('filter')(unit.uuids, {uuid: student.studentScenarios[i].uuid})];
                            i++;
                          });
                          form.remainingStudents += 1;
                          selectedScenario.maxCount -= 1;

                          var testnavScenarioNameFound = false;
                          selectedScenario.units[0].testNavScenarioNames.forEach(function (testnavScenarioName) {
                            if (testnavScenarioName.toLowerCase().includes(student.studentScenarios[0].testnavScenario.scenarioName.toLowerCase())) {
                              testnavScenarioNameFound = true;
                              return;
                            }
                          });
                          if (!testnavScenarioNameFound) {
                            form.selectedScenarios.splice(form.selectedScenarios.indexOf(selectedScenario), 1);
                          }
                        });
                      }
                    });
                    divJobSetupService.setTestForms(testForms);

                    viewModel.alertSuccess = true;
                    viewModel.alertMessage = 'student with battery UUID  "' + student.batteryUuid + '" successfully removed.';
                  }
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = result.errors[0].message;
                }
              }
            );
        });
      } else {
        studentSelectionService
          .releaseReservedStudent(student.uuid)
          .then(
            function (result) {
              if (result.success) {
                var formCode = student.testnavScenario.formCode;
                var testCode = student.testnavScenario.testCode;
                var testForms = divJobSetupService.getTestForms();
                angular.forEach(testForms, function (form) {
                  if (form.testCode === testCode && form.formCode === formCode) {
                    angular.forEach(form.selectedScenarios, function (selectedScenario) {
                      var testnavScenarioIndex = selectedScenario.testNavScenarioNames.indexOf(student.testnavScenario.scenarioName);

                      if (selectedScenario.testNavScenarioNames.length > 0 && testnavScenarioIndex < selectedScenario.testNavScenarioNames.length - 1) {
                        selectedScenario.testNavScenarioNames.splice(testnavScenarioIndex, 1);
                        delete selectedScenario.uuids[student.uuid];
                        form.remainingStudents += 1;
                        selectedScenario.maxCount -= 1;
                      }

                      if (student.epenScenario) {
                        var epenScenarioIndex = selectedScenario.epenScenarioNames.indexOf(student.epenScenario.scenarioName);
                        if (selectedScenario.epenScenarioNames.length > 0 && epenScenarioIndex < selectedScenario.epenScenarioNames.length - 1) {
                          selectedScenario.epenScenarioNames.splice(epenScenarioIndex, 1);
                        }
                        if (selectedScenario.testNavScenarioNames.length === 1 && selectedScenario.epenScenarioNames.length === 1) {
                          selectedScenario.testNavScenarioNames.splice(testnavScenarioIndex, 1);
                          selectedScenario.epenScenarioNames.splice(epenScenarioIndex, 1);
                          form.remainingStudents += 1;
                        }
                      } else if (selectedScenario.testNavScenarioNames.length === 1) {
                        selectedScenario.testNavScenarioNames.splice(testnavScenarioIndex, 1);
                        form.remainingStudents += 1;
                      }


                      var testnavScenarioNameFound = false;
                      selectedScenario.testNavScenarioNames.forEach(function (testnavScenarioName) {
                        if (testnavScenarioName.toLowerCase().includes(student.testnavScenario.scenarioClass.scenarioClass.toLowerCase())) {
                          testnavScenarioNameFound = true;
                          return;
                        }
                      });
                      if (!testnavScenarioNameFound) {
                        form.selectedScenarios.splice(form.selectedScenarios.indexOf(selectedScenario), 1);
                      }
                    });
                  }
                });
                divJobSetupService.setTestForms(testForms);
                viewModel.alertSuccess = true;
                viewModel.alertMessage = 'student with UUID  "' + student.uuid + '" successfully removed.';
              } else {
                viewModel.alertSuccess = false;
                viewModel.alertMessage = result.errors[0].message;
              }
            });
      }

    }

    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    $scope.$on('$destroy', function () {
      $interval.cancel(intervalPromise);
    });
  }
})();
