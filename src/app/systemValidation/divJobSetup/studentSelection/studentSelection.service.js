(function () {

  'use strict';

  angular.module('systemValidation').factory('studentSelectionService', studentSelectionService);

  studentSelectionService.$inject = ['$http', 'systemValidationApiPrefix'];

  function studentSelectionService($http, systemValidationApiPrefix) {
    return {
      startGetStudentScenariosJob: startGetStudentScenariosJob,
      checkGetStudentScenariosJob: checkGetStudentScenariosJob,
      submitStudents: submitStudents,
      releaseReservedStudent: releaseReservedStudent
    };

    function startGetStudentScenariosJob(environment, testType, scopeTreePath, json) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environment.environmentId +
        '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) + '/getStudentScenariosJobs',
        json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function checkGetStudentScenariosJob(jobId) {
      return $http.get(systemValidationApiPrefix + 'getStudentScenariosJobs/' + jobId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function submitStudents(environment, divJobPathId, scopeTreePath, json, testType) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environment.environmentId + '/scopeTreePath/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) +
        '/divJobPaths/' + divJobPathId + '/studentScenarios' + '/testTypes/' + encodeURIComponent(testType), json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function releaseReservedStudent(uuid) {
      return $http.delete(systemValidationApiPrefix + 'reservedStudents/' + uuid + '/reservedStudent')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
