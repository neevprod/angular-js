(function () {

  'use strict';

  angular.module('systemValidation').controller(
    'TestCodeSelectionController', TestCodeSelectionController);

  TestCodeSelectionController.$inject = ['testCodeSelectionService',
    'divJobSetupService', 'environmentService', '$state',
    '$stateParams', '$q'];

  function TestCodeSelectionController(testCodeSelectionService,
                                       divJobSetupService, environmentService, $state, $stateParams, $q) {

    var viewModel = this;
    viewModel.lastKtTestCaseDownloadTime = '';
    viewModel.testCodeLoading = false;

    getEnvironment($stateParams.environmentId);
    getLastKtTestCaseDownloadTime();

    viewModel.testTypes = ['Standard', 'Battery'];
    if (divJobSetupService.getTestType()) {
      viewModel.testType = divJobSetupService.getTestType();
    }

    viewModel.states = [];
    viewModel.scopeTreePaths = [];
    var promise = getPathStates();
    promise.then(function () {
      loadSelectedRecords();
    });
    viewModel.getTestCodes = getTestCodes;
    viewModel.clearScopeTreePathAndTestCodeSelection = clearScopeTreePathAndTestCodeSelection;
    viewModel.submit = submit;
    checkErrorMessage();

    viewModel.getCheckedBoxes = getCheckedBoxes;
    viewModel.loadingScopeTreePaths = false;
    var stats;
    var batteryStats;
    var epen2;
    var beEpen;
    var primaryPV;

    function getEnvironment(environmentId) {
      environmentService.getEnvironment(environmentId).then(
        function (result) {
          if (result.success) {
            divJobSetupService
              .setEnvironment(result.data.environment);
            viewModel.environment = result.data.environment;
          }
        });
    }

    viewModel.notSelected = [];
    viewModel.selected = [];

    /**
     * Loads the list of ScopeTreePath for selected environment.
     */
    function getScopeTreePaths() {
      viewModel.clearAlertMessage();
      viewModel.loadingScopeTreePaths = true;
      viewModel.scopeTreePaths = [];
      testCodeSelectionService
        .getScopeTreePaths($stateParams.environmentId,
          viewModel.testType)
        .then(
          function (result) {
            viewModel.loadingScopeTreePaths = false;
            if (result.success) {
              viewModel.scopeTreePaths = result.data.scopeTreePaths;
              if (!result.data.scopeTreePaths.length) {
                viewModel.alertSuccess = false;
                viewModel.alertMessage = 'Can not find ScopeTreePath for the EnvironmentId: '
                  + $stateParams.environmentId;
              }
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });
    }

    /**
     * Loads the list of TestCodes for the selected environment and
     * ScopeTreePath.
     */
    function getTestCodes() {
      viewModel.testCodeLoading = true;
      viewModel.selectedTestCodes = [];
      if (viewModel.selectedScopeTreePath) {
        testCodeSelectionService
          .getTestCodes(viewModel.environment,
            viewModel.selectedScopeTreePath,
            viewModel.testType)
          .then(
            function (result) {
              if (result.success) {
                viewModel.testDetails = result.data.testDetails;
                viewModel.testCodeLoading = false;
                if (!result.data.testDetails.length) {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = 'Can not find Test Codes for the scopeTreePath: '
                    + viewModel.selectedScopeTreePath;
                }

              } else {
                viewModel.testCodeLoading = false;
                viewModel.alertSuccess = false;
                viewModel.alertMessage = result.errors[0].message;
              }
            });
      } else {
        viewModel.testDetails = [];
        viewModel.selectedTestCodes = [];
      }
    }

    /**
     * Saves the selected environment, ScopeTreePath and TestCodes into the
     * divJobSetupService and submits the form.
     */
    function submit(isValid) {
      if (isValid) {
        if (viewModel.selectedTestCodes === undefined
          || !viewModel.selectedTestCodes.length) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'Please Select at least one Test Code from Available Test Codes.';
        } else {
          for (var i = 0; i < viewModel.states.length; i++) {
            if ((viewModel.selected
              .indexOf(viewModel.states[i].key)) === -1) {
              viewModel.notSelected.push(viewModel.states[i].key);
            }
          }
          if (viewModel.testType === 'Standard'
            && viewModel.selected.length === viewModel.states.length) {
            viewModel.notSelected.push(7);
          } else if (viewModel.testType === 'Standard'
            && viewModel.selected.length !== viewModel.states.length) {
            viewModel.notSelected.push(7);
          } else if (viewModel.testType === 'Battery'
            && viewModel.selected.length === viewModel.states.length) {
            viewModel.notSelected = [];
          }
          testCodeSelectionService
            .getDivJobExecPathId(viewModel.selected,
              viewModel.notSelected)
            .then(
              function (result) {
                if (result.success) {
                  divJobSetupService
                    .setDivJobPathId(result.data.path);
                  viewModel.submitted = true;
                  $state
                    .go(
                      'testSessionSelection',
                      {
                        testType: viewModel.testType,
                        environmentId: viewModel.environment.environmentId,
                        'scopeTreePath': viewModel.selectedScopeTreePath,
                        'testCodes': viewModel.selectedTestCodes,
                        'availableTestCodes': viewModel.testDetails,
                        'selectedPathStates': viewModel.selected
                      });
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = result.errors[0].message;
                }
              });

        }
      } else {
        viewModel.alertSuccess = false;
        viewModel.alertMessage = 'Please select test type, scope tree path and at least one test code.';
      }
    }

    /**
     * Selects the previously selected environment, ScopeTreePath and
     * TestCodes.
     */
    function loadSelectedRecords() {
      if (angular.isObject(divJobSetupService.getEnvironment())) {
        viewModel.environmens = divJobSetupService.environmens;
        viewModel.environment = divJobSetupService.getEnvironment();
        if (viewModel.testType) {
          getScopeTreePaths();
          if (divJobSetupService.getTestType()) {
            viewModel.testType = divJobSetupService.getTestType();
          }
          if (divJobSetupService.getScopeTreePath()) {
            viewModel.selectedScopeTreePath = divJobSetupService.getScopeTreePath();
          }
          if (angular.isObject(divJobSetupService.getAvailableTestCodes())) {
            viewModel.testDetails = divJobSetupService.getAvailableTestCodes();
            viewModel.selectedTestCodes = divJobSetupService.getTestCodes();
          } else {
            getTestCodes();
          }

          if (angular.isObject(divJobSetupService.getTestCodes())) {
            viewModel.selectedTestCodes = divJobSetupService.getTestCodes();
          }
          if (divJobSetupService.getPathStates()) {
            viewModel.selected = divJobSetupService.getPathStates();
          }
        }
      }
    }

    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    function checkErrorMessage() {
      if (divJobSetupService.getErrorMessage() !== undefined) {
        viewModel.alertSuccess = false;
        viewModel.alertMessage = divJobSetupService.getErrorMessage();

        divJobSetupService.setErrorMessage(undefined);
      }
    }

    /**
     * Clears the scope tree path selection and test code selection.
     */
    function clearScopeTreePathAndTestCodeSelection() {
      viewModel.selectedTestCodes = [];
      viewModel.selectedScopeTreePath = '';
      viewModel.testDetails = [];
      viewModel.selected = [];
      if (viewModel.testType === 'Battery') {
        getPathStates();
      } else {
        getPathStates();
      }
      getScopeTreePaths();
    }

    function getPathStates() {
      viewModel.disableBatteryStats = false;
      viewModel.disableEpen2 = false;
      var deferred = $q.defer();
      testCodeSelectionService
        .getPathStates(viewModel.testType)
        .then(
          function (result) {
            if (result.success) {
              viewModel.states = result.data.pathStates;
              for (var i = 0; i < viewModel.states.length; i++) {
                viewModel.selected
                  .push(viewModel.states[i].key);
              }
              deferred.resolve();
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
              deferred.resolve();
            }
          });
      return deferred.promise;
    }

    function getCheckedBoxes(value) {
      if (value === 'BE_EPEN_PATH_VALIDATOR') {
        if (viewModel.selected.indexOf(3) > -1) {
          if (viewModel.selected.indexOf(4) === -1) {
            viewModel.selected.push(4);
          }
          if (viewModel.selected.indexOf(2) === -1) {
            viewModel.selected.push(2);
          }
          if (viewModel.selected.indexOf(5) === -1) {
            viewModel.selected.push(5);
          }
        }
        if (viewModel.selected.indexOf(3) === -1) {
          if (viewModel.testType === 'Battery'
            && viewModel.selected.indexOf(4) > -1) {
            if (viewModel.selected.indexOf(7) > -1) {
              batteryStats = viewModel.selected.indexOf(7);
              if (batteryStats > -1) {
                viewModel.selected.splice(batteryStats, 1);
              }
            }
          }
          if (viewModel.selected.indexOf(4) > -1) {
            if (viewModel.selected.indexOf(6) > -1) {
              stats = viewModel.selected.indexOf(6);
              if (stats > -1) {
                viewModel.selected.splice(stats, 1);
              }
            }
          }
          if (viewModel.selected.indexOf(4) > -1 && viewModel.testType === 'Battery') {
            if (viewModel.selected.indexOf(6) > -1) {
              stats = viewModel.selected.indexOf(6);
              if (stats > -1) {
                viewModel.selected.splice(stats, 1);
              }
            }
          }
        }

      } else if (value === 'EPEN2_AUTOMATION') {
        if (viewModel.selected.indexOf(4) === -1) {
          epen2 = viewModel.selected.indexOf(3);
          if (epen2 > -1) {
            viewModel.selected.splice(epen2, 1);
          }
        }
        if (viewModel.selected.indexOf(4) > -1) {
          if (viewModel.selected.indexOf(5) === -1) {
            viewModel.selected.push(5);
          }
          if (viewModel.selected.indexOf(2) === -1) {
            viewModel.selected.push(2);
          }
          if ((viewModel.selected.indexOf(6) > -1 || viewModel.selected.indexOf(7) > -1) && viewModel.selected.indexOf(3) === -1) {
            viewModel.selected.push(3);
          }
        }
      } else if (value === 'STATS' && viewModel.testType === 'Battery') {
        if (viewModel.selected.indexOf(6) > -1) {
          if (viewModel.selected.indexOf(4) > -1 && viewModel.selected.indexOf(3) === -1) {
            viewModel.selected.push(3);
          }
          if (viewModel.selected.indexOf(2) === -1) {
            viewModel.selected.push(2);
          }
          if (viewModel.selected.indexOf(5) === -1) {
            viewModel.selected.push(5);
          }
          if (viewModel.selected.indexOf(7) === -1) {
            viewModel.selected.push(7);
          }
        }
        if (viewModel.selected.indexOf(6) === -1) {
          if (viewModel.selected.indexOf(7) > -1) {
            batteryStats = viewModel.selected.indexOf(7);
            if (batteryStats > -1) {
              viewModel.selected.splice(batteryStats, 1);
            }
          }
        }
      } else if (value === 'BATTERY_STATS') {
        if (viewModel.selected.indexOf(7) > -1) {
          if (viewModel.selected.indexOf(6) === -1) {
            viewModel.selected.push(6);
          }
          if (viewModel.selected.indexOf(4) > -1 && viewModel.selected.indexOf(3) === -1) {
            viewModel.selected.push(3);
          }
          if (viewModel.selected.indexOf(2) === -1) {
            viewModel.selected.push(2);
          }
          if (viewModel.selected.indexOf(5) === -1) {
            viewModel.selected.push(5);
          }
        }
        if (viewModel.selected.indexOf(7) === -1) {
          if (viewModel.selected.indexOf(6) > -1) {
            stats = viewModel.selected.indexOf(6);
            if (stats > -1) {
              viewModel.selected.splice(stats, 1);
            }
          }
        }
      } else if (value === 'STATS') {
        if (viewModel.selected.indexOf(6) > -1) {
          if (viewModel.selected.indexOf(4) > -1 && viewModel.selected.indexOf(3) === -1) {
            viewModel.selected.push(3);
          }
          if (viewModel.selected.indexOf(2) === -1) {
            viewModel.selected.push(2);
          }
          if (viewModel.selected.indexOf(5) === -1) {
            viewModel.selected.push(5);
          }
        }
        if (viewModel.selected.indexOf(6) === -1) {
          if (viewModel.selected.indexOf(7) > -1) {
            batteryStats = viewModel.selected.indexOf(7);
            if (batteryStats > -1) {
              viewModel.selected.splice(batteryStats, 1);
            }
          }
        }
      } else if (value === 'BE_PRIMARY_PATH_VALIDATOR') {
        if (viewModel.selected.indexOf(5) > -1) {
          if (viewModel.selected.indexOf(2) === -1) {
            viewModel.selected.push(2);
          }
        }
        if (viewModel.selected.indexOf(5) === -1) {

          if (viewModel.selected.indexOf(4) > -1) {
            epen2 = viewModel.selected.indexOf(4);
            if (epen2 > -1) {
              viewModel.selected.splice(epen2, 1);
            }

          }
          if (viewModel.selected.indexOf(3) > -1) {
            beEpen = viewModel.selected.indexOf(3);
            if (beEpen > -1) {
              viewModel.selected.splice(beEpen, 1);
            }
          }
          if (viewModel.selected.indexOf(6) > -1) {
            stats = viewModel.selected.indexOf(6);
            if (stats > -1) {
              viewModel.selected.splice(stats, 1);
            }
          }
          if (viewModel.testType === 'Battery' && viewModel.selected.indexOf(6) > -1) {
            stats = viewModel.selected.indexOf(6);
            if (stats > -1) {
              viewModel.selected.splice(stats, 1);
            }
          }
          if (viewModel.testType === 'Battery' && viewModel.selected.indexOf(7) > -1) {
            batteryStats = viewModel.selected.indexOf(7);
            if (batteryStats > -1) {
              viewModel.selected.splice(batteryStats, 1);
            }
          }
        }
      } else if (value === 'IRIS_PROCESSING_VALIDATOR') {
        if (viewModel.selected.indexOf(2) === -1) {
          if (viewModel.selected.indexOf(5) > -1) {
            primaryPV = viewModel.selected.indexOf(5);
            if (primaryPV > -1) {
              viewModel.selected.splice(primaryPV, 1);
            }
          }
          if (viewModel.selected.indexOf(4) > -1) {
            epen2 = viewModel.selected.indexOf(4);
            if (epen2 > -1) {
              viewModel.selected.splice(epen2, 1);
            }

          }
          if (viewModel.selected.indexOf(3) > -1) {
            beEpen = viewModel.selected.indexOf(3);
            if (beEpen > -1) {
              viewModel.selected.splice(beEpen, 1);
            }
          }
          if (viewModel.selected.indexOf(6) > -1) {
            stats = viewModel.selected.indexOf(6);
            if (stats > -1) {
              viewModel.selected.splice(stats, 1);
            }
          }
          if (viewModel.testType === 'Battery' && viewModel.selected.indexOf(6) > -1) {
            stats = viewModel.selected.indexOf(6);
            if (stats > -1) {
              viewModel.selected.splice(stats, 1);
            }
          }
          if (viewModel.testType === 'Battery' && viewModel.selected.indexOf(7) > -1) {
            batteryStats = viewModel.selected.indexOf(7);
            if (batteryStats > -1) {
              viewModel.selected.splice(batteryStats, 1);
            }
          }
        }
      }
    }

    /**
     * Retrieves the date/time on which the most recent KT test case manifest was processed.
     */
    function getLastKtTestCaseDownloadTime() {
      testCodeSelectionService.getLastKtTestCaseDownloadTime().then(function (result) {
        if (result.success) {
          viewModel.lastKtTestCaseDownloadTime = result.data.lastKtTestCaseDownloadTime;
        } else {
          viewModel.lastKtTestCaseDownloadTime = '';
        }
      });
    }
  }
})();
