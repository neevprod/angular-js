'use strict';

describe('Test Code selection controller', function () {
  var controller;
  var $stateParams;
  var $state;
  var testCodeSelectionService;
  var divJobSetupService;
  var environmentService;
  var deferredEnvironment;
  var deferredScopeTreePaths;
  var deferredPathStates;
  var $rootScope;
  var deferredTestType;
  var $scope = {};
  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, $q, _$rootScope_) {
    $stateParams = {};

    deferredTestType = $q.defer();
    deferredEnvironment = $q.defer();
    deferredScopeTreePaths = $q.defer();
    deferredPathStates = $q.defer();


    divJobSetupService = jasmine.createSpyObj('divJobSetupService', ['setEnvironment', 'getEnvironment',
      'getScopeTreePath', 'getTestType', 'getAvailableTestCodes', 'getTestCodes', 'getErrorMessage', 'getPathStates']);
    divJobSetupService.getEnvironment.and.callFake(function () {
      return deferredEnvironment.promise;
    });

    environmentService = jasmine.createSpyObj('environmentService', ['getEnvironment']);
    environmentService.getEnvironment.and.callFake(function () {
      return deferredEnvironment.promise;
    });

    testCodeSelectionService = jasmine.createSpyObj('testCodeSelectionService', ['getScopeTreePaths', 'getPathStates','getLastKtTestCaseDownloadTime']);
    testCodeSelectionService.getScopeTreePaths.and.callFake(function () {
      return deferredScopeTreePaths.promise;
    });
    testCodeSelectionService.getPathStates.and.callFake(function () {
      return deferredPathStates.promise;
    });
    testCodeSelectionService.getLastKtTestCaseDownloadTime.and.callFake(function () {
      return deferredPathStates.promise;
    });


    $rootScope = _$rootScope_;

    controller = $controller('TestCodeSelectionController', {
      testCodeSelectionService: testCodeSelectionService,
      environmentService: environmentService,
      divJobSetupService: divJobSetupService,
      $stateParams: $stateParams,
      $state: $state,
      $scope:$scope
    });
  }));

  it('should set the list of test codes to an empty array during initialization', function () {
    // Then
    expect(controller.scopeTreePaths).toEqual([]);
  });

  it('should set the environment after environmentService.getEnvironment returns', function () {
    // Given
    var environment = {
      'environmentId': 15,
      'name': 'INT_REF_4.1',
      'beMysqlConnection': {
        'dbConnectionId': 41,
        'dbName': 'config'
      },
      'dataWarehouseMongoConnection': {
        'dbConnectionId': 42,
        'dbName': 'admin'
      },
      'epen2MysqlConnection': {
        'dbConnectionId': 31,
        'dbName': 'epenops'
      },
      'feMysqlConnection': {
        'dbConnectionId': 41,
        'dbName': 'config'
      },
      'irisMysqlConnection': {
        'dbConnectionId': 22,
        'dbName': 'pdev_config'
      },
      'epen2MongoConnection': {
        'dbConnectionId': 32,
        'dbName': 'N/A'
      }
    };

    // When
    deferredEnvironment.resolve({success: true, data: {environment: environment}});
    $rootScope.$digest();

    // Then
    expect(controller.environment).toEqual(environment);
  });

  it('should set the list of scope tree paths after testCodeSelectionService.clearScopeTreePathAndTestCodeSelection returns', function () {
    // Given
    $scope.testType='Standard';
    controller.clearScopeTreePathAndTestCodeSelection();
    var scopeTreePaths = ['/ref/2015-16/refspr16', '/tx/2015-2016/k1603ex', '/tx/2016-2017/s1715al'];

    // When
    deferredScopeTreePaths.resolve({success: true, data: {scopeTreePaths: scopeTreePaths}});
    $rootScope.$digest();

    // Then
    expect(controller.scopeTreePaths).toEqual(scopeTreePaths);
  });

});
