(function () {

  'use strict';

  angular.module('systemValidation').factory('testCodeSelectionService', testCodeSelectionService);

  testCodeSelectionService.$inject = ['$http', 'systemValidationApiPrefix'];

  function testCodeSelectionService($http, systemValidationApiPrefix) {
    return {
      getDivJobExecPathId: getDivJobExecPathId,
      getScopeTreePaths: getScopeTreePaths,
      getTestCodes: getTestCodes,
      getPathStates: getPathStates,
      getLastKtTestCaseDownloadTime: getLastKtTestCaseDownloadTime
    };

    /**
     * Gets the DIV job execution path id.
     *
     * @param selectedStatesIds
     *            The list of selected states
     * @param notSelectedStatesIds
     *            The list of not selected states
     * @returns the JSON result containing DIV job execution path id
     */
    function getDivJobExecPathId(selectedStatesIds, notSelectedStatesIds) {
      var config = {
        method: 'GET',
        url: systemValidationApiPrefix + 'divJobExecPathId',
        params: {
          selectedStatesIds: selectedStatesIds,
          notSelectedStatesIds: notSelectedStatesIds
        }
      };
      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of ScopeTreePath for the given environment from the
     * server.
     *
     * @returns The result from the server containing the list of
     *          ScopeTreePath.
     */
    function getScopeTreePaths(environmentId, testType) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/testTypes/' + testType + '/scopeTreePaths')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of TestCodes for the given environment and ScopeTreePath
     * from the server.
     *
     * @returns The result from the server containing the list of TestCodes.
     */
    function getTestCodes(environment, scopeTreePath, testType) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environment.environmentId +
        '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) + '/testCodes')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of PathStates for the given testType
     *
     * @returns The result from the server containing the list of Path States.
     */
    function getPathStates(testType) {
      return $http.get(systemValidationApiPrefix + 'pathStates/' + testType)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Retrieves the date/time on which the most recent KT test case manifest was processed.
     * @returns The result from the server containing the date/time the most recent KT test case manifest was processed.
     */
    function getLastKtTestCaseDownloadTime() {
      return $http.get(systemValidationApiPrefix + 'lastKtTestCaseDownloadTime')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
