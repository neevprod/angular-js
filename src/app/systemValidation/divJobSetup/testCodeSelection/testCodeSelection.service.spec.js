'use strict';

describe('TestCode Selection service', function () {
  var testCodeSelectionService;
  var $httpBackend;
  var systemValidationApiPrefix;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_testCodeSelectionService_, _$httpBackend_, _systemValidationApiPrefix_) {
    testCodeSelectionService = _testCodeSelectionService_;
    $httpBackend = _$httpBackend_;
    systemValidationApiPrefix = _systemValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getScopeTreePaths', function () {

    it('should submit the scopeTreePath GET request properly', function () {
      // Given
      var data = ['/ref/2015-16/refspr16'];

      // Then
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/1/testTypes/standard/scopeTreePaths').respond(200, {
        success: true,
        data: data
      });

      // When
      testCodeSelectionService.getScopeTreePaths(1, 'standard');
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = ['/ref/2015-16/refspr16'];
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/testTypes/standard/scopeTreePaths').respond(200, {
        success: true,
        data: data
      });

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      testCodeSelectionService.getScopeTreePaths(1, 'standard').then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/testTypes/standard/scopeTreePaths').respond(404, {
        success: false,
        errors: [{
          message: 'Error message.'
        }]
      });

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      testCodeSelectionService.getScopeTreePaths(1, 'standard').then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });
});
