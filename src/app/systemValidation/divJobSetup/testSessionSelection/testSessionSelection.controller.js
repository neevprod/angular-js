(function () {

  'use strict';

  angular.module('systemValidation').controller(
    'TestSessionSelectionController', TestSessionSelectionController);

  TestSessionSelectionController.$inject = ['$stateParams',
    'itemsPerPageOptions', 'divJobSetupService',
    'testSessionSelectionService', '$state'];

  function TestSessionSelectionController($stateParams, itemsPerPageOptions,
                                          divJobSetupService, testSessionSelectionService, $state) {
    var viewModel = this;
    viewModel.loadingTestSessions = true;
    viewModel.testSessionsInitialized = false;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = viewModel.itemsPerPageOptions[0];

    viewModel.environment = divJobSetupService.getEnvironment();
    if (viewModel.environment === null) {
      if (divJobSetupService.getEnvironment() === null) {
        divJobSetupService.setErrorMessage('Please select environment details.');
        $state.go('environments');
        return;
      }
      viewModel.environment = divJobSetupService.getEnvironment();
    }

    var inputTracker = isInputChanged();
    viewModel.testType = $stateParams.testType;
    var scopeTreePath = $stateParams.scopeTreePath;
    var selectedTestcodes = $stateParams.testCodes;
    var pathStates = $stateParams.selectedPathStates;
    if (!inputTracker) {
      viewModel.testType = divJobSetupService.getTestType();
      scopeTreePath = divJobSetupService.getScopeTreePath();
      selectedTestcodes = divJobSetupService.getTestCodes();
      pathStates = divJobSetupService.getPathStates();
    }

    var testCodes = readTestCodes(selectedTestcodes);

    if (!inputTracker) {
      viewModel.testDetails = divJobSetupService.getTestSessions();
      viewModel.loadingTestSessions = false;
      viewModel.testSessionsInitialized = true;
    } else {
      saveRecordsToLocalStorage();
      getTestSessions(scopeTreePath, testCodes);
    }

    viewModel.testcodes = testCodes;
    viewModel.submit = submit;

    /**
     * Loads the list of TestSessions for the given environment,
     * ScopeTreePath and TestCodes.
     */
    function getTestSessions() {
      testSessionSelectionService
        .getTestSessions(viewModel.environment, scopeTreePath, testCodes, viewModel.testType)
        .then(
          function (result) {
            viewModel.loadingTestSessions = false;
            viewModel.testSessionsInitialized = true;

            if (result.success) {
              viewModel.testDetails = result.data.testSessions;
              divJobSetupService
                .setTestSessions(result.data.testSessions);
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });
    }

    /**
     * Loads the testCodes from divJobService.
     */
    function readTestCodes(selectedTestcodes) {
      var testCodes = [];
      angular.forEach(selectedTestcodes, function (selectedTestcode) {
        testCodes.push(selectedTestcode.testCode);
      });

      return testCodes;
    }

    /**
     * Stores the testDetail with TestSession in divJobService and submits
     * the form.
     */
    function submit() {
      viewModel.submitted = true;
      var sessionSelected = true;
      var msg = '';

      for (var j = 0; j < viewModel.testDetails.length; j++) {
        var selectedTs = viewModel.testDetails[j].selectedTestSessions;
        if (selectedTs === undefined) {
          sessionSelected = false;
          msg += viewModel.testDetails[j].testCode + ' ';
        } else if (selectedTs.length === 0 && sessionSelected) {
          msg += viewModel.testDetails[j].testCode + ' ';
          sessionSelected = false;
        }
      }

      if (!sessionSelected) {
        viewModel.submitted = false;
        viewModel.alertSuccess = sessionSelected;
        viewModel.alertMessage = 'Session is not selected for the following TestCodes: ' + msg;
      } else {

        $state.go('scenarioSelection', {
          environmentId: viewModel.environment.environmentId,
          'testSessions': viewModel.testDetails
        });
      }
    }

    /**
     * Saves environment, scopeTreePath and testCodes into divJobService
     */
    function saveRecordsToLocalStorage() {
      divJobSetupService.setTestType($stateParams.testType);
      divJobSetupService.setScopeTreePath($stateParams.scopeTreePath);
      divJobSetupService.setTestCodes($stateParams.testCodes);
      divJobSetupService.setAvailableTestCodes($stateParams.availableTestCodes);
      divJobSetupService.setPathStates($stateParams.selectedPathStates);
    }

    /**
     * Checks if any input selection is changed against the previous
     * selection.
     *
     * @returns true if any input is changed otherwise false.
     */
    function isInputChanged() {
      var oldEnvironment = divJobSetupService.getEnvironment();
      var oldTestType = divJobSetupService.getTestType();
      var oldScopeTreePath = divJobSetupService.getScopeTreePath();
      var oldTestCodes = divJobSetupService.getTestCodes();
      var oldPathStates = divJobSetupService.getPathStates();

      if (oldEnvironment === null || oldTestType === null || oldScopeTreePath === null || oldTestCodes === null || oldPathStates === null) {
        return true;
      }


      if (viewModel.environment === null) {
        viewModel.environment = divJobSetupService.getEnvironment();
      }

      if (viewModel.testType === null || viewModel.testType === undefined) {
        viewModel.testType = divJobSetupService.getTestType();
      }
      if (scopeTreePath === null || scopeTreePath === undefined) {
        scopeTreePath = divJobSetupService.getScopeTreePath();
      }

      if (pathStates === null || pathStates === undefined) {
        pathStates = divJobSetupService.getPathStates();
      }


      var curTestCodes = $stateParams.testCodes;
      if (curTestCodes === null) {
        curTestCodes = oldTestCodes;
      }

      if (viewModel.environment.environmentId === oldEnvironment.environmentId && scopeTreePath === oldScopeTreePath && curTestCodes === oldTestCodes && pathStates === oldPathStates) {
        return false;
      }
      return true;
    }

    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
  }
})();
