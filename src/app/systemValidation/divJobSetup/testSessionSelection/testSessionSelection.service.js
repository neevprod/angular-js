(function () {

  'use strict';

  angular.module('systemValidation').factory('testSessionSelectionService', testSessionSelectionService);

  testSessionSelectionService.$inject = ['$http', 'systemValidationApiPrefix'];

  function testSessionSelectionService($http, systemValidationApiPrefix) {
    return {
      getTestSessions: getTestSessions
    };

    /**
     * Get a list of TestDetail with TestSessions from the server.
     *
     * @returns The result from the server containing the list of TestDetail
     *          with TestSessions.
     */
    function getTestSessions(environment, scopeTreePath, testCodes, testType) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environment.environmentId +
        '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) + '/testCodes/' + testCodes +
        '/testSessionDetails')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
