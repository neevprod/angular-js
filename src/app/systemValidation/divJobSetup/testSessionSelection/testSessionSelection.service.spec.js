'use strict';

describe(
  'TestSession Selection service',
  function () {
    var testSessionSelectionService;
    var $httpBackend;
    var systemValidationApiPrefix;

    beforeEach(module('systemValidation'));

    beforeEach(module(function ($urlRouterProvider) {
      $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(function (_testSessionSelectionService_, _$httpBackend_, _systemValidationApiPrefix_) {
      testSessionSelectionService = _testSessionSelectionService_;
      $httpBackend = _$httpBackend_;
      systemValidationApiPrefix = _systemValidationApiPrefix_;
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    describe(
      'getTestSessions',
      function () {
        var environment = {
          environmentId: 1,
          name: 'INT_REF'
        };
        var scopeTreePath = '/ref/2015-16/refspr16';
        var testCodes = {
          testCode: 'REF081501'
        };
        var testType = 'Standard';
        it(
          'should submit the TestSessions GET request properly',
          function () {
            // Given
            var data = [{
              sessionId: 1,
              sessionName: 'COLE\'S SESSION'
            }, {
              sessionId: 2,
              sessionName: 'NAND\'S SESSION'
            }, {
              sessionId: 3,
              sessionName: 'TATE\'S SESSION'
            }, {
              sessionId: 4,
              sessionName: 'GERT\'S SESSION'
            }];

            // Then
            $httpBackend
              .expectGET(
                systemValidationApiPrefix + 'environments/' + environment.environmentId + '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath
                  .replace(/\//g, '|')) + '/testCodes/' + testCodes + '/testSessionDetails')
              .respond(200, {
                success: true,
                data: data
              });

            // When
            testSessionSelectionService.getTestSessions(environment, scopeTreePath, testCodes, testType);
            $httpBackend.flush();
          });

        it(
          'should return the response data when the call succeeds',
          function () {
            // Given
            var data = [{
              sessionId: 1,
              sessionName: 'COLE\'S SESSION'
            }, {
              sessionId: 2,
              sessionName: 'NAND\'S SESSION'
            }, {
              sessionId: 3,
              sessionName: 'TATE\'S SESSION'
            }, {
              sessionId: 4,
              sessionName: 'GERT\'S SESSION'
            }];
            $httpBackend
              .whenGET(
                systemValidationApiPrefix + 'environments/' + environment.environmentId + '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath
                  .replace(/\//g, '|')) + '/testCodes/' + testCodes + '/testSessionDetails')
              .respond(200, {
                success: true,
                data: data
              });

            // Then
            var handler = jasmine.createSpy('handler').and.callFake(function (result) {
              expect(result.success).toBe(true);
              expect(result.data).toEqual(data);
            });

            // When
            testSessionSelectionService.getTestSessions(environment, scopeTreePath, testCodes, testType)
              .then(handler);
            $httpBackend.flush();

            // Then
            expect(handler).toHaveBeenCalled();
          });

        it(
          'should return the response data when the call fails',
          function () {
            // Given
            $httpBackend
              .whenGET(
                systemValidationApiPrefix + 'environments/' + environment.environmentId + '/testTypes/' + testType + '/scopeTreePaths/' + encodeURIComponent(scopeTreePath
                  .replace(/\//g, '|')) + '/testCodes/' + testCodes + '/testSessionDetails')
              .respond(404, {
                success: false,
                errors: [{
                  message: 'Error message.'
                }]
              });

            // Then
            var handler = jasmine.createSpy('handler').and.callFake(function (result) {
              expect(result.success).toBe(false);
              expect(result.errors[0].message).toBe('Error message.');
            });

            // When
            testSessionSelectionService.getTestSessions(environment, scopeTreePath, testCodes, testType)
              .then(handler);
            $httpBackend.flush();

            // Then
            expect(handler).toHaveBeenCalled();
          });
      });
  });
