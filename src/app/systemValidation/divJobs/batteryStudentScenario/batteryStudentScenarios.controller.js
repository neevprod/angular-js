(function () {

  'use strict';

  angular.module('systemValidation').controller('BatteryStudentScenarioController',
    BatteryStudentScenarioController);
  BatteryStudentScenarioController.$inject = ['$stateParams', 'environmentService', 'batteryStudentScenarioService', 'permissionsService', 'memoryService',
    'itemsPerPageOptions', 'dateTimeFormat', '$uibModal', 'divJobService', '$timeout'];


  function BatteryStudentScenarioController($stateParams, environmentService, batteryStudentScenarioService, permissionsService, memoryService, itemsPerPageOptions,
                                            dateTimeFormat, $uibModal, divJobService, $timeout) {

    var viewModel = this;
    viewModel.environmentId = $stateParams.environmentId;
    viewModel.divJobExecId = $stateParams.divJobExecId;
    viewModel.batteryStudentScenarios = [];
    viewModel.loadingDivJobs = true;
    viewModel.divJobsInitialized = false;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('batteryStudentScenarios', 'perPage', viewModel.divJobExecId) ||
      viewModel.itemsPerPageOptions[0].value;
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.showScenario = showScenario;
    viewModel.showDetails = showDetails;
    viewModel.resumeDivJob = resumeDivJob;
    viewModel.allowDivJobResume = permissionsService.hasEnvironmentLevelPermission('divJob', 'createAccess',
      'systemValidation', viewModel.environmentId);
    viewModel.needToReRunStats = false;
    var needToReRunAndResume = true;
    activate();

    /**
     * Loads the list of Battery StudentScenarios.
     */
    function activate() {
      getEnvironmentName(viewModel.environmentId);
      getDivJob();
      getBatteryStudentScenarios();
    }

    function getEnvironmentName(environmentId) {
      environmentService.getEnvironment(environmentId).then(function (result) {
        if (result.success) {
          viewModel.environmentName = result.data.environment.name;
        }
      });
    }

    function getDivJob() {
      divJobService.getDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (result.success) {
          var divJob = result.data.divJob;

          viewModel.divJobResumable = divJob.numInProgress === 0;
          if (!divJob.hasStatsState && divJob.divJobExecStatus.status.indexOf('Completed') === 0) {
            viewModel.divJobResumable = false;
          }

          if (divJob.hasStatsState === true) {
            if (divJob.divJobExecStatus.status === 'Failed' && divJob.numFailed !== divJob.numStudentScenarios) {
              viewModel.needToReRunStats = true;
              needToReRunAndResume = false;
            } else if (divJob.divJobExecStatus.status.indexOf('Completed') === 0) {
              viewModel.needToReRunStats = true;
              needToReRunAndResume = true;
            }
          }
        }
      });
    }

    function getBatteryStudentScenarios() {
      batteryStudentScenarioService
        .getBatteryStudentScenarios(viewModel.environmentId, viewModel.divJobExecId)
        .then(
          function (result) {
            viewModel.loadingDivJobs = false;
            viewModel.divJobsInitialized = true;
            if (result.success) {
              viewModel.batteryStudentScenarios = result.data.batteryStudentScenarios;
              viewModel.displayedBatteryStudentScenarios = result.data.batteryStudentScenarios;
            }
          });
    }

    /**
     * Show the information for the given scenario in a modal.
     *
     * @param {number} environmentId - The environment ID.
     * @param {number} divJobExecId - The div job exec ID.
     * @param {number} studentScenarioId - The student scenario ID.
     * @param {boolean} isEpenScenario - Whether the scenario is an ePEN scenario.
     */
    function showScenario(environmentId, divJobExecId, studentScenarioId, isEpenScenario) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'ScenarioController as jsonDetailsCtrl',
        resolve: {
          environmentId: function () {
            return environmentId;
          },
          divJobExecId: function () {
            return divJobExecId;
          },
          studentScenarioId: function () {
            return studentScenarioId;
          },
          isEpenScenario: function () {
            return isEpenScenario;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Show the given JSON details in a modal.
     *
     * @param {string} details - The JSON details to show.
     */
    function showDetails(details) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'JsonDetailsController as jsonDetailsCtrl',
        resolve: {
          title: function () {
            return 'Details';
          },
          json: function () {
            return details;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Resume the current DIV job.
     *
     */
    function resumeDivJob() {
      viewModel.divJobResumable = false;
      if (viewModel.needToReRunStats) {
        var modalInstance = $uibModal.open({
          templateUrl: 'app/systemValidation/divJobs/reRunStats/confirmationModal.html',
          controller: 'ReRunStatsController as reRunStatsCtrl',
          backdrop: 'static',
          resolve: {
            reRunOnly: function () {
              return needToReRunAndResume === true;
            }
          }
        });
        modalInstance.result.then(function (action) {
            if (action === 'resumeOnly') {
              resumeFailedUnits();
            } else if (action === 'reRun') {
              reExecuteStatsForCompletedStudentScenarios();
            } else {
              viewModel.divJobResumable = true;
            }
          }
        );
      } else {
        resumeFailedUnits();
      }
    }

    function resumeFailedUnits() {
      divJobService.startDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (!result.success) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'The Data Integrity Validation job could not be resumed.';
        } else {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'The Data Integrity Validation job was successfully resumed.';
          $timeout(function () {
            getBatteryStudentScenarios();
          }, 2000);

        }
      });
    }

    function reExecuteStatsForCompletedStudentScenarios() {
      divJobService.reExecuteStatsForCompletedDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (!result.success) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'The Data Integrity Validation job could not be resumed.';
        } else {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'The Data Integrity Validation job was successfully resumed.';
          $timeout(function () {
            getBatteryStudentScenarios();
          }, 2000);
        }
      });
    }
  }
})();
