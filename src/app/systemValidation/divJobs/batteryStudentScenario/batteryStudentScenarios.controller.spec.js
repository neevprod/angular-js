'use strict';

describe('Battery Student scenarios controller', function () {
  var controller;
  var $stateParams;
  var environmentService;
  var batteryStudentScenarioService;
  var permissionsService;
  var memoryService;
  var divJobService;
  var deferredEnvironment;
  var deferredBatteryStudentScenarios;
  var deferredDivJob;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('systemValidation'));
  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));
  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredEnvironment = $q.defer();
    deferredBatteryStudentScenarios = $q.defer();
    deferredDivJob = $q.defer();

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasEnvironmentLevelPermission']);
    permissionsService.hasEnvironmentLevelPermission.and.callFake(function () {
      return true;
    });

    batteryStudentScenarioService = jasmine.createSpyObj('batteryStudentScenarioService', ['getBatteryStudentScenarios']);
    batteryStudentScenarioService.getBatteryStudentScenarios.and.callFake(function () {
      return deferredBatteryStudentScenarios.promise;
    });
    divJobService = jasmine.createSpyObj('divJobService', ['getDivJob']);
    divJobService.getDivJob.and.callFake(function () {
      return deferredDivJob.promise;
    });

    environmentService = jasmine.createSpyObj('environmentService', ['getEnvironment']);
    environmentService.getEnvironment.and.callFake(function () {
      return deferredEnvironment.promise;
    });

    memoryService = jasmine.createSpyObj('memoryService', ['getItem']);

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('BatteryStudentScenarioController', {
      $stateParams: $stateParams,
      environmentService: environmentService,
      batteryStudentScenarioService: batteryStudentScenarioService,
      permissionsService: permissionsService,
      memoryService: memoryService,
      itemsPerPageOptions: itemsPerPageOptions,
      divJobService: divJobService

    });
  }));

  it('should set the list of Battery Student Scenarios to an empty array during initialization', function () {
    // Then
    expect(controller.batteryStudentScenarios).toEqual([]);
  });

  it('should call batteryStudentScenarioService to get the list of battery student scenarios during initialization', function () {
    // Then
    expect(batteryStudentScenarioService.getBatteryStudentScenarios).toHaveBeenCalled();
  });

  it('should contain the list of units', function () {
    // Given
    var batteryStudentScenarioArray = [
      {
        batteryStudentScenarioId: 4,
        batteryTestCode: 'ELA09',
        batteryUuid: '9e5485a7-ea18-416d-adaa-2ac4a72b983f',
        environmentId: 16,
        stateId: 7,
        details: '{\'message\':\'This is test message\',\'success\':\'yes\'}',
        studentScenarioId: 551,
        numberOfPreviousExecutions: 0,
        status: 'Completed',
        batteryUnits: [{
          formCode: '16EL09FBOE03010100',
          testSessionName: 'AUTO16EL09FBOE0301',
          unitUuid: '5c5c8e8d-57bf-45d7-8e88-336352f969b6',
          testNavScenarioName: 'MAXPOINTS',
          epenScenarioName: 'MAXPOINTS',
          status: 'Completed'
        },
          {
            formCode: '16EL09FBOE03010200',
            testSessionName: 'AUTO16EL09FBOE0301',
            unitUuid: 'd4cd6ac8-8213-429f-af50-7e6f4b55cc7c',
            testNavScenarioName: 'MAXPOINTS',
            epenScenarioName: 'MAXPOINTS',
            status: 'Failed'
          },
          {
            formCode: '16EL09FBOE03010300',
            testSessionName: 'AUTO16EL09FBOE0301',
            unitUuid: '79eca19c-9376-47df-afba-d727bd920826',
            testNavScenarioName: 'MAXPOINTS',
            epenScenarioName: 'MAXPOINTS',
            status: 'Completed'
          }]
      }
    ];

    // When
    deferredBatteryStudentScenarios.resolve({
      success: true, data: {
        batteryStudentScenarios: batteryStudentScenarioArray
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedBatteryStudentScenarios[0].batteryUnits.length).toBe(3);
  });
});
