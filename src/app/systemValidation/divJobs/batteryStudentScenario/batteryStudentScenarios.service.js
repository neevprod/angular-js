(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('batteryStudentScenarioService', batteryStudentScenarioService);

  batteryStudentScenarioService.$inject = ['$http', 'systemValidationApiPrefix'];

  function batteryStudentScenarioService($http, systemValidationApiPrefix) {
    return {
      getBatteryStudentScenarios: getBatteryStudentScenarios
    };

    function getBatteryStudentScenarios(environmentId, divJobExecId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/batteryStudentScenarios')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
