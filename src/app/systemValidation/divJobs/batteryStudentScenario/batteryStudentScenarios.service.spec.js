'use strict';

describe('Battery Student Scenario service', function () {
  var batteryStudentScenarioService;
  var $httpBackend;
  var systemValidationApiPrefix;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_batteryStudentScenarioService_, _$httpBackend_, _systemValidationApiPrefix_) {
    batteryStudentScenarioService = _batteryStudentScenarioService_;
    $httpBackend = _$httpBackend_;
    systemValidationApiPrefix = _systemValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getBatteryStudentScenarios', function () {
    it('should submit the Battery Student Scenarios GET request properly', function () {
      // Given
      var data = [{batteryStudentScenarioId: 1}, {batteryStudentScenarioId: 2}, {batteryStudentScenarioId: 3}];

      // Then
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/1/divJobExecutions/1/batteryStudentScenarios')
        .respond(200, {success: true, data: data});

      // When
      batteryStudentScenarioService.getBatteryStudentScenarios(1, 1);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{batteryStudentScenarioId: 1}, {batteryStudentScenarioId: 2}, {batteryStudentScenarioId: 3}];
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/divJobExecutions/1/batteryStudentScenarios')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      batteryStudentScenarioService.getBatteryStudentScenarios(1, 1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/divJobExecutions/1/batteryStudentScenarios')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      batteryStudentScenarioService.getBatteryStudentScenarios(1, 1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
