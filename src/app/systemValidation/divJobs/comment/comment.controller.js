(function () {

  'use strict';

  angular.module('systemValidation').controller('CommentController',
    CommentController);
  CommentController.$inject = ['$stateParams', 'commentService',
    'environmentService', 'memoryService', 'itemsPerPageOptions',
    'dateTimeFormat', '$uibModal'];

  function CommentController($stateParams, commentService,
                             environmentService, memoryService, itemsPerPageOptions,
                             dateTimeFormat, $uibModal) {
    var viewModel = this;
    viewModel.loadingResults = true;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('comments', 'perPage',
      viewModel.studentScenarioResultId)
      || viewModel.itemsPerPageOptions[0].value;

    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    var divJobExecId = $stateParams.divJobExecId;
    var studentScenarioId = $stateParams.studentScenarioId;
    var stateId = $stateParams.stateId;

    viewModel.studentScenarioResultVersion = $stateParams.studentScenarioResultVersion;

    viewModel.environmentId = $stateParams.environmentId;
    viewModel.batteryStudentScenarioId = $stateParams.batteryStudentScenarioId;
    viewModel.studentScenarioId = studentScenarioId;
    viewModel.studentScenarioResultId = $stateParams.studentScenarioResultId;
    viewModel.divJobExecId = divJobExecId;
    viewModel.stateId = stateId;
    viewModel.getPassFail = getPassFail;
    viewModel.showCommentDetailModal = showCommentDetailModal;

    activate();

    /**
     * Loads the list of comments.
     */
    function activate() {
      getEnvironmentName(viewModel.environmentId);
      commentService
        .getComments($stateParams.environmentId,
          $stateParams.studentScenarioResultId)
        .then(
          function (result) {
            viewModel.loadingResults = false;

            if (result.success) {
              viewModel.comments = result.data.studentScenarioResultComments;
              viewModel.displayedComments = []
                .concat(viewModel.comments);
            }
          });
    }

    function showCommentDetailModal(result, index) {
      var studentScenarioResultId = result.studentScenarioResultId;
      var studentScenarioResultComment = result;
      var modalInstance = $uibModal
        .open({
          templateUrl: 'app/systemValidation/divJobs/comment/commentDetail/commentDetail.html',
          controller: 'CommentDetailController as commentDtlCtrl',
          resolve: {
            studentScenarioResultId: function () {
              return studentScenarioResultId;
            },
            studentScenarioResultComment: function () {
              return studentScenarioResultComment;
            }
          },
          size: 'lg'
        });
      modalInstance.result.then(function (savedComment) {
        if (!savedComment) {
          return;
        }
        viewModel.displayedComments[index] = savedComment;
      });
    }

    /**
     * Returns a string indicating whether the status passed or failed.
     *
     * @param status
     *            The status of the State.
     * @returns {string} A string indicating whether the state is passed.
     */
    function getPassFail(status) {
      if (status) {
        return 'Pass';
      } else {
        return 'Fail';
      }
    }

    function getEnvironmentName(environmentId) {
      environmentService
        .getEnvironment(environmentId)
        .then(
          function (result) {
            if (result.success) {
              viewModel.environmentName = result.data.environment.name;
            }
          });
    }
  }
})();
