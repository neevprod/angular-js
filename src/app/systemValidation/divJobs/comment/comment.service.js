(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('commentService', commentService);

  commentService.$inject = ['$http', 'systemValidationApiPrefix'];

  function commentService($http, systemValidationApiPrefix) {
    return {
      getComments: getComments
    };

    function getComments(environmentId, studentScenarioResultId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/studentScenarioResults/' + studentScenarioResultId + '/studentScenarioResultComments')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
