(function () {

  'use strict';

  angular.module('systemValidation').controller('CommentDetailController',
    commentDetailController);

  commentDetailController.$inject = ['$stateParams', '$uibModalInstance',
    'commentDetailService', 'studentScenarioResultId',
    'studentScenarioResultComment'];

  function commentDetailController($stateParams, $uibModalInstance,
                                   commentDetailService, studentScenarioResultId,
                                   studentScenarioResultComment) {
    var viewModel = this;

    viewModel.save = save;
    viewModel.close = close;
    viewModel.environmentId = $stateParams.environmentId;
    viewModel.divJobExecId = $stateParams.divJobExecId;
    viewModel.studentScenarioId = $stateParams.studentScenarioId;
    viewModel.userId = $stateParams.userId;
    viewModel.alertMessage = '';
    viewModel.validSave = false;

    viewModel.studentScenarioResultId = studentScenarioResultId;
    viewModel.studentScenarioResultComment = angular.copy(studentScenarioResultComment);

    activate();

    function activate() {
      getCommentCategories();
      getExternalDefectRepositories();
      if (viewModel.studentScenarioResultComment === null) {
        viewModel.newComment = true;
        viewModel.modalTitle = 'Create Comment';
        viewModel.submitButtonLabel = 'Save';
      } else {
        viewModel.newComment = false;
        viewModel.modalTitle = 'Update Comment';
        viewModel.submitButtonLabel = 'Update';
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      if (viewModel.alertMessage.length > 0) {
        $uibModalInstance.close(viewModel.studentScenarioResultComment);
      } else {
        $uibModalInstance.close();
      }
    }

    function getCommentCategories() {
      commentDetailService
        .getStudentScenarioResultCommentCategories(
          viewModel.environmentId)
        .then(
          function (result) {
            if (result.success) {
              viewModel.studentScenarioResultCommentCategories = result.data.studentScenarioResultCommentCategories;
            }
          });
    }

    function getExternalDefectRepositories() {
      commentDetailService
        .getExternalDefectRepositories(viewModel.environmentId)
        .then(
          function (result) {
            if (result.success) {
              viewModel.externalDefectRepositories = result.data.externalDefectRepositories;
            }
          });
    }

    /**
     * Sends Comment information to the service
     */
    function save(isValid) {
      if (isValid) {
        if (viewModel.newComment) {
          commentDetailService
            .saveComment(viewModel.environmentId,
              viewModel.divJobExecId,
              viewModel.studentScenarioId,
              viewModel.studentScenarioResultId,
              viewModel.studentScenarioResultComment)
            .then(
              function (result) {
                if (result.success) {
                  viewModel.alertSuccess = true;
                  viewModel.alertMessage = 'Comment details saved sucessfully!';
                  viewModel.validSave = true;
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = result.errors[0].message;
                  viewModel.validSave = true;
                }
              });
        } else {
          commentDetailService
            .updateComment(
              viewModel.environmentId,
              viewModel.divJobExecId,
              viewModel.studentScenarioId,
              viewModel.studentScenarioResultId,
              viewModel.studentScenarioResultComment.resultCommentId,
              viewModel.studentScenarioResultComment)
            .then(
              function (result) {
                if (result.success) {
                  viewModel.alertSuccess = true;
                  viewModel.alertMessage = 'Comment details updated sucessfully!';
                  viewModel.validSave = true;
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = result.errors[0].message;
                  viewModel.validSave = true;
                }
              });
        }
      }
    }
  }
})();
