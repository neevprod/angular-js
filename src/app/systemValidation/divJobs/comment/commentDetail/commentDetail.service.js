(function () {

  'use strict';

  angular.module('systemValidation').factory('commentDetailService', commentDetailService);

  commentDetailService.$inject = ['$http', 'systemValidationApiPrefix'];

  function commentDetailService($http, systemValidationApiPrefix) {
    return {
      getComments: getComments,
      saveComment: saveComment,
      updateComment: updateComment,
      getStudentScenarioResultCommentCategories: getStudentScenarioResultCommentCategories,
      getExternalDefectRepositories: getExternalDefectRepositories
    };

    function saveComment(environmentId, divJobExecId, studentScenarioId, studentScenarioResultId, json) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/studentScenarioResults/' + studentScenarioResultId + '/studentScenarioResultComments', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function updateComment(environmentId, divJobExecId, studentScenarioId, studentScenarioResultId, studentScenarioResultCommentId, json) {
      return $http.put(systemValidationApiPrefix + 'environments/' + environmentId + '/studentScenarioResults/' + studentScenarioResultId + '/studentScenarioResultComments/' + studentScenarioResultCommentId, json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getComments(environmentId, studentScenarioResultId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/studentScenarioResults/' + studentScenarioResultId + '/studentScenarioResultComments')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getStudentScenarioResultCommentCategories(environmentId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/studentScenarioResultCommentCategories')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getExternalDefectRepositories(environmentId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/externalDefectRepositories')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

  }
})();
