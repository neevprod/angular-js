(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('divJobService', divJobService);

  divJobService.$inject = ['$http', 'systemValidationApiPrefix'];

  function divJobService($http, systemValidationApiPrefix) {
    return {
      getDivJobs: getDivJobs,
      getDivJob: getDivJob,
      startDivJob: startDivJob,
      reExecuteStatsForCompletedDivJob: reExecuteStatsForCompletedDivJob,
      getNextState: getNextState,
      getOrderedPathStates: getOrderedPathStates
    };

    /**
     * Get a list of DIV jobs from the server.
     *
     * @returns The result from the server containing the list of DIV jobs.
     */
    function getDivJobs(environmentId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Retrieve the DIV job with the given environmentId and divJobExecId.
     *
     * @param {number} environmentId - The ID of the environment.
     * @param {number} divJobExecId - The ID of the DIV job to retrieve.
     * @returns The requested divJob.
     */
    function getDivJob(environmentId, divJobExecId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId).then(function (result) {
        return result.data;
      }).catch(function (result) {
        return result.data;
      });
    }

    /**
     * Start the given DIV job.
     *
     * @param {number} environmentId - The ID of the environment.
     * @param {number} divJobId - The ID of the DIV job to start.
     * @returns The result from the server.
     */
    function startDivJob(environmentId, divJobId) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobId + '/divJob')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Start the given DIV job.
     *
     * @param {number} environmentId - The ID of the environment.
     * @param {number} divJobId - The ID of the DIV job to start.
     * @returns The result from the server.
     */
    function reExecuteStatsForCompletedDivJob(environmentId, divJobId) {
      return $http.post(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobStatsReExecutions/' +
        divJobId + '/divJob')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the next state following the state with the given ID.
     *
     * @param currentStateId - The ID of the current state.
     * @param hasEpenScenario - Whether the student scenario has an ePEN scenario.
     * @param orderedPathStates - An array of the pathStates in order.
     * @returns The next state.
     */
    function getNextState(currentStateId, hasEpenScenario, orderedPathStates) {
      if (!currentStateId) {
        return orderedPathStates[0].state;
      }
      var currentPathStateId;
      for (var i = 0; i < orderedPathStates.length; i++) {
        if (orderedPathStates[i].state.stateId === currentStateId) {
          currentPathStateId = i;
        }
      }

      if (!hasEpenScenario) {
        while (currentPathStateId < orderedPathStates.length - 1 &&
        (orderedPathStates[currentPathStateId + 1].state.stateName === 'BE_EPEN_PATH_VALIDATOR' ||
          orderedPathStates[currentPathStateId + 1].state.stateName === 'EPEN2_AUTOMATION')) {
          currentPathStateId++;
        }
      }

      return orderedPathStates[Math.min(orderedPathStates.length - 1, currentPathStateId + 1)].state;
    }

    /**
     * Based on the given list of pathStates, get a list of the same pathStates sorted according to their order.
     *
     * @param pathStates - The array of pathStates to sort.
     * @returns An ordered array of pathStates.
     */
    function getOrderedPathStates(pathStates) {
      return angular.copy(pathStates).sort(function (pathState1, pathState2) {
        return pathState1.pathStateOrder - pathState2.pathStateOrder;
      });
    }
  }

})();
