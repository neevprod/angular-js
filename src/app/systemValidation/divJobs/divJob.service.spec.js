'use strict';

describe('DIV job service', function () {
  var divJobService;
  var $httpBackend;
  var systemValidationApiPrefix;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_divJobService_, _$httpBackend_, _systemValidationApiPrefix_) {
    divJobService = _divJobService_;
    $httpBackend = _$httpBackend_;
    systemValidationApiPrefix = _systemValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getDivJobs', function () {
    it('should submit the DIV jobs GET request properly', function () {
      // Given
      var data = [{divJobId: 1}, {divJobId: 2}, {divJobId: 3}];

      // Then
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/1/divJobExecutions')
        .respond(200, {success: true, data: data});

      // When
      divJobService.getDivJobs(1);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{divJobId: 1}, {divJobId: 2}, {divJobId: 3}];
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/divJobExecutions')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      divJobService.getDivJobs(1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/divJobExecutions')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      divJobService.getDivJobs(1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
