(function () {

  'use strict';

  angular.module('systemValidation').controller('DivJobsController',
    divJobsController);

  divJobsController.$inject = ['$stateParams', 'divJobService',
    'environmentService', 'memoryService', 'permissionsService',
    'itemsPerPageOptions', 'dateTimeFormat', 'divJobSetupService',
    '$state', '$uibModal', 'studentScenarioService', 'batteryStudentScenarioService', '$window', 'notificationsService', '$q', '$rootScope'];

  function divJobsController($stateParams, divJobService, environmentService,
                             memoryService, permissionsService, itemsPerPageOptions,
                             dateTimeFormat, divJobSetupService, $state, $uibModal, studentScenarioService, batteryStudentScenarioService, $window, notificationsService, $q, $rootScope) {
    var viewModel = this;

    viewModel.environmentId = $stateParams.environmentId;
    viewModel.loadingDivJobs = true;
    viewModel.divJobsInitialized = false;
    viewModel.divJobs = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService
        .getItem('divJobs', 'perPage', '')
      || viewModel.itemsPerPageOptions[0].value;
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.allowDivJobCreation = permissionsService
      .hasEnvironmentLevelPermission('divJobExecutions',
        'createAccess', 'systemValidation',
        viewModel.environmentId);
    viewModel.allowDivJobResume = permissionsService
      .hasEnvironmentLevelPermission('divJob', 'createAccess',
        'systemValidation', viewModel.environmentId);
    viewModel.hasUsageStatisticsPermission = permissionsService
      .hasAnyEnvironmentLevelPermission('usageStatistics',
        'readAccess', 'systemValidation');
    viewModel.goToDivJobSetup = goToDivJobSetup;
    viewModel.resumeDivJobs = resumeDivJobs;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.goToUsageStatistics = goToUsageStatistics;
    viewModel.goToDivJobForms = goToDivJobForms;
    viewModel.showStatsReport = showStatsReport;
    viewModel.exportFailedScenarios = exportFailedScenarios;
    var needToReRunStats = true;
    var csvContent = 'div job id,battery id, student scenario id,test code,form code,test session,UUID,student code,testnav scenario,epen scenario,current state,status\n';
    activate();
    $rootScope.$on('ReRun', function (evt, data) {
      viewModel.reRun = data;
    });
    viewModel.setStatusFilter = setStatusFilter;
    viewModel.statusFilterMenus = ['No Filter', 'Queued', 'Not Started', 'In Progress', 'Completed', 'Failed'];
    viewModel.selectedFilter = [viewModel.statusFilterMenus[0]];

    /**
     * Loads the list of DIV jobs.
     */
    function activate() {
      var jobsJustStarted = [];
      if ($stateParams.divJobJustCreated) {
        viewModel.alertSuccess = true;
        viewModel.alertMessage = 'A new Data Integrity Validation job was successfully created.';
        jobsJustStarted.push($stateParams.divJobJustCreated);
      }

      if ($stateParams.environmentName) {
        viewModel.environmentName = $stateParams.environmentName;
      } else {
        getEnvironmentName(viewModel.environmentId);
      }

      getDivJobs(jobsJustStarted).then(function (divJobs) {
        viewModel.loadingDivJobs = false;
        viewModel.divJobsInitialized = true;
        viewModel.divJobs = divJobs;
        viewModel.displayedDivJobs = divJobs;
      });
    }

    function getEnvironmentName(environmentId) {
      environmentService
        .getEnvironment(environmentId)
        .then(
          function (result) {
            if (result.success) {
              viewModel.environmentName = result.data.environment.name;
            }
          });
    }

    /**
     * Retrieve the list of DIV jobs.
     *
     * @returns The list of DIV jobs.
     */
    function getDivJobs(jobsJustStarted) {
      return divJobService
        .getDivJobs($stateParams.environmentId)
        .then(
          function (result) {
            if (result.success) {
              var divJobs = result.data.divJobExecutions;
              divJobs
                .forEach(function (divJob) {
                  if (jobsJustStarted && jobsJustStarted.indexOf(divJob.divJobExecId) >= 0) {
                    if (divJob.divJobExecStatus.status === 'Not Started'
                      || divJob.divJobExecStatus.status === 'Failed'
                      || divJob.divJobExecStatus.status.startsWith('Completed')) {
                      divJob.divJobExecStatus.divJobExecStatusId = 2;
                      divJob.divJobExecStatus.status = 'In Progress';
                    }
                  }
                  divJob.isSelectable = divJob.divJobExecStatus.status === 'Not Started'
                    || divJob.divJobExecStatus.status === 'Failed'
                    || (divJob.divJobExecStatus.status.indexOf('Completed') === 0 && divJob.hasStatsState);
                });

              return divJobs;
            }
          });
    }

    /**
     * Navigate to the DIV job setup page.
     */
    function goToDivJobSetup() {
      // clears divJobSetupRecords from localStorage.
      divJobSetupService.clear();
      $state.go('testCodeSelection', {
        environmentId: viewModel.environmentId
      });
    }

    /**
     * Navigate to the UsageStatistics report page.
     */
    function goToUsageStatistics() {
      $state.go('divUsageStatistics', {
        environmentId: viewModel.environmentId
      });
    }

    /**
     * Navigate to the Div Job Forms report page.
     */
    function goToDivJobForms() {
      $state.go('divjobForms', {
        environmentId: viewModel.environmentId
      });
    }

    /**
     * Resume the selected DIV jobs.
     */
    function resumeDivJobs() {
      viewModel.resumingDivJobs = true;
      var selectedResumeDivJobIds = [];
      var selectedReRunStatsDivJobIds = [];

      viewModel.divJobs.forEach(function (divJob) {
        if (divJob.isSelected) {
          if (divJob.hasStatsState && divJob.divJobExecStatus.status.startsWith('Completed')) {
            selectedReRunStatsDivJobIds.push(divJob.divJobExecId);
          } else if (divJob.hasStatsState && divJob.divJobExecStatus.status === 'Failed' && divJob.numFailed !== divJob.numStudentScenarios) {
            needToReRunStats = false;
            selectedReRunStatsDivJobIds.push(divJob.divJobExecId);
          } else {
            needToReRunStats = false;
            selectedResumeDivJobIds.push(divJob.divJobExecId);
          }

        }
      });
      if (selectedReRunStatsDivJobIds.length > 0) {
        reRunStatsAndResumeDivJobs(selectedResumeDivJobIds, selectedReRunStatsDivJobIds);
      } else {
        resumeDivJobsRecursive(selectedResumeDivJobIds, [], 0);
      }
    }

    function reRunStatsAndResumeDivJobs(selectedResumeDivJobIds, selectedReRunStatsDivJobIds) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/systemValidation/divJobs/reRunStats/confirmationModal.html',
        controller: 'ReRunStatsController as reRunStatsCtrl',
        backdrop: 'static',
        resolve: {
          reRunOnly: function () {
            return needToReRunStats;
          }
        }
      });
      modalInstance.result.then(function (action) {
          if (action) {
            if (action === 'reRun') {
              if (selectedResumeDivJobIds.length > 0) {
                resumeDivJobsRecursive(selectedResumeDivJobIds, [], 0);
              }
              if (selectedReRunStatsDivJobIds.length > 0) {
                reExecuteStatsForCompletedDivJob(selectedReRunStatsDivJobIds, [], 0);
              }
            } else if (action === 'resumeOnly') {
              if (selectedResumeDivJobIds.length > 0) {
                resumeDivJobsRecursive(selectedResumeDivJobIds, [], 0);
              }
              if (selectedReRunStatsDivJobIds.length > 0) {
                resumeDivJobsRecursive(selectedReRunStatsDivJobIds, [], 0);
              }
            } else {
              viewModel.resumingDivJobs = false;
              uncheckSelectedJobs();
            }
          } else {
            uncheckSelectedJobs();
            viewModel.resumingDivJobs = false;
          }
        }
      );
    }

    function uncheckSelectedJobs() {
      viewModel.divJobs.forEach(function (divJob) {
        if (divJob.isSelected) {
          divJob.isSelected = false;
        }
      });
    }

    /**
     * Recursively resume the given DIV jobs.
     *
     * @param {number[]} selectedDivJobIds - The IDs of the selected DIV jobs.
     * @param {number[]} failures - The IDs of the DIV jobs that failed to resume.
     * @param {number} currentIndex - The current index.
     */
    function resumeDivJobsRecursive(selectedDivJobIds, failures, currentIndex) {
      divJobService
        .startDivJob(viewModel.environmentId,
          selectedDivJobIds[currentIndex])
        .then(
          function (result) {
            if (!result.success) {
              if (!failures) {
                failures = [];
              }
              failures
                .push(selectedDivJobIds[currentIndex]);
            }

            currentIndex++;
            if (currentIndex < selectedDivJobIds.length) {
              resumeDivJobsRecursive(selectedDivJobIds, failures, currentIndex);
            } else {
              viewModel.resumingDivJobs = false;

              var jobsResumed = selectedDivJobIds
                .filter(function (jobId) {
                  return failures.indexOf(jobId) === -1;
                });

              getDivJobs(jobsResumed)
                .then(
                  function (divJobs) {
                    viewModel.loadingDivJobs = false;
                    viewModel.divJobs = divJobs;
                    viewModel.displayedDivJobs = divJobs;

                    if (failures && failures.length) {
                      viewModel.alertSuccess = false;
                      viewModel.alertMessage = 'The following Data Integrity Validation job(s) could not be resumed: '
                        + failures.join(', ');
                    } else {
                      viewModel.alertSuccess = true;
                      viewModel.alertMessage = 'The Data Integrity Validation job(s) were successfully resumed.';
                    }
                  });


            }
          });
    }

    /**
     * Recursively re-runs stats state of given completed DIV jobs.
     *
     * @param {number[]} selectedDivJobIds - The IDs of the selected DIV jobs.
     * @param {number[]} failures - The IDs of the DIV jobs that failed to resume.
     * @param {number} currentIndex - The current index.
     */
    function reExecuteStatsForCompletedDivJob(selectedDivJobIds, failures, currentIndex) {
      divJobService.reExecuteStatsForCompletedDivJob(viewModel.environmentId, selectedDivJobIds[currentIndex]).then(function (result) {
        if (!result.success) {
          if (!failures) {
            failures = [];
          }
          failures
            .push(selectedDivJobIds[currentIndex]);
        }

        currentIndex++;
        if (currentIndex < selectedDivJobIds.length) {
          reExecuteStatsForCompletedDivJob(selectedDivJobIds, failures, currentIndex);
        } else {
          viewModel.resumingDivJobs = false;

          var jobsResumed = selectedDivJobIds
            .filter(function (jobId) {
              return failures.indexOf(jobId) === -1;
            });

          getDivJobs(jobsResumed)
            .then(
              function (divJobs) {
                viewModel.loadingDivJobs = false;
                viewModel.divJobs = divJobs;
                viewModel.displayedDivJobs = divJobs;

                if (failures && failures.length) {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = 'The following Data Integrity Validation job(s) could not be resumed: '
                    + failures.join(', ');
                } else {
                  viewModel.alertSuccess = true;
                  viewModel.alertMessage = 'The Data Integrity Validation job(s) were successfully resumed.';
                }
              });


        }
      });
    }

    /**
     * Show the STATS failure report for the selected div job(s) in a modal.
     */
    function showStatsReport() {
      var selectedDivJobIds = [];
      viewModel.divJobs.forEach(function (divJob) {
        if (divJob.isSelected) {
          selectedDivJobIds.push(divJob.divJobExecId);
        }
      });
      $uibModal
        .open({
          templateUrl: 'app/systemValidation/divJobs/stats/viewStatsReport.html',
          controller: 'ViewStatsFailureController as viewStatsFailureCtrl',
          resolve: {
            selectedDivJobIds: function () {
              return selectedDivJobIds;
            }
          },
          size: 'xl'
        });
    }

    function battery(i, selectedDivJobIds, downloadId) {
      var promiseChain = [];
      var deferred = $q.defer();

      return batteryStudentScenarioService
        .getBatteryStudentScenarios(viewModel.environmentId, selectedDivJobIds[i])
        .then(
          function (result) {
            if (result.success) {
              result.data.batteryStudentScenarios.forEach(function (batteryStudentScenario) {
                promiseChain.push(studentScenarioService.getBatteryUnits(viewModel.environmentId, selectedDivJobIds[i], batteryStudentScenario.batteryStudentScenarioId).then(function (result) {
                  if (result.success) {
                    result.data.studentScenarios.forEach(function (studentScenario) {
                      if (studentScenario.epenScenario !== undefined) {
                        studentScenario.epenScenario = studentScenario.epenScenario.scenarioName;
                      } else {
                        studentScenario.epenScenario = 'N/A';
                      }
                      studentScenario.currentState = getCurrentState(studentScenario);
                      if (studentScenario.studentScenarioStatus.status === 'Failed') {
                        var line = studentScenario.divJobExec.divJobExecId + ',' + batteryStudentScenario.batteryStudentScenarioId + ',' + studentScenario.studentScenarioId + ',' + studentScenario.testnavScenario.testCode + ',' + studentScenario.testnavScenario.formCode + ',' + studentScenario.testSession.sessionName + ',' + studentScenario.uuid + ',' + studentScenario.studentCode + ',' + studentScenario.testnavScenario.scenarioName + ',' + studentScenario.epenScenario + ',' + studentScenario.currentState + ',' + studentScenario.studentScenarioStatus.status + '\n';
                        csvContent = csvContent + line;
                      }
                    });
                  } else {
                    return notificationsService.notifyDownloadComplete(downloadId, false, null);
                  }
                }));

              });
              $q.all(promiseChain).then(function () {
                deferred.resolve();
              }, function () {
                deferred.reject();
              });
              return deferred.promise;

            } else {
              return notificationsService.notifyDownloadComplete(downloadId, false, null);
            }
          });

    }

    function standard(i, selectedDivJobIds, downloadId) {
      return studentScenarioService.getStudentScenarios(viewModel.environmentId, selectedDivJobIds[i]).then(function (result) {
        if (result.success) {
          result.data.studentScenarios.forEach(function (studentScenario) {
            if (studentScenario.epenScenario !== undefined) {
              studentScenario.epenScenario = studentScenario.epenScenario.scenarioName;
            } else {
              studentScenario.epenScenario = 'N/A';
            }
            studentScenario.currentState = getCurrentState(studentScenario);
            if (studentScenario.studentScenarioStatus.status === 'Failed') {
              var line = studentScenario.divJobExec.divJobExecId + ',' + 'N/A' + ',' + studentScenario.studentScenarioId + ',' + studentScenario.testnavScenario.testCode + ',' + studentScenario.testnavScenario.formCode + ',' + studentScenario.testSession.sessionName + ',' + studentScenario.uuid + ',' + studentScenario.studentCode + ',' + studentScenario.testnavScenario.scenarioName + ',' + studentScenario.epenScenario + ',' + studentScenario.currentState + ',' + studentScenario.studentScenarioStatus.status + '\n';
              csvContent = csvContent + line;
            }
          });
        } else {
          return notificationsService.notifyDownloadComplete(downloadId, false, null);
        }
      });

    }

    /**
     * Export Failed Scenarios.
     */
    function exportFailedScenarios() {
      var deferred = $q.defer();
      csvContent = 'div job id,battery id, student scenario id,test code,form code,test session,UUID,student code,testnav scenario,epen scenario,current state,status\n';
      var selectedDivJobIds = [];
      var selectedBatteryDivJobs = [];
      viewModel.divJobs.forEach(function (divJob) {
        if (divJob.isSelected) {
          selectedDivJobIds.push(divJob.divJobExecId);
          if (divJob.testType === 'Battery') {
            selectedBatteryDivJobs.push(divJob.divJobExecId);
          }
        }
      });
      var downloadId = 0;
      if ($window.localStorage.getItem('downloadId') !== null) {
        downloadId = parseInt($window.localStorage.getItem('downloadId')) + 1;
      }
      $window.localStorage.setItem('downloadId', downloadId);
      notificationsService.notifyNewExport(downloadId);
      var csv = {};
      var promiseArray = [];

      for (var i = 0; i < selectedDivJobIds.length; i++) {
        if (selectedBatteryDivJobs.indexOf(selectedDivJobIds[i]) !== -1) {
          promiseArray.push(battery(i, selectedDivJobIds, downloadId));

        } else {
          promiseArray.push(standard(i, selectedDivJobIds, downloadId));
        }
        if (i === (selectedDivJobIds.length - 1)) {
          getFinalPromise(csv, promiseArray, deferred, downloadId);
        }
      }
      return deferred.promise;
    }

    function getFinalPromise(csv, promiseArray, deferred, downloadId) {
      $q.all(promiseArray)
        .then(function () {
          csv.download = csvContent;
          notificationsService.notifyDownloadComplete(downloadId, true, csv);
          deferred.resolve();
        });
    }

    /**
     * Gets the currentState of the studentScenario based on the studentScenarioStatus.
     */
    function getCurrentState(studentScenario) {
      var orderedPathStates = divJobService.getOrderedPathStates(studentScenario.divJobExec.path.pathStates);
      if (studentScenario.studentScenarioStatus.status === 'Not Started') {
        return orderedPathStates[0].state.stateName;
      } else if (studentScenario.studentScenarioStatus.status === 'Failed') {
        if (!studentScenario.lastCompletedStateId) {
          return orderedPathStates[0].state.stateName;
        } else {
          return divJobService.getNextState(studentScenario.lastCompletedStateId, studentScenario.epenScenario !== 'N/A',
            orderedPathStates).stateName;
        }
      } else if (studentScenario.studentScenarioStatus.status === 'In Progress') {
        return divJobService.getNextState(studentScenario.lastCompletedStateId, studentScenario.epenScenario !== 'N/A',
          orderedPathStates).stateName;
      } else if (studentScenario.studentScenarioStatus.status.startsWith('Completed')) {
        return orderedPathStates[orderedPathStates.length - 1].state.stateName;
      }
    }

    //To store original list of divJobs for No filter.
    var tempDivJobs = [];

    function setStatusFilter(menu) {
      if (menu === 'No Filter' && viewModel.selectedFilter.length === 0) {
        viewModel.selectedFilter.push(menu);
        return;
      }
      if (tempDivJobs.length === 0) {
        tempDivJobs = viewModel.divJobs;
      } else {
        viewModel.divJobs = tempDivJobs;
      }
      if (menu !== 'No Filter') {
        var noFilterIndex = viewModel.selectedFilter.indexOf('No Filter');
        if (noFilterIndex !== -1) {
          viewModel.selectedFilter.splice(noFilterIndex, 1);
        }
        var index = viewModel.selectedFilter.indexOf(menu);
        if (index !== -1) {
          viewModel.selectedFilter.splice(index, 1);
        } else {
          viewModel.selectedFilter.push(menu);
        }
        viewModel.displayedStudentScenarios = [];
        viewModel.divJobs = [];
        if (viewModel.selectedFilter.length > 0) {
          viewModel.statusFilter = true;
          angular.forEach(tempDivJobs, function (divJob) {
            var index = getSelectedStatusIndex(divJob.divJobExecStatus.status);
            if (index !== -1) {
              viewModel.divJobs.push(divJob);
            }
          });
        } else {
          viewModel.statusFilter = false;
          viewModel.divJobs = tempDivJobs;
          viewModel.selectedFilter = [viewModel.statusFilterMenus[0]];
        }
      } else {
        viewModel.selectedFilter = [];
        viewModel.selectedFilter.push(menu);
        viewModel.divJobs = tempDivJobs;
        tempDivJobs = [];
        viewModel.statusFilter = false;
      }
      viewModel.displayedDivJobs = viewModel.divJobs;
    }

    function getSelectedStatusIndex(status) {
      for (var i = 0; i < viewModel.selectedFilter.length; i++) {
        if (status.indexOf(viewModel.selectedFilter[i]) !== -1) {
          return i;
        }
      }
      return -1;
    }
  }
})();
