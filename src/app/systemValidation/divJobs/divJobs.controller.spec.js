'use strict';

describe('DIV jobs controller', function () {
  var controller;
  var $stateParams;
  var divJobService;
  var environmentService;
  var permissionsService;
  var deferredDivJobs;
  var deferredEnvironments;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredDivJobs = $q.defer();
    deferredEnvironments = $q.defer();
    divJobService = jasmine.createSpyObj('divJobService', ['getDivJobs']);
    divJobService.getDivJobs.and.callFake(function () {
      return deferredDivJobs.promise;
    });

    environmentService = jasmine.createSpyObj('environmentService', ['getEnvironment']);
    environmentService.getEnvironment.and.callFake(function () {
      return deferredEnvironments.promise;
    });

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasEnvironmentLevelPermission', 'hasAnyEnvironmentLevelPermission']);
    permissionsService.hasEnvironmentLevelPermission.and.returnValue(true);
    permissionsService.hasAnyEnvironmentLevelPermission.and.returnValue(true);

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('DivJobsController', {
      $stateParams: $stateParams, divJobService: divJobService,
      environmentService: environmentService, permissionsService: permissionsService,
      itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should set the list of DIV jobs to an empty array during initialization', function () {
    // Then
    expect(controller.divJobs).toEqual([]);
  });

  it('should call divJobService to get the list of DIV jobs during initialization', function () {
    // Then
    expect(divJobService.getDivJobs).toHaveBeenCalled();
  });

  it('should set allowDivJobCreation according to the user permissions during initialization', function () {
    // Then
    expect(controller.allowDivJobCreation).toBe(true);
  });

  it('should set allowDivJobResume according to the user permissions during initialization', function () {
    // Then
    expect(controller.allowDivJobResume).toBe(true);
  });

  it('should set the list of DIV jobs after divJobService returns', function () {
    // Given
    var divJobArray = [
      {
        divJobId: 1,
        divJobExecStatus: {
          status: 'Completed'
        }
      },
      {
        divJobId: 2,
        divJobExecStatus: {
          status: 'Not Started'
        }
      },
      {
        divJobId: 3,
        divJobExecStatus: {
          status: 'In Progress'
        }
      }
    ];

    // When
    deferredDivJobs.resolve({success: true, data: {divJobExecutions: divJobArray}});
    $rootScope.$digest();

    // Then
    expect(controller.divJobs).toEqual(divJobArray);
    expect(controller.displayedDivJobs).toEqual(divJobArray);
  });
});
