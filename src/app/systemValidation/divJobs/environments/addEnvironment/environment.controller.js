(function () {

  'use strict';

  angular.module('systemValidation').controller('EnvironmentController', environmentController);

  environmentController.$inject = ['$uibModal', '$stateParams', 'environmentService', '$filter', '$state'];

  function environmentController($uibModal, $stateParams, environmentService, $filter, $state) {

    var viewModel = this;

    viewModel.environment = angular
      .fromJson('{ "feUrl": "", "epenUrl": "", "name": "", "beMysqlConnection": {}, "dataWarehouseMongoConnection": {}, "epen2MysqlConnection": {}, "feMysqlConnection": {}, "irisMysqlConnection": {}, "epen2MongoConnection": {} }');
    viewModel.showDbConnectionModal = showDbConnectionModal;
    viewModel.showApiModal = showApiModal;
    viewModel.validateDbConnection = validateDbConnection;
    viewModel.validateApiConnection = validateApiConnection;
    viewModel.save = save;
    viewModel.update = update;
    viewModel.cancel = cancel;
    viewModel.onSelect = onSelect;
    viewModel.alertSuccess = true;
    viewModel.dbConnections = [];
    getDbConnections();
    getApiConnections();

    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    if ($stateParams.environmentId) {
      getEnvironment();
    }

    /**
     * Get environment details based on the selected environmentId.
     */
    function getEnvironment() {
      return environmentService.getEnvironment($stateParams.environmentId).then(function (result) {
        if (result.success) {
          viewModel.environment = result.data.environment;
          viewModel.environment.feMysqlConnection.host = result.data.environment.feMysqlConnection.host + ' (' + result.data.environment.feMysqlConnection.dbName + ')';
          viewModel.environment.beMysqlConnection.host = result.data.environment.beMysqlConnection.host + ' (' + result.data.environment.beMysqlConnection.dbName + ')';
          viewModel.environment.dataWarehouseMongoConnection.host = result.data.environment.dataWarehouseMongoConnection.host + ' (' + result.data.environment.dataWarehouseMongoConnection.dbName + ')';
          viewModel.environment.epen2MysqlConnection.host = result.data.environment.epen2MysqlConnection.host + ' (' + result.data.environment.epen2MysqlConnection.dbName + ')';
          viewModel.environment.irisMysqlConnection.host = result.data.environment.irisMysqlConnection.host + ' (' + result.data.environment.irisMysqlConnection.dbName + ')';
          viewModel.environment.epen2MongoConnection.host = result.data.environment.epen2MongoConnection.host + ' (' + result.data.environment.epen2MongoConnection.dbName + ')';
        }
      });
    }

    /**
     * Fetches all Database connections.
     */
    function getDbConnections() {
      return environmentService.getDbConnections().then(function (result) {
        if (result.success) {
          viewModel.dbConnections = result.data.dbConnections;
          for (var j = 0; j < viewModel.dbConnections.length; j++) {
            viewModel.dbConnections[j].host = result.data.dbConnections[j].host + ' (' + result.data.dbConnections[j].dbName + ')';
          }
        }
      });
    }

    /**
     * Fetches all API connections.
     */
    function getApiConnections() {
      return environmentService.getApiConnections().then(function (result) {
        if (result.success) {
          viewModel.apiConnections = result.data.apiConnections;
        }
      });
    }

    function onSelect(apiConnection, model) {
      viewModel.environment[model] = angular.copy(apiConnection);
    }

    function validateDbConnection(model) {
      if (!viewModel.environment[model].host) {
        viewModel.environment[model] = {};
        return;
      }

      if (viewModel.environment[model].dbConnectionId) {
        var originalDb = $filter('filter')(viewModel.dbConnections, {dbConnectionId: viewModel.environment[model].dbConnectionId});
        for (var j = originalDb.length - 1; j >= 0; j--) {
          if (!(angular.equals(originalDb[j].dbConnectionId, viewModel.environment[model].dbConnectionId))) {
            originalDb.splice(j, 1);
          }
        }
        if (originalDb[0].host !== viewModel.environment[model].host) {
          viewModel.environment[model] = {};
        }
      } else {
        viewModel.environment[model] = {};
      }
    }

    function validateApiConnection(model) {
      if (!viewModel.environment[model].apiUrl) {
        viewModel.environment[model] = {};
        return;
      }

      if (viewModel.environment[model].apiConnectionId) {
        var originalApi = $filter('filter')(viewModel.apiConnections, {apiConnectionId: viewModel.environment[model].apiConnectionId})[0];
        if (originalApi.apiUrl !== viewModel.environment[model].apiUrl) {
          viewModel.environment[model] = {};
        }
      } else {
        viewModel.environment[model] = {};
      }
    }

    /**
     * Displays a form in a modal for database connection information.
     */
    function showDbConnectionModal(isNew, model, dbConnectionId) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/systemValidation/divJobs/environments/dbConnection/dbConnection.html',
        controller: 'DbConnectionController as dbConnectionCtrl',
        backdrop: 'static',
        resolve: {
          dbConnection: function () {
            if (dbConnectionId) {
              return environmentService.getDbConnection(dbConnectionId).then(function (result) {
                if (result.success) {
                  return result.data.dbConnection;
                }
              });
            }
          },
          environmentId: function () {
            return viewModel.environment.environmentId;
          }
        }
      });
      modalInstance.result.then(function (updatedDbConnection) {
        var oldDbConnection;
        var i;
        if (!updatedDbConnection) {
          return;
        }

        viewModel.environment[model] = updatedDbConnection;
        oldDbConnection = $filter('filter')(viewModel.dbConnections, {dbConnectionId: updatedDbConnection.dbConnectionId});
        if (oldDbConnection !== undefined && oldDbConnection !== '' && oldDbConnection.length > 0) {
          for (i = viewModel.dbConnections.length - 1; i >= 0; i--) {
            if (angular.equals(viewModel.dbConnections[i].dbConnectionId, updatedDbConnection.dbConnectionId)) {
              viewModel.dbConnections.splice(i, 1);
            }
          }
        }
        viewModel.dbConnections.push(updatedDbConnection);
        getDbConnections();
      });
    }

    /**
     * Displays a form in a modal for API connection information.
     */
    function showApiModal(isNew, model, apiConnectionId) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/systemValidation/divJobs/environments/apiConnection/apiConnection.html',
        controller: 'ApiConnectionController as apiConnectionCtrl',
        backdrop: 'static',
        resolve: {
          apiConnection: function () {
            if (apiConnectionId) {
              return environmentService.getApiConnection(apiConnectionId).then(function (result) {
                if (result.success) {
                  return result.data.apiConnection;
                }
              });
            }
          }
        }
      });
      modalInstance.result.then(function (apiConnection) {
        if (!apiConnection) {
          return;
        }
        if (isNew) {
          viewModel.environment[model] = apiConnection;
          viewModel.apiConnections.push(apiConnection);
        } else {
          if (angular.equals(viewModel.environment.feApiConnection.apiConnectionId,
            apiConnection.apiConnectionId)) {
            viewModel.environment.feApiConnection = apiConnection;
          }
          if (angular.equals(viewModel.environment.beApiConnection.apiConnectionId,
            apiConnection.apiConnectionId)) {
            viewModel.environment.beApiConnection = apiConnection;
          }
          if (angular.equals(viewModel.environment.irisApiConnection.apiConnectionId,
            apiConnection.apiConnectionId)) {
            viewModel.environment.irisApiConnection = apiConnection;
          }
          if (angular.equals(viewModel.environment.dataWarehouseApiConnection.apiConnectionId,
            apiConnection.apiConnectionId)) {
            viewModel.environment.dataWarehouseApiConnection = apiConnection;
          }
          if (angular.equals(viewModel.environment.epen2ApiConnection.apiConnectionId,
            apiConnection.apiConnectionId)) {
            viewModel.environment.epen2ApiConnection = apiConnection;
          }

          if (angular.equals(viewModel.environment.iceBridgeApiConnection.apiConnectionId,
            apiConnection.apiConnectionId)) {
            viewModel.environment.iceBridgeApiConnection = apiConnection;
          }

          var oldApiConnection = $filter('filter')(viewModel.apiConnections, {apiConnectionId: apiConnection.apiConnectionId});

          viewModel.apiConnections.splice(viewModel.apiConnections.indexOf(oldApiConnection), 1);
          viewModel.apiConnections.push(apiConnection);
        }
      });
    }

    /**
     * Sends environment details to the service
     */
    function save(isValid) {
      if (isValid && viewModel.environment) {
        environmentService.save(viewModel.environment).then(
          function (result) {
            if (result.success) {
              $state.go('environments', {
                'environmentCreated': true,
                'alertMessage': 'New environment created successfully!'
              });
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });

      } else {
        viewModel.alertSuccess = false;
        viewModel.alertMessage = 'Please address the errors shown below.';
      }

    }

    /**
     * Sends Updated environment details to the service.
     */
    function update(isValid) {
      if (isValid && viewModel.environment) {
        environmentService.update(viewModel.environment).then(
          function (result) {
            if (result.success) {
              viewModel.alertMessage = 'Environment was updated successfully!';
              $state.go('environments', {
                'environmentCreated': true,
                'alertMessage': 'Environment was updated successfully!'
              });
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });
      } else {
        viewModel.alertSuccess = false;
        viewModel.alertMessage = 'Please address the errors shown below.';
      }
    }

    function cancel() {
      $state.go('environments');
    }

  }
})();
