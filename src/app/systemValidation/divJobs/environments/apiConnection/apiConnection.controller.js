(function () {

  'use strict';

  angular.module('systemValidation').controller('ApiConnectionController', ApiConnectionController);

  ApiConnectionController.$inject = ['$uibModalInstance', 'apiConnection', 'apiConnectionService'];

  function ApiConnectionController($uibModalInstance, apiConnection, apiConnectionService) {
    var viewModel = this;
    viewModel.tempApiConnection = angular.copy(apiConnection);
    viewModel.apiConnection = apiConnection;
    viewModel.save = save;
    viewModel.update = update;
    viewModel.close = close;
    viewModel.hideShowSecret = hideShowSecret;
    viewModel.hideButton = false;
    viewModel.inputType = 'password';

    /**
     * Sends API connection information to the service
     */
    function save(isValid) {
      if (isValid && viewModel.apiConnection) {
        apiConnectionService.saveApiConnection(viewModel.apiConnection).then(function (result) {
          if (result.success) {
            viewModel.alertSuccess = true;
            viewModel.hideButton = true;
            viewModel.alertMessage = 'Api Connection information saved sucessfully!';
            viewModel.tempApiConnection = result.data.apiConnection;
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
      }
    }

    /**
     * Sends Updated API Connection information to the service.
     */
    function update(isValid) {
      if (isValid && viewModel.apiConnection) {
        apiConnectionService.updateApiConnection(viewModel.apiConnection.apiConnectionId,
          viewModel.apiConnection).then(function (result) {
          if (result.success) {
            viewModel.alertSuccess = true;
            viewModel.hideButton = true;
            viewModel.alertMessage = 'Api Connection updated sucessfully!';
            viewModel.tempApiConnection = result.data.apiConnection;
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close(viewModel.tempApiConnection);
    }

    function hideShowSecret() {
      if (viewModel.inputType === 'password') {
        viewModel.inputType = 'text';
      } else {
        viewModel.inputType = 'password';
      }
    }
  }

})();
