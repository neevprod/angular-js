(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('apiConnectionService', apiConnectionService);

  apiConnectionService.$inject = ['$http', 'systemValidationApiPrefix'];

  function apiConnectionService($http, systemValidationApiPrefix) {
    return {
      saveApiConnection: saveApiConnection,
      updateApiConnection: updateApiConnection
    };

    function saveApiConnection(json) {
      return $http.post(systemValidationApiPrefix + '/apiConnection', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function updateApiConnection(apiConnectionId, json) {
      return $http.put(systemValidationApiPrefix + 'apiConnections/' + apiConnectionId + '/apiConnection', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
