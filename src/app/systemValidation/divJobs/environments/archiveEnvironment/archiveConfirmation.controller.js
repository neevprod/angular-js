(function () {

  'use strict';

  angular.module('systemValidation').controller('ArchiveConfirmationController', ArchiveConfirmationController);

  ArchiveConfirmationController.$inject = ['$uibModalInstance', 'environmentId', 'environmentName',
    'currentArchiveStatus', 'environmentService'];

  function ArchiveConfirmationController($uibModalInstance, environmentId, environmentName, currentArchiveStatus,
                                         environmentService) {
    var viewModel = this;
    viewModel.alertSuccess = false;
    viewModel.loading = false;
    viewModel.environmentName = environmentName;
    viewModel.archiveType = currentArchiveStatus ? 'un-archive' : 'archive';
    viewModel.changeArchiveStatus = changeArchiveStatus;
    viewModel.close = close;


    /**
     * Changes the archive status of the environment.
     */
    function changeArchiveStatus() {
      viewModel.loading = true;
      environmentService.setArchiveStatus(environmentId, !currentArchiveStatus).then(
        function (result) {
          viewModel.loading = false;

          if (result.success) {
            viewModel.alertSuccess = true;
            close();
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }

})();
