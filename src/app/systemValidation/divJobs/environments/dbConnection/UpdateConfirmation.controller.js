(function () {

  'use strict';

  angular.module('systemValidation').controller('UpdateConfirmationController', UpdateConfirmationController);

  UpdateConfirmationController.$inject = ['$uibModalInstance', 'environmentNames'];

  function UpdateConfirmationController($uibModalInstance, environmentNames) {
    var viewModel = this;
    viewModel.environmentNames = environmentNames;
    viewModel.confirm = confirm;
    viewModel.close = close;

    function confirm() {
      $uibModalInstance.close('yes');
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close('no');
    }


  }

})();
