(function () {

  'use strict';

  angular.module('systemValidation').controller('DbConnectionController', DbConnectionController);

  DbConnectionController.$inject = ['$uibModalInstance', 'dbConnection', 'dbConnectionService', 'environmentService', 'environmentId', '$uibModal'];

  function DbConnectionController($uibModalInstance, dbConnection, dbConnectionService, environmentService, environmentId, $uibModal) {
    var viewModel = this;

    viewModel.tempDbConnection = angular.copy(dbConnection);
    viewModel.dbConnection = dbConnection;
    viewModel.save = save;
    viewModel.update = update;
    viewModel.close = close;
    viewModel.hideShowPassword = hideShowPassword;
    viewModel.hideButton = false;
    viewModel.inputType = 'password';
    getDbConnections();

    /**
     * Sends Database connection information to the service
     */
    function save(isValid) {
      if (isValid && viewModel.dbConnection) {
        viewModel.saveValue = false;
        angular.forEach(viewModel.dbConnections, function (value) {
          if (value.host === viewModel.dbConnection.host && value.dbName === viewModel.dbConnection.dbName) {
            viewModel.hideButton = true;
            viewModel.alertSuccess = false;
            viewModel.alertMessage = 'Another database connection with the same Host and Database Name values already exists!';
            viewModel.tempDbConnection = value;
            viewModel.saveValue = true;
            return viewModel.saveValue;
          }
        });
        if (viewModel.saveValue) {
          return viewModel.tempDbConnection;
        } else {
          dbConnectionService.saveDbConnection(viewModel.dbConnection).then(
            function (result) {
              if (result.success) {
                viewModel.hideButton = true;
                viewModel.alertSuccess = true;
                viewModel.alertMessage = 'Database Connection information saved successfully!';
                viewModel.tempDbConnection = result.data.dbConnection;
              } else {
                viewModel.alertSuccess = false;
                viewModel.alertMessage = result.errors[0].message;
              }
            });
        }
      }
    }

    /**
     * Sends Updated Database Connection information to the service.
     */
    function update(isValid) {
      if (isValid && viewModel.dbConnection) {
        // if no records are updated and update button is clicked then just close the window.
        if (angular.equals(viewModel.dbConnection, viewModel.tempDbConnection)) {
          close();
        } else {
          viewModel.updateValue = false;
          angular.forEach(viewModel.dbConnections, function (value) {
            if (value.host === viewModel.dbConnection.host && value.dbName === viewModel.dbConnection.dbName) {
              viewModel.hideButton = true;
              viewModel.alertSuccess = false;
              viewModel.alertMessage = 'Another database connection with the same Host and Database Name values already exists!';
              viewModel.tempDbConnection = value;
              viewModel.updateValue = true;
              return viewModel.updateValue;
            }
          });
          if (viewModel.updateValue) {
            return viewModel.tempDbConnection;
          } else {
            dbConnectionService.getEnvironments(environmentId, viewModel.dbConnection.dbConnectionId).then(
              function (result) {
                if (result.success) {
                  if (result.data.environments.length !== 0) {
                    var modalInstance = $uibModal.open({
                      template: '<div class="modal-header">' +
                        '<h4 class="modal-title" id="dbConnectionModal">Confirmation Message</h4>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        'Please note that these changes will also impact the following environment(s):' +
                        '<ul>' +
                        '<li class="text-danger" ng-repeat="name in dbUpdateConfirmCtrl.environmentNames">{{name}}</li>' +
                        '</ul>' +
                        '<p class="text-warning">Are you sure you want to make these changes?</p>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" ng-click="dbUpdateConfirmCtrl.close()">NO</button>' +
                        '<button type="submit" class="btn btn-primary" ng-click="dbUpdateConfirmCtrl.confirm()">Yes</button>' +
                        '</div>',
                      controller: 'UpdateConfirmationController as dbUpdateConfirmCtrl',
                      backdrop: 'static',
                      resolve: {
                        environmentNames: function () {
                          return result.data.environments;
                        }
                      }
                    });
                    modalInstance.result.then(function (confirmationMessage) {
                      if (confirmationMessage === 'yes') {
                        dbConnectionService.updateDbConnection(viewModel.dbConnection.dbConnectionId, viewModel.dbConnection).then(
                          function (result) {
                            if (result.success) {
                              viewModel.hideButton = true;
                              viewModel.alertSuccess = true;
                              viewModel.alertMessage = 'Database Connection information saved successfully!';
                              viewModel.tempDbConnection = result.data.dbConnection;
                              viewModel.tempDbConnection.host = value.host + ' (' + value.dbName + ')';
                            } else {
                              viewModel.alertSuccess = false;
                              viewModel.alertMessage = result.errors[0].message;
                            }
                          });
                      } else {
                        close();
                      }
                    });
                  } else {
                    dbConnectionService.updateDbConnection(viewModel.dbConnection.dbConnectionId, viewModel.dbConnection).then(
                      function (result) {
                        if (result.success) {
                          viewModel.hideButton = true;
                          viewModel.alertSuccess = true;
                          viewModel.alertMessage = 'Database Connection information saved successfully!';
                          viewModel.tempDbConnection = result.data.dbConnection;
                        } else {
                          viewModel.alertSuccess = false;
                          viewModel.alertMessage = result.errors[0].message;
                        }
                      });
                  }
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = result.errors[0].message;
                }
              });
          }
        }
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      if (viewModel.tempDbConnection) {
        viewModel.tempDbConnection.host = viewModel.tempDbConnection.host + ' (' + viewModel.tempDbConnection.dbName + ')';
      }
      $uibModalInstance.close(viewModel.tempDbConnection);
    }

    function hideShowPassword() {
      if (viewModel.inputType === 'password') {
        viewModel.inputType = 'text';
      } else {
        viewModel.inputType = 'password';
      }
    }

    /**
     * Fetches all Database connections.
     */
    function getDbConnections() {
      return environmentService.getDbConnections().then(function (result) {
        if (result.success) {
          viewModel.dbConnections = result.data.dbConnections;
        }
      });
    }
  }
})();
