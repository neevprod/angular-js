(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('dbConnectionService', dbConnectionService);

  dbConnectionService.$inject = ['$http', 'systemValidationApiPrefix'];

  function dbConnectionService($http, systemValidationApiPrefix) {
    return {
      saveDbConnection: saveDbConnection,
      updateDbConnection: updateDbConnection,
      getEnvironments: getEnvironments
    };

    function saveDbConnection(json) {
      return $http.post(systemValidationApiPrefix + 'dbConnection', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function updateDbConnection(dbConnectionId, json) {
      return $http.put(systemValidationApiPrefix + 'dbConnections/' + dbConnectionId + '/dbConnection', json)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of Environments from the server for the given dbConnectionId.
     * @param dbConnectionId
     * @param columnName
     * @returns The result from the server containing the list of Environments.
     */
    function getEnvironments(environmentId, dbConnectionId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/dbConnections/' + dbConnectionId + '/environments')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

  }
})();
