(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('environmentService', environmentService);

  environmentService.$inject = ['$http', 'systemValidationApiPrefix'];

  function environmentService($http, systemValidationApiPrefix) {
    return {
      getEnvironments: getEnvironments,
      getEnvironment: getEnvironment,
      getDbConnection: getDbConnection,
      getApiConnection: getApiConnection,
      getDbConnections: getDbConnections,
      getApiConnections: getApiConnections,
      save: save,
      update: update,
      setArchiveStatus: setArchiveStatus
    };

    /**
     * Get a list of Environments from the server.
     *
     * @returns The result from the server containing the list of
     *          Environments.
     */
    function getEnvironments() {
      return $http.get(systemValidationApiPrefix + 'environments')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }


    function getEnvironment(environmentId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/environment')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getDbConnections() {
      return $http.get(systemValidationApiPrefix + 'dbConnections')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getDbConnection(dbConnectionId) {
      return $http.get(systemValidationApiPrefix + 'dbConnections/' + dbConnectionId + '/dbConnection')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getApiConnections() {
      return $http.get(systemValidationApiPrefix + 'apiConnections')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getApiConnection(apiConnectionId) {
      return $http.get(systemValidationApiPrefix + 'apiConnections/' + apiConnectionId + '/apiConnection')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function save(environment) {
      return $http.post(systemValidationApiPrefix + 'environment', environment).then(function (result) {
        return result.data;
      })
        .catch(function (result) {
          return result.data;
        });
    }

    function update(environment) {
      return $http.put(systemValidationApiPrefix + 'environments/' + environment.environmentId + '/environment', environment)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function setArchiveStatus(environmentId, newArchiveStatus) {
      return $http.put(systemValidationApiPrefix + 'environments/' + environmentId + '/archiveStatus',
        {'archiveStatus': newArchiveStatus})
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
