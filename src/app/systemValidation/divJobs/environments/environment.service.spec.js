'use strict';

describe('environment service', function () {
  var environmentService;
  var $httpBackend;
  var systemValidationApiPrefix;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_environmentService_, _$httpBackend_, _systemValidationApiPrefix_) {
    environmentService = _environmentService_;
    $httpBackend = _$httpBackend_;
    systemValidationApiPrefix = _systemValidationApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getEnvironments', function () {
    it('should submit the environments GET request properly', function () {
      // Given
      var data = [{
        environmentId: 1
      }, {
        environmentId: 2
      }, {
        environmentId: 3
      }];

      // Then
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments').respond(200, {
        success: true,
        data: data
      });

      // When
      environmentService.getEnvironments();
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{
        environmentId: 1
      }, {
        environmentId: 2
      }, {
        environmentId: 3
      }];

      $httpBackend.whenGET(systemValidationApiPrefix + 'environments').respond(200, {
        success: true,
        data: data
      });

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      environmentService.getEnvironments().then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments').respond(404, {
        success: false,
        errors: [{
          message: 'Error message.'
        }]
      });

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      environmentService.getEnvironments().then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

  describe('getEnvironment', function () {
    it('should submit the environment GET request properly', function () {
      // Given
      var data = {
        environmentId: 1
      };

      // Then
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/1/environment').respond(200, {
        success: true,
        data: data
      });

      // When
      environmentService.getEnvironment(1);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = {
        environmentId: 1
      };
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/environment').respond(200, {
        success: true,
        data: data
      });

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      environmentService.getEnvironment(1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(systemValidationApiPrefix + 'environments/1/environment').respond(404, {
        success: false,
        errors: [{
          message: 'Error message.'
        }]
      });

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      environmentService.getEnvironment(1).then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
