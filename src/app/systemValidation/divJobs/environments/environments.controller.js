(function () {

  'use strict';

  angular.module('systemValidation').controller('EnvironmentsController', environmentController);

  environmentController.$inject = ['memoryService', 'itemsPerPageOptions', 'dateTimeFormat', 'environmentService',
    'permissionsService', '$state', '$stateParams', '$uibModal'];

  function environmentController(memoryService, itemsPerPageOptions, dateTimeFormat, environmentService,
                                 permissionsService, $state, $stateParams, $uibModal) {

    var viewModel = this;

    viewModel.loadingEnvironments = true;
    viewModel.environmentsInitialized = false;
    viewModel.environments = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('environments', 'perPage', '') || viewModel.itemsPerPageOptions[0].value;
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.hasAddEnvironmentPermission = permissionsService.hasAnyEnvironmentLevelPermission('environments',
      'createAccess', 'systemValidation');
    viewModel.hasAnyEditEnvironmentPermission = permissionsService.hasAnyEnvironmentLevelPermission('environments',
      'updateAccess', 'systemValidation');
    viewModel.hasUsageStatisticsPermission = permissionsService.hasAnyEnvironmentLevelPermission('usageStatistics',
      'readAccess', 'systemValidation');
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    viewModel.hasEditEnvironmentPermission = hasEditEnvironmentPermission;
    viewModel.goToNewEnvironmentSetup = goToNewEnvironmentSetup;
    viewModel.hasDivJobReadAccess = hasDivJobReadAccess;
    viewModel.setArchivedFilter = setArchivedFilter;
    viewModel.goToUsageStatistics = goToUsageStatistics;
    viewModel.showConfirmationDialog = showConfirmationDialog;

    activate();

    /**
     * Loads the list of Environments.
     */
    function activate() {
      var rememberedArchivedFilterValue = memoryService.getItem('environments', 'archivedFilter', 'main');
      viewModel.archivedFilter = rememberedArchivedFilterValue === null ? -1 :
        Number(rememberedArchivedFilterValue);
      setArchivedFilter(viewModel.archivedFilter);

      getEnvironments().then(function (environments) {
        viewModel.loadingEnvironments = false;
        viewModel.environmentsInitialized = true;
        viewModel.environments = environments;
        viewModel.displayedEnvironments = environments;
      });
    }

    /**
     * Retrieve the list of Environments.
     *
     * @returns The list of Environments.
     */
    function getEnvironments() {
      return environmentService.getEnvironments().then(function (result) {
        if (result.success) {
          if ($stateParams.environmentCreated) {
            viewModel.alertSuccess = true;
            viewModel.alertMessage = $stateParams.alertMessage;
          }
          return angular.fromJson(result.data.environments);
        }
      });
    }

    function goToNewEnvironmentSetup() {
      $state.go('newEnvironment');
    }

    /**
     * Find out whether the user has read access to DIV job executions for the given environment.
     *
     * @param {number} environmentId - The environment ID.
     * @returns {boolean} Whether the user has permission.
     */
    function hasDivJobReadAccess(environmentId) {
      return permissionsService.hasEnvironmentLevelPermission('divJobExecutions', 'readAccess',
        'systemValidation', environmentId);
    }

    /**
     * Find out whether the user has update access to the given environment.
     *
     * @param {number} environmentId - The environment ID.
     * @returns {boolean} Whether the user has permission.
     */
    function hasEditEnvironmentPermission(environmentId) {
      return permissionsService.hasEnvironmentLevelPermission('environments',
        'updateAccess', 'systemValidation', environmentId);
    }

    /**
     * Set the archived filter to the given value.
     *
     * @param {number} value - the value to set (must be -1, 0, or 1)
     */
    function setArchivedFilter(value) {
      if (value >= -1 && value <= 1) {
        viewModel.archivedFilter = value;
        memoryService.setItem('environments', 'archivedFilter', 'main', value);

        if (value === 1) {
          viewModel.archivedFilterKey = 'true';
        } else if (value === -1) {
          viewModel.archivedFilterKey = 'false';
        } else {
          viewModel.archivedFilterKey = '';
        }
      }
    }

    function goToUsageStatistics() {
      $state.go('usageStatistics');
    }

    function showConfirmationDialog(environment) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/systemValidation/divJobs/environments/archiveEnvironment/archiveConfirmationModal.html',
        controller: 'ArchiveConfirmationController as archiveConfirmationCtrl',
        backdrop: 'static',
        resolve: {
          environmentId: function () {
            return environment.environmentId;
          },
          environmentName: function () {
            return environment.name;
          },
          currentArchiveStatus: function () {
            return environment.archived;
          }
        }
      });
      modalInstance.result.then(function () {
        viewModel.loadingEnvironments = true;
        getEnvironments().then(function (environments) {
          viewModel.loadingEnvironments = false;
          viewModel.environments = environments;
          viewModel.displayedEnvironments = environments;
        });
      });
    }
  }
})();
