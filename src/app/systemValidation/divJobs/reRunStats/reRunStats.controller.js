(function () {

  'use strict';

  angular.module('systemValidation').controller('ReRunStatsController',
    ReRunStatsController);

  ReRunStatsController.$inject = ['$uibModalInstance', 'reRunOnly'];

  function ReRunStatsController($uibModalInstance, reRunOnly) {
    var viewModel = this;

    viewModel.reRunMessage = 'Would you like to rerun STATS for Completed student scenarios?';
    viewModel.reRunStatsOnly = reRunOnly;
    viewModel.resumeOnly = resumeOnly;
    viewModel.resumeAndReRunCompletedStats = resumeAndReRunCompletedStats;
    viewModel.close = close;

    function resumeOnly() {
      $uibModalInstance.close('resumeOnly');
    }

    function resumeAndReRunCompletedStats() {
      $uibModalInstance.close('reRun');
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }

})();
