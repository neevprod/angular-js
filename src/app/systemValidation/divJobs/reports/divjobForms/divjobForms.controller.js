(function () {

  'use strict';

  angular.module('systemValidation').controller('DivjobFormsController', divjobFormsController);

  divjobFormsController.$inject = ['$stateParams', 'environmentService', 'divjobFormsService', 'itemsPerPageOptions', 'memoryService'];

  function divjobFormsController($stateParams, environmentService, divjobFormsService, itemsPerPageOptions, memoryService) {
    var viewModel = this;
    viewModel.environmentId = $stateParams.environmentId;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('divJobForms', 'perPage', '') || viewModel.itemsPerPageOptions[0].value;
    viewModel.displayedDivJobForms = [];
    viewModel.divjobForms = [];
    activate();

    function activate() {
      getEnvironmentName(viewModel.environmentId);
      getDivjobForms(viewModel.environmentId);
    }

    function getEnvironmentName(environmentId) {
      environmentService.getEnvironment(environmentId).then(function (result) {
        if (result.success) {
          viewModel.environmentName = result.data.environment.name;
        }
      });
    }

    function getDivjobForms(environmentId) {
      divjobFormsService.getDivjobForms(environmentId).then(function (result) {
        if (result.success) {
          viewModel.divjobForms = angular.fromJson(result.data.divjobForms);
          viewModel.displayedDivJobForms = viewModel.divjobForms;
        }
      });
    }
  }
})();
