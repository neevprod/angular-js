(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('divjobFormsService', divjobFormsService);

  divjobFormsService.$inject = ['$http', 'systemValidationApiPrefix'];

  function divjobFormsService($http, systemValidationApiPrefix) {
    return {
      getDivjobForms: getDivjobForms
    };

    function getDivjobForms(environmentId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId +
        '/divjobForms')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
