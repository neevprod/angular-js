(function () {

  'use strict';

  angular.module('systemValidation').controller('UsageStatisticsController', usageStatisticsController);

  usageStatisticsController.$inject = ['$stateParams', 'environmentService', '$filter', '$state', 'testCodeSelectionService', 'usageStatisticsService', 'moment'];

  function usageStatisticsController($stateParams, environmentService, $filter, $state, testCodeSelectionService, usageStatisticsService, moment) {
    var viewModel = this;

    viewModel.environments = [];
    viewModel.getScopeTreePaths = getScopeTreePaths;
    viewModel.selectedScopeTreePath = 'ALL';
    viewModel.today = today;
    viewModel.clear = clear;
    viewModel.popupStartCalendar = {};
    viewModel.popupStartCalendar.opened = false;
    viewModel.openStartCalendar = openStartCalendar;
    viewModel.popupEndCalendar = {};
    viewModel.popupEndCalendar.opened = false;
    viewModel.openEndCalendar = openEndCalendar;
    viewModel.submit = submit;
    viewModel.showStats = false;
    viewModel.clearAlertMessage = clearAlertMessage;
    viewModel.maxDate = new Date();
    viewModel.format = 'MM/dd/yyyy';
    viewModel.scopeTreePaths = [];
    viewModel.overAllStatistics = [];
    viewModel.displayedOverAllStatistics = [];
    viewModel.failedTestAttemptsPerState = [];
    viewModel.displayedTestAttemptsPerState = [];
    viewModel.testAttemptsPerForm = [];
    viewModel.displayedTestAttemptsPerForm = [];
    viewModel.environmentName = 'test';
    activate();
    toggleMin();

    /**
     * Loads the list of Environments.
     */
    function activate() {
      viewModel.environmentId = $stateParams.environmentId;
      getEnvironments().then(function (environments) {
        viewModel.displayedEnvironments = environments;
        var envSelect = {
          environmentId: 0,
          name: 'ALL'
        };
        viewModel.displayedEnvironments.splice(0, 0, envSelect);

        if (viewModel.environmentId) {
          viewModel.environment = viewModel.environmentId;
          getScopeTreePaths();
        }
        else {
          viewModel.environment = 0;
          var treeSelect = 'ALL';
          viewModel.scopeTreePaths.splice(0, 0, treeSelect);
        }
        getEnvironmentName(viewModel.environmentId);

      });
    }

    /**
     * Retrieve the list of Environments.
     *
     * @returns The list of Environments.
     */
    function getEnvironments() {
      return usageStatisticsService.getEnvironments().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.environments);
        }
      });
    }

    function getEnvironmentName(environmentId) {
      for(var i = 0; i < viewModel.displayedEnvironments.length; i++) {
        if (viewModel.displayedEnvironments[i].environmentId === environmentId) {
          viewModel.environmentName = viewModel.displayedEnvironments[i].name;
          break;
        }
      }
    }


    /**
     * Loads the list of ScopeTreePath for selected environment.
     */
    function getScopeTreePaths() {
      if (viewModel.environment === 0) {
        viewModel.scopeTreePaths = [];
        var treeSelect = 'ALL';
        viewModel.scopeTreePaths.splice(0, 0, treeSelect);
      }
      else {
        usageStatisticsService
          .getScopeTreePaths(viewModel.environment)
          .then(
            function (result) {
              if (result.success) {
                viewModel.scopeTreePaths = result.data.scopeTreePaths;
                var treeSelect = 'ALL';
                viewModel.scopeTreePaths.splice(0, 0, treeSelect);
                if (!result.data.scopeTreePaths.length) {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = 'Can not find ScopeTreePath for the EnvironmentId: ' + $stateParams.environmentId;
                }
              } else {
                viewModel.alertSuccess = false;
                viewModel.alertMessage = result.errors[0].message;
              }
            });
      }

    }

    function today() {
      viewModel.startDate = new Date();
    }

    function clear() {
      viewModel.startDate = null;
    }

    function submit(isValid) {
      if (isValid) {
        clearAlertMessage();
        var fromdateForTime = new Date(viewModel.startDate);
        var todateForTime = new Date(viewModel.endDate);
        fromdateForTime.setHours(0);
        fromdateForTime.setMinutes(0);
        var fromDate = fromdateForTime;
        todateForTime.setHours(23);
        todateForTime.setMinutes(59);
        var toDate = todateForTime;
        if (fromDate && toDate) {
          if (Date.parse(fromDate) > Date.parse(toDate)) {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = 'End Date should be greater than Start Date.';
          }
        }
        var sdate = moment(fromDate).valueOf().toString();
        var edate = moment(toDate).valueOf().toString();
        if (!viewModel.alertMessage) {
          viewModel.loading = true;
          viewModel.showStats = true;
          usageStatisticsService
            .getTestAttempts(viewModel.environment, viewModel.selectedScopeTreePath, sdate, edate)
            .then(
              function (result) {
                viewModel.loading = false;
                if (result.success) {
                  viewModel.overAllStatistics = result.data.finalMap.overAllStatistics;
                  viewModel.displayedOverAllStatistics = result.data.finalMap.overAllStatistics;
                  viewModel.failedTestAttemptsPerState = result.data.finalMap.failedTestAttemptsPerState;
                  viewModel.displayedTestAttemptsPerState = result.data.finalMap.failedTestAttemptsPerState;
                  viewModel.testAttemptsPerForm = result.data.finalMap.testAttemptsPerForm;
                  viewModel.displayedTestAttemptsPerForm = result.data.finalMap.testAttemptsPerForm;
                }
              });
        }
      }
    }

    function openStartCalendar() {
      viewModel.popupStartCalendar.opened = true;
    }

    function openEndCalendar() {
      viewModel.popupEndCalendar.opened = true;
    }

    function clearAlertMessage() {
      viewModel.alertMessage = '';
      viewModel.overAllStatistics = '';
      viewModel.failedTestAttemptsPerState = '';
      viewModel.testAttemptsPerForm = '';
    }

    function toggleMin() {
      viewModel.minDate = viewModel.minDate ? null : new Date();
    }
  }
})();
