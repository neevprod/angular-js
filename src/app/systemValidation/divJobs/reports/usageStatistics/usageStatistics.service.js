(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('usageStatisticsService', usageStatisticsService);

  usageStatisticsService.$inject = ['$http', 'systemValidationApiPrefix'];

  function usageStatisticsService($http, systemValidationApiPrefix) {
    return {
      getTestAttempts: getTestAttempts,
      getEnvironments: getEnvironments,
      getScopeTreePaths: getScopeTreePaths,
      getEnvironment: getEnvironment
    };


    function getTestAttempts(environmentId, scopeTreePath, startDate, endDate) {
      if (scopeTreePath === 'ALL') {
        return $http.get(systemValidationApiPrefix + 'environments/' + environmentId +
          '/scopeTreePaths/' + scopeTreePath + '/startTime/' + startDate + '/endTime/' + endDate)
          .then(function (result) {
            return result.data;
          })
          .catch(function (result) {
            return result.data;
          });
      }

      else {
        return $http.get(systemValidationApiPrefix + 'environments/' + environmentId +
          '/scopeTreePaths/' + encodeURIComponent(scopeTreePath.replace(/\//g, '|')) + '/startTime/' + startDate + '/endTime/' + endDate)
          .then(function (result) {
            return result.data;
          })
          .catch(function (result) {
            return result.data;
          });
      }

    }

    /**
     * Get a list of Environments from the server.
     *
     * @returns The result from the server containing the list of
     *          Environments.
     */
    function getEnvironments() {
      return $http.get(systemValidationApiPrefix + 'usageStatistics/environments')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get Environment from the server.
     *
     * @returns The result from the server containing the list of
     *          Environments.
     */
    function getEnvironment(environmentId) {
      return $http.get(systemValidationApiPrefix + 'usageStatistics/environments/' + environmentId)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of ScopeTreePath for the given environment from the
     * server.
     *
     * @returns The result from the server containing the list of
     *          ScopeTreePath.
     */
    function getScopeTreePaths(environmentId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/scopeTreePath')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
