(function () {

  'use strict';

  angular.module('systemValidation')
    .controller('ScenarioController', ScenarioController);

  ScenarioController.$inject = ['$uibModalInstance', 'scenarioService', 'environmentId', 'divJobExecId',
    'studentScenarioId', 'isEpenScenario'];

  function ScenarioController($uibModalInstance, scenarioService, environmentId, divJobExecId, studentScenarioId,
                              isEpenScenario) {
    var viewModel = this;

    viewModel.title = isEpenScenario ? 'ePEN Scenario' : 'TestNav Scenario';
    viewModel.loadingJsonDetails = true;
    viewModel.confirmationTooltipEnabled = false;
    viewModel.close = close;
    viewModel.copySuccess = copySuccess;
    viewModel.copyError = copyError;
    viewModel.disableConfirmationTooltip = disableConfirmationTooltip;

    activate();

    /**
     * Load the scenario.
     */
    function activate() {
      if (isEpenScenario) {
        scenarioService.getEpenScenario(environmentId, divJobExecId, studentScenarioId)
          .then(processScenarioResult);
      } else {
        scenarioService.getTestNavScenario(environmentId, divJobExecId, studentScenarioId)
          .then(processScenarioResult);
      }
    }

    /**
     * Get the JSON from the result, format it, and set it to the viewmodel.
     *
     * @param result - The scenario result from the server.
     */
    function processScenarioResult(result) {
      viewModel.loadingJsonDetails = false;
      if (result.success) {
        viewModel.json = JSON.stringify(result.data.scenario.scenarioContent, null, 2);
      }
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }

    /**
     * Show the tooltip to indicate copy success.
     */
    function copySuccess() {
      viewModel.confirmationTooltipText = 'Copy succeeded.';
      viewModel.confirmationTooltipEnabled = true;
    }

    /**
     * Show the tooltip to indicate copy error.
     */
    function copyError() {
      viewModel.confirmationTooltipText = 'Copy failed.';
      viewModel.confirmationTooltipEnabled = true;
    }

    /**
     * Disable the copy confirmation tooltip.
     */
    function disableConfirmationTooltip() {
      viewModel.confirmationTooltipEnabled = false;
    }

  }

})();
