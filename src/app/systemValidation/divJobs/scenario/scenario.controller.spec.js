'use strict';

describe('scenario controller', function () {
  var controller;
  var scenarioService;
  var deferredScenario;
  var $uibModalInstance;
  var $rootScope;

  var environmentId;
  var divJobExecId;
  var studentScenarioId;
  var scenario;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($q, _$rootScope_, $controller) {
    deferredScenario = $q.defer();
    scenarioService = jasmine.createSpyObj('scenarioService', ['getTestNavScenario', 'getEpenScenario']);
    scenarioService.getTestNavScenario.and.callFake(function () {
      return deferredScenario.promise;
    });
    scenarioService.getEpenScenario.and.callFake(function () {
      return deferredScenario.promise;
    });

    $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
    $rootScope = _$rootScope_;

    environmentId = 2;
    divJobExecId = 4;
    studentScenarioId = 9;

    controller = $controller('ScenarioController', {
      $uibModalInstance: $uibModalInstance,
      scenarioService: scenarioService, environmentId: environmentId, divJobExecId: divJobExecId,
      studentScenarioId: studentScenarioId, isEpenScenario: false
    });
  }));

  it('should disable the confirmation tooltip on start', function () {
    // Then
    expect(controller.confirmationTooltipEnabled).toBe(false);
  });

  describe('for TestNav scenario', function () {
    beforeEach(inject(function ($controller) {
      controller = $controller('ScenarioController', {
        $uibModalInstance: $uibModalInstance,
        scenarioService: scenarioService, environmentId: environmentId, divJobExecId: divJobExecId,
        studentScenarioId: studentScenarioId, isEpenScenario: false
      });

      scenario = {
        scenarioId: 1,
        scenarioContent: {
          thing1: 'info1',
          thing2: 'info2'
        }
      };
    }));

    it('should set the correct title', function () {
      // Then
      expect(controller.title).toBe('TestNav Scenario');
    });

    it('should call scenarioService to get the TestNav scenario information during initialization', function () {
      // Then
      expect(scenarioService.getTestNavScenario).toHaveBeenCalled();
    });

    it('should set the scenario information after scenarioService returns', function () {
      // When
      deferredScenario.resolve({success: true, data: {scenario: scenario}});
      $rootScope.$digest();

      // Then
      expect(controller.json).toEqual(JSON.stringify(scenario.scenarioContent, null, 2));
    });
  });

  describe('for ePEN scenario', function () {
    beforeEach(inject(function ($controller) {
      controller = $controller('ScenarioController', {
        $uibModalInstance: $uibModalInstance,
        scenarioService: scenarioService, environmentId: environmentId, divJobExecId: divJobExecId,
        studentScenarioId: studentScenarioId, isEpenScenario: true
      });
    }));

    it('should set the correct title', function () {
      // Then
      expect(controller.title).toBe('ePEN Scenario');
    });

    it('should call scenarioService to get the ePEN scenario information during initialization', function () {
      // Then
      expect(scenarioService.getEpenScenario).toHaveBeenCalled();
    });

    it('should set the scenario information after scenarioService returns', function () {
      // When
      deferredScenario.resolve({success: true, data: {scenario: scenario}});
      $rootScope.$digest();

      // Then
      expect(controller.json).toEqual(JSON.stringify(scenario.scenarioContent, null, 2));
    });
  });

  describe('close', function () {
    it('should dismiss the modal', function () {
      // When
      controller.close();

      // Then
      expect($uibModalInstance.dismiss).toHaveBeenCalledWith('cancel');
    });
  });

  describe('copySuccess', function () {
    it('should set the confirmation tooltip to display the correct text', function () {
      // When
      controller.copySuccess();

      // Then
      expect(controller.confirmationTooltipText).toBe('Copy succeeded.');
    });

    it('should enable the confirmation tooltip', function () {
      // When
      controller.copySuccess();

      // Then
      expect(controller.confirmationTooltipEnabled).toBe(true);
    });
  });

  describe('copyError', function () {
    it('should set the confirmation tooltip to display the correct text', function () {
      // When
      controller.copyError();

      // Then
      expect(controller.confirmationTooltipText).toBe('Copy failed.');
    });

    it('should enable the confirmation tooltip', function () {
      // When
      controller.copyError();

      // Then
      expect(controller.confirmationTooltipEnabled).toBe(true);
    });
  });

  describe('disableConfirmationTooltip', function () {
    it('should disable the confirmation tooltip', function () {
      // Given
      controller.confirmationTooltipEnabled = true;

      // When
      controller.disableConfirmationTooltip();

      // Then
      expect(controller.confirmationTooltipEnabled).toBe(false);
    });
  });
});
