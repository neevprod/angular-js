(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('scenarioService', scenarioService);

  scenarioService.$inject = ['$http', 'systemValidationApiPrefix'];

  function scenarioService($http, systemValidationApiPrefix) {
    return {
      getTestNavScenario: getTestNavScenario,
      getEpenScenario: getEpenScenario
    };

    /**
     * Get the TestNav scenario from the server for the given student scenario.
     *
     * @param {number} environmentId - The environment ID.
     * @param {number} divJobExecId - The DIV job execution ID.
     * @param {number} studentScenarioId - The student scenario ID.
     * @returns {*} The TestNav scenario.
     */
    function getTestNavScenario(environmentId, divJobExecId, studentScenarioId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/testNavScenario')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the ePEN scenario from the server for the given student scenario.
     *
     * @param {number} environmentId - The environment ID.
     * @param {number} divJobExecId - The DIV job execution ID.
     * @param {number} studentScenarioId - The student scenario ID.
     * @returns {*} The ePEN scenario.
     */
    function getEpenScenario(environmentId, divJobExecId, studentScenarioId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/epenScenario')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
