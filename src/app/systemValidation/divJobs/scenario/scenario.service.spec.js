'use strict';

describe('scenario service', function () {
  var scenarioService;
  var $httpBackend;
  var systemValidationApiPrefix;

  var environmentId;
  var divJobExecId;
  var studentScenarioId;
  var scenarioId;
  var data;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_scenarioService_, _$httpBackend_, _systemValidationApiPrefix_) {
    scenarioService = _scenarioService_;
    $httpBackend = _$httpBackend_;
    systemValidationApiPrefix = _systemValidationApiPrefix_;
  }));

  beforeEach(function () {
    environmentId = 2;
    divJobExecId = 5;
    studentScenarioId = 4;
    scenarioId = 3;
    data = {scenarioId: scenarioId};
  });

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getTestNavScenario', function () {
    it('should submit the validated items GET request properly', function () {
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/testNavScenario')
        .respond(200, {success: true, data: data});

      scenarioService.getTestNavScenario(environmentId, divJobExecId, studentScenarioId);

      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/testNavScenario')
        .respond(200, {success: true, data: data});
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      scenarioService.getTestNavScenario(environmentId, divJobExecId, studentScenarioId)
        .then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      $httpBackend.expectGET(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/testNavScenario')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      scenarioService.getTestNavScenario(environmentId, divJobExecId, studentScenarioId)
        .then(handler);

      $httpBackend.flush();
      expect(handler).toHaveBeenCalled();
    });
  });

});
