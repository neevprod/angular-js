(function () {

  'use strict';

  angular.module('systemValidation').controller('ViewStatsFailureController',
    ViewStatsFailureController);

  ViewStatsFailureController.$inject = ['$uibModalInstance',
    'selectedDivJobIds', 'viewStatsFailureService',
    'itemsPerPageOptions'];

  function ViewStatsFailureController($uibModalInstance, selectedDivJobIds,
                                      viewStatsFailureService, itemsPerPageOptions) {
    var viewModel = this;
    viewModel.title = 'Stats Report';
    viewModel.close = close;
    viewModel.statsFailureReport = [];
    viewModel.displayStatsFailureReport = [];
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = viewModel.itemsPerPageOptions[0].value;
    activate();

    /**
     * Load the stats report.
     */
    function activate() {
      return viewStatsFailureService
        .getStatsReport(selectedDivJobIds)
        .then(
          function (result) {
            if (result.success) {
              viewModel.statsFailureReport = result.data.failedStats;
              viewModel.displayStatsFailureReport = result.data.failedStats;
            }
            else {
              viewModel.alertMessage = result.errors[0].message;
            }
          });
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();
