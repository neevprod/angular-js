(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('viewStatsFailureService', viewStatsFailureService);

  viewStatsFailureService.$inject = ['$http', 'systemValidationApiPrefix'];

  function viewStatsFailureService($http, systemValidationApiPrefix) {
    return {
      getStatsReport: getStatsReport
    };

    /**
     * Get Stats Report from the server for the selected div job ids.
     *
     * @param {number} selectedDivJobIds - The student scenario ID.
     * @returns {*} Stats Report.
     */
    function getStatsReport(selectedDivJobIds) {

      var config = {
        method: 'GET',
        url: systemValidationApiPrefix + 'StatsFailure',
        params: {
          selectedDivJobIds: selectedDivJobIds
        }
      };

      return $http(config)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
