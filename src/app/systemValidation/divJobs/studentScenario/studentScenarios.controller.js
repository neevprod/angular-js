(function () {

  'use strict';

  angular.module('systemValidation').controller('StudentScenariosController',
    StudentScenariosController);

  StudentScenariosController.$inject = ['$stateParams', 'environmentService', 'studentScenarioService', 'permissionsService', 'memoryService',
    'itemsPerPageOptions', 'dateTimeFormat', '$uibModal', 'divJobService', '$timeout'];

  function StudentScenariosController($stateParams, environmentService, studentScenarioService, permissionsService, memoryService, itemsPerPageOptions,
                                      dateTimeFormat, $uibModal, divJobService, $timeout) {
    var viewModel = this;
    viewModel.environmentId = $stateParams.environmentId;
    viewModel.divJobExecId = $stateParams.divJobExecId;
    viewModel.batteryStudentScenarioId = $stateParams.batteryStudentScenarioId;
    viewModel.studentScenarios = [];
    viewModel.loadingDivJobs = true;
    viewModel.divJobsInitialized = false;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('studentScenarios', 'perPage', viewModel.divJobExecId) ||
      viewModel.itemsPerPageOptions[0].value;
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.showScenario = showScenario;
    viewModel.resumeDivJob = resumeDivJob;
    viewModel.allowDivJobResume = permissionsService.hasEnvironmentLevelPermission('divJob', 'createAccess',
      'systemValidation', viewModel.environmentId);
    viewModel.needToReRunStats = false;
    var needToReRunAndResume = true;
    viewModel.setStatusFilter = setStatusFilter;
    viewModel.statusFilterMenus = ['No Filter', 'Not Started', 'In Progress', 'Completed', 'Failed'];
    viewModel.selectedFilter = [viewModel.statusFilterMenus[0]];
    activate();

    /**
     * Loads the list of StudentScenarios.
     */
    function activate() {
      getEnvironmentName(viewModel.environmentId);
      getDivJob();
      if (viewModel.batteryStudentScenarioId) {
        getBatteryUnits();
      } else {
        getStudentScenarios();
      }
    }

    function getEnvironmentName(environmentId) {
      environmentService.getEnvironment(environmentId).then(function (result) {
        if (result.success) {
          viewModel.environmentName = result.data.environment.name;
        }
      });
    }

    function getDivJob() {
      divJobService.getDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (result.success) {
          var divJob = result.data.divJob;
          viewModel.divJobResumable = divJob.numInProgress === 0;
          if (!divJob.hasStatsState && divJob.divJobExecStatus.status.indexOf('Completed') === 0) {
            viewModel.divJobResumable = false;
          }
          if (divJob.divJobExecStatus.status === 'Failed' && divJob.numFailed !== divJob.numStudentScenarios) {
            enableStatsReRun(divJob);
            needToReRunAndResume = false;
          } else if (divJob.divJobExecStatus.status === 'Completed') {
            enableStatsReRun(divJob);
            needToReRunAndResume = true;
          }
        }
      });
    }

    function enableStatsReRun(divJob) {
      if (divJob.hasStatsState) {
        viewModel.needToReRunStats = true;
      }
    }

    function getBatteryUnits() {
      studentScenarioService
        .getBatteryUnits(viewModel.environmentId, viewModel.divJobExecId, viewModel.batteryStudentScenarioId)
        .then(
          function (result) {
            viewModel.loadingDivJobs = false;
            viewModel.divJobsInitialized = true;
            if (result.success) {
              viewModel.studentScenarios = [];
              angular.forEach(result.data.studentScenarios, function (res) {
                res.currentState = getCurrentState(res);
                viewModel.studentScenarios.push(res);
              });
              viewModel.displayedStudentScenarios = result.data.studentScenarios;
            }
          });
    }

    function getStudentScenarios() {
      studentScenarioService
        .getStudentScenarios(viewModel.environmentId, viewModel.divJobExecId)
        .then(
          function (result) {
            viewModel.loadingDivJobs = false;
            viewModel.divJobsInitialized = true;
            if (result.success) {
              viewModel.studentScenarios = [];

              angular.forEach(result.data.studentScenarios, function (res) {
                res.currentState = getCurrentState(res);
                viewModel.studentScenarios.push(res);
              });

              viewModel.displayedStudentScenarios = result.data.studentScenarios;
            }
          });
    }

    /**
     * Gets the currentState of the studentScenario based on the studentScenarioStatus.
     */
    function getCurrentState(studentScenario) {
      var orderedPathStates = getOrderedPathStates(studentScenario.divJobExec.path.pathStates);
      if (studentScenario.studentScenarioStatus.status === 'Not Started') {
        return orderedPathStates[0].state.stateName;
      } else if (studentScenario.studentScenarioStatus.status === 'Failed') {
        if (!studentScenario.lastCompletedStateId) {
          return orderedPathStates[0].state.stateName;
        } else {
          return getNextState(studentScenario.lastCompletedStateId, !!studentScenario.epenScenario,
            orderedPathStates).stateName;
        }
      } else if (studentScenario.studentScenarioStatus.status === 'In Progress') {
        return getNextState(studentScenario.lastCompletedStateId, !!studentScenario.epenScenario,
          orderedPathStates).stateName;
      } else if (studentScenario.studentScenarioStatus.status.indexOf('Completed') === 0) {
        return orderedPathStates[orderedPathStates.length - 1].state.stateName;
      }
    }

    /**
     * Based on the given list of pathStates, get a list of the same pathStates sorted according to their order.
     *
     * @param pathStates - The array of pathStates to sort.
     * @returns An ordered array of pathStates.
     */
    function getOrderedPathStates(pathStates) {
      return angular.copy(pathStates).sort(function (pathState1, pathState2) {
        return pathState1.pathStateOrder - pathState2.pathStateOrder;
      });
    }

    /**
     * Get the next state following the state with the given ID.
     *
     * @param currentStateId - The ID of the current state.
     * @param hasEpenScenario - Whether the student scenario has an ePEN scenario.
     * @param orderedPathStates - An array of the pathStates in order.
     * @returns The next state.
     */
    function getNextState(currentStateId, hasEpenScenario, orderedPathStates) {
      if (!currentStateId) {
        return orderedPathStates[0].state;
      }
      var currentPathStateId;
      for (var i = 0; i < orderedPathStates.length; i++) {
        if (orderedPathStates[i].state.stateId === currentStateId) {
          currentPathStateId = i;
        }
      }

      if (!hasEpenScenario) {
        while (currentPathStateId < orderedPathStates.length - 1 &&
        (orderedPathStates[currentPathStateId + 1].state.stateName === 'BE_EPEN_PATH_VALIDATOR' ||
          orderedPathStates[currentPathStateId + 1].state.stateName === 'EPEN2_AUTOMATION')) {
          currentPathStateId++;
        }
      }

      return orderedPathStates[Math.min(orderedPathStates.length - 1, currentPathStateId + 1)].state;
    }

    /**
     * Show the information for the given scenario in a modal.
     *
     * @param {number} environmentId - The environment ID.
     * @param {number} divJobExecId - The div job exec ID.
     * @param {number} studentScenarioId - The student scenario ID.
     * @param {boolean} isEpenScenario - Whether the scenario is an ePEN scenario.
     */
    function showScenario(environmentId, divJobExecId, studentScenarioId, isEpenScenario) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'ScenarioController as jsonDetailsCtrl',
        resolve: {
          environmentId: function () {
            return environmentId;
          },
          divJobExecId: function () {
            return divJobExecId;
          },
          studentScenarioId: function () {
            return studentScenarioId;
          },
          isEpenScenario: function () {
            return isEpenScenario;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Resume the current DIV job.
     *
     */
    function resumeDivJob() {
      viewModel.divJobResumable = false;
      if (viewModel.needToReRunStats) {
        var modalInstance = $uibModal.open({
          templateUrl: 'app/systemValidation/divJobs/reRunStats/confirmationModal.html',
          controller: 'ReRunStatsController as reRunStatsCtrl',
          backdrop: 'static',
          resolve: {
            reRunOnly: function () {
              return needToReRunAndResume;
            }
          }
        });
        modalInstance.result.then(function (action) {
            if (action === 'resumeOnly') {
              resumeFailedStudentScenarios();
            } else if (action === 'reRun') {
              reExecuteStatsForCompletedStudentScenarios();
            } else {
              viewModel.divJobResumable = true;
            }
          }
        );
      } else {
        resumeFailedStudentScenarios();
      }
    }

    function resumeFailedStudentScenarios() {
      divJobService.startDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (!result.success) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'The Data Integrity Validation job could not be resumed.';
        } else {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'The Data Integrity Validation job was successfully resumed.';
          $timeout(function () {
            if (viewModel.batteryStudentScenarioId) {
              getBatteryUnits();
            } else {
              getStudentScenarios();
            }
          }, 2000);
        }
      });
    }

    function reExecuteStatsForCompletedStudentScenarios() {
      divJobService.reExecuteStatsForCompletedDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (!result.success) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'The Data Integrity Validation job could not be resumed.';
        } else {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'The Data Integrity Validation job was successfully resumed.';
          $timeout(function () {
            if (viewModel.batteryStudentScenarioId) {
              getBatteryUnits();
            } else {
              getStudentScenarios();
            }
          }, 2000);
        }
      });
    }

    //To store original list of studentScenarios for No filter.
    var tempStudentScenarios = [];

    function setStatusFilter(menu) {
      if (menu === 'No Filter' && viewModel.selectedFilter.length === 0) {
        viewModel.selectedFilter.push(menu);
        return;
      }
      if (tempStudentScenarios.length === 0) {
        tempStudentScenarios = viewModel.studentScenarios;
      } else {
        viewModel.studentScenarios = tempStudentScenarios;
      }
      if (menu !== 'No Filter') {
        var noFilterIndex = viewModel.selectedFilter.indexOf('No Filter');
        if (noFilterIndex !== -1) {
          viewModel.selectedFilter.splice(noFilterIndex, 1);
        }
        var index = viewModel.selectedFilter.indexOf(menu);
        if (index !== -1) {
          viewModel.selectedFilter.splice(index, 1);
        } else {
          viewModel.selectedFilter.push(menu);
        }
        viewModel.displayedStudentScenarios = [];
        viewModel.studentScenarios = [];
        if (viewModel.selectedFilter.length > 0) {
          viewModel.statusFilter = true;
          angular.forEach(tempStudentScenarios, function (studentScenario) {
            var index = viewModel.selectedFilter.indexOf(studentScenario.studentScenarioStatus.status);
            if (index !== -1) {
              viewModel.studentScenarios.push(studentScenario);
            }
          });
        } else {
          viewModel.statusFilter = false;
          viewModel.studentScenarios = tempStudentScenarios;
          viewModel.selectedFilter = [viewModel.statusFilterMenus[0]];
        }
      } else {
        viewModel.selectedFilter = [];
        viewModel.selectedFilter.push(menu);
        viewModel.studentScenarios = tempStudentScenarios;
        tempStudentScenarios = [];
        viewModel.statusFilter = false;
      }
      viewModel.displayedStudentScenarios = viewModel.studentScenarios;
    }
  }
})();
