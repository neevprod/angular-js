'use strict';

describe('Student scenarios controller', function () {
  var controller;
  var $stateParams;
  var environmentService;
  var studentScenarioService;
  var permissionsService;
  var memoryService;
  var divJobService;
  var deferredEnvironment;
  var deferredStudentScenarios;
  var deferredDivJob;
  var itemsPerPageOptions;
  var $rootScope;
  var path;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();

    path = {
      pathStates: [
        {
          pathStateId: 1,
          pathStateOrder: 1,
          state: {
            stateId: 1,
            stateName: 'TN8_API_DATA_LOADER'
          }
        },
        {
          pathStateId: 2,
          pathStateOrder: 2,
          state: {
            stateId: 2,
            stateName: 'IRIS_PROCESSING_VALIDATOR'
          }
        },
        {
          pathStateId: 3,
          pathStateOrder: 5,
          state: {
            stateId: 3,
            stateName: 'BE_EPEN_PATH_VALIDATOR'
          }
        },
        {
          pathStateId: 4,
          pathStateOrder: 4,
          state: {
            stateId: 4,
            stateName: 'EPEN2_AUTOMATION'
          }
        },
        {
          pathStateId: 5,
          pathStateOrder: 3,
          state: {
            stateId: 5,
            stateName: 'BE_PRIMARY_PATH_VALIDATOR'
          }
        },
        {
          pathStateId: 6,
          pathStateOrder: 6,
          state: {
            stateId: 6,
            stateName: 'STATS'
          }
        }
      ]
    };
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredEnvironment = $q.defer();
    deferredStudentScenarios = $q.defer();
    deferredDivJob = $q.defer();

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasEnvironmentLevelPermission']);
    permissionsService.hasEnvironmentLevelPermission.and.callFake(function () {
      return true;
    });

    environmentService = jasmine.createSpyObj('environmentService', ['getEnvironment']);
    environmentService.getEnvironment.and.callFake(function () {
      return deferredEnvironment.promise;
    });

    studentScenarioService = jasmine.createSpyObj('studentScenarioService',['getStudentScenarios']);
    studentScenarioService.getStudentScenarios.and.callFake(function () {
      return deferredStudentScenarios.promise;
    });
    divJobService=jasmine.createSpyObj('divJobService',['getDivJob']);
    divJobService.getDivJob.and.callFake(function () {
      return deferredDivJob.promise;
    });

    memoryService = jasmine.createSpyObj('memoryService', ['getItem']);

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('StudentScenariosController', {
      $stateParams: $stateParams,
      environmentService: environmentService,
      studentScenarioService: studentScenarioService,
      permissionsService: permissionsService,
      memoryService: memoryService,
      itemsPerPageOptions: itemsPerPageOptions,
      divJobService: divJobService
    });
  }));

  it('should set the correct current state when the job is complete and there is an ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'Completed'
        },
        lastCompletedStateId: 6,
        epenScenario: {
          scenarioId: 1
        },
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('STATS');
  });

  it('should set the correct current state when the job is complete and there is no ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'Completed'
        },
        lastCompletedStateId: 6,
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('STATS');
  });

  it('should set the correct current state when the job is in progress and there is an ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'In Progress'
        },
        lastCompletedStateId: 5,
        epenScenario: {
          scenarioId: 1
        },
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('EPEN2_AUTOMATION');
  });

  it('should set the correct current state when the job is in progress and there is no ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'In Progress'
        },
        lastCompletedStateId: 5,
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('STATS');
  });

  it('should set the correct current state when the job is failed and there is an ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'Failed'
        },
        lastCompletedStateId: 4,
        epenScenario: {
          scenarioId: 1
        },
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('BE_EPEN_PATH_VALIDATOR');
  });

  it('should set the correct current state when the job failed and there is no ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'Failed'
        },
        lastCompletedStateId: 5,
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('STATS');
  });

  it('should set the correct current state when the job has not yet started and there is an ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'Not Started'
        },
        epenScenario: {
          scenarioId: 1
        },
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('TN8_API_DATA_LOADER');
  });

  it('should set the correct current state when the job has not yet started and there is no ePEN scenario', function () {
    // Given
    var studentScenarios = [
      {
        studentScenarioStatus: {
          status: 'Not Started'
        },
        divJobExec: {
          path: path
        }
      }
    ];

    // When
    deferredStudentScenarios.resolve({
      success: true,
      data: {
        studentScenarios: studentScenarios
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.displayedStudentScenarios[0].currentState).toBe('TN8_API_DATA_LOADER');
  });
});
