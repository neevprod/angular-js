(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('studentScenarioService', studentScenarioService);

  studentScenarioService.$inject = ['$http', 'systemValidationApiPrefix'];

  function studentScenarioService($http, systemValidationApiPrefix) {
    return {
      getStudentScenarios: getStudentScenarios,
      getBatteryUnits: getBatteryUnits
    };

    function getStudentScenarios(environmentId, divJobExecId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getBatteryUnits(environmentId, divJobExecId, batteryStudentScenarioId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/batteryStudentScenarios/' + batteryStudentScenarioId + '/batteryUnits')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
