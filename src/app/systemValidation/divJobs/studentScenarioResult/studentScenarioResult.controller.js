(function () {

  'use strict';

  angular.module('systemValidation').controller(
    'StudentScenarioResultController', StudentScenarioResultController);

  StudentScenarioResultController.$inject = ['$stateParams', 'environmentService', 'studentScenarioResultService',
    'permissionsService', 'itemsPerPageOptions', 'dateTimeFormat', '$uibModal', 'divJobService', '$timeout'];

  function StudentScenarioResultController($stateParams, environmentService, studentScenarioResultService,
                                           permissionsService, itemsPerPageOptions, dateTimeFormat,
                                           $uibModal, divJobService, $timeout) {
    var viewModel = this;
    viewModel.loadingResult = true;
    viewModel.scenarioResultInitialized = false;
    viewModel.needToReRunStats = false;
    var needToReRunAndResume = true;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = viewModel.itemsPerPageOptions[0];
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.showCommentDetailModal = showCommentDetailModal;

    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    viewModel.environmentId = $stateParams.environmentId;
    viewModel.studentScenarioId = $stateParams.studentScenarioId;
    viewModel.divJobExecId = $stateParams.divJobExecId;
    viewModel.batteryStudentScenarioId = $stateParams.batteryStudentScenarioId;
    viewModel.showDetails = showDetails;
    viewModel.showScenario = showScenario;
    viewModel.resumeDivJob = resumeDivJob;
    viewModel.allowDivJobResume = permissionsService.hasEnvironmentLevelPermission('divJob', 'createAccess',
      'systemValidation', viewModel.environmentId);

    activate();

    function activate() {
      getEnvironmentName(viewModel.environmentId);
      getDivJob();
      getStudentScenarioResult();
    }

    function getDivJob() {
      divJobService.getDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (result.success) {
          var divJob = result.data.divJob;
          viewModel.divJobResumable = divJob.numInProgress === 0;
          if (!divJob.hasStatsState && divJob.divJobExecStatus.status.indexOf('Completed') === 0) {
            viewModel.divJobResumable = false;
          }
          if (divJob.divJobExecStatus.status === 'Failed' && divJob.numFailed !== divJob.numStudentScenarios) {
            enableStatsReRun(divJob);
            needToReRunAndResume = false;
          } else if (divJob.divJobExecStatus.status === 'Completed') {
            enableStatsReRun(divJob);
            needToReRunAndResume = true;
          }
        }
      });
    }

    function enableStatsReRun(divJob) {
      if (divJob.hasStatsState) {
        viewModel.needToReRunStats = true;
      }
    }

    function getStudentScenarioResult() {
      studentScenarioResultService
        .getStudentScenarioResult(viewModel.environmentId,
          viewModel.divJobExecId, viewModel.studentScenarioId).then(function (result) {
        viewModel.loadingResult = false;
        viewModel.scenarioResultInitialized = true;
        if (result.success) {
          viewModel.studentScenarioResult = [];
          var inProgressStateFound = false;
          angular.forEach(result.data.studentScenarioResults, function (res) {
            res.lastStatus = getLastStatus(res, inProgressStateFound);
            if (res.lastStatus === 'In Progress') {
              inProgressStateFound = true;
            }

            if (!res.studentScenarioResultComments) {
              res.studentScenarioResultComments = [];
            }
            viewModel.studentScenarioResult.push(res);
          });
        }
      });
    }

    /**
     * Returns a string indicating whether the status passed or failed.
     *
     * @param ssr
     *            The StudentScenarioResult.
     * @param inProgressStateFound
     *            Indicates whether a previous state was found to be in progress.
     * @returns {string} A string indicating the status of the state.
     */
    function getLastStatus(ssr, inProgressStateFound) {
      if (ssr.success) {
        var result = '';
        if (ssr.state.stateName === 'STATS') {
          var temp = angular.fromJson(ssr.resultDetails).success;
          var suffix = temp.substring(4, temp.length);
          if (suffix) {
            result = ' (' + suffix + ')';
          }
        }
        return 'Completed' + result;
      } else if ((!ssr.success && !ssr.studentScenarioResultId)
        || ssr.studentScenario.studentScenarioStatusId === 2) {
        if (!ssr.studentScenario.epenScenario
          && (ssr.state.stateName === 'BE_EPEN_PATH_VALIDATOR' || ssr.state.stateName === 'EPEN2_AUTOMATION')) {
          return 'N/A';
        } else if (ssr.studentScenario.studentScenarioStatus.status === 'In Progress' && !inProgressStateFound) {
          return 'In Progress';
        } else {
          return 'Not Started';
        }
      } else {
        return 'Failed';
      }
    }


    /**
     * Show the given JSON details in a modal.
     *
     * @param {string} details - The JSON details to show.
     */
    function showDetails(details) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'JsonDetailsController as jsonDetailsCtrl',
        resolve: {
          title: function () {
            return 'Details';
          },
          json: function () {
            return details;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Show the specified comment details in a modal.
     *
     * @param {int} studentScenarioResultId - The student scenario result associated with the comment.
     * @param {Object[]} studentScenarioResultComments - The list of student scenario result comments associated with the student scenario result, if any.
     */
    function showCommentDetailModal(studentScenarioResultId,
                                    studentScenarioResultComments) {
      var modalInstance = $uibModal
        .open({
          templateUrl: 'app/systemValidation/divJobs/comment/commentDetail/commentDetail.html',
          controller: 'CommentDetailController as commentDtlCtrl',
          resolve: {
            studentScenarioResultId: function () {
              return studentScenarioResultId;
            },
            studentScenarioResultComment: function () {
              return null;
            }
          },
          size: 'lg'
        });
      modalInstance.result.then(function (savedComment) {
        if (!savedComment) {
          return;
        }
        studentScenarioResultComments.push(savedComment);
      });

    }

    function getEnvironmentName(environmentId) {
      environmentService
        .getEnvironment(environmentId)
        .then(
          function (result) {
            if (result.success) {
              viewModel.environmentName = result.data.environment.name;
            }
          });
    }

    /**
     * Show the information for the given scenario in a modal.
     *
     * @param {number} environmentId - The environment ID.
     * @param {number} divJobExecId - The div job exec ID.
     * @param {number} studentScenarioId - The student scenario ID.
     * @param {boolean} isEpenScenario - Whether the scenario is an ePEN scenario.
     */
    function showScenario(environmentId, divJobExecId, studentScenarioId,
                          isEpenScenario) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'ScenarioController as jsonDetailsCtrl',
        resolve: {
          environmentId: function () {
            return environmentId;
          },
          divJobExecId: function () {
            return divJobExecId;
          },
          studentScenarioId: function () {
            return studentScenarioId;
          },
          isEpenScenario: function () {
            return isEpenScenario;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Resume the current DIV job.
     *
     */
    function resumeDivJob() {
      viewModel.divJobResumable = false;
      if (viewModel.needToReRunStats) {
        var modalInstance = $uibModal.open({
          templateUrl: 'app/systemValidation/divJobs/reRunStats/confirmationModal.html',
          controller: 'ReRunStatsController as reRunStatsCtrl',
          backdrop: 'static',
          resolve: {
            reRunOnly: function () {
              return needToReRunAndResume;
            }
          }
        });
        modalInstance.result.then(function (action) {
            if (action === 'resumeOnly') {
              resumeFailedStudentScenarios();
            } else if (action === 'reRun') {
              reExecuteStatsForCompletedStudentScenarios();
            } else {
              viewModel.divJobResumable = true;
            }
          }
        );
      } else {
        resumeFailedStudentScenarios();
      }
    }

    function resumeFailedStudentScenarios() {
      divJobService.startDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (!result.success) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'The Data Integrity Validation job could not be resumed.';
        } else {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'The Data Integrity Validation job was successfully resumed.';
          $timeout(function () {
            getStudentScenarioResult();
          }, 2000);
        }
      });
    }

    function reExecuteStatsForCompletedStudentScenarios() {
      divJobService.reExecuteStatsForCompletedDivJob(viewModel.environmentId, viewModel.divJobExecId).then(function (result) {
        if (!result.success) {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = 'The Data Integrity Validation job could not be resumed.';
        } else {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = 'The Data Integrity Validation job was successfully resumed.';
          $timeout(function () {
            getStudentScenarioResult();
          }, 2000);
        }
      });
    }
  }
})();
