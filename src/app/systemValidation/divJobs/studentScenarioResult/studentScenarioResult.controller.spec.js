'use strict';

describe('Student scenario result controller', function () {
  var controller;
  var $stateParams;
  var environmentService;
  var permissionsService;
  var studentScenarioResultService;
  var divJobService;
  var deferredEnvironments;
  var deferredStudentScenarioResults;
  var deferredDivJob;
  var deferredOrderedPathStates;
  var deferredCurrentStateName;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('systemValidation'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredEnvironments = $q.defer();
    deferredStudentScenarioResults = $q.defer();
    deferredDivJob = $q.defer();
    deferredOrderedPathStates = $q.defer();
    deferredCurrentStateName = $q.defer();

    permissionsService = jasmine.createSpyObj('permissionsService', ['hasEnvironmentLevelPermission']);
    permissionsService.hasEnvironmentLevelPermission.and.callFake(function () {
      return true;
    });

    environmentService = jasmine.createSpyObj('environmentService', ['getEnvironment']);
    environmentService.getEnvironment.and.callFake(function () {
      return deferredEnvironments.promise;
    });

    studentScenarioResultService = jasmine.createSpyObj('studentScenarioResultService', ['getStudentScenarioResult']);
    studentScenarioResultService.getStudentScenarioResult.and.callFake(function () {
      return deferredStudentScenarioResults.promise;
    });
    divJobService = jasmine.createSpyObj('divJobService', ['getDivJob', 'getOrderedPathStates', 'getNextState']);
    divJobService.getDivJob.and.callFake(function () {
      return deferredDivJob.promise;
    });

    divJobService.getOrderedPathStates.and.callFake(function () {
      return deferredOrderedPathStates.promise;
    });
    divJobService.getNextState.and.callFake(function () {
      return deferredCurrentStateName.promise;
    });

    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('StudentScenarioResultController', {
      $stateParams: $stateParams,
      environmentService: environmentService,
      studentScenarioResultService: studentScenarioResultService,
      permissionsService: permissionsService,
      itemsPerPageOptions: itemsPerPageOptions,
      divJobService: divJobService
    });
  }));

  it('should set the correct statuses for each state when the job is complete and there is an ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 4,
        success: true,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 5,
        success: true,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 6,
        resultDetails: '{"success": "yes"}',
        success: true,
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Completed');
  });

  it('should set the correct statuses for each state when the job is complete and there is no ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: []
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 6,
        resultDetails: '{"success": "yes"}',
        success: true,
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 4,
          studentScenarioStatus: {status: 'Passed', studentScenarioStatusId: 4},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });

    deferredOrderedPathStates.resolve({
      success: true,
      data: {}
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Completed');
  });

  it('should set the correct statuses for each state when the job is in progress and there is an ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        resultDetails: '{"success": "no"}',
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('In Progress');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Not Started');
  });

  it('should set the correct statuses for each state when the job is in progress and there is no ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        resultDetails: '{"success": "no"}',
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('In Progress');
  });

  it('should set the correct statuses for each state when the job is in progress, has previously failed, and there ' +
    'is an ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 4,
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        resultDetails: '{"success": "no"}',
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('In Progress');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Not Started');
  });

  it('should set the correct statuses for each state when the job is in progress, has previously failed, and there ' +
    'is no ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 3,
        resultDetails: '{"success": "no"}',
        success: false,
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 2,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('In Progress');
  });

  it('should set the correct statuses for each state when the job is failed and there is an ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 4,
        success: true,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        studentScenarioResultId: 5,
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        resultDetails: '{"success": "no"}',
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('Failed');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Not Started');
  });

  it('should set the correct statuses for each state when the job failed and there is no ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        studentScenarioResultId: 1,
        success: true,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 2,
        success: true,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 3,
        success: true,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'In Progress', studentScenarioStatusId: 2},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        studentScenarioResultId: 3,
        resultDetails: '{"success": "no"}',
        success: false,
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 3,
          studentScenarioStatus: {status: 'Failed', studentScenarioStatusId: 3},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Completed');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Failed');
  });

  it('should set the correct statuses for each state when the job has not yet started and there is an ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        success: false,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      },
      {
        success: false,
        resultDetails: '{"success": "no"}',
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          },
          epenScenario: {
            scenarioId: 1
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Not Started');
  });

  it('should set the correct statuses for each state when the job has not yet started and there is no ePEN scenario', function () {
    // Given
    var studentScenarioResults = [
      {
        success: false,
        state: {
          stateName: 'TN8_API_DATA_LOADER'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'IRIS_PROCESSING_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_PRIMARY_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'EPEN2_AUTOMATION'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        state: {
          stateName: 'BE_EPEN_PATH_VALIDATOR'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      },
      {
        success: false,
        resultDetails: '{"success": "no"}',
        state: {
          stateName: 'STATS'
        },
        studentScenario: {
          studentScenarioStatusId: 1,
          studentScenarioStatus: {status: 'Not Started', studentScenarioStatusId: 1},
          divJobExec: {
            divJobExecId: 1,
            path: {
              pathId: 8,
              pathStates: [{pathStateId: 33, pathStateOrder: 1, state: {stateId: 1, stateName: 'TN8_API_DATA_LOADER'}}]
            }
          }
        }
      }
    ];

    // When
    deferredStudentScenarioResults.resolve({
      success: true,
      data: {
        studentScenarioResults: studentScenarioResults
      }
    });
    $rootScope.$digest();

    // Then
    expect(controller.studentScenarioResult[0].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[1].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[2].lastStatus).toBe('Not Started');
    expect(controller.studentScenarioResult[3].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[4].lastStatus).toBe('N/A');
    expect(controller.studentScenarioResult[5].lastStatus).toBe('Not Started');
  });
});
