(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('studentScenarioResultService', studentScenarioResultService);

  studentScenarioResultService.$inject = ['$http', 'systemValidationApiPrefix'];

  function studentScenarioResultService($http, systemValidationApiPrefix) {
    return {
      getStudentScenarioResult: getStudentScenarioResult
    };

    function getStudentScenarioResult(environmentId, divJobExecId, studentScenarioId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/studentScenarioResults')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
