(function () {

  'use strict';

  angular.module('systemValidation').controller(
    'StudentScenarioResultHistoryController',
    StudentScenarioResultHistoryController);

  StudentScenarioResultHistoryController.$inject = ['$stateParams',
    'studentScenarioResultHistoryService', 'itemsPerPageOptions',
    'dateTimeFormat', '$uibModal', 'environmentService'];

  function StudentScenarioResultHistoryController($stateParams,
                                                  studentScenarioResultHistoryService, itemsPerPageOptions,
                                                  dateTimeFormat, $uibModal, environmentService) {
    var viewModel = this;
    viewModel.loadingResult = true;
    viewModel.scenarioResultHistoryInitialized = false;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = viewModel.itemsPerPageOptions[0];
    viewModel.dateTimeFormat = dateTimeFormat;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    var divJobExecId = $stateParams.divJobExecId;
    var studentScenarioId = $stateParams.studentScenarioId;
    var stateId = $stateParams.stateId;
    viewModel.environmentId = $stateParams.environmentId;
    viewModel.studentScenarioId = studentScenarioId;
    viewModel.divJobExecId = divJobExecId;
    viewModel.stateId = stateId;
    viewModel.batteryStudentScenarioId = $stateParams.batteryStudentScenarioId;
    viewModel.getPassFail = getPassFail;
    viewModel.showDetails = showDetails;
    viewModel.showCommentDetailModal = showCommentDetailModal;

    activate();

    /**
     * Loads the list of studentScenarioStateHistory.
     */
    function activate() {
      getEnvironmentName(viewModel.environmentId);
      studentScenarioResultHistoryService
        .getStudentScenarioResultHistory(
          $stateParams.environmentId, viewModel.divJobExecId,
          studentScenarioId, stateId)
        .then(
          function (result) {
            viewModel.loadingResult = false;
            viewModel.scenarioResultHistoryInitialized = true;
            if (result.success) {
              viewModel.studentScenarioResultHistory = result.data.studentScenarioResults;
              viewModel.displayedstudentScenarioResultHistory = result.data.studentScenarioResults;
            }
          });
    }

    /**
     * Show the given JSON details in a modal.
     *
     * @param {string}
     *            details - The JSON details to show.
     */
    function showDetails(details) {
      $uibModal.open({
        templateUrl: 'app/core/jsonDetails/jsonDetails.html',
        controller: 'JsonDetailsController as jsonDetailsCtrl',
        resolve: {
          title: function () {
            return 'Details';
          },
          json: function () {
            return details;
          }
        },
        size: 'lg'
      });
    }

    /**
     * Returns a string indicating whether the status passed or failed.
     *
     * @param status
     *            The status of the State.
     * @returns {string} A string indicating whether the state is passed.
     */
    function getPassFail(status) {
      if (status) {
        return 'Pass';
      } else {
        return 'Fail';
      }
    }

    function showCommentDetailModal(studentScenarioResultId,
                                    studentScenarioResultComments) {
      var modalInstance = $uibModal
        .open({
          templateUrl: 'app/systemValidation/divJobs/comment/commentDetail/commentDetail.html',
          controller: 'CommentDetailController as commentDtlCtrl',
          resolve: {
            studentScenarioResultId: function () {
              return studentScenarioResultId;
            },
            studentScenarioResultComment: function () {
              return null;
            }
          },
          size: 'lg'
        });
      modalInstance.result.then(function (savedComment) {
        if (!savedComment) {
          return;
        }
        studentScenarioResultComments.push(savedComment);
      });
    }

    function getEnvironmentName(environmentId) {
      environmentService
        .getEnvironment(environmentId)
        .then(
          function (result) {
            if (result.success) {
              viewModel.environmentName = result.data.environment.name;
            }
          });
    }
  }
})();
