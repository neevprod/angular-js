(function () {

  'use strict';

  angular
    .module('systemValidation')
    .factory('studentScenarioResultHistoryService', studentScenarioResultHistoryService);

  studentScenarioResultHistoryService.$inject = ['$http', 'systemValidationApiPrefix'];

  function studentScenarioResultHistoryService($http, systemValidationApiPrefix) {
    return {
      getStudentScenarioResultHistory: getStudentScenarioResultHistory
    };

    function getStudentScenarioResultHistory(environmentId, divJobExecId, studentScenarioId, stateId) {
      return $http.get(systemValidationApiPrefix + 'environments/' + environmentId + '/divJobExecutions/' +
        divJobExecId + '/studentScenarios/' + studentScenarioId + '/states/' + stateId +
        '/studentScenarioResultHistory')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }

})();
