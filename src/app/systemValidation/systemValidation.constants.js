(function () {

  'use strict';

  angular.module('systemValidation')
    .constant('systemValidationApiPrefix',
      '/api/systemValidation/v1/');
})();
