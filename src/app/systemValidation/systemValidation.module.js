(function () {

  'use strict';

  angular.module('systemValidation', ['core']).config(configure);

  configure.$inject = ['$stateProvider'];

  function configure($stateProvider) {
    $stateProvider
      .state(
        'environments',
        {
          parent: 'main',
          url: '/systemValidation/environments',
          params: {
            environmentCreated: false,
            alertMessage: null
          },
          templateUrl: 'app/systemValidation/divJobs/environments/environments.html',
          controller: 'EnvironmentsController as envCtrl'
        })
      .state(
        'newEnvironment',
        {
          parent: 'main',
          url: '/systemValidation/newEnvironment',
          templateUrl: 'app/systemValidation/divJobs/environments/addEnvironment/environment.html',
          controller: 'EnvironmentController as addEnvCtrl'
        })
      .state(
        'environment',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId}/environment',
          templateUrl: 'app/systemValidation/divJobs/environments/addEnvironment/environment.html',
          controller: 'EnvironmentController as addEnvCtrl'
        })
      .state(
        'divJobs',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs',
          params: {
            divJobJustCreated: null,
            environmentName: null
          },
          templateUrl: 'app/systemValidation/divJobs/divJobs.html',
          controller: 'DivJobsController as divJobsCtrl'
        })
      .state(
        'divJobs.studentScenarios',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/studentScenarios',
          templateUrl: 'app/systemValidation/divJobs/studentScenario/studentScenarios.html',
          controller: 'StudentScenariosController as stdScenarioCtrl'
        })
      .state(
        'divJobs.batteryStudentScenarios',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/batteries',
          templateUrl: 'app/systemValidation/divJobs/batteryStudentScenario/batteryStudentScenarios.html',
          controller: 'BatteryStudentScenarioController as bssCtrl'
        })
      .state(
        'divJobs.batteryStudentScenarios.batteryUnits',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/batteries/{batteryStudentScenarioId:int}/units',
          templateUrl: 'app/systemValidation/divJobs/studentScenario/studentScenarios.html',
          controller: 'StudentScenariosController as stdScenarioCtrl'
        })
      .state(
        'divJobs.studentScenarios.studentScenarioResult',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/studentScenarios/{studentScenarioId:int}/studentScenarioResults',
          templateUrl: 'app/systemValidation/divJobs/studentScenarioResult/studentScenarioResult.html',
          controller: 'StudentScenarioResultController as ssResultCtrl'
        })
      .state(
        'divJobs.batteryStudentScenarios.batteryUnits.studentScenarioResult',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/batteries/{batteryStudentScenarioId:int}/units/{studentScenarioId:int}/studentScenarioResults',
          templateUrl: 'app/systemValidation/divJobs/studentScenarioResult/studentScenarioResult.html',
          controller: 'StudentScenarioResultController as ssResultCtrl'
        })
      .state(
        'divJobs.studentScenarios.studentScenarioResult.states',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/studentScenarios/{studentScenarioId:int}/states/{stateId:int}/studentScenarioResultHistory',
          templateUrl: 'app/systemValidation/divJobs/studentScenarioResultHistory/studentScenarioResultHistory.html',
          controller: 'StudentScenarioResultHistoryController as ssrHistoryCtrl'
        })
      .state(
        'divJobs.batteryStudentScenarios.batteryUnits.studentScenarioResult.states',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/batteries/{batteryStudentScenarioId:int}/units/{studentScenarioId:int}/states/{stateId:int}/studentScenarioResultHistory',
          templateUrl: 'app/systemValidation/divJobs/studentScenarioResultHistory/studentScenarioResultHistory.html',
          controller: 'StudentScenarioResultHistoryController as ssrHistoryCtrl'
        })
      .state(
        'testCodeSelection',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobSetup/testCodeSelection',
          templateUrl: 'app/systemValidation/divJobSetup/testCodeSelection/testCodeSelection.html',
          controller: 'TestCodeSelectionController as tcsCtrl'
        })
      .state(
        'testSessionSelection',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobSetup/testSessionSelection',
          params: {
            testType: null,
            environmentId: null,
            scopeTreePath: null,
            testCodes: null,
            availableTestCodes: null,
            selectedPathStates: null
          },
          templateUrl: 'app/systemValidation/divJobSetup/testSessionSelection/testSessionSelection.html',
          controller: 'TestSessionSelectionController as tssCtrl'
        })
      .state(
        'scenarioSelection',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobSetup/scenarioSelection',
          params: {
            testSessions: null
          },
          templateUrl: 'app/systemValidation/divJobSetup/scenarioSelection/scenarioSelection.html',
          controller: 'ScenarioSelectionController as ssCtrl'
        })
      .state(
        'studentSelection',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobSetup/studentSelection',
          params: {
            scenarios: null
          },
          templateUrl: 'app/systemValidation/divJobSetup/studentSelection/studentSelection.html',
          controller: 'StudentSelectionController as stdSelectionCtrl'
        })
      .state(
        'comments',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/studentScenarios/{studentScenarioId:int}/states/{stateId:int}/studentScenarioResults/{studentScenarioResultId:int}/studentScenarioResultVersion/{studentScenarioResultVersion:int}/comments',
          templateUrl: 'app/systemValidation/divJobs/comment/comment.html',
          controller: 'CommentController as commentCtrl'
        })
      .state(
        'batteryComments',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divJobs/{divJobExecId:int}/batteries/{batteryStudentScenarioId:int}/units/{studentScenarioId:int}/states/{stateId:int}/studentScenarioResults/{studentScenarioResultId:int}/studentScenarioResultVersion/{studentScenarioResultVersion:int}/comments',
          templateUrl: 'app/systemValidation/divJobs/comment/comment.html',
          controller: 'CommentController as commentCtrl'
        })
      .state(
        'usageStatistics',
        {
          parent: 'main',
          url: '/systemValidation/usageStatistics',
          templateUrl: 'app/systemValidation/divJobs/reports/usageStatistics/usageStatistics.html',
          controller: 'UsageStatisticsController as usageStatisticsCtrl'
        })
      .state(
        'divUsageStatistics',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/usageStatistics',
          templateUrl: 'app/systemValidation/divJobs/reports/usageStatistics/usageStatistics.html',
          controller: 'UsageStatisticsController as usageStatisticsCtrl'
        })
      .state(
        'divjobForms',
        {
          parent: 'main',
          url: '/systemValidation/environments/{environmentId:int}/divjobForms',
          templateUrl: 'app/systemValidation/divJobs/reports/divjobForms/divjobForms.html',
          controller: 'DivjobFormsController as divjobFormsCtrl'
        });
  }
})();
