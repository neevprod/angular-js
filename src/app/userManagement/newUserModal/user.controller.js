(function () {

  'use strict';

  angular.module('userManagement').controller('UserController', userController);

  userController.$inject = ['$uibModalInstance', 'userService'];

  function userController($uibModalInstance, userService) {
    var viewModel = this;
    viewModel.save = save;
    viewModel.close = close;
    viewModel.displayUsers = displayUsers;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    /**
     * Sends Database connection information to the service
     */
    function save(isValid) {
      if (isValid && viewModel.humanUser) {
        viewModel.savingUser = true;
        userService.addUser(viewModel.humanUser).then(function (result) {
          viewModel.savingUser = false;
          if (result.success) {
            viewModel.alertSuccess = true;
            viewModel.alertMessage = 'User created sucessfully!';
            viewModel.humanUser = result.data.user;
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });

      }

    }

    function displayUsers() {
      $uibModalInstance.close(viewModel.humanUser);
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }

})();
