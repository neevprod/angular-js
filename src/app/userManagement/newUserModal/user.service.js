(function () {

  'use strict';

  angular
    .module('userManagement')
    .factory('userService', userService);

  userService.$inject = ['$http', 'userManagementApiPrefix'];

  function userService($http, userManagementApiPrefix) {
    return {
      addUser: addUser
    };

    function addUser(humanUser) {
      return $http.post(userManagementApiPrefix + 'user', humanUser)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
