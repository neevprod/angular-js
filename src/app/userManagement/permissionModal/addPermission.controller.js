(function () {

  'use strict';

  angular.module('userManagement').controller('AddPermissionController', addPermissionController);

  addPermissionController.$inject = ['$uibModalInstance', 'addPermissionService', 'humanUserId'];

  function addPermissionController($uibModalInstance, addPermissionService, humanUserId) {
    var viewModel = this;
    viewModel.humanUserId = humanUserId;
    viewModel.close = close;
    viewModel.disableTenants = false;
    viewModel.getPermissionSettings = getPermissionSettings;
    viewModel.getPreviewerTenants = getPreviewerTenants;
    viewModel.getEnvironments = getEnvironments;
    viewModel.disableTenantList = disableTenantList;
    viewModel.disableEnvironmentList = disableEnvironmentList;
    viewModel.alertSuccess = false;
    viewModel.alertMessage = '';
    viewModel.save = save;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    activate();

    /**
     * Loads the list of application modules.
     */
    function activate() {
      getApplicationModules().then(function (applicationModules) {
        viewModel.applicationModules = applicationModules;
      });
    }

    /**
     * Retrieves the list of application modules.
     *
     * @returns The list of application modules users
     */
    function getApplicationModules() {
      return addPermissionService.getApplicationModules().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.applicationModules);
        }
      });
    }

    /**
     * Retrieves the list of permission settings.
     * @returns The list of permission settings
     */
    function getPermissionSettings() {
      viewModel.userPermission.allEnvironments = undefined;
      viewModel.userPermission.allTenants = undefined;
      viewModel.userPermission.previewer = undefined;
      viewModel.userPermission.tenant = undefined;
      viewModel.existingUserPermission = undefined;
      if (viewModel.userPermission && viewModel.userPermission.applicationModule) {
        if (viewModel.userPermission.applicationModule.permissionType.permissionTypeName === 'TenantLevelPermission') {
          getPreviewers().then(function (previewers) {
            viewModel.previewers = previewers;
          });
        } else {
          viewModel.environments = undefined;
        }

        return addPermissionService.getPermissionSettings(viewModel.userPermission.applicationModule.applicationModuleId, viewModel.humanUserId).then(function (result) {
          if (result.success) {
            viewModel.permissionSettings = angular.fromJson(result.data.permissionSettings);
          }
        });
      } else {
        viewModel.permissionSettings = undefined;
      }
    }

    /**
     * Retrieves the list of previewer tenants.
     * @returns {*|webdriver.promise.Promise}
     */
    function getPreviewerTenants() {
      if (viewModel.applicationModule && !viewModel.userPermission.permissionSetting) {
        viewModel.alertSuccess = false;
        viewModel.alertMessage = 'Please first select role.';
      }

      if (viewModel.userPermission.permissionSetting && viewModel.userPermission.previewer) {
        getUserPermission(viewModel.humanUserId,
          viewModel.userPermission.applicationModule.applicationModuleId,
          viewModel.userPermission.permissionSetting.permissionSettingsId,
          viewModel.userPermission.previewer.previewerId).then(function (existingUserPermission) {
          viewModel.existingUserPermission = existingUserPermission;
          if (!existingUserPermission || !existingUserPermission.isWildcard) {
            viewModel.userPermission.allTenants = false;
            return addPermissionService.getPreviewerTenants(
              viewModel.humanUserId,
              viewModel.userPermission.permissionSetting.permissionSettingsId,
              viewModel.userPermission.previewer.previewerId).then(function (result) {
              if (result.success) {
                viewModel.alertMessage = '';
                viewModel.tenants = angular.fromJson(result.data.tenants);
              } else {
                viewModel.alertSuccess = false;
                viewModel.alertMessage = result.errors[0].message;
              }
            });
          } else {
            viewModel.alertMessage = '';
            viewModel.tenants = [];
          }
        });
      }
    }

    function getUserPermission(humanUserId, applicationModuleId, permissionSettingsId, previewerId) {
      return addPermissionService.getUserPermission(humanUserId, applicationModuleId, permissionSettingsId, previewerId)
        .then(function (result) {
          if (result.data) {
            return angular.fromJson(result.data.userPermission);
          }
        });
    }

    /**
     * Retrieve the list of Environments.
     *
     * @returns The list of Environments.
     */
    function getEnvironments() {
      if (viewModel.userPermission.permissionSetting) {
        getUserPermission(viewModel.humanUserId,
          viewModel.userPermission.applicationModule.applicationModuleId,
          viewModel.userPermission.permissionSetting.permissionSettingsId).then(function (existingUserPermission) {
          viewModel.existingUserPermission = existingUserPermission;
          if (!existingUserPermission || !existingUserPermission.isWildcard) {
            viewModel.userPermission.allEnvironments = false;
            return addPermissionService.getEnvironments(viewModel.humanUserId, viewModel.userPermission.permissionSetting.permissionSettingsId)
              .then(function (result) {
                if (result.success) {
                  viewModel.alertMessage = '';
                  viewModel.environments = angular.fromJson(result.data.environments);
                } else {
                  viewModel.alertSuccess = false;
                  viewModel.alertMessage = result.errors[0].message;
                }
              });
          } else {
            viewModel.alertMessage = '';
            viewModel.environments = [];
          }
        });


      }
    }

    /**
     * Retrieve the list of Environments.
     *
     * @returns The list of Environments.
     */
    function getPreviewers() {
      return addPermissionService.getPreviewers().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.previewers);
        }
      });
    }

    function disableTenantList() {
      if (viewModel.userPermission.allTenants) {
        viewModel.disableTenants = true;
        viewModel.userPermission.tenant = undefined;
      } else {
        viewModel.disableTenants = false;
      }
    }

    function disableEnvironmentList() {
      if (viewModel.userPermission.allEnvironments) {
        viewModel.disableEnvironments = true;
        viewModel.userPermission.environment = undefined;
      } else {
        viewModel.disableEnvironments = false;
      }
    }

    function save(isValid) {
      if (isValid && viewModel.userPermission) {
        viewModel.savingPermissions = true;
        addPermissionService.savePermissions(viewModel.userPermission, viewModel.humanUserId).then(
          function (result) {
            viewModel.savingPermissions = false;
            if (result.success) {
              $uibModalInstance.close(result);
            } else {
              viewModel.alertSuccess = false;
              viewModel.alertMessage = result.errors[0].message;
            }
          });

      }

    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }

})();
