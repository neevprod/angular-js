(function () {

  'use strict';

  angular
    .module('userManagement')
    .factory('addPermissionService', addPermissionService);

  addPermissionService.$inject = ['$http', 'userManagementApiPrefix'];

  function addPermissionService($http, userManagementApiPrefix) {
    return {
      getApplicationModules: getApplicationModules,
      getPermissionSettings: getPermissionSettings,
      getUserPermission: getUserPermission,
      getEnvironments: getEnvironments,
      getPreviewers: getPreviewers,
      getPreviewerTenants: getPreviewerTenants,
      savePermissions: savePermissions
    };

    function getApplicationModules() {
      return $http.get(userManagementApiPrefix + 'applicationModules')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getPermissionSettings(applicationModuleId, humanUserId) {
      return $http.get(userManagementApiPrefix + 'applicationModules/' + applicationModuleId + '/humanUsers/' + humanUserId + '/permissionSettings')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getUserPermission(humanUserId, applicationModuleId, roleId, previewerId) {
      var url = 'users/' + humanUserId + '/applicationModules/' + applicationModuleId + '/roles/' + roleId + '/previewers/' + previewerId + '/userPermission';
      if (!previewerId) {
        url = 'users/' + humanUserId + '/applicationModules/' + applicationModuleId + '/roles/' + roleId + '/userPermission';
      }
      return $http.get(userManagementApiPrefix + url)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of Environments from the server.
     *
     * @param userId The user Id
     * @param roleId The role id
     * @returns The result from the server containing the list of environments.
     */
    function getEnvironments(userId, roleId) {
      return $http.get(userManagementApiPrefix + 'users/' + userId + '/roles/' + roleId + '/environments')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get the list of previewers from the server.
     *
     * @returns the result from the server containing the list of previewers
     */
    function getPreviewers() {
      return $http.get(userManagementApiPrefix + 'previewers')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function getPreviewerTenants(userId, roleId, previewerId) {
      return $http.get(userManagementApiPrefix + 'users/' + userId + '/roles/' + roleId + '/previewers/' + previewerId + '/tenants')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function savePermissions(userPermission, userId) {
      return $http.post(userManagementApiPrefix + 'users/' + userId + '/userPermission', userPermission)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
