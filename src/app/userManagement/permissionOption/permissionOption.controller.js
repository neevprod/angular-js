(function () {

  'use strict';

  angular.module('userManagement').controller('PermissionOptionController', permissionOptionController);

  permissionOptionController.$inject = ['$uibModalInstance', 'permissionOptionService', 'role'];

  function permissionOptionController($uibModalInstance, permissionOptionService, role) {
    var viewModel = this;

    viewModel.permissionOptions = [];
    viewModel.save = save;
    viewModel.close = close;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };

    getPermissionOptions(role);

    /**Retrieves the permissionOptions for the given role.
     *
     * @param role The role object
     * @returns {webdriver.promise.Promise|*}
     */
    function getPermissionOptions(role) {
      return permissionOptionService.getPermissionSettingsValues(role.applicationModuleId, role.permissionSettingsId)
        .then(function (result) {
          if (result.success) {
            viewModel.permissionSettingsValues = result.data.permissionSettingsValues;
          } else {
            viewModel.alertMessage = result.errors[0].message;
          }

        });
    }

    /**
     * Sends Permission Options to the server
     */
    function save() {
      viewModel.savingPermissionOptions = true;
      permissionOptionService.savePermissionOptionValues(viewModel.permissionSettingsValues)
        .then(function (result) {
          viewModel.savingPermissionOptions = false;
          if (result.success) {
            viewModel.alertSuccess = true;
            viewModel.alertMessage = result.data.message;
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close();
    }
  }
})();
