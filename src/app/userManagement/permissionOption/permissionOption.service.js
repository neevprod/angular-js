/**
 * Created by Nand Joshi on 11/2/2016.
 */
(function () {

  'use strict';

  angular.module('userManagement').factory('permissionOptionService', permissionOptionService);
  permissionOptionService.$inject = ['$http', 'userManagementApiPrefix'];

  function permissionOptionService($http, userManagementApiPrefix) {
    return {
      getApplicationModules: getApplicationModules,
      getPermissionSettingsValues: getPermissionSettingsValues,
      savePermissionOptionValues: savePermissionOptionValues
    };

    /**
     * Gets a list of application modules that has at least one permission option exist.
     *
     * @returns The result from the server containing the list of application modules
     */
    function getApplicationModules() {
      return $http.get(userManagementApiPrefix + 'applicationModulesWithPermissionOption')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Gets a list of permission options for the given application module id.
     * @param applicationModuleId The application module id
     ** @param roleId The role Id
     * @returns The result from the server containing the list of permissionSettingsValues
     */
    function getPermissionSettingsValues(applicationModuleId, roleId) {
      return $http.get(userManagementApiPrefix + 'applicationModules/' + applicationModuleId + '/roles/' + roleId + '/permissionSettingsValues')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Sends POST request to server to save a permissionOptionValues.
     * @param permissionOptionValues The permissionOptionValues to be created
     * @returns {*}
     */
    function savePermissionOptionValues(permissionOptionValues) {
      return $http.post(userManagementApiPrefix + 'permissionOptionValues', permissionOptionValues)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
