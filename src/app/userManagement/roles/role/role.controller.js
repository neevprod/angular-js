(function () {

  'use strict';

  angular.module('userManagement').controller('RoleController', roleController);

  roleController.$inject = ['$uibModalInstance', 'roleService', 'role'];

  function roleController($uibModalInstance, roleService, role) {
    var viewModel = this;
    viewModel.save = save;
    viewModel.close = close;
    viewModel.displayRoles = displayRoles;
    viewModel.permissionSetting = role;

    activate();

    /**
     * Loads the list of application modules.
     */
    function activate() {
      getApplicationModules().then(function (applicationModules) {
        viewModel.applicationModules = applicationModules;
      });
    }

    /**
     * Retrieves the list of application modules.
     *
     * @returns The list of application modules users
     */
    function getApplicationModules() {
      return roleService.getAllApplicationModules().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.applicationModules);
        }
      });
    }

    /**
     * Sends role information to the service
     */
    function save(isValid) {
      if (isValid) {
        viewModel.savingRole = true;
        roleService.addRole(viewModel.permissionSetting).then(function (result) {
          viewModel.savingRole = false;
          if (result.success) {
            viewModel.alertSuccess = true;
            viewModel.alertMessage = result.data.message;
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });

      } else {
        viewModel.alertSuccess = false;
        viewModel.alertMessage = 'Please fill the highlighted fields.';
      }

    }

    function displayRoles() {
      $uibModalInstance.close(true);
    }

    /**
     * Close the modal window.
     */
    function close() {
      $uibModalInstance.close(false);
    }
  }

})();
