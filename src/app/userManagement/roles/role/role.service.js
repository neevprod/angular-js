(function () {

  'use strict';

  angular
    .module('userManagement')
    .factory('roleService', roleService);

  roleService.$inject = ['$http', 'userManagementApiPrefix'];

  function roleService($http, userManagementApiPrefix) {
    return {
      getAllApplicationModules: getAllApplicationModules,
      addRole: addRole
    };

    /**
     * Sends GET request to server to retrieve all application modules.
     * @returns {*}
     */
    function getAllApplicationModules() {
      return $http.get(userManagementApiPrefix + 'allApplicationModules')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Sends POST request to server to save a role.
     * @param role The role to be created
     * @returns {*}
     */
    function addRole(role) {
      return $http.post(userManagementApiPrefix + 'role', role)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }


  }
})();
