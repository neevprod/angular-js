(function () {

  'use strict';

  angular.module('userManagement').controller('RolesController', rolesController);

  rolesController.$inject = ['memoryService', 'itemsPerPageOptions', 'rolesService', '$uibModal', '$state'];

  function rolesController(memoryService, itemsPerPageOptions, rolesService, $uibModal, $state) {
    var viewModel = this;

    viewModel.loadingRoles = true;
    viewModel.rolesInitialized = false;
    viewModel.alertSuccess = false;
    viewModel.roles = [];
    viewModel.displayedRoles = [];

    viewModel.displayAddRoleModal = displayAddRoleModal;
    viewModel.goToRoleOptionSetup = goToRoleOptionSetup;
    viewModel.deleteRole = deleteRole;
    viewModel.displayPermissionOptionModal = displayPermissionOptionModal;

    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('roles', 'perPage', '') || viewModel.itemsPerPageOptions[0].value;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    activate();


    /**
     * Loads the list of roles.
     */
    function activate() {
      getRoles().then(function (roles) {
        viewModel.loadingRoles = false;
        viewModel.alertSuccess = true;
        viewModel.rolesInitialized = true;
        viewModel.roles = roles;
        viewModel.displayedRoles = roles;
      });
    }

    /**
     * Retrieves the list of roles.
     *
     * @returns The list of roles.
     */
    function getRoles() {
      return rolesService.getRoles().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.roles);
        } else {
          viewModel.alertMessage = result.errors[0].message;
        }
      });
    }

    function displayAddRoleModal(role) {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/userManagement/roles/role/role.html',
        controller: 'RoleController as roleCtrl',
        backdrop: 'static',
        resolve: {
          role: function () {
            if (role) {
              return role;
            }
          }
        }
      });
      modalInstance.result.then(function (isRoleCreated) {
        if (isRoleCreated) {
          viewModel.loadingRoles = true;
          getRoles().then(function (roles) {
            viewModel.loadingRoles = false;
            viewModel.alertSuccess = true;
            viewModel.roles = roles;
            viewModel.displayedRoles = roles;
          });
        }
      });
    }

    function displayPermissionOptionModal(role) {
      $uibModal.open({
        templateUrl: 'app/userManagement/permissionOption/permissionOption.html',
        controller: 'PermissionOptionController as permissionOptionCtrl',
        backdrop: 'static',
        resolve: {
          role: function () {
            if (role) {
              return role;
            }
          }
        }
      });
    }

    function deleteRole(role) {
      if (role) {
        return rolesService.deleteRole(role.permissionSettingsId).then(function (result) {
          if (result.success) {
            viewModel.loadingRoles = true;
            viewModel.alertMessage = result.data.message;
            getRoles().then(function (roles) {
              viewModel.loadingRoles = false;
              viewModel.alertSuccess = true;
              viewModel.roles = roles;
              viewModel.displayedRoles = roles;
            });
          } else {
            viewModel.alertSuccess = false;
            viewModel.alertMessage = result.errors[0].message;
          }
        });
      }
    }

    function goToRoleOptionSetup(role) {
      $state.go('permissionOption', {
        'roleId': role.permissionSettingsId,
        'applicationModuleId': role.applicationModuleId
      });
    }
  }
})();
