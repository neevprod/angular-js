'use strict';

describe('Roles controller', function () {
  var controller;
  var $stateParams;
  var rolesService;
  var deferredRoles;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('userManagement'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredRoles = $q.defer();
    rolesService = jasmine.createSpyObj('rolesService', ['getRoles']);
    rolesService.getRoles.and.callFake(function () {
      return deferredRoles.promise;
    });


    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('' +
      'RolesController', {
      $stateParams: $stateParams, rolesService: rolesService,
      itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should set the list of roles to an empty array during initialization', function () {
    // Then
    expect(controller.roles).toEqual([]);
  });

  it('should call getRoles to get the list of roles during initialization', function () {
    // Then
    expect(rolesService.getRoles).toHaveBeenCalled();
  });

  it('should set the list of roles after rolesService returns', function () {
    // Given
    var rolesArray = [
      {
        'permissionSettingsId': 13,
        'name': 'Read Test Cases',
        'lastUpdate': 1478620971000,
        'applicationModuleId': 1,
        'applicationModule': {
          'applicationModuleId': 1,
          'applicationModuleName': 'qtiValidation'
        }
      },
      {
        'permissionSettingsId': 17,
        'name': 'Environment',
        'lastUpdate': 1478728632000,
        'applicationModuleId': 3,
        'applicationModule': {
          'applicationModuleId': 3,
          'applicationModuleName': 'systemValidation'
        }
      }
    ];

    // When
    deferredRoles.resolve({success: true, data: {roles: rolesArray}});
    $rootScope.$digest();

    // Then
    expect(controller.roles).toEqual(rolesArray);
    expect(controller.displayedRoles).toEqual(rolesArray);
  });
});
