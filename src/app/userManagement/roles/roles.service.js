/**
 * Created by Nand Joshi on 11/2/2016.
 */
(function () {
  'use strict';
  angular.module('userManagement').factory('rolesService', rolesService);
  rolesService.$inject = ['$http', 'userManagementApiPrefix'];

  function rolesService($http, userManagementApiPrefix) {
    return {
      getRoles: getRoles,
      deleteRole: deleteRole
    };

    /**
     * Get a list of roles
     *
     * @returns The result from the server containing the list of roles
     */
    function getRoles() {
      return $http.get(userManagementApiPrefix + 'roles')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    function deleteRole(permissionSettingsId) {
      return $http.delete(userManagementApiPrefix + 'roles/' + permissionSettingsId + '/role')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
