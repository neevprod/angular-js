'use strict';

describe('Roles service', function () {
  var rolesService;
  var $httpBackend;
  var userManagementApiPrefix;

  beforeEach(module('userManagement'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_rolesService_, _$httpBackend_, _userManagementApiPrefix_) {
    rolesService = _rolesService_;
    $httpBackend = _$httpBackend_;
    userManagementApiPrefix = _userManagementApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getRoles', function () {
    it('should submit the roles GET request properly', function () {
      // Given
      var data = [{permissionSettingsId: 1}, {permissionSettingsId: 2}, {permissionSettingsId: 3}];

      // Then
      $httpBackend.expectGET(userManagementApiPrefix + 'roles')
        .respond(200, {success: true, data: data});

      // When
      rolesService.getRoles();
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{permissionSettingsId: 1}, {permissionSettingsId: 2}, {permissionSettingsId: 3}];
      $httpBackend.whenGET(userManagementApiPrefix + 'roles')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      rolesService.getRoles().then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(userManagementApiPrefix + 'roles')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      rolesService.getRoles().then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
