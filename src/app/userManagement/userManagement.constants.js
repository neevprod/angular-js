(function () {

  'use strict';

  angular.module('userManagement')
    .constant('userManagementApiPrefix',
      'api/userManagement/v1/');
})();
