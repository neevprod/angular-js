(function () {

  'use strict';

  angular.module('userManagement', ['core']).config(configure);
  configure.$inject = ['$stateProvider'];

  function configure($stateProvider) {
    $stateProvider.state('users', {
      parent: 'main',
      url: '/userManagement/users',
      templateUrl: 'app/userManagement/users.html',
      controller: 'UsersController as usersCtrl'

    })
      .state('users.user', {
        parent: 'main',
        url: '/userManagement/users/{humanUserId}/humanUser',
        templateUrl: 'app/userManagement/userPermission/userPermissions.html',
        controller: 'UserPermissionController as userPermissionCtrl'
      })
      .state('roles', {
        parent: 'main',
        url: '/userManagement/roles',
        templateUrl: 'app/userManagement/roles/roles.html',
        controller: 'RolesController as rolesCtrl'
      });
    //     .state('permissionOption', {
    //         parent: 'main',
    //         url: '/userManagement/applicationModules/{applicationModuleId}/roles/{roleId}/permissionOption',
    //         templateUrl: 'app/userManagement/permissionOption/permissionOption.html',
    //         controller: 'PermissionOptionController as permissionOptionCtrl'
    //     })
    // ;
  }
})();
