(function () {

  'use strict';

  angular.module('userManagement').controller('UserPermissionController', userPermissionController);

  userPermissionController.$inject = ['$stateParams', '$uibModal', 'userPermissionsService', 'itemsPerPageOptions', 'memoryService'];

  function userPermissionController($stateParams, $uibModal, userPermissionsService, itemsPerPageOptions, memoryService) {
    var viewModel = this;
    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('divJobs', 'perPage', '') || viewModel.itemsPerPageOptions[0].value;
    viewModel.userPermissions = [];
    viewModel.displayedUserPermissions = [];
    viewModel.alertSuccess = false;
    viewModel.alertMessage = '';
    viewModel.displayPermissionModal = displayPermissionModal;
    viewModel.deleteUserPermission = deleteUserPermission;
    viewModel.updateAdminRole = updateAdminRole;
    viewModel.clearAlertMessage = function () {
      viewModel.alertMessage = '';
    };
    activate();

    /**
     * Loads the human user.
     */
    function activate() {
      getUser($stateParams.humanUserId).then(function (user) {
        viewModel.humanUser = user;
      });

      getUserPermissions($stateParams.humanUserId).then(function (userPermissions) {
        viewModel.userPermissions = userPermissions;
        viewModel.displayedUserPermissions = userPermissions;
      });

    }

    /**
     * Retrieves the list of user permissions for given user.
     * @param userId The human user id
     * @returns The list of user permissions.
     */
    function getUserPermissions(userId) {
      return userPermissionsService.getUserPermissions(userId).then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.userPermissions);
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
        }
      });
    }

    /**
     * Retrieves the list of human users.
     *
     * @returns The list of human users.
     */
    function getUser(humanUserId) {
      return userPermissionsService.getUser(humanUserId).then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.humanUser);
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
        }
      });
    }

    /**
     * Calls service method to delete userPermission
     * @param userPermissionId The userPermissionId
     * @returns {*|webdriver.promise.Promise}
     */
    function deleteUserPermission(userPermissionId) {
      return userPermissionsService.deleteUserPermission(userPermissionId).then(function (result) {
        if (result.success) {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = result.data.message;
          getUserPermissions($stateParams.humanUserId).then(function (userPermissions) {
            viewModel.userPermissions = userPermissions;
          });
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
        }
      });
    }

    function updateAdminRole() {
      userPermissionsService.updateAdminRole(viewModel.humanUser).then(function (result) {
        if (result.success) {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = result.data.message;
          getUser($stateParams.humanUserId).then(function (user) {
            viewModel.humanUser = user;
          });
        } else {
          viewModel.alertSuccess = false;
          viewModel.alertMessage = result.errors[0].message;
        }
      });

    }

    /**
     * Displays a form in a modal to add user permission.
     */
    function displayPermissionModal() {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/userManagement/permissionModal/addPermission.html',
        controller: 'AddPermissionController as addPermissionCtrl',
        backdrop: 'static',
        resolve: {
          humanUserId: function () {
            return $stateParams.humanUserId;
          }
        },
        size: 'lg'
      });
      modalInstance.result.then(function (modalResult) {
        if (modalResult && modalResult.success) {
          viewModel.alertSuccess = true;
          viewModel.alertMessage = modalResult.data.message;
          getUserPermissions($stateParams.humanUserId).then(function (userPermissions) {
            viewModel.userPermissions = userPermissions;
          });
        }
      });
    }
  }
})();
