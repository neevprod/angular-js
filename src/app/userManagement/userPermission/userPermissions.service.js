(function () {
  'use strict';
  angular.module('userManagement').factory('userPermissionsService', userPermissionsService);
  userPermissionsService.$inject = ['$http', 'userManagementApiPrefix'];

  function userPermissionsService($http, userManagementApiPrefix) {
    return {
      getUser: getUser,
      getUserPermissions: getUserPermissions,
      deleteUserPermission: deleteUserPermission,
      updateAdminRole: updateAdminRole
    };

    function getUserPermissions(humanUserId) {
      return $http.get(userManagementApiPrefix + 'humanUsers/' + humanUserId + '/userPermissions')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Get a list of human users
     *
     * @returns The result from the server containing the list of human users
     */
    function getUser(humanUserId) {
      return $http.get(userManagementApiPrefix + 'humanUsers/' + humanUserId + '/humanUser')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Sends request to the server to delete userPermission.
     * @param userPermissionId The userPermissionId
     * @returns {*}
     */
    function deleteUserPermission(userPermissionId) {
      return $http.delete(userManagementApiPrefix + 'userPermissions/' + userPermissionId + '/userPermission')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }

    /**
     * Sends request to server to update admin role for given user.
     * @param humanUser The human user
     * @returns {*}
     */
    function updateAdminRole(humanUser) {
      return $http.put(userManagementApiPrefix + 'users/' + humanUser.humanUserId + '/humanUser', humanUser)
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });

    }
  }
})();
