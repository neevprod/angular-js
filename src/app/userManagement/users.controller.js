(function () {

  'use strict';

  angular.module('userManagement').controller('UsersController', usersController);

  usersController.$inject = ['memoryService', 'itemsPerPageOptions', 'usersService', '$uibModal', '$state'];

  function usersController(memoryService, itemsPerPageOptions, usersService, $uibModal, $state) {
    var viewModel = this;

    viewModel.loadingUsers = true;
    viewModel.usersInitialized = false;
    viewModel.users = [];

    viewModel.itemsPerPageOptions = itemsPerPageOptions;
    viewModel.itemsPerPage = memoryService.getItem('users', 'perPage', '') || viewModel.itemsPerPageOptions[0].value;
    viewModel.goToRolesPage = goToRolesPage;
    viewModel.goToPermissionOptionPage = goToPermissionOptionPage;

    activate();

    viewModel.displayNewUserModal = displayNewUserModal;

    /**
     * Loads the list of human users.
     */
    function activate() {
      getUsers().then(function (users) {
        viewModel.loadingUsers = false;
        viewModel.usersInitialized = true;
        viewModel.users = users;
        viewModel.displayedUsers = users;
      });
    }

    /**
     * Retrieves the list of human users.
     *
     * @returns The list of human users.
     */
    function getUsers() {
      return usersService.getUsers().then(function (result) {
        if (result.success) {
          return angular.fromJson(result.data.users);
        }
      });
    }

    /**
     * Displays a form in a modal to create a new user.
     */
    function displayNewUserModal() {
      var modalInstance = $uibModal.open({
        templateUrl: 'app/userManagement/newUserModal/user.html',
        controller: 'UserController as userCtrl',
        backdrop: 'static'
      });
      modalInstance.result.then(function (newUser) {
        if (newUser) {
          $state.go('users.user', {
            humanUserId: newUser.humanUserId
          });
        }
      });
    }

    function goToRolesPage() {
      $state.go('roles');
    }

    function goToPermissionOptionPage() {
      $state.go('permissionOption');
    }
  }

})();
