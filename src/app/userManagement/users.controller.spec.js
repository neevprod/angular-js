'use strict';

describe('Users controller', function () {
  var controller;
  var $stateParams;
  var usersService;
  var deferredUsers;
  var itemsPerPageOptions;
  var $rootScope;

  beforeEach(module('userManagement'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function ($controller, _itemsPerPageOptions_, $q, _$rootScope_) {
    $stateParams = {};

    deferredUsers = $q.defer();
    usersService = jasmine.createSpyObj('usersService', ['getUsers']);
    usersService.getUsers.and.callFake(function () {
      return deferredUsers.promise;
    });


    itemsPerPageOptions = _itemsPerPageOptions_;
    $rootScope = _$rootScope_;
    controller = $controller('UsersController', {
      $stateParams: $stateParams, usersService: usersService,
      itemsPerPageOptions: itemsPerPageOptions
    });
  }));

  it('should set itemsPerPageOptions to the dependency injected options during initialization', function () {
    // Then
    expect(controller.itemsPerPageOptions).toEqual(itemsPerPageOptions);
  });

  it('should set itemsPerPage to the value of the first element of itemsPerPageOptions during initialization', function () {
    // Then
    expect(controller.itemsPerPage).toEqual(itemsPerPageOptions[0].value);
  });

  it('should set the list of users to an empty array during initialization', function () {
    // Then
    expect(controller.users).toEqual([]);
  });

  it('should call getUsers to get the list of users during initialization', function () {
    // Then
    expect(usersService.getUsers).toHaveBeenCalled();
  });

  it('should set the list of users after usersService returns', function () {
    // Given
    var usersArray = [
      {
        'humanUserId': 2,
        'userId': 2,
        'email': 'nand.joshi@pearson.com',
        'networkId': 'vjoshna',
        'user': {
          'id': 2,
          'fullname': 'Nand Joshi',
          'isAdmin': false
        }
      },
      {
        'humanUserId': 17,
        'userId': 17,
        'email': 'Abhas.Kumar@Pearson.com',
        'networkId': 'UKUMAA4',
        'user': {
          'id': 17,
          'fullname': 'Abhas Kumar',
          'isAdmin': false
        }
      }
    ];

    // When
    deferredUsers.resolve({success: true, data: {users: usersArray}});
    $rootScope.$digest();

    // Then
    expect(controller.users).toEqual(usersArray);
    expect(controller.displayedUsers).toEqual(usersArray);
  });
});
