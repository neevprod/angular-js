(function () {
  'use strict';
  angular.module('userManagement').factory('usersService', usersService);
  usersService.$inject = ['$http', 'userManagementApiPrefix'];

  function usersService($http, userManagementApiPrefix) {
    return {
      getUsers: getUsers
    };

    /**
     * Get a list of human users
     *
     * @returns The result from the server containing the list of human users
     */
    function getUsers() {
      return $http.get(userManagementApiPrefix + 'users')
        .then(function (result) {
          return result.data;
        })
        .catch(function (result) {
          return result.data;
        });
    }
  }
})();
