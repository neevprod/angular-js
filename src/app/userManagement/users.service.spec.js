'use strict';

describe('Users service', function () {
  var usersService;
  var $httpBackend;
  var userManagementApiPrefix;

  beforeEach(module('userManagement'));

  beforeEach(module(function ($urlRouterProvider) {
    $urlRouterProvider.deferIntercept();
  }));

  beforeEach(inject(function (_usersService_, _$httpBackend_, _userManagementApiPrefix_) {
    usersService = _usersService_;
    $httpBackend = _$httpBackend_;
    userManagementApiPrefix = _userManagementApiPrefix_;
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('getUsers', function () {
    it('should submit the users GET request properly', function () {
      // Given
      var data = [{humanUserId: 1}, {humanUserId: 2}, {humanUserId: 3}];

      // Then
      $httpBackend.expectGET(userManagementApiPrefix + 'users')
        .respond(200, {success: true, data: data});

      // When
      usersService.getUsers(1);
      $httpBackend.flush();
    });

    it('should return the response data when the call succeeds', function () {
      // Given
      var data = [{humanUserId: 1}, {humanUserId: 2}, {humanUserId: 3}];
      $httpBackend.whenGET(userManagementApiPrefix + 'users')
        .respond(200, {success: true, data: data});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(true);
        expect(result.data).toEqual(data);
      });

      // When
      usersService.getUsers().then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });

    it('should return the response data when the call fails', function () {
      // Given
      $httpBackend.whenGET(userManagementApiPrefix + 'users')
        .respond(404, {success: false, errors: [{message: 'Error message.'}]});

      // Then
      var handler = jasmine.createSpy('handler').and.callFake(function (result) {
        expect(result.success).toBe(false);
        expect(result.errors[0].message).toBe('Error message.');
      });

      // When
      usersService.getUsers().then(handler);
      $httpBackend.flush();

      // Then
      expect(handler).toHaveBeenCalled();
    });
  });

});
