##### Version 1.8.17
- [AUTO-1676](https://jira.assessment.pearson.com/browse/AUTO-1676): System Validation - Fix issue that could cause an infinite loop during test case coverage validation in DIV job setup when no KT test cases are present for multi-part items containing one or more AI scored parts.

##### Version 1.8.16
- [AUTO-1659](https://jira.assessment.pearson.com/browse/AUTO-1659): System Validation - Fix issue that could result in duplicate db_connection records in environment setup.
- [AUTO-1673](https://jira.assessment.pearson.com/browse/AUTO-1673): System Validation - Add attemptedness flag to itemState for all test attempts submitted by TN8 API Data Loader.
- [PSAS-95](https://agile-jira.pearson.com/browse/PSAS-95): QTI response generator - Fix for multiple decimals in fractional value.
- [PSAS-99](https://agile-jira.pearson.com/browse/PSAS-99): QTI response generator - Fix for AllStringToNumber without parameters.
- [PSAS-100](https://agile-jira.pearson.com/browse/PSAS-100): QTI response generator - Flip source/target for IsCorrectByMappedSum if needed.

##### Version 1.8.15
- [AUTO-1672](https://jira.assessment.pearson.com/browse/AUTO-1672): System Validation - Correct issue where items containing KT parts were not filtered out correctly during the non-KT test case coverage validation, which could cause maxPoints coverage to incorrectly be marked as failed.

##### Version 1.8.14
- [AUTO-1634](https://jira.assessment.pearson.com/browse/AUTO-1634): System Validation - Correct concurrency issue in ePEN Bulk scorer caused by use of singleton for ApplicationConfig.
- [AUTO-1671](https://jira.assessment.pearson.com/browse/AUTO-1671): System Validation - Fix issue that caused the ePEN2 Automation component to not fail gracefully when no ePEN scoring eligible item statuses were found in the Backend Path.
- [AUTO-1672](https://jira.assessment.pearson.com/browse/AUTO-1672): System Validation - Correct issue that would cause a problem in the maxPoints calculation during test case coverage validation for multi-part items that contain machine scored parts and AI scored parts.

##### Version 1.8.13
- [AUTO-1650](https://jira.assessment.pearson.com/browse/AUTO-1650): System Validation - Add timestamp on which the most recent KT test case manifest was loaded to DIV job setup page.
- [AUTO-1658](https://jira.assessment.pearson.com/browse/AUTO-1658): System Validation - Remove unnecessary duplication of T3 testmap retrieval during DIV job setup.
- [AUTO-1668](https://jira.assessment.pearson.com/browse/AUTO-1668): System Validation - Correct issue that prevented older Response Generator versions from displaying in the drop down on the QTI Validation (Previewers) page.
- [PSAS-93](https://agile-jira.pearson.com/browse/PSAS-93): QTI response generator - Fix for when MapResponse has multiple mappings using default value.

##### Version 1.8.12
- [AUTO-856](https://jira.assessment.pearson.com/browse/AUTO-856): System Validation - Ensure responses for all non-zero scores for machine-scored and AI-scored items are available.
- [AUTO-1666](https://jira.assessment.pearson.com/browse/AUTO-1666): System Validation - Correct issue that could cause STATS failures when Group Level names contain Spanish characters.
- [PSAS-87](https://agile-jira.pearson.com/browse/PSAS-87): QTI response generator - Fix for discerning when a decimal is or isn't allowed.
- [PSAS-88](https://agile-jira.pearson.com/browse/PSAS-88): QTI response generator - Added additional checks for decimals.
- [PSAS-89](https://agile-jira.pearson.com/browse/PSAS-89): QTI response generator - Fix for comma checking in AllStringToNumber.

##### Version 1.8.11
- [PSAS-84](https://agile-jira.pearson.com/browse/PSAS-84): QTI response generator - Remove commas when commaOn and commaOnPrecise are in AllStringToNumber.
- [PSAS-86](https://agile-jira.pearson.com/browse/PSAS-86): QTI response generator - Fix for when a decimal is in a value but DecimalOn is not specified.

##### Version 1.8.10
- [AUTO-1664](https://jira.assessment.pearson.com/browse/AUTO-1664): System Validation - Fix issue that may have caused ePEN2 Mongo DB connections to be held open by the EPEN2_AUTOMATION component in certain situations.
- [AUTO-1665](https://jira.assessment.pearson.com/browse/AUTO-1665): System Validation - Fix issue in test case coverage validation logic that could cause DIV Job setup to hang when navigating to page 3/4 for forms that include items which contain multiple KT parts where the score of each part is 2 or greater.

##### Version 1.8.9
- [AUTO-1662](https://jira.assessment.pearson.com/browse/AUTO-1662): System Validation - Fix issue where not all KT part scores were included in the item level score for items containing multiple KT parts.
- [AUTO-1663](https://jira.assessment.pearson.com/browse/AUTO-1663): System Validation - Fix issue where DIV job does not resume when STATS state is not in the path and "rerun STATS for Completed student scenarios" is selected.
- [PSAS-78](https://agile-jira.pearson.com/browse/PSAS-78): QTI response generator - Fix issue with non-float values being converted to float in Variable operator.

##### Version 1.8.8
- [AUTO-1661](https://jira.assessment.pearson.com/browse/AUTO-1661): System Validation - Improve performance of "View STATS failures" report and address timeout issues.

##### Version 1.8.7
- [AUTO-1644](https://jira.assessment.pearson.com/browse/AUTO-1644): System Validation - Correct issue that could cause IRIS validator to wait too long or not long enough.

##### Version 1.8.6.3
Apply configuration changes to address potential thread starvation issues:
- Increase maxConcurrentDivJobs from 15 to 30.
- Reduce maxConcurrentStudentScenario from 30 to 15.
- Increase parallelism-min and parallelism-max from 403 to 455.

##### Version 1.8.6.2
- Decrease maximum DB pool size and DB connection timeout to address performance issues.

##### Version 1.8.6.1
- [AUTO-453](https://jira.assessment.pearson.com/browse/AUTO-453): System Validation - Add allCorrectWithUniqueScore scenario.
- [AUTO-1651](https://jira.assessment.pearson.com/browse/AUTO-1651): System Validation - Correct issue where Right-to-Wrong and Wrong-to-Wrong scenario creation could fail for forms containing sample and survey items.
- [AUTO-1656](https://jira.assessment.pearson.com/browse/AUTO-1656): System Validation - Correct issue where test cases for items with multiple KT scored parts could not be loaded correctly for one or more of the parts present in the KT manifest.
- Increase maximum DB pool size from 10 to 40. Increase DB connection timeout from 30 seconds to 60 seconds.
- [PSAS-66](https://agile-jira.pearson.com/browse/PSAS-66): QTI response generator - Change result of Variable return to parse based on BaseType.
- [PSAS-76](https://agile-jira.pearson.com/browse/PSAS-76): QTI response generator - Differentiate fieldValue operator between single and multi-dimensional array.

##### Version 1.8.5
- [AUTO-1599](https://jira.assessment.pearson.com/browse/AUTO-1599): System Validation - Correct issue where human scorable items are sometimes treated as machine scorable items during STATS validation.
- [AUTO-1643](https://jira.assessment.pearson.com/browse/AUTO-1643): Setup API user for KT, add API docs and create AKKA actor to run updateKtTestCases job asynchronously.
- [AUTO-1648](https://jira.assessment.pearson.com/browse/AUTO-1648): System Validation - Correct issue affecting student reservation expiration.
- Increase maximum number of concurrent Data Integrity Validation jobs from 10 to 15.                                                                     
- [PSAS-59](https://agile-jira.pearson.com/browse/PSAS-59): QTI response generator - Fix issue with missing abbiType for TeiGraphingInteractions.
- [PSAS-61](https://agile-jira.pearson.com/browse/PSAS-61): QTI response generator - Fix issue with maxChoices being 0 in HotTextInteraction.
- [PSAS-63](https://agile-jira.pearson.com/browse/PSAS-63): QTI response generator - Fix issue with fieldvalue points returning nested array.
- [PSAS-66](https://agile-jira.pearson.com/browse/PSAS-66): QTI response generator - Change result of Variable return to parse based on BaseType.

##### Version 1.8.4
- [AUTO-1641](https://jira.assessment.pearson.com/browse/AUTO-1641): System Validation - Revise handling of simulation response data.
- [PSAS-52](https://agile-jira.pearson.com/browse/PSAS-52): QTI response generator - Fixed issue when maxChoice = 0 in HotSpotInteraction.
- [PSAS-53](https://agile-jira.pearson.com/browse/PSAS-53): QTI response generator - Fixed issue with single cardinality in points that satisfy equation.

##### Version 1.8.3
- [AUTO-1640](https://jira.assessment.pearson.com/browse/AUTO-1640): Test Case Coverage Validation: remove teiPciResponsesAreNotEmpty validation for composite items containing sim parts.
- Add indexes to testmap_detail table to address DIV job setup performance issues.
- Correct JPA issue that could result in intermittent application errors when adding scenarios for standard tests during DIV job setup.
- QTI Response Generator: Add ability to parse assessmentItem from mixed XML files.
- QTI Response Generator: Add support for QTI3 choice interactions.
- QTI Response Generator: Add support for qti-match-interaction.

##### Version 1.8.2
- System Validation: Correct issue that could cause a null pointer exception when adding a scenario to a form for a non-battery test.
- System Validation: Correct issue that could cause the response data for KT items not to get updated for non-battery tests.

##### Version 1.8.1
- [AUTO-1534](https://jira.assessment.pearson.com/browse/AUTO-1534): QTI response generator - Fix for seriesX_points being off by 1.
- [AUTO-1546](https://jira.assessment.pearson.com/browse/AUTO-1546): QTI response generator - fix typo in TeiPointGraphPolygon.
- [AUTO-1621](https://jira.assessment.pearson.com/browse/AUTO-1621): QTI response generator - Add further support for record cardinality.
- [AUTO-1625](https://jira.assessment.pearson.com/browse/AUTO-1625): QTI response generator - Changed equation libraries for CountPointsThatSatisfyEquation.
- Correct issue where KT test case data was not correctly deleted prior to inserting new manifest file data.

##### Version 1.8.0
- [AUTO-67](https://jira.assessment.pearson.com/browse/AUTO-67): Add Support to Validate KT Essay Item Scoring.
- [AUTO-69](https://jira.assessment.pearson.com/browse/AUTO-69): Implement functionality to download and process KT test case manifest from S3 bucket.

##### Version 1.7.12
- [AUTO-1617](https://jira.assessment.pearson.com/browse/AUTO-1617): Bypass verification of simulation responses for multi-part, machine scorable items during test case coverage validation.
- [AUTO-1624](https://jira.assessment.pearson.com/browse/AUTO-1624): Correct issue that caused certain non-attempted responses for EXTENDEDTEXTINTERACTION.TEI-EE items to be flagged as attempted in error.
- [AUTO-1629](https://jira.assessment.pearson.com/browse/AUTO-1629): Display proper error message when Previewer API call fails.

##### Version 1.7.11
- [AUTO-1631](https://jira.assessment.pearson.com/browse/AUTO-1631): Correct issue that prevented users from removing scenarios on page 4/4 of DIV job setup.
- Address issue that caused the DIV job status to incorrectly display "Completed (OE OP/FT Excluded)" if one of its student scenarios had this status, even if another student scenario had failed.

##### Version 1.7.10
- [AUTO-1598](https://jira.assessment.pearson.com/browse/AUTO-1598): Additional performance enhancements.
- [AUTO-1625](https://jira.assessment.pearson.com/browse/AUTO-1625): Add fix for sin/cos/tan.
- QTI response generator: Fix for PI in CountPointsThatSatisfyEquation.
- QTI response generator: Fix for substring after/before not parsing properly.

##### Version 1.7.9
- [AUTO-1630](https://jira.assessment.pearson.com/browse/AUTO-1630): Correct issue where users with permissions to the gridAutomation module were not able to submit new jobs.
- Add instructions to DIV job setup page 1/4 for when a scope tree path is missing.

##### Version 1.7.8
- [AUTO-1598](https://jira.assessment.pearson.com/browse/AUTO-1598): Address issue that could cause out of memory errors in certain situations.
- [AUTO-1623](https://jira.assessment.pearson.com/browse/AUTO-1623): Correct query used to identify item statuses eligible for ePEN scoring to account for legacy procs.

##### Version 1.7.7
- [AUTO-1578](https://jira.assessment.pearson.com/browse/AUTO-1578): QTI Scoring api: fix for when there is no denominator.
- [AUTO-1607](https://jira.assessment.pearson.com/browse/AUTO-1607): Fix issue where success is not "yes" even when no validation errors occur.
- [AUTO-1608](https://jira.assessment.pearson.com/browse/AUTO-1608): Add check for previous test code, in addition to checking previous form code.
- [SIP-6635](https://jira.assessment.pearson.com/browse/SIP-6635): Grid automation: initial work for 6 by 8 and Owatonna release.

##### Version 1.7.6
- Revert from Akka HTTP Backend to Netty Server Backend. 

##### Version 1.7.5
- [AUTO-1578](https://jira.assessment.pearson.com/browse/AUTO-1578): QTI Scoring api: fix for AllStringToNumber 'slashOn' issue.
- [AUTO-1582](https://jira.assessment.pearson.com/browse/AUTO-1582): QTI Scoring api: fix for graphicGapMatchInteraction directedPair issue.
- [AUTO-1594](https://jira.assessment.pearson.com/browse/AUTO-1594): Upgrade application to Play 2.6.20 and update PlayFramework cache to SyncCache.
- [AUTO-1609](https://jira.assessment.pearson.com/browse/AUTO-1609): Correct issue where the status for completed div jobs could remain stuck in progress.
- [AUTO-1610](https://jira.assessment.pearson.com/browse/AUTO-1610): Update regular expression to identify non-attempted response for tei-pci items where the response is of format "[mix,mix],[-1,-1,-1],[-1,-1]".
- [AUTO-1611](https://jira.assessment.pearson.com/browse/AUTO-1611): Correct issue where the choiceInteraction.cloze type was not properly interpreted when downloading responses when the classValue was "clozeinline-undefined".
- [AUTO-1612](https://jira.assessment.pearson.com/browse/AUTO-1612): QTI Scoring api: fix for tei-functiongraph item issue.
- [AUTO-1613](https://jira.assessment.pearson.com/browse/AUTO-1613): Correct issue where DIV Job creation could fail for very large testmaps.
- Grid Automation: Update odm-grid-automation to version 1.0.23. 

##### Version 1.7.4
- [AUTO-1601](https://jira.assessment.pearson.com/browse/AUTO-1601): System Validation - Improve performance of DIV jobs page and address out of memory issues that could occur for environments with a large amount of student scenarios.
- QTI-response-generator: Update QTI-response-generator to version 0.0.46. 

##### Version 1.7.3
- [AUTO-1588](https://jira.assessment.pearson.com/browse/AUTO-1588): QTI Validation - QTI-response-generator: fix for commas in TEI-EE responses.
- [AUTO-1603](https://jira.assessment.pearson.com/browse/AUTO-1603): QTI Validation - QTI-response-generator: fix for issue with AllStringToNumber processing decimals in fractions.

##### Version 1.7.2
- [SIP-6015](https://jira.assessment.pearson.com/browse/SIP-6015): Grid Automation: Correct issue that prevented minimum dates from working on the ScheduleGridJob Calendar.
- [AUTO-1601](https://jira.assessment.pearson.com/browse/AUTO-1601): Update mysql-connector-java to version 8.0.13, increase maximumDBPoolSize from 10 to 20.

##### Version 1.7.1
- [SIP-5980](https://jira.assessment.pearson.com/browse/SIP-5980): Grid Automation: Correct issue that prevented the selection of future dates on the ScheduleGridJob Calendar.
- [SIP-5981](https://jira.assessment.pearson.com/browse/SIP-5981): Grid Automation: Correct issue that prevented Grid Jobs from showing on the Grid Automation Jobs page.

##### Version 1.7.0
- Grid Automation: Add initial version of Grid Automation module.

##### Version 1.6.56
- [AUTO-1506](https://jira.assessment.pearson.com/browse/AUTO-1506): QTI Validation: Correct issue that could generate incorrect responses with max # of objects for graphicGapMatchInteraction.
- [AUTO-1527](https://jira.assessment.pearson.com/browse/AUTO-1527): QTI Validation: Correct issue that could generate invalid non-attempted responses for tei-ee item types.
- [AUTO-1547](https://jira.assessment.pearson.com/browse/AUTO-1547): QTI Validation: Correct issue that could generate invalid incorrect responses with insufficient objects for orderInteraction.
- [AUTO-1564](https://jira.assessment.pearson.com/browse/AUTO-1564): QTI Validation: Correct issue that could generate invalid response exceeding text box length for textEntryInteraction.
- [AUTO-1583](https://jira.assessment.pearson.com/browse/AUTO-1583): System Validation: Ignore test maps with format other than "e"
- [AUTO-1590](https://jira.assessment.pearson.com/browse/AUTO-1590): System Validation: Correct issue where the choiceInteraction.cloze type was not properly interpreted when downloading responses when the classValue is "clozeblock-fillin".
- [AUTO-1591](https://jira.assessment.pearson.com/browse/AUTO-1591): System Validation: Correct issue where certain items would incorrectly be assigned the subtype of tei-texthighlighter in the item response pool.

##### Version 1.6.55
- [AUTO-998](https://jira.assessment.pearson.com/browse/AUTO-998): System Validation: Add support for right-to-wrong, wrong-to-right and wrong-to-wrong TestNav scenarios.
- [AUTO-1586](https://jira.assessment.pearson.com/browse/AUTO-1586): System Validation: Change behavior so the scope tree paths for standard tests are no longer loaded by default during DIV job setup.  
- [AUTO-1587](https://jira.assessment.pearson.com/browse/AUTO-1587): System Validation: Correct issue in IRIS_PROCESSING_VALIDATOR when a duplicate, but inactive, project configuration exists.
- [AUTO-1593](https://jira.assessment.pearson.com/browse/AUTO-1593): System Validation: Correct issue that could cause OE items to receive a score that exceeded the maximum item score.

##### Version 1.6.54
- [AUTO-1584](https://jira.assessment.pearson.com/browse/AUTO-1584): System Validation: Correct issue that could cause students from an unselected session to be associated to a selected scenario. 
- [AUTO-1585](https://jira.assessment.pearson.com/browse/AUTO-1585): System Validation: Correct issue that would prevent users from selecting more than 1 scenario for battery tests.

##### Version 1.6.53
- [AUTO-1581](https://jira.assessment.pearson.com/browse/AUTO-1581): System Validation: Correct issue that caused the add scenario functionality to fail for battery test. 

##### Version 1.6.52
- [AUTO-1572](https://jira.assessment.pearson.com/browse/AUTO-1572): System Validation: Add Test Code to the "DIV Job Forms" report. 
- [AUTO-1573](https://jira.assessment.pearson.com/browse/AUTO-1573): System Validation: Correct issue that caused the application to attempt to create an ePen scenario for units that do not contain OE items in certain cases. 
- [AUTO-1574](https://jira.assessment.pearson.com/browse/AUTO-1574): System Validation: Correct issue that prevented the student count from showing on the scenario selection page during DIV job setup. 

##### Version 1.6.51
- [AUTO-1473](https://jira.assessment.pearson.com/browse/AUTO-1473): System Validation: Add multi-select filter to the Status column on the divJob and studentScenario pages.
- [AUTO-1533](https://jira.assessment.pearson.com/browse/AUTO-1533): System Validation: Limit check of reserved students to only the students that were reserved in the last week.
- [AUTO-1543](https://jira.assessment.pearson.com/browse/AUTO-1543): QTI Validation: Add regular expression to correctly identify non-attempted responses for tei-pci items for which the non-attempted response is of the format "ans_-1".
- [AUTO-1568](https://jira.assessment.pearson.com/browse/AUTO-1568): System Validation: Correct issue that could occur on page 3/4 of DIV job setup when either multiple testmaps or testmaps with a publish format other than "E" were found.  
- [AUTO-1569](https://jira.assessment.pearson.com/browse/AUTO-1569): System Validation: Correct issue that prevented test case coverage details from being displayed on page 3/4 of DIV job setup.


##### Version 1.6.50
- [AUTO-272](https://jira.assessment.pearson.com/browse/AUTO-272): System Validation: Improve scenario selection workflow during DIV job setup.
- [AUTO-1481](https://jira.assessment.pearson.com/browse/AUTO-1481): System Validation: Persist TestMap, TestDef and ItemResponsePool in Quality Monitoring Database.
- [AUTO-1494](https://jira.assessment.pearson.com/browse/AUTO-1494): System Validation: Change behavior of Min to Max scenario for battery tests so score points increment in succession by unit instead of concurrently.

##### Version 1.6.49
- [AUTO-1527](https://jira.assessment.pearson.com/browse/AUTO-1527): QTI Validation - QTI-response-generator: Add default answer for tei-ee-el.
- [AUTO-1528](https://jira.assessment.pearson.com/browse/AUTO-1528): System Validation: Add option to view 500, 1000 and 2500 items (test maps) per page.
- [AUTO-1531](https://jira.assessment.pearson.com/browse/AUTO-1531): System Validation: Correct issue that could cause the same ePEN scenario to be added multiple times for the same battery test, causing duplication on page 4 of DIV job setup.
- [AUTO-1532](https://jira.assessment.pearson.com/browse/AUTO-1532): System Validation: Correct issue where the EPEN2_AUTOMATION state would occasionally pass when the value of inputData.data returned by ePEN2 Bulk Scorer was an empty array (i.e. no scoring occurred), but the success value returned by Bulk Scorer was "yes".
- [AUTO-1535](https://jira.assessment.pearson.com/browse/AUTO-1535): QTI Validation - QTI-response-generator:  fix for float values not wrapped in StringToNumber causing exceptions.
- [AUTO-1539](https://jira.assessment.pearson.com/browse/AUTO-1539): System Validation: Correct issue where the message "Unable to retrieve form information" could be displayed during DIV job setup for scopeTreePaths that contain 4 levels.
- QTI-response-generator: Fix for 518054_01_SP/351/TN8
- System Validation: Update message displayed when resuming DIV job(s) which contain one or more completed student scenarios.

##### Version 1.6.48
- [AUTO-1530](https://jira.assessment.pearson.com/browse/AUTO-1530): System Validation: Correct issue that could cause the EPEN2_AUTOMATION state to continuously fail for resumed DIV jobs that had previously failed in this state.

##### Version 1.6.47
- [AUTO-1498](https://jira.assessment.pearson.com/browse/AUTO-1498): QTI Validation: Change interactionType to "customInteraction" when the subType starts with "tei-ee" (equation editor) to match the interactionType in the testDef.
- [AUTO-1524](https://jira.assessment.pearson.com/browse/AUTO-1524): System Validation: Correct issue where spoiled items were included in the ePEN scenario.

##### Version 1.6.46
- [AUTO-1523](https://jira.assessment.pearson.com/browse/AUTO-1523): System Validation: Correct issue in getBatteryStudentSessionsSql which allowed for duplicate records to be returned during DIV job setup.

##### Version 1.6.45
- [AUTO-1508](https://jira.assessment.pearson.com/browse/AUTO-1508): System Validation: Correct issue where the EPEN2_AUTOMATION state could pass when no response data was found in ePEN2 and no scoring was performed. 
- [AUTO-1519](https://jira.assessment.pearson.com/browse/AUTO-1519): System Validation: Correct issue where an unexpected error could be returned if some units within a battery test did not contain ePEN scorable items when an ePEN scenario was selected.

##### Version 1.6.44
- [AUTO-1515](https://jira.assessment.pearson.com/browse/AUTO-1515): System Validation: Correct the issue where STATS function was not returning weighted scores for RAW_SCORE and Group level score.
- [AUTO-1518](https://jira.assessment.pearson.com/browse/AUTO-1518): System Validation: Add support for non-scorable items (direction and welcome items) which do not contain the type attribute in the testmap.

##### Version 1.6.43
- [AUTO-1514](https://jira.assessment.pearson.com/browse/AUTO-1514): System Validation: Correct issue where the application failed to read the TestNav Version.

##### Version 1.6.42
- [AUTO-1507](https://jira.assessment.pearson.com/browse/AUTO-1507): System Validation: Correct issue where form spiralling within the same session was not working properly.
- [AUTO-1512](https://jira.assessment.pearson.com/browse/AUTO-1512): QTI Validation: Correct issue where composite items with textEntryInteraction parts failed to score item response.

##### Version 1.6.41
- [AUTO-1490](https://jira.assessment.pearson.com/browse/AUTO-1490): System Validation: Add functionality to view forms used in each DIV job.
- [AUTO-1504](https://jira.assessment.pearson.com/browse/AUTO-1504): System Validation: Correct issue that caused field test units to be included for orgs for which the core.org_part_test.allow_field_test value was 0 or null.
- Adjust breadcrumbs and group buttons in the application.

##### Version 1.6.40
- [AUTO-1498](https://jira.assessment.pearson.com/browse/AUTO-1498): QTI Validation: Correct issue where the full sub-type for tei-ee items (e.g. tei-ee-el, tei-ee-hs, etc.)  was not present in the item response pool.
- Update response generator library to version 0.0.39

##### Version 1.6.39
- [AUTO-1488](https://jira.assessment.pearson.com/browse/AUTO-1488): QTI Validation: Correct issue where the extendedTextInteraction.tei-rte type was not properly interpreted when downloading responses.
- [AUTO-1489](https://jira.assessment.pearson.com/browse/AUTO-1489): QTI Validation: Correct issue where the choiceInteraction.cloze type was not properly interpreted when downloading responses.
- [AUTO-1492](https://jira.assessment.pearson.com/browse/AUTO-1492): QTI Validation: Correct issue where the orderInteraction.tei-order type was not properly interpreted when downloading responses.
- [AUTO-1495](https://jira.assessment.pearson.com/browse/AUTO-1495): QTI Validation: Increase test case creation timeout from 15 to 45 seconds to address an issue where response gen may take more than 15 seconds to generate response data.
- [AUTO-1499](https://jira.assessment.pearson.com/browse/AUTO-1499): System Validation: DIV Job Setup: Correct issue that could cause the application to hang during DIV Job Setup when attempting to get the SOAP body from the testmap XML.

##### Version 1.6.38
- [AUTO-1497](https://jira.assessment.pearson.com/browse/AUTO-1497): System Validation: Div Job Setup: Correct issue which caused scopeTreePaths to not be present when an additional scope level was present (e.g. /va/sol/2017-2018/nwsumm18).

##### Version 1.6.37
- [AUTO-1484](https://jira.assessment.pearson.com/browse/AUTO-1484): QTI Validation: Test case coverage: Add validation to assure simulation responses are not empty (teiPciResponsesAreNotEmpty) as this could cause problems in IRIS.
- [AUTO-1486](https://jira.assessment.pearson.com/browse/AUTO-1486): QTI Validation: Test case coverage: Correct issue that caused test case coverage to not properly flag non-attempted tei-pci responses as non-attempted.
- [AUTO-1491](https://jira.assessment.pearson.com/browse/AUTO-1491): System Validation: Div Job Setup: Correct unexpected exception when creating a battery DIV job where units after unit 1 do not contain OE items.

##### Version 1.6.36
- [AUTO-1487](https://jira.assessment.pearson.com/browse/AUTO-1487): System Validation: Div Job Setup: Correct issue which caused the incorrect test/form code to be assigned if multiple test codes were selected during standard DIV job setup.

##### Version 1.6.35
- [AUTO-1135](https://jira.assessment.pearson.com/browse/AUTO-1135): System Validation: Add ability to run STATS for only OE Operational, OE Field Test or both.
- [AUTO-1476](https://jira.assessment.pearson.com/browse/AUTO-1476): System Validation: Correct issue that caused high CPU usage for certain jobs that have a path ending with the EPEN2_AUTOMATION state.
- [AUTO-1480](https://jira.assessment.pearson.com/browse/AUTO-1480): System Validation: Correct issue that could cause an older version of the TestMap to be used for some units in certain Battery Tests.

##### Version 1.6.34
- [AUTO-433](https://jira.assessment.pearson.com/browse/AUTO-433): System Validation: Add ability to re-run STATS for completed DIV jobs.
- [AUTO-1472](https://jira.assessment.pearson.com/browse/AUTO-1472): System Validation: Address an issue where a success result is incorrectly returned in the EPEN2_AUTOMATION state in certain situations.
- [AUTO-1477](https://jira.assessment.pearson.com/browse/AUTO-1477): System Validation: Environment Setup: Add validation of PANext FrontEnd URL.

##### Version 1.6.33
- [AUTO-1385](https://jira.assessment.pearson.com/browse/AUTO-1385): QTI Validation: Add ability to view item XML from the items page.
- [AUTO-1471](https://jira.assessment.pearson.com/browse/AUTO-1471): System Validation: Div Job Setup: Correct issue where the available TestNav and ePEN scenarios were not retained when navigating back to the scenario selection page.
- [AUTO-1474](https://jira.assessment.pearson.com/browse/AUTO-1474): System Validation: Correct a performance issue that could occur when the number of concurrent student scenarios across DIV jobs exceeded 300.

##### Version 1.6.32
- [AUTO-1462](https://jira.assessment.pearson.com/browse/AUTO-1462): System Validation: Div Job Setup: Correct an issue where missing item test case coverage could display an incorrect error message.
- [AUTO-1466](https://jira.assessment.pearson.com/browse/AUTO-1466): System Validation: STATS: Correct potential rounding issues in calculated item and group level scores when score weights are not 1.
- [AUTO-1468](https://jira.assessment.pearson.com/browse/AUTO-1468): System Validation: EPEN\_AUTOMATION: Evaluate the primary backend path to identify scoreEligible OE items and consider only these items for ePEN2 scoring.

##### Version 1.6.31
- [AUTO-1459](https://jira.assessment.pearson.com/browse/AUTO-1459): System Validation: Div Job Setup: fix for "Request entity too large" errors that could occur when navigating to the last page of DIV job setup. 
- [AUTO-1464](https://jira.assessment.pearson.com/browse/AUTO-1464): System Validation: Div Job Setup: Correct issue that prevented epen scoring scenarios from getting created for some items.

##### Version 1.6.30.1
- [AUTO-1463](https://jira.assessment.pearson.com/browse/AUTO-1463): System Validation: Wrap studentScenarioService call in JPAApi.withTransaction to address regression issue while executing battery DIV jobs.

##### Version 1.6.30
- [AUTO-1386](https://jira.assessment.pearson.com/browse/AUTO-1386): QTI Validation: Add ability to view/filter items by form.
- [AUTO-1402](https://jira.assessment.pearson.com/browse/AUTO-1402): QTI Validation: Add ability to deploy response generator separately from the Quality Monitoring application.

##### Version 1.6.29
- [AUTO-1447](https://jira.assessment.pearson.com/browse/AUTO-1447): System Validation: DIV Job Setup: display a better error message when PAN FE DB Connection is invalid.
- [AUTO-1449](https://jira.assessment.pearson.com/browse/AUTO-1449): System Validation: Reduce JPA transaction times where possible and add logic to handle the SQLTransientConnectionExceptions.
- [AUTO-1457](https://jira.assessment.pearson.com/browse/AUTO-1457): System Validation: DIV Job Setup: Correct cosmetic issue where the checked state of the "Disable ePEN2 scenarios" checkboxes was not maintained.
- [AUTO-1461](https://jira.assessment.pearson.com/browse/AUTO-1461): Update Play Framework from version 2.5.4 to version 2.5.18.
- Remove code related to deprecated response gen web service. 

##### Version 1.6.27
- [AUTO-1455](https://jira.assessment.pearson.com/browse/AUTO-1455): System Validation: DIV job setup: remove test case coverage validation for survey items.

##### Version 1.6.26
- [AUTO-1436](https://jira.assessment.pearson.com/browse/AUTO-1436): QTI Validation: Add basic support for TEI-PCI in response generator.
- [AUTO-1437](https://jira.assessment.pearson.com/browse/AUTO-1437): System Validation: Correct an issue in ePEN bulk scorer that caused the EPEN2\_AUTOMATION state to pass when no responses were actually scored in ePEN.
- [AUTO-1448](https://jira.assessment.pearson.com/browse/AUTO-1448): QTI Validation: Correct issue where non-attempted responses for CUSTOMINTERACTION.TEI-EE items were flagged as attempted.
- [AUTO-1453](https://jira.assessment.pearson.com/browse/AUTO-1453): System Validation: Correct possible NullpointerExceptions in STATS and BATTERY STATS when no assessment record was found in DataWarehouse.
- System Validation: Correct issue that caused the response data for certain non-OE item types to incorrectly get updated.

##### Version 1.6.25
- [AUTO-1392](https://jira.assessment.pearson.com/browse/AUTO-1392): QTI Validation: Correct an issue that caused a timeout when attempting to download a large amount of item identifiers.
- [AUTO-1429](https://jira.assessment.pearson.com/browse/AUTO-1429): QTI Validation: Correct an issue that prevented function graph items from getting scored successfully.
- [AUTO-1438](https://jira.assessment.pearson.com/browse/AUTO-1438): System Validation: Environment Setup - Database Connection Detail: add field validations to host field.
- [AUTO-1439](https://jira.assessment.pearson.com/browse/AUTO-1439): System Validation: Environment Setup - Database Connection Detail: display a warning message when updating an existing database connection.
- [AUTO-1444](https://jira.assessment.pearson.com/browse/AUTO-1444): System Validation: Correct an issue that allowed for more concurrent student scenarios to execute than configured.
- [AUTO-1445](https://jira.assessment.pearson.com/browse/AUTO-1445): System Validation: Correct an issue in the View Stats Failures functionality where in certain situations no results would be displayed.
- [AUTO-1450](https://jira.assessment.pearson.com/browse/AUTO-1450): System Validation: Correct an issue that could cause an unexpected exception in STATS if the status for an item was OperationalNotReportable in the testmap.
- [AUTO-1451](https://jira.assessment.pearson.com/browse/AUTO-1451): System Validation: Environment Setup - Database Connection Detail: add additional field validations to host and name fields.
- QTI Validation - Test case creation: Fix for extended text interaction items.

##### Version 1.6.24
- [AUTO-1428](https://jira.assessment.pearson.com/browse/AUTO-1428): System Validation: Correct an issue where invalid response data was submitted for drawing interaction items.
- [AUTO-1443](https://jira.assessment.pearson.com/browse/AUTO-1443): System Validation: Fix for MySQLIntegrityConstraintViolationExceptions and AskTimeoutExceptions that could occur when the application incorrectly attempts to use the same run\_number for a given job\_id.

##### Version 1.6.23
- [AUTO-1355](https://jira.assessment.pearson.com/browse/AUTO-1355): System Validation: Resolve issue where students scenarios in "In Progress" state may not resume when resuming a DIV job.
- [AUTO-1435](https://jira.assessment.pearson.com/browse/AUTO-1435): QTI Validation: Put inline choice interaction responses in jsonarrays & fix for GapMatchInteraction with no mapping.
- System Validation: Force use of https for TestNav application URL during TN8\_API\_DATA\_LOADER execution.
- System Validation: Resolve LazyInitializationException errors during DIV job execution. 

##### Version 1.6.22
- Roll back a few recent performance optimizations due to inconsistent behavior during DIV job execution.

##### Version 1.6.21
- [AUTO-1430](https://jira.assessment.pearson.com/browse/AUTO-1430): System Validation: Correct issue that prevented certain DIV path states to complete.
- System Validation: Update response generator to use a longer, non-silent audio sample for tei-audiocapture items.

##### Version 1.6.20
- [AUTO-1399](https://jira.assessment.pearson.com/browse/AUTO-1399): QTI Validation: Correct issue where the downloaded response file designates only incorrect test cases as attempted for certain items.
- [AUTO-1404](https://jira.assessment.pearson.com/browse/AUTO-1404): System Validation: Update error message received when Back End database connection details are incorrect.
- [AUTO-1418](https://jira.assessment.pearson.com/browse/AUTO-1418): System Validation: Correct issue where failed battery student scenarios beyond the configured maxConcurrentStudents would not be re-executed.
- [AUTO-1427](https://jira.assessment.pearson.com/browse/AUTO-1427): System Validation: Correct issue which caused exceptions in EPEN2\_AUTOMATION when scoring guide traits did not contain condition codes of score type "Blank".
- QTI Validation: Add Schoolnet Previewer.
- System Validation: Reduce number of concurrent DIV jobs from 15 to 10, reduce number of concurrent student scenarios from 50 to 40 for performance reasons.

##### Version 1.6.19
- [AUTO-1380](https://jira.assessment.pearson.com/browse/AUTO-1380): System Validation: Correct issue where updated DB connections would not take effect until after application restart.
- [AUTO-1403](https://jira.assessment.pearson.com/browse/AUTO-1403): System Validation: Correct issue where in certain conditions incorrect counts would be displayed in summary emails.
- [AUTO-1410](https://jira.assessment.pearson.com/browse/AUTO-1410): System Validation: Enhance STATS execution performance by caching Testmap data.

##### Version 1.6.18
- [AUTO-1409](https://jira.assessment.pearson.com/browse/AUTO-1409): System Validation: Correct issue where the password and port values of an existing database connection could not be updated.
- [AUTO-1411](https://jira.assessment.pearson.com/browse/AUTO-1411): System Validation: Correct issue where students that were not assigned all of the correct forms in a battery were considered for a battery DIV job.

##### Version 1.6.17
- [AUTO-1414](https://jira.assessment.pearson.com/browse/AUTO-1414): QTI Validation: Correct issue where correct test cases were not being included for items using an external match\_correct responseProcessing template.
- [AUTO-1416](https://jira.assessment.pearson.com/browse/AUTO-1416): QTI Validation: Correct issue where invalid incorrect test cases were generated for tei-complexhotspot items exceeding the maxChoices attribute.

##### Version 1.6.16
- [AUTO-1412](https://jira.assessment.pearson.com/browse/AUTO-1412): QTI Validation: Correct issue where only 1 incorrect response was generated for gap match items w/minAssoc attribute.
- [AUTO-1413](https://jira.assessment.pearson.com/browse/AUTO-1413): QTI Validation: Correct issue where the QTI scoring api could return an incorrect score for certain item responses.

##### Version 1.6.15
- [AUTO-918](https://jira.assessment.pearson.com/browse/AUTO-918): System Validation: Address potential performance issues during STATS execution.
- [AUTO-1406](https://jira.assessment.pearson.com/browse/AUTO-1406): QTI Validation/System Validation: Correct issue in test case coverage validation when score system was missing from item XML.
- [AUTO-1407](https://jira.assessment.pearson.com/browse/AUTO-1407): QTI Validation - test case creation: Add support for min/max association in graphic/gap match interactions.

##### Version 1.6.14
- [AUTO-1377](https://jira.assessment.pearson.com/browse/AUTO-1377): QTI Validation/System Validation: Add support for tei-audiocapture items. 
- [AUTO-1401](https://jira.assessment.pearson.com/browse/AUTO-1401): QTI Validation - test case creation: create test cases with as many unique non-zero scores as possible.
- QTI Validation - test case creation: Addressed occasional deadlock issues during response generation.
- QTI Validation - test case creation: ignore simulations.

##### Version 1.6.13
- [AUTO-1377](https://jira.assessment.pearson.com/browse/AUTO-1377): QTI Validation: Add support for test case creation for teiaudiocapture items.
- QTI Validation: Addressed issue where non-attempted test case data was no longer being generated.

##### Version 1.6.12
- [AUTO-1363](https://jira.assessment.pearson.com/browse/AUTO-1363): System Validation: Updated Add/Edit Environment page to make selection of DB connections clearer and avoid duplicates.
- [AUTO-1379](https://jira.assessment.pearson.com/browse/AUTO-1379): System Validation: Improved error message when TestNav is down during DIV Job setup.
- [AUTO-1382](https://jira.assessment.pearson.com/browse/AUTO-1382): System Validation: Addressed issue which caused no response data from being saved in TestNav 8.10 and greater.
- [AUTO-1384](https://jira.assessment.pearson.com/browse/AUTO-1384): QTI Validation: Terminate item test case creation if this exceeds 15 seconds for an item. This change also addresses potential out of memory situations caused by items with a very large set of possible responses.
- [AUTO-1387](https://jira.assessment.pearson.com/browse/AUTO-1387): QTI Validation: Updated logic to continue to create test cases when an item causes errors.

##### Version 1.6.10
- [AUTO-1367](https://jira.assessment.pearson.com/browse/AUTO-1367): System Validation: Attempt to address/improve inconsistent failures in the EPEN2\_AUTOMATION state.
- [AUTO-1373](https://jira.assessment.pearson.com/browse/AUTO-1373): QTI Validation - test case creation: fix for items not in respProcessing.
- [AUTO-1374](https://jira.assessment.pearson.com/browse/AUTO-1374): QTI Validation - test case creation: fix issue with textentryinteraction.
- [AUTO-1375](https://jira.assessment.pearson.com/browse/AUTO-1375): QTI Validation - test case creation: added support for tei-complex-slider response generation.
- [AUTO-1376](https://jira.assessment.pearson.com/browse/AUTO-1376): System Validation: corrected issue which caused "Unable to retrieve form information" errors during DIV job setup when an additional scopeTreePath level was present.
- [AUTO-1378](https://jira.assessment.pearson.com/browse/AUTO-1378): System Validation: corrected issue which caused "Failed to find project ID" errors in IRIS\_PROCESSING\_VALIDATOR.

##### Version 1.6.9

- [AUTO-1368](https://jira.assessment.pearson.com/browse/AUTO-1368): TN8\_API\_DATA\_LOADER: Correct sporadic failures in export API validation.
- [AUTO-1369](https://jira.assessment.pearson.com/browse/AUTO-1369): Correct issue preventing the score API from scoring responses when P\_XSD\_VALIDATION\_ERROR IssueTypes are present.
- [AUTO-1370](https://jira.assessment.pearson.com/browse/AUTO-1370): DIV Job Setup: Fix loading animation on test session selection table.
- [AUTO-1371](https://jira.assessment.pearson.com/browse/AUTO-1371): Address OOM errors when creating test cases for certain items.

##### Version 1.6.8

- [AUTO-1347](https://jira.assessment.pearson.com/browse/AUTO-1347): Corrected exceptions that occurred in STATS for battery DIV jobs when a session was not started.
- [AUTO-1351](https://jira.assessment.pearson.com/browse/AUTO-1351): Use provided ePEN2 Mongo credentials, if available.
- [AUTO-1360](https://jira.assessment.pearson.com/browse/AUTO-1360): Fixed issue in test case creation for some items.
- [AUTO-1364](https://jira.assessment.pearson.com/browse/AUTO-1364): Updated DEV and ABBI Previewer URLs.
- [AUTO-1365](https://jira.assessment.pearson.com/browse/AUTO-1365): Corrected issues with DIV job- and item test case creation.

##### Version 1.6.7
- [AUTO-1339](https://jira.assessment.pearson.com/browse/AUTO-1339): Corrected potential duplication of Unit Test Codes during Battery DIV job Setup.

##### Version 1.6.5

- [AUTO-1267](https://jira.assessment.pearson.com/browse/AUTO-1267): Added web service to get the status of a DIV job.
- [AUTO-1303](https://jira.assessment.pearson.com/browse/AUTO-1303): Corrected issue that caused problems with pagination on the battery DIV jobs page.
- [AUTO-1310](https://jira.assessment.pearson.com/browse/AUTO-1310): Added "Battery ID" column to the "Export Failed Scenarios" csv file.
- [AUTO-1311](https://jira.assessment.pearson.com/browse/AUTO-1311): Corrected issue that caused the "View Stats Failures" summary report to return no results.
- [AUTO-1318](https://jira.assessment.pearson.com/browse/AUTO-1318): Updated STATS to exclude units with do\_not\_report flag set to 1 from the expected results calculation.
- [AUTO-1343](https://jira.assessment.pearson.com/browse/AUTO-1343): Corrected issue in TN8\_API\_DATA\_LOADER when multiple forms in a battery have the same formNumber.

##### Version 1.6.4

- [AUTO-1308](https://jira.assessment.pearson.com/browse/AUTO-1308): Corrected issue when generating test cases for TeiComplexHotspotInteraction items.

##### Version 1.6.3

- [AUTO-993](https://jira.assessment.pearson.com/browse/AUTO-993): Added ability to export failed student scenarios to a CSV file.
- [AUTO-1066](https://jira.assessment.pearson.com/browse/AUTO-1066): Added summary report for STATS failures.
- [AUTO-1288](https://jira.assessment.pearson.com/browse/AUTO-1288): Corrected issue when resuming Battery DIV jobs in certain fail states.
- [AUTO-1299](https://jira.assessment.pearson.com/browse/AUTO-1299): Added missing loading animation on the Batteries page.
- [AUTO-1304](https://jira.assessment.pearson.com/browse/AUTO-1304): Corrected testdef decryption issue in TN8\_API\_DATA\_LOADER when using TestNav 8.9.x.

##### Version 1.6.2

- [AUTO-261](https://jira.assessment.pearson.com/browse/AUTO-261): Added Mass Test Case Deletion functionality to the QTI Validation module.
- [AUTO-448](https://jira.assessment.pearson.com/browse/AUTO-448): Added ability to resume the current DIV job From the Student Scenarios and States pages.
- [AUTO-458](https://jira.assessment.pearson.com/browse/AUTO-458): Added ability to archive environments to the System Validation module.
- [AUTO-1075](https://jira.assessment.pearson.com/browse/AUTO-1075): Refactor code to reuse ePEN user IDs.
- [AUTO-1243](https://jira.assessment.pearson.com/browse/AUTO-1243): Corrected query used to remove entries from student\_reservation table.
- [AUTO-1247](https://jira.assessment.pearson.com/browse/AUTO-1247): Corrected hyperlink for battery DIV jobs in completion email.
- [AUTO-1248](https://jira.assessment.pearson.com/browse/AUTO-1248): Corrected counts displayed in the email sent upon battery DIV job completion.
- [AUTO-1249](https://jira.assessment.pearson.com/browse/AUTO-1249): Added permission to hide the "Subscribed" column for users that do not have access to this functionality. 
- [AUTO-1282](https://jira.assessment.pearson.com/browse/AUTO-1282): Corrected issue where sometimes a student reservation record was deleted prematurely.
- [AUTO-1295](https://jira.assessment.pearson.com/browse/AUTO-1295): Corrected TN8\_API\_DATA\_LOADER authentication failures when using TestNav 8.9 and higher.

##### Version 1.6.1

- [AUTO-1226](https://jira.assessment.pearson.com/browse/AUTO-1226): Fixed issues in the error message that appears when there are not enough students available for a Data Integrity Validation job.
- [AUTO-1227](https://jira.assessment.pearson.com/browse/AUTO-1227): Added a missing loading animation to the Usage Statistics page.
- [AUTO-1228](https://jira.assessment.pearson.com/browse/AUTO-1228): Fixed an issue with the Environment dropdown on the Usage Statistics page.
- [AUTO-1229](https://jira.assessment.pearson.com/browse/AUTO-1229): Fixed a sorting issue on the Usage Statistics page.
- [AUTO-1230](https://jira.assessment.pearson.com/browse/AUTO-1230): Fixed an issue in battery Data Integrity Validation jobs where a failure in TN8 API Data Loader would cause the job to error out.

##### Version 1.6.0

- [AUTO-14](https://jira.assessment.pearson.com/browse/AUTO-14): Added support for battery tests in Data Integrity Validation jobs.
- [AUTO-878](https://jira.assessment.pearson.com/browse/AUTO-878): Data Integrity Validation jobs will now add the following data to all human-scored responses (except for non-attempted responses): test code, form code, item UIN, and response identifier. This is to assist the ePEN team with their testing.
- [AUTO-1039](https://jira.assessment.pearson.com/browse/AUTO-1039): Added a Usage Statistics page to the System Validation module. This new page allows users to run reports on the number/details of test attempts run during specified time periods.
- [AUTO-1196](https://jira.assessment.pearson.com/browse/AUTO-1196): Applied a fix for an issue where the nightly QTI Score Consistency Validation job would sometimes not run for specific tenants.
- [AUTO-1205](https://jira.assessment.pearson.com/browse/AUTO-1205): Applied a fix for an issue where the nightly QTI Score Consistency Validation job could sometimes get stuck and not complete.
- [AUTO-1209](https://jira.assessment.pearson.com/browse/AUTO-1209): Fixed an issue in IRIS Processing Validator where it could not find the right IRIS project if there were two projects with the same admin but different accounts.
- [AUTO-1214](https://jira.assessment.pearson.com/browse/AUTO-1214): Fixed an issue where the status of a Data Integrity Validation job would not get updated upon completion if the email failed to send.

##### Version 1.5.0

- [AUTO-87](https://jira.assessment.pearson.com/browse/AUTO-87): Added the ability to unsubscribe from the QTI Score Consistency Validation nightly job.

##### Version 1.4.7

- [AUTO-783](https://jira.assessment.pearson.com/browse/AUTO-783): Improved storage efficiency of database table structure.

##### Version 1.4.6

- [AUTO-987](https://jira.assessment.pearson.com/browse/AUTO-987): Fixed issue with generating invalid responses for tei-graphing items.
- [AUTO-1092](https://jira.assessment.pearson.com/browse/AUTO-1092): Fixed issue with creating too many connections to the PANext MySQL DBs.
- [AUTO-1167](https://jira.assessment.pearson.com/browse/AUTO-1167): Added validation to prevent non-numeric values from being entered in the Port field during Environment setup.

##### Version 1.4.5

- [AUTO-1097](https://jira.assessment.pearson.com/browse/AUTO-1097): Fixed issue with creating too many connections to the IRIS DB during IRIS\_PROCESSING\_VALIDATOR.
- [AUTO-1107](https://jira.assessment.pearson.com/browse/AUTO-1107): Fixed issue with creating too many connections to the Data Warehouse Mongo DB during STATS.
- [AUTO-1146](https://jira.assessment.pearson.com/browse/AUTO-1146): Fixed handling of Spanish characters to address false mismatches in STATS.
- [AUTO-1158](https://jira.assessment.pearson.com/browse/AUTO-1158): Fixed issue that could cause an invalid failure in ePEN scenario creation during DIV job setup.

##### Version 1.4.4

- [AUTO-1061](https://jira.assessment.pearson.com/browse/AUTO-1061): Additional fix to prevent machine scored Field Test items from being excluded during the STATS comparison.
- [AUTO-1102](https://jira.assessment.pearson.com/browse/AUTO-1102): Fixed issue with creating too many connections to the ePEN Mongo DB during EPEN2\_AUTOMATION.
- [AUTO-1141](https://jira.assessment.pearson.com/browse/AUTO-1141): Display correct message when authenticateTest API call fails in TN8\_API\_DATA\_LOADER.

##### Version 1.4.3

- Re-enabled the nightly QTI Scoring Validation job.

##### Version 1.4.2

- [AUTO-1060](https://jira.assessment.pearson.com/browse/AUTO-1060): Fixed issue with creating too many connections to the ePEN MySQL DB during EPEN2\_AUTOMATION, which caused DIV job failures.
- [AUTO-1061](https://jira.assessment.pearson.com/browse/AUTO-1061): Only include the eligible item statuses defined in the Backend path when performing item level comparison in STATS.

##### Version 1.4.1

- Temporarily disabled the nightly QTI Scoring Validation job.

##### Version 1.4.0

- [AUTO-897](https://jira.assessment.pearson.com/browse/AUTO-897): Fixed issue with calling TestNav API when the TestNav configuration is overridden for a given test administration in PANext Frontend.
- [AUTO-925](https://jira.assessment.pearson.com/browse/AUTO-925): Display the Student Code in the DIV job results.
- [AUTO-930](https://jira.assessment.pearson.com/browse/AUTO-930): Added an "Other" option for the defect repository when creating comments for DIV jobs.
- [AUTO-981](https://jira.assessment.pearson.com/browse/AUTO-981): Improved performance of queries used when creating ePEN scenarios.
- [AUTO-985](https://jira.assessment.pearson.com/browse/AUTO-985): Improved performance of query used to retrieve scope tree paths.
- [AUTO-986](https://jira.assessment.pearson.com/browse/AUTO-986): Fixed issue with DIV job comment creation.
- [AUTO-1047](https://jira.assessment.pearson.com/browse/AUTO-1047): Added support for generating responses for tei-pointgraph-scatter interactions.
- [AUTO-1056](https://jira.assessment.pearson.com/browse/AUTO-1056): Show a more specific error message in STATS if the environment is configured to use the wrong Data Warehouse instance.
- [AUTO-1074](https://jira.assessment.pearson.com/browse/AUTO-1074): Added support for generating responses for tei-complexslider interactions.

##### Version 1.3.2

- [AUTO-891](https://jira.assessment.pearson.com/browse/AUTO-891): Fixed responses being generated in an incorrect format for hot text interactions.
- [AUTO-898](https://jira.assessment.pearson.com/browse/AUTO-898): In some cases, DIV jobs were getting stuck in STATS when the application couldn't connect to T3. It will now time out if it takes longer than a minute.
- [AUTO-988](https://jira.assessment.pearson.com/browse/AUTO-988): When determining item test case coverage, take the response identifier score system into account when determining if an item requires a correct response.

##### Version 1.3.1

- [AUTO-887](https://jira.assessment.pearson.com/browse/AUTO-887): When calculating expected total points in step 4 of DIV Job Setup, don't consider non-reportable items. Also, when creating allCorrect and allIncorrect scenarios, don't consider Sample or NA items when calculating the maximum number of correct/incorrect responses (this was causing us to generate more scenarios than necessary in some cases).
- [AUTO-939](https://jira.assessment.pearson.com/browse/AUTO-939): When generating responses, always include a non-attempted response.
- [AUTO-958](https://jira.assessment.pearson.com/browse/AUTO-958): Improved error handling/reporting in TestNav 8 API Data Loader when an item UIN in the scenario is not found in the testDef.
- [AUTO-989](https://jira.assessment.pearson.com/browse/AUTO-989): Improved error handling/reporting for database connection issues in Backend Path Validator.

##### Version 1.3.0

- [AUTO-819](https://jira.assessment.pearson.com/browse/AUTO-819): Added Quality Monitoring version and release notes to the application.
- [AUTO-940](https://jira.assessment.pearson.com/browse/AUTO-940): Fixed issue detecting non-attempted responses for equation editor items during test case coverage validation.
- [AUTO-942](https://jira.assessment.pearson.com/browse/AUTO-942): Fixed issue in STATS that affected retrieval and calculation of expected scores for OE items.
